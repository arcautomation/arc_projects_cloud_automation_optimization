package com.arc.projects.testscripts;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectAndFolder_Level_Search;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ViewerScreenPage;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class Viewer_PageTestCases {
	 

		public static WebDriver driver;
	    
		ProjectsLoginPage projectsLoginPage;
	    ProjectDashboardPage projectDashboardPage;   
	    ViewerScreenPage viewerScreenPage; 
	    FolderPage folderPage;
	   ProjectAndFolder_Level_Search projectAndFolder_Level_Search;
	    
	    @Parameters("browser")
	    @BeforeMethod
        public WebDriver beforeTest(String browser) {
	        
	    	if(browser.equalsIgnoreCase("firefox")) 
	        {
	               File dest = new File("./drivers/win/geckodriver.exe");
	               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
	               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
	               driver = new FirefoxDriver();
	               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	        }
	        
	        else if (browser.equalsIgnoreCase("chrome")) 
	        { 
	        File dest = new File("./drivers/win/chromedriver.exe");
	        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
	        Map<String, Object> prefs = new HashMap<String, Object>();
	        prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
	        ChromeOptions options = new ChromeOptions();
	        options.addArguments("--start-maximized");
	        options.setExperimentalOption("prefs", prefs);
	        driver = new ChromeDriver( options );
	        driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
	        } 
	        
	        else if (browser.equalsIgnoreCase("safari"))
	        {
	               System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
	               driver = new SafariDriver();
	               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	        }
	   return driver;
	 }
	    
	    
	    /** TC_023,TC_024 (Viewer Page):Verification of  Multiple Tools Object . Scripted By Ranjan P
	     * @throws Throwable 
	     */
	             
	              
	    @Test(priority = 0, enabled = true , description = "Verification and Validation of  MarkUp Saving All Validation- Single File ")
	    public void Verify_MarkUPSavingAllValidation() throws Throwable
	         {         	 
	         try
	         {
	             Log.testCaseInfo("TC_023,TC_024 (Multiple ToolsBaar validation):Verification and Validation of  MarkUp Saving All Validation- Single File ");
	             String uName = PropertyReader.getProperty("Username1");
	             String pWord = PropertyReader.getProperty("Password1");
	             //==============Login with valid UserID/PassWord===========================
	             projectsLoginPage = new ProjectsLoginPage(driver).get();  
	             projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
	             //Create project 
	             String Project_Name = "Specific_Testdata_Static";
	 			 folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
	 			 //select folder			
	 			 String Foldername = "RFI_SinglePage";
	 			 folderPage.Select_Folder(Foldername);	
	 			 viewerScreenPage = new ViewerScreenPage(driver).get();
	             //=======MarkUP  Validation========================  			         		
	             viewerScreenPage = new ViewerScreenPage(driver).get();
	          
	             viewerScreenPage.ToolBarSelection_And_Validation();
	              	       			
	              		
	         } catch (Exception e) {
	 			e.getCause();
	 			Log.exception(e, driver);
	 			CommonMethod.analyzeLog(driver);

	 		} finally {
	 			Log.endTestCase();
	 			driver.quit();
	 		}
	           	
	           }  
	          	     
	    
	    
	    /*Test case : TC_064.Navigate from RFI listings and try to create Photo annotation.
	     * Create one in document level in online mode with To, Cc, Subject, disciple, sheet number, custom attributes, specification, potential cost impact(checked), potential schedule impact(checked), Question and attachment(external and internal)
	       1) Navigate to the document and validate the document.
	       2) Validate the postion and content of RFI.
	       3) Close the pop over and tap on view all and validate the position of mark ups.
	       4) Create RFI, Punch and photo annotation on the document an validate those in web.
	     */

	    @Test(priority = 1 , enabled = true , description = "Navigate from RFI listings and try to create Photo Annotation")
	    public void NavigatefromRFIListingtocreatePhotoAnnotation() throws Throwable
	         {         	 
	         try
	         {
	             Log.testCaseInfo("TC_064 (RFI Related Testcases):Navigate from RFI listings and try to create  Photo Annotation");
	             String uName = PropertyReader.getProperty("Username1");
	             String pWord = PropertyReader.getProperty("Password1");
	             //==============Login with valid UserID/PassWord===========================
	             projectsLoginPage = new ProjectsLoginPage(driver).get();  
	             projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
	             
	             String PhotoPath = PropertyReader.getProperty("Upload_PhotoFile");
	             String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
	             int FileCount = Integer.parseInt(CountOfFilesInFolder);
	             String Project_Name = "Specific_Testdata_Static";
	 			 folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
	 			 //select folder			
	 			 String Foldername = "RFI_SinglePage";
	 			 folderPage.Select_Folder(Foldername);	
	 			 viewerScreenPage = new ViewerScreenPage(driver).get();
	             //============RFI Creation ON first File===========================
	             String parentHandle = driver.getWindowHandle();
	    		 viewerScreenPage.Image1();
	    		 viewerScreenPage.PhotoAnnotation(parentHandle);
	    		 viewerScreenPage.UploadFiles_Photo();		 
	    		 viewerScreenPage.untitledphotovalidation();
	    		 driver.close();
	    		 driver.switchTo().window(parentHandle);
	    		 viewerScreenPage.Logout();	 
	    		 
	    		 // Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);

	              	       			
	              		
	         } catch (Exception e) {
	 			e.getCause();
	 			Log.exception(e, driver);
	 			CommonMethod.analyzeLog(driver);

	 		} finally {
	 			Log.endTestCase();
	 			driver.quit();
	 		}
	           	
	           }  
	          	    
	    
	    
	    /** TC_025 (ViewerPage):Verification of  Multi-page navigation along with Mark-up loading. Scripted By Ranjan P
         * @throws Throwable 
         */
         @Test(priority = 2, enabled = true, description = "Verification and Validation of  Multipage navigation along with Markup loading.")
         public void Verify_MarkUP_MultipleNavigationAlongWithMarkUpLoading() throws Throwable
         {
        	 
         	try
         	{
         		Log.testCaseInfo("TC_025 (Multiple ToolsBaar validation): Multiple Navigation Along with markup Loading- Multiple File");
         		String uName = PropertyReader.getProperty("Username1");
        		String pWord = PropertyReader.getProperty("Password1");
        		//=====Login with valid UserID/PassWord
          		projectsLoginPage = new ProjectsLoginPage(driver).get();  
          		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);          		
          
          		
          		
          		 String Project_Name = "Specific_Testdata_Static";
	 			 folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
	 			 //select folder			
	 			  String Foldername = "RFI_MultiplePage";
	 			 folderPage.Select_Folder(Foldername);	
	 			 viewerScreenPage = new ViewerScreenPage(driver).get();
         		
         		//=======MarkUP Validation========================  			         		
         		viewerScreenPage = new ViewerScreenPage(driver).get(); 
         		//viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
          		viewerScreenPage.MultipleMarkUpSheetValidation();
         		
         		 			
         		
         	} catch (Exception e) {
    			e.getCause();
    			Log.exception(e, driver);
    			CommonMethod.analyzeLog(driver);

    		} finally {
    			Log.endTestCase();
    			driver.quit();
    		}
      	
      }
         

	    
	    
	    
  
	        
		    /** TC_20(Viewer Page- document level):Navigating to the document by clicking on the document thumbnail of the RFI and try to save mark up for single page documents.   
		    */
		    
		    @Test(priority = 3, enabled = true, description = "Recipient opening the same posting answer and attaching a document then sending to owner answered RFI closing by the creator.")
		    public void Verify_NavigatetoDocumentAndClickTheThumbnail() throws Throwable
		    {		   	 
		    	
		    	try
		    	{
		    		Log.testCaseInfo("TC_20 (RFI ToolsBaar validation): Navigating to the document by clicking on the document thumbnail of the RFI and try to save mark up for single page documents");
		    		String uName = PropertyReader.getProperty("Username1");
		   		    String pWord = PropertyReader.getProperty("Password1");	   		    
		   		    String uName1 = PropertyReader.getProperty("Username2");
				    String pWord1 = PropertyReader.getProperty("Password2");
		   		    //==============================Login with valid UserID/PassWord=====================
		     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
		     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);     		
		     		
		     		
		     		 String Project_Name = "Specific_Testdata_Static";
		 			 folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
		 			 //select folder			
		 			 String Foldername = "RFI_SinglePage";
		 			 folderPage.Select_Folder(Foldername);	
		 			
		     		//=======RFI Creation and  Validation========================     		
		     		 viewerScreenPage = new ViewerScreenPage(driver).get();     		
		     		
		     		 viewerScreenPage.Image1();		     		 
		     		 viewerScreenPage.RFI_TestDataCreation_ownerLevel();
		     		 //viewerScreenPage.RFI_OwnerLevel_validation();
		     		 // Log.assertThat(viewerScreenPage.RFI_TestDataCreation_ownerLevel(), "Test Data Creation working Successfully","Test Data Creation not working Successfully", driver);
		     		  		
		     		//viewerScreenPage.RFI_OwnerLevel_validation();
					// driver.close();				
		     		//viewerScreenPage.swichToMainWindow();
					 Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);
					 		     		
		    		
		    	} catch (Exception e) {
					e.getCause();
					Log.exception(e, driver);
					CommonMethod.analyzeLog(driver);

				} finally {
					Log.endTestCase();
					driver.quit();
				}
		    	
		    }
		    	
		    
		    
		    
		    
	    
	    
	    
	         
	        
	    
	          
	            
	         
	         
	         
	           
	          
	             
	           
	           
	         
	    
	    
	    
	    
	    
	    
	       
	    
	    
      
}
