package com.arc.projects.testscripts;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.SubmitalPage;
import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)

public class Projects_SubmitalModule_Suit {
	
	String webSite;
    ITestResult result;
    
    static WebDriver driver;
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    FolderPage folderPage;
    SubmitalPage submitalPage;
    
    @Parameters("browser")
    @BeforeMethod(groups = "naresh_test")//Newly Added due to group execution Naresh
 public WebDriver beforeTest(String browser) {
        
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        	File dest = new File("./drivers/win/chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            driver = new ChromeDriver( options );
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
        	System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
            driver = new SafariDriver();
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver;
 }
 
    /** TC_001 (Submittal Module): Verify Create a workflow template-1 then create submittal and submit with an attachment as PDF to an approver(employee).
     * Scripted By : NARESH  BABU kavuru
     * @throws Exception
     */
     @Test(priority = 0, enabled = true, description = "TC_001 (Submittal Module): Verify Create a workflow template-1 then create submittal and submit with an attachment as PDF to an approver(employee).")
     public void verify_Submit_Approve_submittal_Template1() throws Exception
     {
     	try
     	{
     		Log.testCaseInfo("TC_001 (Submittal Module): Verify Create a workflow template-1 then create submittal and submit with an attachment as PDF to an approver(employee).");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("Shared_User");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Emp_Details = PropertyReader.getProperty("NewData_Username");
   		String SubmComment_WhileSubmit = PropertyReader.getProperty("Submital_Comment_submit");
   		String Approver_Comment = PropertyReader.getProperty("Approver_Comment");
   		String Project_Name = PropertyReader.getProperty("Shared_Project1");
   		String WF_Template_Name = "WF_"+Generate_Random_Number.getRandomMobileNo();
  
   		File Path_FMProperties = new File(PropertyReader.getProperty("Submital_Attachments_Path"));
   		String Submittal_FoldPath = Path_FMProperties.getAbsolutePath().toString();
   		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		folderPage=projectDashboardPage.Validate_Select_SharedProject(Project_Name);//Select A Project
  		
     		submitalPage = new SubmitalPage(driver).get();
     		folderPage=submitalPage.SelectSubmittalList();//Select Submittal List
     		
     		submitalPage.select_Workflow_Template(WF_Template_Name);//Select WF Template
     		
     		String Submittal_Number = "SubId_"+Generate_Random_Number.getRandomMobileNo();
     		String Submittal_Name = "SubName_"+Generate_Random_Number.getRandomMobileNo();
     		Log.assertThat(submitalPage.Create_Submital_AndValidate(Submittal_Number, Submittal_Name), "Create submittal is working Successfully", "Create submittal is Not working", driver);
     		
     		String Search_FileName = "Sample";
     		Log.assertThat(submitalPage.Submit_Submittal_With_BothTypesOf_Attachments(Submittal_Number, Emp_Details, Submittal_FoldPath,SubmComment_WhileSubmit,Search_FileName), "Submiting submital successfully", "Failed to Submit Submittal", driver);//Submit Submittal
     		
     		projectDashboardPage.Logout_Projects();//Logout from Submitter
     		projectsLoginPage.loginWithValidCredential(Emp_Details,pWord);//Login as Approver
     		
     		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     		submitalPage.SelectSubmittalList();//Select Submittal List
     		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
     		Log.assertThat(submitalPage.Comment_Approve_Submittal_AsAnApprover(Submittal_Number, Approver_Comment, Submittal_Name, SubmComment_WhileSubmit), "Comment & Approved success from Approver", "Comment & Approved Failed from Approver", driver);
     		     
     	}
         catch(Exception e)
         {
        	 AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
         }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }
   
     /** TC_002 (Submittal Module.): Verify Create submittal, Reject, resubmit, Finally Approver will approves the Submittal.
      * Scripted By : NARESH  BABU kavuru
      * @throws Exception
      */
      @Test(priority = 1, enabled = true, description = "TC_002 (Submittal Module.): Verify Create submittal, Reject, resubmit, Finally Approver will approves the Submittal.")
      public void verify_Submit_RejectA_Resubmit_Approve_submittal_Template1() throws Exception
      {
      	try
      	{
      		Log.testCaseInfo("TC_002 (Create a workflow template-1 then create submittal and submit with an attachment as PDF to an approver(employee).): Verify the created submittal.");
      	//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("Shared_User");
    		String pWord = PropertyReader.getProperty("NewData_Password");
    		String Emp_Details = PropertyReader.getProperty("NewData_Username");
    		String SubmComment_WhileSubmit = PropertyReader.getProperty("Submital_Comment_submit");
    		String Approver_Comment = PropertyReader.getProperty("Approver_Comment");
    		String Approver_RejectComment = PropertyReader.getProperty("Approver_Reject_Comment");
    		String Resubmit_Comment = PropertyReader.getProperty("Resubmit_Comment");
    		String Approver_Name = PropertyReader.getProperty("Employee_Name");
    		String Project_Name = PropertyReader.getProperty("Shared_Project1");
       		String WF_Template_Name = "WF_"+Generate_Random_Number.getRandomMobileNo();
       		
       		File Path_FMProperties = new File(PropertyReader.getProperty("Submital_Attachments_Path"));
       		String Submittal_FoldPath = Path_FMProperties.getAbsolutePath().toString();
       		
       		File ResubmitPath_FMProperties = new File(PropertyReader.getProperty("Resubmit_Attachments_Path"));
       		String Resubmit_FoldPath = ResubmitPath_FMProperties.getAbsolutePath().toString();
       		
         	projectsLoginPage = new ProjectsLoginPage(driver).get();  
         	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
         		
         	folderPage=projectDashboardPage.Validate_Select_SharedProject(Project_Name);//Select A Project
      		
         	submitalPage = new SubmitalPage(driver).get();
         	folderPage=submitalPage.SelectSubmittalList();//Select Submittal List
         		
         	submitalPage.select_Workflow_Template(WF_Template_Name);//Select WF Template
         		
         	String Submittal_Number = "SubId_"+Generate_Random_Number.getRandomMobileNo();
         	String Submittal_Name = "SubName_"+Generate_Random_Number.getRandomMobileNo();
         	Log.assertThat(submitalPage.Create_Submital_AndValidate(Submittal_Number, Submittal_Name), 
         			"Create submittal is working Successfully", "Create submittal is Not working", driver);         	
       		Log.assertThat(submitalPage.Submit_Submittal_WithAttachments(Submittal_Number, Emp_Details, Submittal_FoldPath,SubmComment_WhileSubmit), 
       				"Submiting submital successfully", "Failed to Submit Submittal", driver);//Submit Submittal
	
      		projectDashboardPage.Logout_Projects();//Logout from Submitter
      		projectsLoginPage.loginWithValidCredential(Emp_Details,pWord);//Login as Approver
      		
      		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     		submitalPage.SelectSubmittalList();//Select Submittal List
      		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
      		Log.assertThat(submitalPage.Comment_Reject_Submittal_AsAnApprover(Submittal_Number, Approver_RejectComment, Submittal_Name, SubmComment_WhileSubmit), "Comment & Reject success from Approver", "Comment & Reject Failed from Approver", driver);
      		
      		projectDashboardPage.Logout_Projects();//Logout from Approver
      		projectsLoginPage.loginWithValidCredential(uName,pWord);//Login as Submitter
      		
      		projectDashboardPage.Validate_Select_SharedProject(Project_Name);//Select A Shared Project
     		submitalPage.SelectSubmittalList();//Select Submittal List
      		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
      		Log.assertThat(submitalPage.Resubmit_Submittal_AsASubmitter(Submittal_Number, Submittal_Name, Approver_Name, Emp_Details, Resubmit_Comment, Resubmit_FoldPath), "Comment & Resubmit success from Submitter", "Comment & Resubmit Failed from Submitter", driver);

      		projectDashboardPage.Logout_Projects();//Logout from Submitter
      		projectsLoginPage.loginWithValidCredential(Emp_Details,pWord);//Login as Approver
      
      		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     		submitalPage.SelectSubmittalList();//Select Submittal List
      		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
      		Log.assertThat(submitalPage.Approve_ResubmittedSubmittal_AsAnApprover(Submittal_Number, Approver_Comment, Submittal_Name, Resubmit_Comment), "Comment & Approved success from Approver", "Comment & Approved Failed from Approver", driver);
      		     
      	}
          catch(Exception e)
          {
        	  AnalyzeLog.analyzeLog(driver);
          	e.getCause();
          	Log.exception(e, driver);
          }
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }
 
      /** TC_003 (Submittal Module): Verify Create submittal, Reassign, Finally reassigny  will approves the Submittal.
       * Scripted By : NARESH  BABU kavuru
       * @throws Exception
       */
       @Test(priority = 2, enabled = true, description = "TC_003 (Submittal Module): Verify Create submittal, Reassign, Finally reassigny  will approves the Submittal.")
       public void verify_Submit_Reassign_Approve_submittal_Template1() throws Exception
       {
       	try
       	{
       		Log.testCaseInfo("TC_003 (Submittal Module): Verify Create submittal, Reassign, Finally reassigny  will approves the Submittal.");
       	//Getting Static data from properties file
     		String uName = PropertyReader.getProperty("Shared_User");
     		String pWord = PropertyReader.getProperty("NewData_Password");
     		String Emp_Details = PropertyReader.getProperty("NewData_Username");
     		String Emp2_MailId = PropertyReader.getProperty("Employee2_Email");
     		String Emp2_Name = PropertyReader.getProperty("Employee2_Name");
     		String SubmComment_WhileSubmit = PropertyReader.getProperty("Submital_Comment_submit");
     		String Approver_ReassignComment = PropertyReader.getProperty("Approver_Reassign_Comment");
     		String Approver_Comment = PropertyReader.getProperty("Approver_Comment");
     		String Project_Name = PropertyReader.getProperty("Shared_Project1");
       		String WF_Template_Name = "WF_"+Generate_Random_Number.getRandomMobileNo();
       		
       		File Path_FMProperties = new File(PropertyReader.getProperty("Submital_Attachments_Path"));
       		String Submittal_FoldPath = Path_FMProperties.getAbsolutePath().toString();
     		
       		projectsLoginPage = new ProjectsLoginPage(driver).get();  
       		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
       		
       		folderPage=projectDashboardPage.Validate_Select_SharedProject(Project_Name);//Select A Project
      		
         	submitalPage = new SubmitalPage(driver).get();
         	folderPage=submitalPage.SelectSubmittalList();//Select Submittal List
         		
         	submitalPage.select_Workflow_Template(WF_Template_Name);//Select WF Template
         		
         	String Submittal_Number = "SubId_"+Generate_Random_Number.getRandomMobileNo();
         	String Submittal_Name = "SubName_"+Generate_Random_Number.getRandomMobileNo();
         	Log.assertThat(submitalPage.Create_Submital_AndValidate(Submittal_Number, Submittal_Name), "Create submittal is working Successfully", "Create submittal is Not working", driver);         	
       		Log.assertThat(submitalPage.Submit_Submittal_WithAttachments(Submittal_Number, Emp_Details, Submittal_FoldPath,SubmComment_WhileSubmit), "Submiting submital successfully", "Failed to Submit Submittal", driver);//Submit Submittal
	
       		projectDashboardPage.Logout_Projects();//Logout from Submitter
       		projectsLoginPage.loginWithValidCredential(Emp_Details,pWord);//Login as Approver
       		
       		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     		submitalPage.SelectSubmittalList();//Select Submittal List
       		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
       		Log.assertThat(submitalPage.Comment_Reassign_Submittal_ToOtheremployee(Submittal_Number, Submittal_Name, Approver_ReassignComment, Emp2_MailId,Emp2_Name), "Comment & Reassign success from Approver", "Comment & Reassign Failed from Approver", driver);
       		
       		projectDashboardPage.Logout_Projects();//Logout from Approver
       		projectsLoginPage.loginWithValidCredential(Emp2_MailId,pWord);//Login as a Re-assignee
       		
       		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     		submitalPage.SelectSubmittalList();//Select Submittal List
       		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
     		Log.assertThat(submitalPage.Comment_Approve_Submittal_AsAnApprover(Submittal_Number, Approver_Comment, Submittal_Name, Approver_ReassignComment), "Comment & Approved success from Reassignee side", "Comment & Approved Failed from Reassignee side", driver);
 
       	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
        	e.getCause();
           	Log.exception(e, driver);
        }
       	finally
       	{
       		Log.endTestCase();
       		driver.quit();
       	}
       }

       /** TC_004 (Submittal Module): Verify Create submittal, Reassign, Reject_Reassigny, Resubmit Finally reassigny  will approves the Submittal.
        * Scripted By : NARESH  BABU kavuru
        * @throws Exception
        */
        @Test(priority = 3, enabled = true, description = "TC_004 (Submittal Module): Verify Create submittal, Reassign, Reject_Reassigny, Resubmit Finally reassigny  will approves the Submittal.",groups = "naresh_test")
        public void verify_Submit_Reassign_Reject_Resubmit_Approve_submittal_Template1() throws Exception
        {
        	try
        	{
        		Log.testCaseInfo("TC_004 (Submittal Module): Verify Create submittal, Reassign, Reject_Reassigny, Resubmit Finally reassigny  will approves the Submittal.");
        	//Getting Static data from properties file
      		String uName = PropertyReader.getProperty("Shared_User");
      		String pWord = PropertyReader.getProperty("NewData_Password");
      		String Emp_Details = PropertyReader.getProperty("NewData_Username");
      		String Emp2_MailId = PropertyReader.getProperty("Employee2_Email");
      		String Emp2_Name = PropertyReader.getProperty("Employee2_Name");
      		String SubmComment_WhileSubmit = PropertyReader.getProperty("Submital_Comment_submit");
      		String Approver_RejectComment = PropertyReader.getProperty("Approver_Reject_Comment");
      		String Resubmit_Comment = PropertyReader.getProperty("Resubmit_Comment");
      		String Approver_ReassignComment = PropertyReader.getProperty("Approver_Reassign_Comment");
      		String Approver_Comment = PropertyReader.getProperty("Approver_Comment");
      		String Project_Name = PropertyReader.getProperty("Shared_Project1");
       		String WF_Template_Name = "WF_"+Generate_Random_Number.getRandomMobileNo();
       		
       		File Path_FMProperties = new File(PropertyReader.getProperty("Submital_Attachments_Path"));
       		String Submittal_FoldPath = Path_FMProperties.getAbsolutePath().toString();
       		File ResubmitPath_FMProperties = new File(PropertyReader.getProperty("Resubmit_Attachments_Path"));
       		String Resubmit_FoldPath = ResubmitPath_FMProperties.getAbsolutePath().toString();
      		
        		projectsLoginPage = new ProjectsLoginPage(driver).get();  
        		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
        		
        		folderPage=projectDashboardPage.Validate_Select_SharedProject(Project_Name);//Select A Project
          		
             	submitalPage = new SubmitalPage(driver).get();
             	folderPage=submitalPage.SelectSubmittalList();//Select Submittal List
             		
             	submitalPage.select_Workflow_Template(WF_Template_Name);//Select WF Template
             		
             	String Submittal_Number = "SubId_"+Generate_Random_Number.getRandomMobileNo();
             	String Submittal_Name = "SubName_"+Generate_Random_Number.getRandomMobileNo();
             	Log.assertThat(submitalPage.Create_Submital_AndValidate(Submittal_Number, Submittal_Name), "Create submittal is working Successfully", "Create submittal is Not working", driver);         	
           		Log.assertThat(submitalPage.Submit_Submittal_WithAttachments(Submittal_Number, Emp_Details, Submittal_FoldPath,SubmComment_WhileSubmit), "Submiting submital successfully", "Failed to Submit Submittal", driver);//Submit Submittal
    	
        		projectDashboardPage.Logout_Projects();//Logout from Submitter
        		projectsLoginPage.loginWithValidCredential(Emp_Details,pWord);//Login as Approver
        		
        		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
         		submitalPage.SelectSubmittalList();//Select Submittal List
        		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
        		Log.assertThat(submitalPage.Comment_Reassign_Submittal_ToOtheremployee(Submittal_Number, Submittal_Name, Approver_ReassignComment, Emp2_MailId,Emp2_Name), "Comment & Reassign success from Approver", "Comment & Reassign Failed from Approver", driver);
        		
        		projectDashboardPage.Logout_Projects();//Logout from Approver
        		projectsLoginPage.loginWithValidCredential(Emp2_MailId,pWord);//Login as a Re-assignee
        		
        		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
         		submitalPage.SelectSubmittalList();//Select Submittal List
        		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
          		Log.assertThat(submitalPage.Comment_Reject_Submittal_AsAnApprover(Submittal_Number, Approver_RejectComment, Submittal_Name, Approver_ReassignComment), "Comment & Reject success from Reassigny", "Comment & Reject Failed from Reassigny", driver);

          		projectDashboardPage.Logout_Projects();//Logout from Submitter
        		projectsLoginPage.loginWithValidCredential(uName,pWord);//Login as Submitter
        		
        		projectDashboardPage.Validate_Select_SharedProject(Project_Name);//Select A Shared Project
         		submitalPage.SelectSubmittalList();//Select Submittal List
        		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
          		Log.assertThat(submitalPage.Resubmit_Submittal_AsASubmitter(Submittal_Number, Submittal_Name, Emp2_Name, Emp2_MailId, Resubmit_Comment, Resubmit_FoldPath), "Comment & Resubmit success from Submitter", "Comment & Resubmit Failed from Submitter", driver);
        		
          		projectDashboardPage.Logout_Projects();//Logout from Approver
        		projectsLoginPage.loginWithValidCredential(Emp2_MailId,pWord);//Login as a Re-assignee
        		
        		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
         		submitalPage.SelectSubmittalList();//Select Submittal List
        		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
        		Log.assertThat(submitalPage.Approve_ResubmittedSubmittal_AsAnApprover(Submittal_Number, Approver_Comment, Submittal_Name, Resubmit_Comment), "Comment & Approved success from Approver", "Comment & Approved Failed from Approver", driver);  
        	}
            catch(Exception e)
            {
            	AnalyzeLog.analyzeLog(driver);
            	e.getCause();
            	Log.exception(e, driver);
            }
        	finally
        	{
        		Log.endTestCase();
        		driver.quit();
        	}
        }
        
        /**TC_005 (Submittal Module): Verify Import submittal with template1 and validate.
         * Scripted By : NARESH  BABU kavuru
         * @throws Exception
         */
         @Test(priority = 4, enabled = true, description = "TC_005 (Submittal Module): Verify Import submittal with template1 and validate.")
         public void verify_Import_Submittal_Template1() throws Exception
         {
         	try
         	{
         		Log.testCaseInfo("TC_005 (Submittal Module): Verify Import submittal with template1 and validate.");
         	//Getting Static data from properties file
       		String uName = PropertyReader.getProperty("NewData_Username");
       		String pWord = PropertyReader.getProperty("NewData_Password");
       		String WF_Template_Name = "WF_"+Generate_Random_Number.getRandomMobileNo();
       		
       		File Path_FMProperties = new File(PropertyReader.getProperty("Submital_Import_FilePath"));
       		String Submit_Import_FilePath = Path_FMProperties.getAbsolutePath().toString();
       		
         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
         		
         		String Project_Name = "Submittal_"+Generate_Random_Number.generateRandomValue();
           		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
         		
         		submitalPage = new SubmitalPage(driver).get();
         		folderPage=submitalPage.SelectSubmittalList();//Select Submittal List
         		
         		Log.assertThat(submitalPage.Import_Submital_AndValidate(Submit_Import_FilePath,WF_Template_Name), "Import submittal is working Successfully", "Import submittal is Not working", driver);
         		
         	}
             catch(Exception e)
             {
            	 AnalyzeLog.analyzeLog(driver);
             	e.getCause();
             	Log.exception(e, driver);
             }
         	finally
         	{
         		Log.endTestCase();
         		driver.quit();
         	}
         }
         
/** TC_006 (Submittal Module): Verify Create a WF template-2 then submit, Approver Comment, Assign to 2nd Approver and approve.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
	@Test(priority = 5, enabled = true, description = "TC_006 (Submittal Module): Verify Create a WF template-2 then submit, Approver Comment, Assign to 2nd Approver and approve.")
	public void verify_Submit_AssignToSecondApprover_Approve_Template2() throws Exception
    {
		try
        {
			Log.testCaseInfo("TC_006 (Submittal Module): Verify Create a WF template-2 then submit, Approver Comment, Assign to 2nd Approver and approve.");
          	//Getting Static data from properties file
			String uName = PropertyReader.getProperty("Shared_User");
    		String pWord = PropertyReader.getProperty("NewData_Password");
    		String Emp_Details = PropertyReader.getProperty("NewData_Username");
    		String Emp2_MailId = PropertyReader.getProperty("Employee2_Email");
    		String SubmComment_WhileSubmit = PropertyReader.getProperty("Submital_Comment_submit");
    		String Approver_Comment = PropertyReader.getProperty("Approver_Comment");
    		String Approver_ReassignComment = PropertyReader.getProperty("Approver_Reassign_Comment");
    		String Project_Name = PropertyReader.getProperty("Shared_Project2");
       		String WF_Template_Name = "WF_"+Generate_Random_Number.getRandomMobileNo();
    		int FinalApproveType_Approved = 1;
    		
    		File Path_FMProperties = new File(PropertyReader.getProperty("Submital_Attachments_Path"));
    		String Submittal_FoldPath = Path_FMProperties.getAbsolutePath().toString();
    		File ResubmitPath_FMProperties = new File(PropertyReader.getProperty("Resubmit_Attachments_Path"));
    		String Resubmit_FoldPath = ResubmitPath_FMProperties.getAbsolutePath().toString();
    		    		
    		projectsLoginPage = new ProjectsLoginPage(driver).get();  
    		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
    		
    		folderPage=projectDashboardPage.Validate_Select_SharedProject(Project_Name);//Select A Project
      		
         	submitalPage = new SubmitalPage(driver).get();
         	folderPage=submitalPage.SelectSubmittalList();//Select Submittal List
      		
      		submitalPage.select_Workflow_Template2(WF_Template_Name);//Select WF Template2
      		
      		String Submittal_Number = "SubId_"+Generate_Random_Number.getRandomMobileNo();
      		String Submittal_Name = "SubName_"+Generate_Random_Number.getRandomMobileNo();
      		Log.assertThat(submitalPage.Create_Submital_AndValidate(Submittal_Number, Submittal_Name), 
      				"Create submittal is working Successfully", "Create submittal is Not working", driver);
      		
      		Log.assertThat(submitalPage.Submit_Submittal_WithAttachments(Submittal_Number, Emp_Details, Submittal_FoldPath,
      				SubmComment_WhileSubmit), "Submiting submital successfully", "Failed to Submit Submittal", driver);//Submit Submittal
      		
      		projectDashboardPage.Logout_Projects();//Logout from Submitter
      		projectsLoginPage.loginWithValidCredential(Emp_Details,pWord);//Login as Approver
      		
      		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     		submitalPage.SelectSubmittalList();//Select Submittal List
      		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
      		Log.assertThat(submitalPage.Comment_Approve_Submittal_BySelect2ndApprover(Submittal_Number, Approver_Comment,
      				Submittal_Name, SubmComment_WhileSubmit, Emp2_MailId, Approver_ReassignComment, Resubmit_FoldPath), 
      				"Comment & Approved success from 1st Approver", "Comment & Approved Failed from 1st Approver", driver);
      		     
      		projectDashboardPage.Logout_Projects();//Logout from Submitter
      		projectsLoginPage.loginWithValidCredential(Emp2_MailId,pWord);//Login as 2nd Approver
      		
      		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     		submitalPage.SelectSubmittalList();//Select Submittal List
      		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
     		Log.assertThat(submitalPage.Approve_Submittal_AsA2ndApprover(Submittal_Number, Submittal_Name, FinalApproveType_Approved), 
     				"Approved success from 2nd approver side", "Approved Failed from 2nd approver side", driver);    		
      	}
          catch(Exception e)
          {
        	  AnalyzeLog.analyzeLog(driver);
          	e.getCause();
          	Log.exception(e, driver);
          }
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
    }

	 /** TC_007 (Submittal Module): Create and submit, A1 Comment and Assign to A2, A2 Reject, A1 review and assign to A2, A2 approved as Noted.
     * Scripted By : NARESH  BABU kavuru
     * @throws Exception
     */
@Test(priority = 6, enabled = true, description = "TC_007 (Submittal Module): Create and submit, A1 Comment and Assign to A2, A2 Reject, A1 review and assign to A2, A2 approved as Noted.")
     public void verify_Submit_A1AssignToA2_A2Reject_A1ReviewAndReassign_A2ApproveAsNoted_Template2() throws Exception
     {
     	try
     	{
		Log.testCaseInfo("TC_007 (Submittal Module): Create and submit, A1 Comment and Assign to A2, A2 Reject, A1 review and assign to A2, A2 approved as Noted.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("Shared_User");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		String Emp_Details = PropertyReader.getProperty("NewData_Username");
   		String Emp2_MailId = PropertyReader.getProperty("Employee2_Email");
   		String Emp2_Name = PropertyReader.getProperty("Employee2_Name");
   		String SubmComment_WhileSubmit = PropertyReader.getProperty("Submital_Comment_submit");
   		String Approver_Comment = PropertyReader.getProperty("Approver_Comment");
   		String Approver_RejectComment = PropertyReader.getProperty("Approver_Reject_Comment");
   		String Approver_ReassignComment = PropertyReader.getProperty("Approver_Reassign_Comment");
   		String Project_Name = PropertyReader.getProperty("Shared_Project1");
   		String WF_Template_Name = "WF_"+Generate_Random_Number.getRandomMobileNo();
   		int FinalApproveType_Noted = 2;
   		
   		File Path_FMProperties = new File(PropertyReader.getProperty("Submital_Attachments_Path"));
		String Submittal_FoldPath = Path_FMProperties.getAbsolutePath().toString();
		File ResubmitPath_FMProperties = new File(PropertyReader.getProperty("Resubmit_Attachments_Path"));
		String Resubmit_FoldPath = ResubmitPath_FMProperties.getAbsolutePath().toString();

   		
   			projectsLoginPage = new ProjectsLoginPage(driver).get();  
   			projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
		
   			folderPage=projectDashboardPage.Validate_Select_SharedProject(Project_Name);//Select A Project
  		
   			submitalPage = new SubmitalPage(driver).get();
   			folderPage=submitalPage.SelectSubmittalList();//Select Submittal List
     		submitalPage.select_Workflow_Template2(WF_Template_Name);//Select WF Template2
       
     		String Submittal_Number = "SubId_"+Generate_Random_Number.getRandomMobileNo();
     		String Submittal_Name = "SubName_"+Generate_Random_Number.getRandomMobileNo();
     		Log.assertThat(submitalPage.Create_Submital_AndValidate(Submittal_Number, Submittal_Name), 
     				"Create submittal is working Successfully", "Create submittal is Not working", driver);
     		
     		Log.assertThat(submitalPage.Submit_Submittal_WithAttachments(Submittal_Number, Emp_Details, Submittal_FoldPath,
     				SubmComment_WhileSubmit), "Submiting submital successfully", "Failed to Submit Submittal", driver);//Submit Submittal
     		
     		projectDashboardPage.Logout_Projects();//Logout from Submitter
     		projectsLoginPage.loginWithValidCredential(Emp_Details,pWord);//Login as Approver1
     		
     		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     		submitalPage.SelectSubmittalList();//Select Submittal List
     		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
     		Log.assertThat(submitalPage.Comment_Approve_Submittal_BySelect2ndApprover(Submittal_Number, Approver_Comment,
     				Submittal_Name, SubmComment_WhileSubmit, Emp2_MailId, Approver_ReassignComment, Resubmit_FoldPath), 
     				"Comment & Approved success from 1st Approver", "Comment & Approved Failed from 1st Approver", driver);
     		
     		projectDashboardPage.Logout_Projects();//Logout from Submitter
     		projectsLoginPage.loginWithValidCredential(Emp2_MailId,pWord);//Login as 2nd Approver
     		
     		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     		submitalPage.SelectSubmittalList();//Select Submittal List
     		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
      		Log.assertThat(submitalPage.Comment_Reject_Submittal_AsAnApprover2(Submittal_Number, Approver_RejectComment, Submittal_Name, 
      				Approver_ReassignComment), "Comment & Reject success from Approver2", "Comment & Reject Failed from Approver2", driver);

      		projectDashboardPage.Logout_Projects();//Logout from Submitter
     		projectsLoginPage.loginWithValidCredential(Emp_Details,pWord);//Login as Approver1
     		
     		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     		submitalPage.SelectSubmittalList();//Select Submittal List
     		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
      		Log.assertThat(submitalPage.A1WillReview_AgainAssaignBackToA2_AsA1(Submittal_Number, Submittal_Name, Emp2_Name, Emp2_MailId), 
      				"Review & Assign back to Approver2 success from Approver1", "Review & Assign back to Approver2 Failed from Approver1", driver);

      		projectDashboardPage.Logout_Projects();//Logout from Submitter
     		projectsLoginPage.loginWithValidCredential(Emp2_MailId,pWord);//Login as 2nd Approver
     		
     		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
     		submitalPage.SelectSubmittalList();//Select Submittal List
     		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
    		Log.assertThat(submitalPage.Approve_Submittal_AsA2ndApprover(Submittal_Number, Submittal_Name, FinalApproveType_Noted), 
    				"Approved success from 2nd approver side", "Approved Failed from 2nd approver side", driver);
     		
     	}
         catch(Exception e)
         {
        	 AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
         }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }

	/** TC_008 (Submittal Module): Create and submit, A1 Comment and Reject, Resubmit to A1, A1 review and assign to A2, A2 approved as No exception taken.
	 * Scripted By : NARESH  BABU kavuru
	 * @throws Exception
	 */
@Test(priority = 7, enabled = true, description = "TC_008 (Submittal Module): Create and submit, A1 Comment and Reject, Resubmit to A1, A1 review and assign to A2, A2 approved as No exception taken.")
public void verify_Submit_A1CommentAndReject_A1AssignToA2_A2ApproveAsNoException_Template2() throws Exception
{
	try
	{
	Log.testCaseInfo("TC_008 (Submittal Module): Create and submit, A1 Comment and Reject, Resubmit to A1, A1 review and assign to A2, A2 approved as No exception taken.");
	//Getting Static data from properties file
		String uName = PropertyReader.getProperty("Shared_User");
		String pWord = PropertyReader.getProperty("NewData_Password");
		String Emp_Details = PropertyReader.getProperty("NewData_Username");
		String Approver_Name = PropertyReader.getProperty("Employee_Name");
		String Emp2_MailId = PropertyReader.getProperty("Employee2_Email");
		String SubmComment_WhileSubmit = PropertyReader.getProperty("Submital_Comment_submit");
		String Resubmit_Comment = PropertyReader.getProperty("Resubmit_Comment");
		String Approver_RejectComment = PropertyReader.getProperty("Approver_Reject_Comment");
		String Approver_ReassignComment = PropertyReader.getProperty("Approver_Reassign_Comment");
		String Project_Name = PropertyReader.getProperty("Shared_Project2");
   		String WF_Template_Name = "WF_"+Generate_Random_Number.getRandomMobileNo();
   		
   		File Path_FMProperties = new File(PropertyReader.getProperty("Submital_Attachments_Path"));
		String Submittal_FoldPath = Path_FMProperties.getAbsolutePath().toString();
		File ResubmitPath_FMProperties = new File(PropertyReader.getProperty("Resubmit_Attachments_Path"));
		String Resubmit_FoldPath = ResubmitPath_FMProperties.getAbsolutePath().toString();
		
		projectsLoginPage = new ProjectsLoginPage(driver).get();  
		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
		
		folderPage=projectDashboardPage.Validate_Select_SharedProject(Project_Name);//Select A Project
  		
		submitalPage = new SubmitalPage(driver).get();
		folderPage=submitalPage.SelectSubmittalList();//Select Submittal List
 		submitalPage.select_Workflow_Template2(WF_Template_Name);//Select WF Template2
		
		String Submittal_Number = "SubId_"+Generate_Random_Number.getRandomMobileNo();
		String Submittal_Name = "SubName_"+Generate_Random_Number.getRandomMobileNo();
		Log.assertThat(submitalPage.Create_Submital_AndValidate(Submittal_Number, Submittal_Name), 
				"Create submittal is working Successfully", "Create submittal is Not working", driver);
		
		Log.assertThat(submitalPage.Submit_Submittal_WithAttachments(Submittal_Number, Emp_Details, Submittal_FoldPath,
				SubmComment_WhileSubmit), "Submiting submital successfully", "Failed to Submit Submittal", driver);//Submit Submittal
		
		projectDashboardPage.Logout_Projects();//Logout from Submitter
		projectsLoginPage.loginWithValidCredential(Emp_Details,pWord);//Login as Approver1
		
		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		submitalPage.SelectSubmittalList();//Select Submittal List
		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
  		Log.assertThat(submitalPage.Comment_Reject_Submittal_AsAnApprover(Submittal_Number, Approver_RejectComment, Submittal_Name, 
  				SubmComment_WhileSubmit), "Comment & Reject success from Approver", "Comment & Reject Failed from Approver", driver);

  		projectDashboardPage.Logout_Projects();//Logout from Approver
  		projectsLoginPage.loginWithValidCredential(uName,pWord);//Login as Submitter
  		
  		projectDashboardPage.Validate_Select_SharedProject(Project_Name);//Select A Shared Project
  		submitalPage.SelectSubmittalList();//Select Submittal List
  		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
  		Log.assertThat(submitalPage.Resubmit_Submittal_AsASubmitter(Submittal_Number, Submittal_Name, Approver_Name, Emp_Details, Resubmit_Comment, Resubmit_FoldPath), "Comment & Resubmit success from Submitter", "Comment & Resubmit Failed from Submitter", driver);

  		projectDashboardPage.Logout_Projects();//Logout from Submitter
		projectsLoginPage.loginWithValidCredential(Emp_Details,pWord);//Login as Approver1
		
		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		submitalPage.SelectSubmittalList();//Select Submittal List
		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
 		Log.assertThat(submitalPage.Approve_ResubmittedSubmittal_BySelect2ndApprover(Submittal_Number, Submittal_Name, Emp2_MailId, Approver_ReassignComment), 
 				"Approved success from 1st Approver", "Approved Failed from 1st Approver", driver);
  	
		projectDashboardPage.Logout_Projects();//Logout from Submitter
		projectsLoginPage.loginWithValidCredential(Emp2_MailId,pWord);//Login as 2nd Approver
 			
		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		submitalPage.SelectSubmittalList();//Select Submittal List
		//submitalPage.SelectSubmit_Icon_For_ExpectProject(Project_Name);//Select expected project submittal icon
		Log.assertThat(submitalPage.Approve_Submittal_BySelect_NoExceptionsTaken(Submittal_Number, Submittal_Name), 
				"Approved success by Select No Exception Taken", "Approved Failed by Select No Exception Taken", driver);
		
	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
    	e.getCause();
    	Log.exception(e, driver);
    }
	finally
	{
		Log.endTestCase();
		driver.quit();
	}
}

/** TC_009 (Download sumbittal report of the version of V1 and V2 and validate.): Verify Download sumbittal report of the version of V1 and V2 and validate.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 *//*
 @Test(priority = 8, enabled = true, description = "TC_009 (Download sumbittal report of the version of V1 and V2 and validate.): Verify Download sumbittal report of the version of V1 and V2 and validate.")
 public void verify_DownloadSubmittal_Report_V1_V2() throws Exception
 {
 	try
 	{
 		Log.testCaseInfo("TC_009 (Download sumbittal report of the version of V1 and V2 and validate.): Verify Download sumbittal report of the version of V1 and V2 and validate.");
 	//Getting Static data from properties file
		String uName = PropertyReader.getProperty("Shared_User");
		String pWord = PropertyReader.getProperty("NewData_Password");
		String Emp_Details = PropertyReader.getProperty("Employee_Email");
		String SubmComment_WhileSubmit = PropertyReader.getProperty("Submital_Comment_submit");
		File Path_FMProperties = new File(PropertyReader.getProperty("Submital_Attachments_Path"));
		String Submittal_FoldPath = Path_FMProperties.getAbsolutePath().toString();
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
 		
 		String Project_Name = "Submittal_"+Generate_Random_Number.generateRandomValue();
 		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
 		
 		String Foldername = "Fold_"+Generate_Random_Number.generateRandomValue();
 		folderPage.New_Folder_Create(Foldername);//Create a new Folder
 		  		
 		submitalPage = new SubmitalPage(driver).get();
 		folderPage=submitalPage.SelectSubmittalList();//Select Submittal List
 		
 		submitalPage.select_Workflow_Template(Project_Name);//Select WF Template
 		
 		String Submittal_Number = "SubId_"+Generate_Random_Number.generateRandomValue();
 		String Submittal_Name = "SubName_"+Generate_Random_Number.generateRandomValue();
 		Log.assertThat(submitalPage.Create_Submital_AndValidate(Submittal_Number, Submittal_Name), "Create submittal is working Successfully", "Create submittal is Not working", driver);
 		Log.assertThat(submitalPage.Submit_Submittal_WithAttachments(Submittal_Number, Emp_Details, Submittal_FoldPath,SubmComment_WhileSubmit), "Submiting submital successfully", "Failed to Submit Submittal", driver);//Submit Submittal
 		
 		Log.assertThat(submitalPage.Submittal_ReportDownload_V1(Submittal_Number), "Download submittal Report is working Successfully", "Download submittal Report is Not working", driver);
 	}
     catch(Exception e)
     {
     	e.getCause();
     	Log.exception(e, driver);
     }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }*/
       
}
