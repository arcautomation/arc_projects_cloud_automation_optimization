package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.ContactsPage;
import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.PunchPage;
import com.arc.projects.pages.RFIPage;
import com.arc.projects.pages.Search_Testcases;
import com.arc.projects.pages.SubmitalPage;
import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.getCurrentDate;
import com.arcautoframe.utils.Log;

public class UK_Production_Sanity_Suit {
	
	
	String webSite;
    ITestResult result;
    
    static WebDriver driver;
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    FolderPage folderPage;
    SubmitalPage submitalPage;
    ContactsPage contactsPage;
    PunchPage punchPage;
    RFIPage rfiPage;
    Search_Testcases search_Testcases;
    
    @Parameters("browser")
    @BeforeMethod (groups = "naresh_test")//Newly Added due to group execution Naresh
 public WebDriver beforeTest(String browser) {
    	
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL_UK"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        	File dest = new File("./drivers/win/chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
            
            prefs.put("safebrowsing.enabled", "true");//this condition should be f@lse every time
 
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            options.setExperimentalOption("prefs", prefs);
            driver = new ChromeDriver( options );
            driver.get(PropertyReader.getProperty("SkysiteProdURL_UK"));
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
        	System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
            driver = new SafariDriver();
            driver.get(PropertyReader.getProperty("SkysiteProdURL_UK"));
        }
   return driver;
 }
 
/** TC_001 (Sanity Suit): Verify Create a project and upload files using Without Index.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
     
@Test(priority = 0, enabled = true, description = "TC_001 (Sanity Suit): Verify Create a project and upload files using Without Index.")
public void VERIFY_UPLOAD_WITHOUTINDEX() throws Exception
{
	try
	{
 		Log.testCaseInfo("TC_001 (Sanity Suit): Verify Create a project and upload files using Without Index.");
 	//Getting Static data from properties file
 		String uName = PropertyReader.getProperty("Sanity_Username");
 		String pWord = PropertyReader.getProperty("Sanity_Password");
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
 		
 		//Defining static project based on the day throughout the suit
    	String Project_Name = "Prj_"+getCurrentDate.getISTTime();
 		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
 		
 		String Foldername = "WITHOUT_INDEX";	
 		folderPage.New_Folder_Create(Foldername);//Create a new Folder
 		
 		folderPage.Select_Folder(Foldername);//Select Folder
 		
 		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
   		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
   		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
   		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
 		Log.assertThat(folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount), 
 				"Upload using Without Index working successfully","Upload using Without Index is Failed", driver);                
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
     	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }

/** TC_002 (Sanity Suit): Verify upload files using With Index (Index By File Name).
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
     
@Test(priority = 1, enabled = true, description = "TC_002 (Sanity Suit): Verify upload files using With Index (Index By File Name).")
public void VERIFY_UPLOAD_INDEXBYFILENAME() throws Exception
{
	try
	{
 		Log.testCaseInfo("TC_002 (Sanity Suit): Verify upload files using With Index (Index By File Name).");
 	//Getting Static data from properties file
 		String uName = PropertyReader.getProperty("Sanity_Username");
 		String pWord = PropertyReader.getProperty("Sanity_Password");
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
 		
 		//Defining static project based on the day throughout the suit
    	String Project_Name = "Prj_"+getCurrentDate.getISTTime();
 		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
 		
 		String Foldername = "IND_BY_FILENAME";	
 		folderPage.New_Folder_Create(Foldername);//Create a new Folder
 		
 		folderPage.Select_Folder(Foldername);//Select Folder
 		
 		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFile_MultiPage_Path"));
   		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
   		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_Single");
   		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
   		File Path_SheetCount = new File(PropertyReader.getProperty("SheetCount_XlPath"));
   		String sheetCount_FilePath = Path_SheetCount.getAbsolutePath().toString();
 		Log.assertThat(folderPage.UploadFiles_IndexByFileName(FolderPath, FileCount, sheetCount_FilePath), 
 				"Upload using Index by file name is working successfully","Upload using Index by file name is Failed", driver); 
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
     	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }

/** TC_003 (Sanity Suit): Verify upload files using With Index (Index By Sheet Number).
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
     
@Test(priority = 2, enabled = true, description = "TC_003 (Sanity Suit): Verify upload files using With Index (Index By Sheet Number).")
public void VERIFY_UPLOAD_INDEXBYSHEETNUMBER() throws Exception
{
	try
	{
 		Log.testCaseInfo("TC_003 (Sanity Suit): Verify upload files using With Index (Index By Sheet Number).");
 	//Getting Static data from properties file
 		String uName = PropertyReader.getProperty("Sanity_Username");
 		String pWord = PropertyReader.getProperty("Sanity_Password");
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
 		
 		//Defining static project based on the day throughout the suit
    	String Project_Name = "Prj_"+getCurrentDate.getISTTime();
 		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
 		
 		String Foldername = "IND_BY_SHEETNUMBER";	
 		folderPage.New_Folder_Create(Foldername);//Create a new Folder
 		
 		folderPage.Select_Folder(Foldername);//Select Folder
 		
 		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFile_MultiPage_Path"));
   		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
   		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_Single");
   		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
   		File Path_SheetCount = new File(PropertyReader.getProperty("SheetNumber_XLpath"));
   		String sheetCount_FilePath = Path_SheetCount.getAbsolutePath().toString();
 		Log.assertThat(folderPage.UploadFiles_IndexBySheetNumber(FolderPath, FileCount, sheetCount_FilePath), 
 				"Upload using Index by sheet number is working successfully","Upload using Index by sheet number is Failed", driver);                 
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
     	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }

/** TC_004 (Sanity Suit): Verify Do Folder Send link and download from link.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
     
@Test(priority = 3, enabled = true, description = "TC_004 (Sanity Suit): Verify Do Folder Send link and download from link")
public void VERIFY_FOLDER_SENDLINK() throws Exception
{
	try
	{
 		Log.testCaseInfo("TC_004 (Sanity Suit): Verify Do Folder Send link and download from link");
 	//Getting Static data from properties file
 		String uName = PropertyReader.getProperty("Sanity_Username");
 		String pWord = PropertyReader.getProperty("Sanity_Password");
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
 		
 		//Defining static project based on the day throughout the suit
    	String Project_Name = "Prj_"+getCurrentDate.getISTTime();
 		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
 		
 		String Foldername = "WITHOUT_INDEX";
 		String usernamedir=System.getProperty("user.name");
 		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
 		Log.assertThat(folderPage.Validate_FolderSendLink_DownloadFromLink(Sys_Download_Path, Foldername), 
 				"Folder SendLink and Download working successfully","Folder SendLink and Download is NOT working", driver);                 
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
     	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }

/** TC_005 (Sanity Suit): Verify Contacts Export and Validate.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
     
@Test(priority = 4, enabled = true, description = "TC_005 (Sanity Suit): Verify Contacts Export and Validate.")
public void VERIFY_CONTACT_EXPORT() throws Exception
{
	try
	{
 		Log.testCaseInfo("TC_005 (Sanity Suit): Verify Contacts Export and Validate.");
 	//Getting Static data from properties file
 		String uName = PropertyReader.getProperty("Sanity_Username");
 		String pWord = PropertyReader.getProperty("Sanity_Password");
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
 		
 		contactsPage = projectDashboardPage.Validate_SelectContactsBut();//Click on Contacts
 		String Random_Contct_Name = "Contact_"+Generate_Random_Number.generateRandomValue();
   		contactsPage.Add_New_Contact(Random_Contct_Name);
   		String usernamedir=System.getProperty("user.name");
   		
  		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
  		String csvFileToRead = "C:\\" + "Users\\" + usernamedir + "\\Downloads\\Contact.csv";
   		Log.assertThat(contactsPage.Add_New_Contact_AndExport(Sys_Download_Path,Random_Contct_Name,csvFileToRead), "Create Contact and Export to CSV is working Successfully", "Create Contact and Export to CSV is NOT working", driver);                
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
     	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }

/** TC_006 (Sanity Suit): Verify Contact Import and Validate.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
    
@Test(priority = 5, enabled = true, description = "TC_006 (Sanity Suit): Verify Contact Import and Validate.")
public void VERIFY_CONTACT_IMPORT() throws Exception
{
	try
	{
 		Log.testCaseInfo("TC_006 (Sanity Suit): Verify Contact Import and Validate.");
 	//Getting Static data from properties file
 		String uName = PropertyReader.getProperty("Sanity_Username");
 		String pWord = PropertyReader.getProperty("Sanity_Password");
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
 		
 		contactsPage = projectDashboardPage.Validate_SelectContactsBut();//Click on Contacts
 		contactsPage.Write_newContactDetails_IntoImportFile();//Write new values into xlsx file
  		
  		File ContImp = new File(PropertyReader.getProperty("ImpContacts_FolderPath"));
  		String ImpContact_FoldPath = ContImp.getAbsolutePath().toString();
 		Log.assertThat(contactsPage.Import_Contact_FromXLSX_Validate(ImpContact_FoldPath), 
 				"Import Contact using .XLSX working Successfully", "Import Contact using .XLSX is NOT working", driver);
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
     	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }

/** TC_007 (Sanity Suit): Verify Do Send Files and validate under tracking.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
     
@Test(priority = 6, enabled = true, description = "TC_007 (Sanity Suit): Verify Do Send Files and validate under tracking.")
public void VERIFY_SENDFILES() throws Exception
{
	try
	{
 		Log.testCaseInfo("TC_007 (Sanity Suit): Verify Do Send Files and validate under tracking.");
 	//Getting Static data from properties file
 		String uName = PropertyReader.getProperty("Sanity_Username");
 		String pWord = PropertyReader.getProperty("Sanity_Password");
 		String Rec_MailID = PropertyReader.getProperty("User_Folder_Exp");
 		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
 		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
 		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
 		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer 
 		
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
 		
 		Log.assertThat(projectDashboardPage.validate_SendFilesModule(FolderPath, Rec_MailID, FileCount), 
 				"Send Files module is working.", "Send Files module is NOT working.", driver);
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
     	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }

/** TC_008 (Sanity Suit): Verify Create Punch and Generate punch report.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
     
@Test(priority = 7, enabled = true, description = "TC_008 (Sanity Suit): Verify Create Punch and Generate punch report.")
public void VERIFY_PUNCH_CREATE_GENERATEREPORT() throws Exception
{
	try
	{
 		Log.testCaseInfo("TC_008 (Sanity Suit): Verify Create Punch and Generate punch report.");
 	//Getting Static data from properties file
 		String uName = PropertyReader.getProperty("Sanity_Username");
 		String pWord = PropertyReader.getProperty("Sanity_Password");
  		String Employee_Name = PropertyReader.getProperty("Sanity_Emp1Name");
  		String Employee_Mailid = PropertyReader.getProperty("Sanity_Emp1Mail");
		String Punch_Description = PropertyReader.getProperty("Punch_Descr_Atchmts");
		String File_Name = PropertyReader.getProperty("Single_File_Name");
 		
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
 		
 		//Defining static project based on the day throughout the suit
    	String Project_Name = "Prj_"+getCurrentDate.getISTTime();
 		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
   		
  		punchPage = new PunchPage(driver).get();
  		folderPage=punchPage.SelectPunchList();//Select Punch List
  		
  		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
 		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
  		String Stamp_Number = Generate_Random_Number.randomStampNumber();
  		Log.assertThat(punchPage.Create_Punch_WithAttachments_AndValidate(Stamp_Number,Employee_Name,Employee_Mailid,FolderPath,Punch_Description,File_Name), 
  				"Create Punch with attachments working Successfully", "Create Punch with attachments is Not working", driver);
  		
  		String usernamedir=System.getProperty("user.name");
 		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
 		Log.assertThat(punchPage.Verify_download_Punch_Report(Sys_Download_Path), 
   				"Generate punch report working successfullt.","Generate punch report not working.", driver);
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
     	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }

/** TC_009 (Sanity Suit): Verify ZIP download.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
     
@Test(priority = 8, enabled = true, description = "TC_009 (Sanity Suit): Verify ZIP download.")
public void VERIFY_ZIP_DOWNLOAD() throws Exception
{
	try
	{
 		Log.testCaseInfo("TC_009 (Sanity Suit): Verify ZIP download.");
 	//Getting Static data from properties file
 		String uName = PropertyReader.getProperty("Sanity_Username");
 		String pWord = PropertyReader.getProperty("Sanity_Password");
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
 		
 		//Defining static project based on the day throughout the suit
    	String Project_Name = "Prj_"+getCurrentDate.getISTTime();
 		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
 		
 		String Foldername = "WITHOUT_INDEX";	
 		String usernamedir=System.getProperty("user.name");
 		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
 		
 		Log.assertThat(folderPage.Download_Folder(Sys_Download_Path, Foldername), 
 				"Folder download working successfully","Folder download is NOT working", driver);                 
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
     	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }

/** TC_010 (Sanity Suit): Verify Move Include LD Folder to Exclude LD Folder and validate then Download.
 * Scripted By : NARESH BABU kavuru
 * @throws Exception
 * Move �Exclude LD Folder� to �Excluded LD Folder� then Download
 */
 @Test(priority = 9, enabled = true, description = "TC_010 (Sanity Suit): Verify Move Include LD Folder to Exclude LD Folder and validate then Download.")
 public void verify_Move_IncLdFolder_ToDestinationExcLdFolder_Downlaod() throws Exception
 {
 	try
 	{
 		Log.testCaseInfo("TC_010 (Sanity Suit): Verify Move Include LD Folder to Exclude LD Folder and validate then Download.");
 	//Getting Static data from properties file
 		String uName = PropertyReader.getProperty("Sanity_Username");
 		String pWord = PropertyReader.getProperty("Sanity_Password");
		
 		projectsLoginPage = new ProjectsLoginPage(driver).get();  
 		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
 		
 		String Project_Name = "Move_"+getCurrentDate.getISTTime();
 		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
 		
 		String Foldername = "SourcFold_"+Generate_Random_Number.generateRandomValue();
 		folderPage.New_Folder_Create(Foldername);//Create a New Folder Include LD
 		
 		folderPage.Select_Folder(Foldername);//Select a folder - newly created
 		
 		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
 		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
 		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
 		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
 		folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount);//Calling Upload using do not index
		
 		String Destination_Folder = "DestinationFold_"+Generate_Random_Number.generateRandomValue();
 		folderPage.New_Folder_Create_ExcludeLD(Destination_Folder);//Create a New Folder Exclude LD - Destination
 		
 		folderPage.Select_Folder(Destination_Folder);//Select a folder - newly created
 		
 		File Path_FMProperties1 = new File(PropertyReader.getProperty("Two_PdfFiles"));
 		String FolderPath1 = Path_FMProperties1.getAbsolutePath().toString();
 		String CountOfFilesInFolder1 = PropertyReader.getProperty("Count_Two_PdfFiles");
 		int FileCount1 = Integer.parseInt(CountOfFilesInFolder1);//String to Integer
 		folderPage.UploadFiles_WithoutIndex(FolderPath1,FileCount1);//Calling Upload using do not index
 		
 		Log.assertThat(folderPage.Move_Folder_ToDestinationFolder(Foldername, Destination_Folder), "Move Folder is working successfully","Move Folder is Not working", driver);
 		
 		Log.assertThat(folderPage.Validate_LD_FolderIsEmpty(), "LD Folder is working successfully","LD Folder is Not working", driver);
 		String usernamedir=System.getProperty("user.name");
   		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
 		Log.assertThat(folderPage.Download_Folder_ValidateInsideZip(Sys_Download_Path,Destination_Folder,FolderPath,FileCount), "Download & Validate ZIP Folder is working Successfully", "Download & Validate ZIP Folder is Not working", driver);
 
 	}
     catch(Exception e)
     {
    	 AnalyzeLog.analyzeLog(driver);
     	e.getCause();
     	Log.exception(e, driver);
     }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }
 
 /** TC_011 (Sanity Suit): Verify Create RFI With Attachments and Download RFI report.
  * Scripted By : NARESH BABU KAVURU
  * @throws Exception
  */
      
 @Test(priority = 10, enabled = true, description = "TC_011 (Sanity Suit): Verify Create RFI With Attachments and Download RFI report.")
 public void VERIFY_RFI_CREATE_DOWNLOAD_REPORT() throws Exception
 {
 	try
 	{
  		Log.testCaseInfo("TC_011 (Sanity Suit): Verify Create RFI With Attachments and Download RFI report.");
  	//Getting Static data from properties file
  		String uName = PropertyReader.getProperty("Sanity_Username");
  		String pWord = PropertyReader.getProperty("Sanity_Password");
   		String Employee_Name = PropertyReader.getProperty("Sanity_Emp1Name");
   		String Employee_Mailid = PropertyReader.getProperty("Sanity_Emp1Mail");
   		String Emp2_Name = PropertyReader.getProperty("Sanity_Emp2Name");
 		String File_Name = PropertyReader.getProperty("Single_File_Name");
  		
  		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
  		
  		//Defining static project based on the day throughout the suit
     	String Project_Name = "Prj_"+getCurrentDate.getISTTime();
  		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
    		
  		rfiPage = new RFIPage(driver).get();
   		folderPage=rfiPage.SelectRFIList();//Select RFI List
   		
   		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
  		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
   		Log.assertThat(rfiPage.Create_RFI_WithAttachments_AndValidate(Employee_Name, Employee_Mailid, Emp2_Name, FolderPath, File_Name), 
   				"Create RFI with attachments working Successfully", "Create RFI with attachments is Not working", driver);
   		
   		String usernamedir=System.getProperty("user.name");
  		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
  		Log.assertThat(rfiPage.Verify_download_RFI_Report(Sys_Download_Path), 
    				"Download RFI report working successfullt.","Download RFI report not working.", driver);
  	}
     catch(Exception e)
     {
    	 AnalyzeLog.analyzeLog(driver);
      	e.getCause();
      	Log.exception(e, driver);
     }
  	finally
  	{
  		Log.endTestCase();
  		driver.quit();
  	}
  }
 
 
 /** TC_012 (Sanity Suit): Verify Search for Project, Folder and File.
  * Scripted By : NARESH BABU KAVURU
 * @throws Throwable 
  */
      
 @Test(priority = 11, enabled = true, description = "TC_012 (Sanity Suit): Verify Search for Project, Folder and File.")
 public void VERIFY_SEARCH_PROJECT_FOLDER_FILE() throws Throwable
 {
 	try
 	{
  		Log.testCaseInfo("TC_012 (Sanity Suit): Verify Search for Project, Folder and File.");
  	//Getting Static data from properties file
  		String uName = PropertyReader.getProperty("Sanity_Username");
  		String pWord = PropertyReader.getProperty("Sanity_Password");
   		
  		
  		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
  		
  		//Defining static project based on the day throughout the suit
     	String Project_Name = "Prj_"+getCurrentDate.getISTTime();
     	search_Testcases = new Search_Testcases(driver).get();
		search_Testcases.Searchscenario(Project_Name);
		Log.assertThat(search_Testcases.Searchprojectvalidation(Project_Name), "Project Search working Successfully",
				"Project Search Not working Successfully", driver);
		
		driver.findElement(By.xpath("//span[@data-bind='text: ProjectName()']")).click();//Select project after search
		Thread.sleep(20000);
		
		String Foldername = "WITHOUT_INDEX";
		search_Testcases.Searchscenario(Foldername);
		Log.assertThat(search_Testcases.SearchFoldervalidation(Foldername),
				"Search folder verified Successfully", "Search Folder Not verified Successfully", driver);
		
		driver.findElement(By.xpath("//a[@id='btnSearchBack']")).click();//Click on Reset
		Thread.sleep(10000);
		
		String Filename = "1_Document.pdf";
		search_Testcases.Searchscenario(Filename);
		Log.assertThat(search_Testcases.SearchFilevalidation(Filename), "File Search verified Successfully",
				"File Search is not working.", driver);
  		
  	}
     catch(Exception e)
     {
    	 AnalyzeLog.analyzeLog(driver);
      	e.getCause();
      	Log.exception(e, driver);
     }
  	finally
  	{
  		Log.endTestCase();
  		driver.quit();
  	}
  }
 
 /** TC_013 (Sanity Suit): Verify create a Submittal by using Template1.
  * Scripted By : NARESH BABU KAVURU
 * @throws Throwable 
  */
      
 @Test(priority = 12, enabled = true, description = "TC_013 (Sanity Suit): Verify create a Submittal by using Template1.")
 public void VERIFY_Create_Submittal_UsingTemplate1() throws Throwable
 {
 	try
 	{
  		Log.testCaseInfo("TC_013 (Sanity Suit): Verify create a Submittal by using Template1.");
  	//Getting Static data from properties file
  		String uName = PropertyReader.getProperty("Sanity_Username");
  		String pWord = PropertyReader.getProperty("Sanity_Password");
  		String Emp_Details = PropertyReader.getProperty("Sanity_Emp1Mail");
  		String SubmComment_WhileSubmit = PropertyReader.getProperty("Submital_Comment_submit");
   		String Approver_Comment = PropertyReader.getProperty("Approver_Comment");
   		String WF_Template_Name = "WF_"+Generate_Random_Number.generateRandomValue();
   		File Path_FMProperties = new File(PropertyReader.getProperty("Submital_Attachments_Path"));
   		String Submittal_FoldPath = Path_FMProperties.getAbsolutePath().toString();
  
  		projectsLoginPage = new ProjectsLoginPage(driver).get();  
  		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
  		
  		//Defining static project based on the day throughout the suit
     	String Project_Name = "Prj_"+getCurrentDate.getISTTime();
  		//String Project_Name = "Prj_10-05-2018";
     	folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
     	
     	submitalPage = new SubmitalPage(driver).get();
 		folderPage=submitalPage.SelectSubmittalList();//Select Submittal List
 		
 		submitalPage.select_Workflow_Template(WF_Template_Name);//Select WF Template
 		
 		String Submittal_Number = "SubId_"+Generate_Random_Number.generateRandomValue();
 		String Submittal_Name = "SubName_"+Generate_Random_Number.generateRandomValue();
 		Log.assertThat(submitalPage.Create_Submital_AndValidate(Submittal_Number, Submittal_Name), 
 				"Create submittal is working Successfully", "Create submittal is Not working", driver);
 		
 		String Search_FileName = "1_Document.pdf";
 		Log.assertThat(submitalPage.Submit_Submittal_With_BothTypesOf_Attachments(Submittal_Number, Emp_Details, Submittal_FoldPath,SubmComment_WhileSubmit,Search_FileName), 
 				"Submiting submital successfully", "Failed to Submit Submittal", driver);//Submit Submittal
 		
 		projectDashboardPage.Logout_Projects();//Logout from Submitter
 		projectsLoginPage.loginWithValidCredential(Emp_Details,pWord);//Login as Approver
 		
 		projectDashboardPage.Validate_SelectAProject(Project_Name);//Select A Project
 		submitalPage.SelectSubmittalList();//Select Submittal List
 		Log.assertThat(submitalPage.Comment_Approve_Submittal_AsAnApprover(Submittal_Number, Approver_Comment, Submittal_Name, SubmComment_WhileSubmit), 
 				"Comment & Approved success from Approver", "Comment & Approved Failed from Approver", driver);
 		     
  	}
     catch(Exception e)
     {
    	 AnalyzeLog.analyzeLog(driver);
      	e.getCause();
      	Log.exception(e, driver);
     }
  	finally
  	{
  		Log.endTestCase();
  		driver.quit();
  	}
  }


}
