package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.SubmitalPage;
import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.Log;

public class File_Level_TestCases {
	
	String webSite;
    ITestResult result;
    
    static WebDriver driver;
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    FolderPage folderPage;
    SubmitalPage submitalPage;
    
    @Parameters("browser")
    @BeforeMethod (groups = "naresh_test")//Newly Added due to group execution Naresh
 public WebDriver beforeTest(String browser) {
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        	File dest = new File("./drivers/win/chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
            
            prefs.put("safebrowsing.enabled", "false");//this condition should be f@lse every time
 
            ChromeOptions options = new ChromeOptions();
             options.addArguments("--start-maximized");
            options.setExperimentalOption("prefs", prefs);
            driver = new ChromeDriver( options );
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
        	System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
            driver = new SafariDriver();
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver; 
 }

/**TC_001_File Level Operations (Verify Edit file and validate.): Verify Edit file and validate.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
@Test(priority = 0, enabled = true, description = "TC_001_File Level Operations (Verify Edit file and validate.): Verify Edit file and validate.")
public void verify_File_Edit() throws Exception
{
	try
	{	
		Log.testCaseInfo("TC_001_File Level Operations (Verify Edit file and validate.): Verify Edit file and validate.");
     	//Getting Static data from properties file
		String uName = PropertyReader.getProperty("Username_Export");
     	String pWord = PropertyReader.getProperty("Password_Export");
   
     	projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
     	
     	String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
 
		String Foldername = PropertyReader.getProperty("Fold_FileOperations");
		folderPage.Select_Folder(Foldername);//Select Folder
		
		String Edit_FileName = "Edit_"+Generate_Random_Number.generateRandomValue();
   		Log.assertThat(folderPage.Validate_File_Edit(Edit_FileName), 
   				"File Edit is working successfully","File Edit is Not working", driver);
    }
	catch(Exception e)
    {
		AnalyzeLog.analyzeLog(driver);
    	e.getCause();
    	Log.exception(e, driver);
    }
    finally
    {
    	Log.endTestCase();
    	driver.quit();
    }
}

/**TC_002_File Level Operations (Verify File Download and validate.): Verify File Download and validate.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
@Test(priority = 1, enabled = true, description = "TC_002_File Level Operations (Verify File Download and validate.): Verify File Download and validate.")
public void verify_File_Download() throws Exception
{
	try
	{	
		Log.testCaseInfo("TC_002_File Level Operations (Verify File Download and validate.): Verify File Download and validate.");
     	//Getting Static data from properties file
		String uName = PropertyReader.getProperty("Username_Export");
     	String pWord = PropertyReader.getProperty("Password_Export");
   
     	projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
     	
     	String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
 
		String Foldername = PropertyReader.getProperty("Fold_FileOperations");
		folderPage.Select_Folder(Foldername);//Select Folder
		
		Thread.sleep(5000);
		String FileName = driver.findElement(By.xpath("(//span[@class='h4_docname document-preview-event'])[1]")).getText();
		String usernamedir=System.getProperty("user.name");
 		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
   		Log.assertThat(folderPage.Download_File(Sys_Download_Path,FileName), 
   				"File Download is working successfully","File Download is Not working", driver);
    }
	catch(Exception e)
    {
		AnalyzeLog.analyzeLog(driver);
    	e.getCause();
    	Log.exception(e, driver);
    }
    finally
    {
    	Log.endTestCase();
    	driver.quit();
    }
}

/**TC_003_File Level Operations (Verify File Send Link.): Verify File Send Link.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
@Test(priority = 2, enabled = true, description = "TC_003_File Level Operations (Verify File Send Link.): Verify File Send Link.")
public void verify_File_SendLink() throws Exception
{
	try
	{	
		Log.testCaseInfo("TC_003_File Level Operations (Verify File Send Link.): Verify File Send Link.");
     	//Getting Static data from properties file
		String uName = PropertyReader.getProperty("Username_Export");
     	String pWord = PropertyReader.getProperty("Password_Export");
   
     	projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
     	
     	String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
 
		String Foldername = PropertyReader.getProperty("Fold_FileOperations");
		folderPage.Select_Folder(Foldername);//Select Folder
		
		Thread.sleep(5000);
		String FileName = driver.findElement(By.xpath("(//span[@class='h4_docname document-preview-event'])[1]")).getText();
		String usernamedir=System.getProperty("user.name");
 		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
   		Log.assertThat(folderPage.Validate_FileSendLink_FileDownloadFromLink(Sys_Download_Path,FileName), 
   				"File Sendlink is working successfully","File Sendlink is Not working", driver);
    }
	catch(Exception e)
    {
		AnalyzeLog.analyzeLog(driver);
    	e.getCause();
    	Log.exception(e, driver);
    }
    finally
    {
    	Log.endTestCase();
    	driver.quit();
    }
}

/**TC_004_File Level Operations (Verify File Delete.): Verify File Delete.
 * Scripted By : NARESH BABU KAVURU
 * @throws Exception
 */
@Test(priority = 3, enabled = true, description = "TC_004_File Level Operations (Verify File Delete.): Verify File Delete.")
public void verify_File_Delete() throws Exception
{
	try
	{	
		Log.testCaseInfo("TC_004_File Level Operations (Verify File Delete.): Verify File Delete.");
     	//Getting Static data from properties file
		String uName = PropertyReader.getProperty("NewData_Username");
     	String pWord = PropertyReader.getProperty("NewData_Password");
   
     	projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
     	
    	String Project_Name = "Ba_"+Generate_Random_Number.generateRandomValue();
 		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
 		
 		String Foldername = "DELETE_SINGLE_FILE";	
 		folderPage.New_Folder_Create(Foldername);//Create a new Folder
 		
 		folderPage.Select_Folder(Foldername);//Select Folder
 		
 		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFile_MultiPage_Path"));
   		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
   		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_Single");
   		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
 		Log.assertThat(folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount), 
 				"Upload using Without Index working successfully","Upload using Without Index is Failed", driver); 
 		
 		folderPage.Select_Folder(Foldername);//Select Folder
 		String FileName = "NonConst.pdf";
   		Log.assertThat(folderPage.Delete_File(FileName), 
   				"File Delete is working successfully","File Delete is Not working", driver);
    }
	catch(Exception e)
    {
		AnalyzeLog.analyzeLog(driver);
    	e.getCause();
    	Log.exception(e, driver);
    }
    finally
    {
    	Log.endTestCase();
    	driver.quit();
    }
}

/** TC_005_File Level Operations (Select Multiple Files and Download_GridView): Verify Select Multiple Files and Download.
 * Scripted By : NARESH  BABU kavuru
 * @throws Exception
 */
 @Test(priority = 4, enabled = true, description = "TC_005_File Level Operations (Select Multiple Files and Download_GridView): Verify Select Multiple Files and Download.")
 public void verify_Download_BySelectMultipleFiles() throws Exception
 {
 	try
 	{
 		Log.testCaseInfo("TC_005_File Level Operations (Select Multiple Files and Download_GridView): Verify Select Multiple Files and Download.");
 	//Getting Static data from properties file
 		String uName = PropertyReader.getProperty("Username_Export");
     	String pWord = PropertyReader.getProperty("Password_Export");
   
     	projectsLoginPage = new ProjectsLoginPage(driver).get();  
     	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
     	
     	String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
 
		String Foldername = PropertyReader.getProperty("Fold_LevOperations");
		folderPage.Select_Folder(Foldername);//Select Folder
 		
		String usernamedir=System.getProperty("user.name");
 		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
 		
 		Log.assertThat(folderPage.Validate_SelectAllFiles_GridView(), 
 			"Select All Files is working Successfully", "Select All Files is Not working", driver);
 		Log.assertThat(folderPage.ValidateDownload_BySelectMultFiles_GridView(Sys_Download_Path,Foldername), 
 			"Multiple files Download is working Successfully", "Multiple files Download is Not working", driver);
 	}
    catch(Exception e)
    {
    	AnalyzeLog.analyzeLog(driver);
     	e.getCause();
     	Log.exception(e, driver);
    }
 	finally
 	{
 		Log.endTestCase();
 		driver.quit();
 	}
 }
 
 /** TC_007_File Level Operations (Select Multiple Files and do Send Link_GridView): Verify Select Multiple Files and do Send Link.
  * TC_006 was covered in Download Suit
  * Scripted By : NARESH  BABU kavuru
  * @throws Exception
  */
  @Test(priority = 5, enabled = true, description = "TC_007_File Level Operations (Select Multiple Files and do Send Link_GridView): Verify Select Multiple Files and do Send Link.")
  public void verify_SendLink_BySelectMultipleFiles() throws Exception
  {
  	try
  	{
  		Log.testCaseInfo("TC_007_File Level Operations (Select Multiple Files and do Send Link_GridView): Verify Select Multiple Files and do Send Link.");
  	//Getting Static data from properties file
  		String uName = PropertyReader.getProperty("Username_Export");
      	String pWord = PropertyReader.getProperty("Password_Export");
    
      	projectsLoginPage = new ProjectsLoginPage(driver).get();  
      	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
      	
      	String Project_Name = PropertyReader.getProperty("prj_FolderOperations");
 		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
  
 		String Foldername = PropertyReader.getProperty("Fold_LevOperations");
 		folderPage.Select_Folder(Foldername);//Select Folder
  		
 		String usernamedir=System.getProperty("user.name");
  		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
  		
  		Log.assertThat(folderPage.Validate_SelectAllFiles_GridView(), 
  			"Select All Files is working Successfully", "Select All Files is Not working", driver);
  		Log.assertThat(folderPage.ValidateDownload_FromSendLink_BySelectAllFiles_GridView(Sys_Download_Path,Foldername), 
  				"Multiple file sendlink and Download is working Successfully", "Multiple file sendlink and Download is Not working", driver);
  	}
  	catch(Exception e)
  	{
  		AnalyzeLog.analyzeLog(driver);
      	e.getCause();
      	Log.exception(e, driver);
  	}
  	finally
  	{
  		Log.endTestCase();
  		driver.quit();
  	}
  }
  
  /** TC_009_File Level Operations (Select Multiple Files and verify delete_GridView): Verify Select Multiple Files and Delete.
   * TC_008 was covered in Download Suit
   * Scripted By : NARESH  BABU kavuru
   * @throws Exception
   */
   @Test(priority = 6, enabled = true, description = "TC_009_File Level Operations (Select Multiple Files and verify delete_GridView): Verify Select Multiple Files and Delete.",groups = "naresh_test")
   public void verify_Delete_BySelectMultipleFiles_GridView() throws Exception
   {
   	try
   	{
   		Log.testCaseInfo("TC_009_File Level Operations (Select Multiple Files and verify delete_GridView): Verify Select Multiple Files and Delete.");
   	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
     	String pWord = PropertyReader.getProperty("NewData_Password");
     
       	projectsLoginPage = new ProjectsLoginPage(driver).get();  
       	projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login Method
       	
       	String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
 		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
 		
 		String Foldername = "DELETE_ALL_FILE";	
 		folderPage.New_Folder_Create(Foldername);//Create a new Folder
 		
 		folderPage.Select_Folder(Foldername);//Select Folder
 		
 		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
   		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
   		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
   		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
 		Log.assertThat(folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount), 
 				"Upload using Without Index working successfully","Upload using Without Index is Failed", driver); 
 		folderPage.Select_Folder(Foldername);//Select Folder
   		Log.assertThat(folderPage.Validate_SelectAllFiles_GridView(), 
   			"Select All Files is working Successfully", "Select All Files is Not working", driver);
   		Log.assertThat(folderPage.Validate_Delete_BySelectAllFiles_GridView(), 
   				"Multiple file Delete is working Successfully", "Multiple file Delete is Not working", driver);
   	}
   	catch(Exception e)
   	{
   		AnalyzeLog.analyzeLog(driver);
       	e.getCause();
       	Log.exception(e, driver);
   	}
   	finally
   	{
   		Log.endTestCase();
   		driver.quit();
   	}
   }

    
    
    
}
