package com.arc.projects.testscripts;


import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.Log;

public class Recurring_UploadSuit {
	
	String webSite;
    ITestResult result;
    
    static WebDriver driver;
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    FolderPage folderPage;
    
    @Parameters("browser")
    @BeforeMethod(groups = "naresh_test")//Newly Added due to group execution Naresh
 public WebDriver beforeTest(String browser) {
        
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteStgURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        	File dest = new File("./drivers/win/chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            driver = new ChromeDriver( options );
            driver.get(PropertyReader.getProperty("SkysiteProdURL_USA"));
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
        	System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
            driver = new SafariDriver();
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver;
 }
    
    /** TC_001_Recurring Upload: Verify Recurring Upload.
     * Scripted By : NARESH  BABU kavuru
     * @throws Exception
     */
     @Test(priority = 0, enabled = true, description = "TC_001_Recurring Upload: Verify Recurring Upload.")
     public void verify_Recurring_Upload_Files() throws Exception
     {
     	try
     	{
     			Log.testCaseInfo("TC_001_Recurring Upload: Verify Recurring Upload.");
         		//Getting Static data from properties file
         		String uName = PropertyReader.getProperty("Recurring_UserName");
         		String pWord = PropertyReader.getProperty("Recurring_Password");
       		
         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
         		
         		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
         		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
         		        		
         		String Foldername = "Fold_"+Generate_Random_Number.generateRandomValue();
         		folderPage.New_Folder_Create(Foldername);//Create a new Folder
         		
         		folderPage.Select_Folder_RecurringUpload(Foldername);//Select a folder - newly created
         			
         		File Path_FMProperties = new File(PropertyReader.getProperty("Recurring_Larg_SingleFile"));
         		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
         		String CountOfFilesInFolder = PropertyReader.getProperty("LargSingle_FileCount");
         		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
         		
         		Log.assertThat(folderPage.Recurring_UploadFiles_WithoutIndex(FolderPath,FileCount), "Upload Success", "Upload Failed", driver);
         		Log.assertThat(folderPage.Select_Folder_RecurringUpload(Foldername),"Folder selected","Folder not selected");//Select a folder - newly created

         		String File_Name = "26.pdf";
         		Log.assertThat(folderPage.Verify_Select_Expected_File_ValidateInViewer(File_Name), "File Selection is working Successfully","File Selection is Not working", driver);
         		
         		projectDashboardPage.Logout_Projects();
     
     	}
        catch(Exception e)
        {
        	AnalyzeLog.analyzeLog(driver);
        	e.getCause();
         	Log.exception(e, driver);
        }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     		
     }
     
    
    
  

}
