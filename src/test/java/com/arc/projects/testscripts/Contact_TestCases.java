package com.arc.projects.testscripts;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.ContactsPage;
import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class Contact_TestCases {
	 
	public static WebDriver driver;
	ProjectsLoginPage projectsLoginPage;
	ProjectDashboardPage projectDashboardPage;
	FolderPage folderPage;
	ContactsPage contactsPage;

	@Parameters("browser")
	@BeforeMethod(groups = "naresh_test")
	public WebDriver beforeTest(String browser) {

		if (browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			// System.setProperty("webdriver.gecko.driver",
			// dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}

		else if (browser.equalsIgnoreCase("chrome")) {
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory", "C:" + File.separator + "Users" + File.separator
					+ System.getProperty("user.name") + File.separator + "Downloads");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));

		}

		else if (browser.equalsIgnoreCase("safari")) {
			System.setProperty("webdriver.safari.noinstall", "false"); 
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}

	
/**TC_001(Verify Create A New Contact and Validate.): Verify Create A New Contact and Validate.
 * Scripted By: NARESH BABU KAVURU
 * @throws Exception
 */
@Test(priority = 0, enabled = true, description = "TC_001(Verify Create A New Contact and Validate.): Verify Create A New Contact and Validate.")
	public void Create_NewContact_AndValidate() throws Exception
    {
     	try
     	{
     		Log.testCaseInfo("TC_001(Verify Create A New Contact and Validate.): Verify Create A New Contact and Validate.");
     	//Getting Static data from properties file naresh1
     		String uName = PropertyReader.getProperty("Username");
     		String pWord = PropertyReader.getProperty("Password");
   		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		contactsPage = projectDashboardPage.Validate_SelectContactsBut();//Click on Contacts
     		String Random_Contct_Name = "Expected_"+Generate_Random_Number.generateRandomValue();
     		
     		Log.assertThat(contactsPage.Add_New_Contact(Random_Contct_Name), "Create Contact and Validate is working Successfully", 
     				"Create Contact and Validate is NOT working", driver);
     	}
     	catch(Exception e)
     	{
     		AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
     	}
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }

/**TC_002(Verify Edit contact details and validate.): Verify Edit contact details and validate.
 * Scripted By: NARESH BABU KAVURU
 * @throws Exception
 */
@Test(priority = 1, enabled = true, description = "TC_002(Verify Edit contact details and validate.): Verify Edit contact details and validate.")
	public void EditContact_AndValidate() throws Exception
    {
     	try
     	{
     		Log.testCaseInfo("TC_002(Verify Edit contact details and validate.): Verify Edit contact details and validate.");
     	//Getting Static data from properties file naresh1
     		String uName = PropertyReader.getProperty("Username");
     		String pWord = PropertyReader.getProperty("Password");
   		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		contactsPage = projectDashboardPage.Validate_SelectContactsBut();//Click on Contacts
     		String Exp_ContName = "Expected_";
     		String New_ContactName = "New_"+Generate_Random_Number.generateRandomValue();
     		
     		Log.assertThat(contactsPage.Edit_Contact_AndValidate(Exp_ContName, New_ContactName), "Edit Contact and Validate is working Successfully", 
     				"Edit Contact and Validate is NOT working", driver);
     	}
     	catch(Exception e)
     	{
     		AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
     	}
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }

/**TC_003(Verify Delete a contact and validate.): Verify Delete a contact and validate.
 * Scripted By: NARESH BABU KAVURU
 * @throws Exception
 */
@Test(priority = 2, enabled = true, description = "TC_003(Verify Delete a contact and validate.): Verify Delete a contact and validate.")
	public void DeleteContact_AndValidate() throws Exception
    {
     	try
     	{
     		Log.testCaseInfo("TC_003(Verify Delete a contact and validate.): Verify Delete a contact and validate.");
     	//Getting Static data from properties file naresh1
     		String uName = PropertyReader.getProperty("Username");
     		String pWord = PropertyReader.getProperty("Password");
   		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		contactsPage = projectDashboardPage.Validate_SelectContactsBut();//Click on Contacts
     		String New_ContactName = "New_"+Generate_Random_Number.generateRandomValue();
     		
     		Log.assertThat(contactsPage.Add_New_Contact(New_ContactName), "Create Contact and Validate is working Successfully", 
     				"Create Contact and Validate is NOT working", driver);
     		
     		Log.assertThat(contactsPage.Delete_Contact_AndValidate(New_ContactName), "Delete Contact and Validate is working Successfully", 
     				"Delete Contact and Validate is NOT working", driver);
     	}
     	catch(Exception e)
     	{
     		AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
     	}
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }

/**TC_004(Verify create a group by adding contact.): Verify create a group by adding contact.
 * Scripted By: NARESH BABU KAVURU
 * @throws Exception
 */
@Test(priority = 3, enabled = true, description = "TC_004(Verify create a group by adding contact.): Verify create a group by adding contact.",groups = "naresh_test")
	public void CreateGroup_ByAddingContact_AndValidate() throws Exception
    {
     	try
     	{
     		Log.testCaseInfo("TC_004(Verify create a group by adding contact.): Verify create a group by adding contact.");
     	//Getting Static data from properties file naresh1
     		String uName = PropertyReader.getProperty("Username");
     		String pWord = PropertyReader.getProperty("Password");
   		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		contactsPage = projectDashboardPage.Validate_SelectContactsBut();//Click on Contacts
     		String Group_Name = "AGroup_"+Generate_Random_Number.generateRandomValue();
     		String New_ContactName = "New_"+Generate_Random_Number.generateRandomValue();
     		
     		Log.assertThat(contactsPage.Add_NewGroup_AndValidate(Group_Name), "Add a Group and Validate is working Successfully", 
     				"Add a Group and Validate is NOT working", driver);
     		
     		Log.assertThat(contactsPage.Add_New_Contact(New_ContactName), "Create Contact and Validate is working Successfully", 
     				"Create Contact and Validate is NOT working", driver);
     	}
     	catch(Exception e)
     	{
     		AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
     	}
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }

/**TC_005(Verify Edit a group and Delete.): Verify Edit a group and Delete.
 * Scripted By: NARESH BABU KAVURU
 * @throws Exception
 */
@Test(priority = 4, enabled = true, description = "TC_005(Verify Edit a group and Delete.): Verify Edit a group and Delete.",groups = "naresh_test")
	public void EditGroup_DeleteGroup_AndValidate() throws Exception
    {
     	try
     	{
     		Log.testCaseInfo("TC_005(Verify Edit a group and Delete.): Verify Edit a group and Delete.");
     	//Getting Static data from properties file naresh1
     		String uName = PropertyReader.getProperty("Username");
     		String pWord = PropertyReader.getProperty("Password");
   		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		contactsPage = projectDashboardPage.Validate_SelectContactsBut();//Click on Contacts
     		String Group_Name = "AGroup_";
     		String Edit_GroupName = "AnEdit_"+Generate_Random_Number.generateRandomValue();
     		
     		Log.assertThat(contactsPage.Edit_Group_AndValidate(Group_Name, Edit_GroupName), "Edit Group and Validate is working Successfully", 
     				"Edit Group and Validate is NOT working", driver);
     		
     		Log.assertThat(contactsPage.Delete_Group_AndValidate(Edit_GroupName), "Edit Group and Validate is working Successfully", 
     				"Edit Group and Validate is NOT working", driver);
     	}
     	catch(Exception e)
     	{
     		AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
     	}
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }


}
