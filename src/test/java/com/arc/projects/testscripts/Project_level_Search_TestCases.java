package com.arc.projects.testscripts;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectAndFolder_Level_Search;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ViewerScreenPage;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)
public class Project_level_Search_TestCases {
	 

		public static WebDriver driver;	    
		ProjectsLoginPage projectsLoginPage;
	    ProjectDashboardPage projectDashboardPage;   
	    ViewerScreenPage viewerScreenPage; 
	    FolderPage folderPage;
	    ProjectAndFolder_Level_Search projectAndFolder_Level_Search;
	    
	    @Parameters("browser")
	    @BeforeMethod
	    public WebDriver beforeTest(String browser) {
	
	    	
	        if(browser.equalsIgnoreCase("firefox")) 
	        {
	               File dest = new File("./drivers/win/geckodriver.exe");
	               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
	               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
	               driver = new FirefoxDriver();
	               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	        }
	        
	        else if (browser.equalsIgnoreCase("chrome")) 
	        { 
	        File dest = new File("./drivers/win/chromedriver.exe");
	        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
	        Map<String, Object> prefs = new HashMap<String, Object>();
	        prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
	        ChromeOptions options = new ChromeOptions();
	         options.addArguments("--start-maximized");
	        options.setExperimentalOption("prefs", prefs);
	        driver = new ChromeDriver( options );
	        driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	 
	        } 
	        
	        else if (browser.equalsIgnoreCase("safari"))
	        {
	               System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
	               driver = new SafariDriver();
	               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
	        }
	   return driver;
	 }
	    
	    
	/*
	 * TC_034(Local Search an RFI with it's RFI number.) 1) Verify all the
	 * content of the RFI including status opening the RFI in the search result
	 * window. 2) Try to download associated attachments and verify. 3) Change
	 * the status of the RFI from the search result window and verify in cloud.
	 * 
	 */

	@Test(priority = 0, enabled = true, description = "Local Search an RFI with it's RFI number.")
	public void VerifyRFI_LocalSearch_With_RFINumber() throws Throwable {

		try {
			Log.testCaseInfo("TC_034(RFI Project Level Validation):Local Search an RFI with it's RFI number.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create Project
			// Randomly===================================
			String Project_Name = "Prj_" + Generate_Random_Number.generateRandomValue();
			String RFINumber = Generate_Random_Number.generateRandomValue1();
			Log.message("Subject verified Sucessfully :- " + RFINumber);
			folderPage = projectDashboardPage.createProjectwithSpecificRFINumber(Project_Name, RFINumber);
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFICREATION_New();
			projectAndFolder_Level_Search.RFINumberWithLocalSearch(RFINumber);
			String usernamedir = System.getProperty("user.name");
			String Sys_Download_Path = "C:\\" + "Users\\" + usernamedir + "\\Downloads";
			folderPage.DownloadFiledownload(Sys_Download_Path);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.close();
		}

	}
		   
		    
		    
		       /*TC_035(Local search an RFI by it's subject.) 
			   1) Verify all the content of the RFI including status opening the RFI in the search result window.
		       2) Try to download associated attachments and verify.
		       3) Change the status of the RFI from the search result window and verify in cloud.
			    *
			    *
			    *
			    */
			    			    
			    @Test(priority = 1, enabled = true, description = "Local search an RFI by it's subject.")
		        public void Verify_RFI_LocalSearch_With_Subject() throws Throwable
		        {
		       	 
		        	try
		        	{
		        		Log.testCaseInfo("TC_035(RFI Project Level Validation):Local search an RFI by it's subject.");
		        		String uName = PropertyReader.getProperty("Username1");
		       		    String pWord = PropertyReader.getProperty("Password1");
		       		    //==============Login with valid UserID/PassWord===========================
		         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
		         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);	         		
		         		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
		         		String Subject = "Subject123"+Generate_Random_Number.generateRandomValue1();
		         		Log.message("Subject verified Sucessfully "+Subject);
		         		folderPage=projectDashboardPage.CreateProject(Project_Name);		         		         		
		         		projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();		         		
		         		projectAndFolder_Level_Search.ProjectManagement();		         		
		         		projectAndFolder_Level_Search.RFICreation_withSubject(Subject);		         		
		         		projectAndFolder_Level_Search.LocalSearchwithAnything(Subject);
		         		String usernamedir=System.getProperty("user.name");
		          		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";	          	            		
		         		folderPage.DownloadFiledownload(Sys_Download_Path);
		         			        	       			
		        		
		        	}
		            catch(Exception e)
		            {
		            	e.getCause();
		            	Log.exception(e, driver);
		            }
		        	finally
		        	{
		        		Log.endTestCase();
		        		driver.close();
		        	}
		     	
		     }       
			    
	          
	    
/*TC_036(Local search an RFI by it's custom attribute..) 
 1) Verify all the content of the RFI including status opening the RFI in the search result window.
 2) Try to download associated attachments and verify.
 3) Change the status of the RFI from the search result window and verify in cloud.
 4) Verify the order of custom attribute from search result window.
*/
				    
				    
@Test(priority =2, enabled = true, description = "Local search an RFI by custom attribute.")
public void Verify_RFI_LocalSearch_With_customattribute() throws Throwable
			        
    {
			       	 
			        	
	try
			        	
	{
		
		Log.testCaseInfo("TC_036(RFI Project Level Validation):Local search an RFI by it's custom attribute..");			        		
		String uName = PropertyReader.getProperty("Username1");			       		    
		String pWord = PropertyReader.getProperty("Password1");			       		    
		//==============Login with valid UserID/PassWord===========================			         		
		projectsLoginPage = new ProjectsLoginPage(driver).get(); 			         		
		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);			         		
		//=========Create Project Randomly===================================			         		
		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();			         		
		String CustumAttribute = "NewScenario"+Generate_Random_Number.generateRandomValue1();			         		
		Log.message("Custom Attribute Display As: "+CustumAttribute);			         		
		folderPage=projectDashboardPage.createProjectwithCustomAtribute(Project_Name, CustumAttribute);			         		         		
		projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();			         		
		projectAndFolder_Level_Search.ProjectManagement();			         		
		projectAndFolder_Level_Search.RFI_CreationwithcustomAttribute(CustumAttribute);
		projectAndFolder_Level_Search.ProjectManagement();
		projectAndFolder_Level_Search.LocalSearchwithAnything(CustumAttribute);	
		String usernamedir=System.getProperty("user.name");
	    String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";			         		         	            		
		folderPage.DownloadFiledownload(Sys_Download_Path);
			         		       		
		}
		catch(Exception e)
       {
		e.getCause();
		Log.exception(e, driver);
	   }
		finally
	   {
		Log.endTestCase();
	    driver.close();
	   }
	   }     
	    
	    
	    /*
	    1) TC_037 .Verify all the content of the RFI including status opening the RFI in the search result window.
        2) Try to download associated attachments and verify.
        3) Change the status of the RFI from the search result window and verify in cloud.
        4) Verify this with all kind of status.
        
	    */	    
	    
	    @Test(priority = 3, enabled = true, description = "Local Search the RFI with the status.")
      public void Verify_RFI_LocalSearch_AllStatus() throws Throwable
      {
     	 
      	try
      	{
      		Log.testCaseInfo("TC_037(RFI Project Level Validation):Local Search RFI by it's Status");
      		String uName = PropertyReader.getProperty("Username1");
     		String pWord = PropertyReader.getProperty("Password1");
     		String uName1 = PropertyReader.getProperty("Username2");
		    String pWord1 = PropertyReader.getProperty("Password2");
     		//String SystemDownloadPath = PropertyReader.getProperty("SystemDownloadPath");
     		String RFINumber = Generate_Random_Number.generateRandomValue1();
     		String Subject = "Subject123"+Generate_Random_Number.generateRandomValue1();
     		//==============Login with valid UserID/PassWord===========================
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
       		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);    		
       		
       		//=========Create Project Randomly===================================
       	    projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
       		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
       		folderPage=projectDashboardPage.CreateProject(Project_Name);
            projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();        		
       		projectAndFolder_Level_Search.ProjectManagement();	       		
            projectAndFolder_Level_Search.RFICREATION_New() ;       	    
       	    projectAndFolder_Level_Search.ProjectManagement();
       	    String Open = PropertyReader.getProperty("Status");
       	    projectAndFolder_Level_Search.AdvanceStatus(Open);     		       		
       	   	String usernamedir=System.getProperty("user.name");
      		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";	          	            		
     		folderPage.DownloadFiledownload(Sys_Download_Path);       		
            projectAndFolder_Level_Search.MainStatus();//open/close/reopen 
            
            
            
       	     //projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
           // projectAndFolder_Level_Search.Logout();
           // Log.assertThat( projectAndFolder_Level_Search.Logout(), "Logout working Successfully","Logout not working Successfully", driver);
			 
           //==============Employee Level================================
            /*String Project_Name1 ="Prj_22842";
       		projectsLoginPage = new ProjectsLoginPage(driver).get();  
       		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName1,pWord1);    		
       		projectDashboardPage.Validate_SelectAProject(Project_Name1) ;
       		projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
       		projectAndFolder_Level_Search.ProjectManagement();
            projectAndFolder_Level_Search.EmployeeStatus();//Responded
            */
           // projectAndFolder_Level_Search.RFICreation_withSubject(Subject);
     		
     		//projectAndFolder_Level_Search.LocalSearchwithAnything(Subject);
            
            //folderPage.ValidateDownload_File_download(SystemDownloadPath);
            
            //String Status = PropertyReader.getProperty("Status");
       		//projectAndFolder_Level_Search.FilterSearchwithStatus(Status);
       		
       		
       		//projectAndFolder_Level_Search.DownloadPDF();
       		
       		 			
      		
      	}
          catch(Exception e)
          {
          	e.getCause();
          	Log.exception(e, driver);
          }
      	finally
      	{
      		Log.endTestCase();
      		driver.close();
      	}
   	
   }       
	    
	  
		    
	    /*TC_031(Global Search an RFI with it's RFI number) 
	    1) Verify all the content of the RFI including status opening the RFI in the search result window.
	    2) Try to download associated attachments and verify.
	    3) Change the status of the RFI from the search result window and verify in cloud.
	    *
	    *
	    *
	    */
	    
	    
	   @Test(priority =4, enabled = true, description = "Global Search an RFI with it's RFI number")
        public void Verify_RFI_GlobalSearch_with_RFINumber() throws Throwable
        {
       	 
        	try
        	{
        		Log.testCaseInfo("TC_031(RFI Project Level Validation):Global Search an RFI with it's RFI number");
        		String uName = PropertyReader.getProperty("Username1");
       		    String pWord = PropertyReader.getProperty("Password1");
       		    //==============Login with valid UserID/PassWord===========================
         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord); 
         		
         		//projectDashboardPage.projectselection();
         		
         		//=========Create Project Randomly===================================
         		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
         		String rFINumber = Generate_Random_Number.generateRandomValue1();
         		Log.message("Subject verified Sucessfully "+rFINumber);
       		    folderPage=projectDashboardPage.createProjectwithSpecificRFINumber(Project_Name, rFINumber);         		         		
         		projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();          		
         		projectAndFolder_Level_Search.ProjectManagement();         		
         		projectAndFolder_Level_Search.RFICREATION_New() ;         		
         		projectAndFolder_Level_Search.SearchRFI_RFINumber(rFINumber);         		
         		
         		
         		
         		//projectAndFolder_Level_Search.Logout() ;         	
        	       			
        		
        	}
            catch(Exception e)
            {
            	e.getCause();
            	Log.exception(e, driver);
            }
        	finally
        	{
        		Log.endTestCase();
        		driver.close();
        	}
     	
     }
	    

	    
	    
	  /* TC_032(Global Search an RFI with it's subject ) 
	    1) Verify all the content of the RFI including status opening the RFI in the search result window.
        2) Try to download associated attachments and verify.
        3) Change the status of the RFI from the search result window and verify in cloud.
	    *
	   */ 	    
	    
	    @Test(priority = 5, enabled = true, description = "Global Search an RFI By Its Subject")
        public void Verify_RFI_GlobalSearch_By_SubJect() throws Throwable
        {
       	 
        	try
        	{
        		Log.testCaseInfo("TC_32(RFI Project Level Validation):Global Search an RFI By Its Subject");
        		String uName = PropertyReader.getProperty("Username1");
       		    String pWord = PropertyReader.getProperty("Password1");
       		    //==============Login with valid UserID/PassWord===========================
         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);    		
         		
         		//=========Create Project Randomly===================================
         		String Project_Name = PropertyReader.getProperty("StaticProject");
    			projectDashboardPage.ValidateSelectProject(Project_Name);
         		
                projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
         		
         		projectAndFolder_Level_Search.ProjectManagement();
         		
         		String Sub_Name = "subject_"+Generate_Random_Number.generateRandomValue();
         		
         		projectAndFolder_Level_Search.RFICreation_withSubject(Sub_Name);
         		
         		projectAndFolder_Level_Search.SearchRFISubject_Global(Sub_Name);
         		
         				
        		
        	}
            catch(Exception e)
            {
            	e.getCause();
            	Log.exception(e, driver);
            }
        	finally
        	{
        		Log.endTestCase();
        		driver.close();
        	}
     	
     }
	    

	   /*  TC_033(Global Search RFI by it's custom attribute) 
	    1) Verify all the content of the RFI including status opening the RFI in the search result window.
        2) Try to download associated attachments and verify.
        3) Change the status of the RFI from the search result window and verify in cloud.
	    
	    */
	    	    
	    
	    @Test(priority = 6, enabled = true, description = "Global Search RFI by it's custom attribute.")
        public void Verify_RFI_GlobalSearch_customattribute() throws Throwable
        {
       	 
        	try
        	{
        		Log.testCaseInfo("TC_033(RFI Project Level Validation):Global Search RFI by it's custom attribute");
        		String uName = PropertyReader.getProperty("Username1");
       		    String pWord = PropertyReader.getProperty("Password1");
       		    //==============Login with valid UserID/PassWord===========================
         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);    		
         		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
         		String CustumAttribute = "NewScenario"+Generate_Random_Number.generateRandomValue1();
         		projectDashboardPage.createProjectwithCustomAtribute(Project_Name, CustumAttribute);
         		
                projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get(); 
         		
         		projectAndFolder_Level_Search.ProjectManagement();
         		
         		projectAndFolder_Level_Search.RFI_CreationwithcustomAttribute(CustumAttribute);
         		
         		projectAndFolder_Level_Search.SearchRFI_Global(CustumAttribute);
        	       			
        		
        	}
            catch(Exception e)
            {
            	e.getCause();
            	Log.exception(e, driver);
            }
        	finally
        	{
        		Log.endTestCase();
        		driver.close();
        	}
     	
     }       
	    
	    

	   
	        
	      
		          
		   
			   
			    
			     
	   //=================================== Document Level Search funactionality========================
       
			   /* 
			        TC_015) (Global Search an RFI with it's RFI number.)
	                   1) Verify all the content of the RFI including status opening the RFI in the search result window.
                       2) Try to download associated attachments and verify.
                       3) Change the status of the RFI from the search result window and verify in cloud.
				    *
				    *
				    
				    
				    
				    @Test(priority = 6, enabled = true, description = "Global Search an RFI with it's RFI number.")
			        public void Verify_RFI_GlobalSearch_With_RFINumber() throws Throwable
			        {
			       	 
			        	try
			        	{
			        		Log.testCaseInfo("TC_15(RFI Document Level Validation):Global Search an RFI with it's RFI number.");
			        		String uName = PropertyReader.getProperty("Username1");
			       		    String pWord = PropertyReader.getProperty("Password1");
			       		    //==============Login with valid UserID/PassWord===========================
			         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
			         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);    		
			         		
			         		//=========Create Project Randomly===================================
			         		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
			         		folderPage=projectDashboardPage.createProject(Project_Name);
			         		
			         		
			         	
			        	       			
			        		
			        	}
			            catch(Exception e)
			            {
			            	e.getCause();
			            	Log.exception(e, driver);
			            }
			        	finally
			        	{
			        		Log.endTestCase();
			        		driver.close();
			        	}
			     	
			     }    
				    
				    
				     TC_016) (Global search an RFI by it's subject.)
	                1) Verify all the content of the RFI including status opening the RFI in the search result window.
                    2) Try to download associated attachments and verify.
                    3) Change the status of the RFI from the search result window and verify in cloud.
				    *
				    *
				    				    
				    
				    @Test(priority = 7, enabled = true, description = "Global search an RFI by it's subject.")
			        public void Verify_RFI_GlobalSearch_With_Subject() throws Throwable
			        {
			       	 
			        	try
			        	{
			        		Log.testCaseInfo("TC_16(RFI Document Level Validation):Global search an RFI by it's subject.");
			        		String uName = PropertyReader.getProperty("Username1");
			       		    String pWord = PropertyReader.getProperty("Password1");
			       		    //==============Login with valid UserID/PassWord===========================
			         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
			         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);    		
			         		
			         		//=========Create Project Randomly===================================
			         		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
			         		folderPage=projectDashboardPage.createProject(Project_Name);
			         		
			         		
			         	
			        	       			
			        		
			        	}
			            catch(Exception e)
			            {
			            	e.getCause();
			            	Log.exception(e, driver);
			            }
			        	finally
			        	{
			        		Log.endTestCase();
			        		driver.close();
			        	}
			     	
			     }   
				    
				    
				     TC_017) (Global Search an RFI by it's custom attribute.)
	               1) Verify all the content of the RFI including status opening the RFI in the search result window.
                   2) Try to download associated attachments and verify.
                   3) Change the status of the RFI from the search result window and verify in cloud.
				    *
				    *
				    
				    
				    
				    @Test(priority = 8, enabled = true, description = "Global Search an RFI by it's custom attribute.")
			        public void Verify_RFI_GlobalSearch_With_CustomAttribute() throws Throwable
			        {
			       	 
			        	try
			        	{
			        		Log.testCaseInfo("TC_17(RFI Document Level Validation):Global Search an RFI by it's custom attribute.");
			        		String uName = PropertyReader.getProperty("Username1");
			       		    String pWord = PropertyReader.getProperty("Password1");
			       		    //==============Login with valid UserID/PassWord===========================
			         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
			         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);    		
			         		
			         		//=========Create Project Randomly===================================
			         		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
			         		folderPage=projectDashboardPage.createProject(Project_Name);
			         		
			         		
			         	
			        	       			
			        		
			        	}
			            catch(Exception e)
			            {
			            	e.getCause();
			            	Log.exception(e, driver);
			            }
			        	finally
			        	{
			        		Log.endTestCase();
			        		driver.close();
			        	}
			     	
			     }       
				 
				    
				    
				   ( TC_018) (Local Search an RFI with it's RFI number.)
	               1) Verify all the content of the RFI including status opening the RFI in the search result window.
                   2) Try to download associated attachments and verify.
                   3) Change the status of the RFI from the search result window and verify in cloud.
				    *
				    *
				    
				    
				    
				    @Test(priority = 9, enabled = true, description = "Local Search an RFI with it's RFI number.")
			        public void Verify_LocalSearch_With_RFINumber1() throws Throwable
			        {
			       	 
			        	try
			        	{
			        		Log.testCaseInfo("TC_18(RFI Document Level Validation):Local Search an RFI with it's RFI number.");
			        		String uName = PropertyReader.getProperty("Username1");
			       		    String pWord = PropertyReader.getProperty("Password1");
			       		    //==============Login with valid UserID/PassWord===========================
			         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
			         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);    		
			         		
			         		//=========Create Project Randomly===================================
			         		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
			         		folderPage=projectDashboardPage.createProject(Project_Name);
			         		
			         		
			         	
			        	       			
			        		
			        	}
			            catch(Exception e)
			            {
			            	e.getCause();
			            	Log.exception(e, driver);
			            }
			        	finally
			        	{
			        		Log.endTestCase();
			        		driver.close();
			        	}
			     	
			     }       
				    
				    
				    ( TC_019) (Local search an RFI by it's subject.)
		               1) Verify all the content of the RFI including status opening the RFI in the search result window.
	                   2) Try to download associated attachments and verify.
	                   3) Change the status of the RFI from the search result window and verify in cloud.
					    *
					    *
					    
					    
					    
					    @Test(priority = 10, enabled = true, description = "Local search an RFI by it's subject.")
				        public void Verify_LocalSearch_With_Subject() throws Throwable
				        {
				       	 
				        	try
				        	{
				        		Log.testCaseInfo("TC_19(RFI Document Level Validation):Local search an RFI by it's subject.");
				        		String uName = PropertyReader.getProperty("Username1");
				       		    String pWord = PropertyReader.getProperty("Password1");
				       		    //==============Login with valid UserID/PassWord===========================
				         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
				         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);    		
				         		
				         		//=========Create Project Randomly===================================
				         		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
				         		folderPage=projectDashboardPage.createProject(Project_Name);
				         		
				         		
				         	
				        	       			
				        		
				        	}
				            catch(Exception e)
				            {
				            	e.getCause();
				            	Log.exception(e, driver);
				            }
				        	finally
				        	{
				        		Log.endTestCase();
				        		driver.close();
				        	}
				     	
				     }       
					    
					       
				    
				    
				       ( TC_020) (Local Search an RFI by it's custom attribute.)
		               1) Verify all the content of the RFI including status opening the RFI in the search result window.
                       2) Try to download associated attachments and verify.
                       3) Change the status of the RFI from the search result window and verify in cloud.
                       4) Verify the order of custom attribute from search result window.
					    *
					    *
					    
					    
					    
					    @Test(priority = 11, enabled = true, description = "Local Search an RFI by it's custom attribute.")
				        public void Verify_LocalSearch_With_RFINumber() throws Throwable
				        {
				       	 
				        	try
				        	{
				        		Log.testCaseInfo("TC_20(RFI Document Level Validation):Local Search an RFI by it's custom attribute.");
				        		String uName = PropertyReader.getProperty("Username1");
				       		    String pWord = PropertyReader.getProperty("Password1");
				       		    //==============Login with valid UserID/PassWord===========================
				         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
				         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);    		
				         		
				         		//=========Create Project Randomly===================================
				         		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
				         		folderPage=projectDashboardPage.createProject(Project_Name);
				         		
				         		
				         	
				         	
				        	       			
				        		
				        	}
				            catch(Exception e)
				            {
				            	e.getCause();
				            	Log.exception(e, driver);
				            }
				        	finally
				        	{
				        		Log.endTestCase();
				        		driver.close();
				        	}
				     	
				     }       
					    
					    
					    
					    
					    		    
				    
				    
					      ( TC_021) (Local Search the RFI with the status.)
			               1) Verify all the content of the RFI including status opening the RFI in the search result window.
                           2) Try to download associated attachments and verify.
                           3) Change the status of the RFI from the search result window and verify in cloud.
                           4) Verify this with all kind of status.
						    *
						    *
						    
						    
						    
						    @Test(priority = 12, enabled = true, description = "Local Search the RFI with the status.")
					        public void Verify_LocalSearch_RFI_With_Status() throws Throwable
					        {
					       	 
					        	try
					        	{
					        		Log.testCaseInfo("TC_21(RFI Document Level Validation):Local Search the RFI with the status.");
					        		String uName = PropertyReader.getProperty("Username1");
					       		    String pWord = PropertyReader.getProperty("Password1");
					       		    //==============Login with valid UserID/PassWord===========================
					         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
					         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);    		
					         		
					         		//=========Create Project Randomly===================================
					         		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
					         		folderPage=projectDashboardPage.createProject(Project_Name);
					         		
					         		
					         	
					        	       			
					        		
					        	}
					            catch(Exception e)
					            {
					            	e.getCause();
					            	Log.exception(e, driver);
					            }
					        	finally
					        	{
					        		Log.endTestCase();
					        		driver.close();
					        	}
					     	
					     }       
						    	    
				    
				    
				    
				    
				    
				    */
      
}
