package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectAndFolder_Level_Search;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ViewerScreenPage;
import com.arc.projects.pages.Viewer_ScreenRFI;
import com.arc.projects.pages.Viewerlevel_RFI_Script;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class ViewerPageRFITestcase1 {
	public static WebDriver driver;
	ProjectsLoginPage projectsLoginPage;
	ProjectDashboardPage projectDashboardPage;
	Viewerlevel_RFI_Script viewerlevel_RFI_Script;
	FolderPage folderPage;
	ProjectAndFolder_Level_Search projectAndFolder_Level_Search;

	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) {

		if (browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		} else if (browser.equalsIgnoreCase("chrome")) {
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory", "C:" + File.separator + "Users" + File.separator
					+ System.getProperty("user.name") + File.separator + "Downloads");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		} else if (browser.equalsIgnoreCase("safari")) {
			System.setProperty("webdriver.safari.noinstall", "");
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}

	/**
	 * TC_001 : Verify RFI creation with To, CC user, Attachment - Upload File,
	 * Project File and validating Answer and Comment received from 'To' user by
	 * Owner.
	 */

	@Test(priority = 0, enabled = true , description = "Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Answer and Comment received from 'To' user by Owner.")
	public void RFIcreationAndValidate_AnsAndCommentByOwner() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_001 (RFI ToolsBaar validation):  Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Answer and Comment received from 'To' user by Owner.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String uName1 = PropertyReader.getProperty("Username2");
			String pWord1 = PropertyReader.getProperty("Password2");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);

			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);

			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_ownerLevel();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully","Logout not working Successfully", driver);

			// Employee level login
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
			// Select Project
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			folderPage.Select_Folder(Foldername);
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFI_EMPLOYEEATTACHAndMessage();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

			// Owner level Validation
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			folderPage.Select_Folder(Foldername);
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFIOwnerLevelValidation();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	/**
	 * TC_003 : Verify RFI creation with To, CC user, Attachment - Upload File,
	 * Project File and validating Close functionlity for 'To' user.
	 */

	@Test(priority = 1, enabled = true, description = "Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Close functionlity for 'To' user.")
	public void RFIValidation_withcloseStatus() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_003 (RFI ToolsBaar validation):  Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Close functionlity for 'To' user.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFIOwnerLevel_Close_Statusvalidation();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_004 : Verify RFI creation with To, CC user, Attachment - Upload File,
	 * Project File and validating Re-open functionlity for 'To' user..
	 */

	@Test(priority = 2, enabled = true, description = "Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Re-open functionlity for 'To' user.")
	public void REOPENRFIByOwner() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_004 (RFI ToolsBaar validation):  Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Re-open functionlity for 'To' user.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String uName1 = PropertyReader.getProperty("Username2");
			String pWord1 = PropertyReader.getProperty("Password2");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFIOwnerLevel_Reopen_Statusvalidation();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

			// Employee level- for responded
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
			projectDashboardPage.ValidateSelectProject(Project_Name);
			folderPage.Select_Folder(Foldername);
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFI_EMPLOYEEResponded();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_005 : Verify RFI creation with To, CC user, Attachment - Upload File,
	 * Project File and validating Reject functionlity for 'To' user..
	 */

	@Test(priority = 3, enabled = true, description = "Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Reject functionlity for 'To' user.")
	public void RejectRFIByOwner() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_005 (RFI ToolsBaar validation):  Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating Reject functionlity for 'To' user.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFIOwnerLevel_Reject_Statusvalidation();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_006 : Verify RFI creation with To, CC user, Attachment - Upload File,
	 * Project File and validating VOID functionlity for 'To' user..
	 */

	@Test(priority = 4, enabled = true, description = "Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating void functionlity for 'To' user.")
	public void VoidRFIByOwner() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_006 (RFI ToolsBaar validation):  Verify RFI creation with To, CC user, Attachment -  Upload File, Project File and validating void functionlity for 'To' user.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);

			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);

			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_ownerLevel_void();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_007 : Verify RFI REASSIGNING with To, Attachment - Upload File,
	 * Project File and validating Answer and Comment received from 'To' user by
	 * Owner...
	 */

	@Test(priority = 5, enabled = true, description = "Verify RFI REASSIGNING with To, Attachment -  Upload File, Project File and validating Answer and Comment received from 'To' user by Owner..")
	public void ReassignRFIWithAttachment() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_007 (RFI ToolsBaar validation):  Verify RFI REASSIGNING with To, Attachment -  Upload File, Project File and validating Answer and Comment received from 'To' user by Owner.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String uName1 = PropertyReader.getProperty("Username2");
			String pWord1 = PropertyReader.getProperty("Password2");
			String parentHandle = driver.getWindowHandle();
			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_OwnerLevelAndReassign();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	/**
	 * TC_008 :Verify RFI is visible in RFI listing which is created through
	 * viewer level.
	 */

	@Test(priority = 6, enabled = true, description = "Verify RFI is visible in RFI listing which is created through viewer level.")
	public void CreateRFIIn_viewer_validateinDocument() throws Throwable {

		try {
			Log.testCaseInfo("TC_008 Verify RFI is visible in RFI listing which is created through viewer level.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_ownerLevel();
			viewerlevel_RFI_Script.ProjectManagement();
			viewerlevel_RFI_Script.validationInRFIList();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_011 : Verify RFI creartion with custom Attribute (5)
	 */

	@Test(priority = 7, enabled = true, description = "Verify RFI creartion with custom Attribute (5)")
	public void VerifyRFIwithCustomAttributeFive() throws Throwable {

		try {
			Log.testCaseInfo("TC_011 (RFI ToolsBaar validation): Verify RFI creartion with custom Attribute (5)");

			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String CustomAttribute5 = PropertyReader.getProperty("customAttribute5");
			String CustomAttribute1 = PropertyReader.getProperty("customAttribute1");
			String CustomAttribute2 = PropertyReader.getProperty("customAttribute2");
			String CustomAttribute3 = PropertyReader.getProperty("customAttribute3");
			String CustomAttribute4 = PropertyReader.getProperty("customAttribute4");
			String RandomNo = Generate_Random_Number.generateRandomValue();
			String Project_Name = "Proj_" + Generate_Random_Number.generateRandomValue();
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create Project
			// Randomly===================================
			folderPage = projectDashboardPage.createProjectwith_Five_CustomAtribute(Project_Name, CustomAttribute1,
					CustomAttribute2, CustomAttribute3, CustomAttribute4, CustomAttribute5, RandomNo);
			// ================File Upload without Index=====================
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			projectAndFolder_Level_Search.RFI_CreationWithMultipleAttribute();
			Log.assertThat(projectAndFolder_Level_Search.RFI_AttributeValidation(),
					"RFI Attribute Validation Completed Sucessfully",
					"RFI Attribute Validation  not Completed Sucessfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_012 : Verify sorting in RFI listing - Owner.
	 */

	@Test(priority = 8, enabled = true, description = "Verify sorting in RFI listing - Owner")
	public void RFIlistAscendingdecending_Owner() throws Throwable {

		try {
			Log.testCaseInfo("TC_012 (RFI ToolsBaar validation): Verify sorting in RFI listing - Owner");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// =========Create Project
			// Randomly===================================
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// ================File Upload without Index=====================
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			Log.assertThat(projectAndFolder_Level_Search.RFI_AscendingorderValidation(),
					"Ascending order Validation working Successfully",
					"Ascending order Validation not working Successfully", driver);
			Log.assertThat(projectAndFolder_Level_Search.RFI_DecendingorderValidation(),
					"Decending order Validation working Successfully",
					"Decending order Validation not working Successfully", driver);
			projectAndFolder_Level_Search.Logout();

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	/**
	 * TC_013 : Verify sorting in RFI listing - Employee.
	 */

	@Test(priority = 9, enabled = true, description = "Verify sorting in RFI listing - Employee")
	public void RFIlistAscendingdecending_Employee() throws Throwable {

		try {
			Log.testCaseInfo("TC_013 (RFI ToolsBaar validation): Verify sorting in RFI listing - Employee");
			String uName1 = PropertyReader.getProperty("Username2");
			String pWord1 = PropertyReader.getProperty("Password2");
			// ========= Employee Level validation=============================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// ================File Upload without Index=====================
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			Log.assertThat(projectAndFolder_Level_Search.RFI_AscendingorderValidation(),
					"Ascending order Validation working Successfully",
					"Ascending order Validation not working Successfully", driver);

			Log.assertThat(projectAndFolder_Level_Search.RFI_DecendingorderValidation(),
					"Decending order Validation working Successfully",
					"Decending order Validation not working Successfully", driver);
			projectAndFolder_Level_Search.Logout();

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_014 : Verify sorting in RFI listing - Guest User.
	 */

	@Test(priority = 10, enabled = true, description = "Verify sorting in RFI listing - Guest User")
	public void RFIlistAscendingdecending_GuestUser() throws Throwable {

		try {
			Log.testCaseInfo("TC_014 (RFI ToolsBaar validation): Verify sorting in RFI listing - Guest User");
			String uName2 = PropertyReader.getProperty("GuestUserName");
			String pWord2 = PropertyReader.getProperty("GuestUserPassword");
			// =======Guest User Level Validation=======================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			String Project_Name = "Specific_Testdata_Static";
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName2, pWord2);
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// ================File Upload without Index=====================
			projectAndFolder_Level_Search = new ProjectAndFolder_Level_Search(driver).get();
			projectAndFolder_Level_Search.ProjectManagement();
			Log.assertThat(projectAndFolder_Level_Search.RFI_AscendingorderValidation(),
					"Ascending order Validation working Successfully",
					"Ascending order Validation not working Successfully", driver);
			Log.assertThat(projectAndFolder_Level_Search.RFI_DecendingorderValidation(),
					"Decending order Validation working Successfully",
					"Decending order Validation not working Successfully", driver);
			projectAndFolder_Level_Search.Logout();

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_002 :Verify RFI creation with CC user, Attachment - Upload File,
	 * Project File and validating Comment received from 'CC' user by Owner. .
	 */

	@Test(priority = 11, enabled = true, description = "Verify RFI creation with  CC user, Attachment -  Upload File, Project File and validating Answer and Comment received from 'To' user by Owner.")
	public void RFI_CC_User_Validate_AndCommentByCCUser() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_002 (RFI ToolsBaar validation):  Verify RFI creation with  CC user, Attachment -  Upload File, Project File and validating Answer and Comment received from 'To' user by Owner.");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			String uName1 = PropertyReader.getProperty("Email_CCOption");
			String pWord1 = PropertyReader.getProperty("Password2");

			// Login with valid UserID/PassWord

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_ownerLevel();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

			// Employee level
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName1, pWord1);

			// Select Project
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder

			folderPage.Select_Folder(Foldername);
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFI_CC_Employee();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_009 : Navigate to Document level RFI through RFI listing and create
	 * new RFI
	 */

	@Test(priority = 12, enabled = true, description = "Navigate to Document level RFI through RFI listing and create new RFI")
	public void NavigatefromRFIListingtocreateRFI() throws Throwable {
		try {
			Log.testCaseInfo(
					"TC_009 (RFI Related Testcases):Navigate to Document level RFI through RFI listing and create new RFI");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");
			// ==============Login with valid
			// UserID/PassWord===========================
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);

			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_ownerLevel();
			// viewerlevel_RFI_Script.ProjectManagement();
			// viewerlevel_RFI_Script.FromRFIlist_RFICreation();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_015 :Verify RFI assigne level - 2 [Owner assigning Employee and then
	 * Employee assiging another Employee]
	 */

	@Test(priority = 13, enabled = true, description = "Verify RFI assigne level - 2 [Owner assigning Employee and then Employee assiging another Employee]")
	public void OwnerAssignTwoEmployee() throws Throwable {

		try {
			Log.testCaseInfo(
					"TC_015:Verify RFI assigne level - 2 [Owner assigning Employee and then Employee assiging another Employee]");
			String uName = PropertyReader.getProperty("Username1");
			String pWord = PropertyReader.getProperty("Password1");

			// Login with valid UserID/PassWord
			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			// Select Project
			String Project_Name = "Specific_Testdata_Static";
			folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
			// select folder
			String Foldername = "RFI_SinglePage";
			folderPage.Select_Folder(Foldername);
			// Owner Level
			viewerlevel_RFI_Script = new Viewerlevel_RFI_Script(driver).get();
			viewerlevel_RFI_Script.Image1();
			viewerlevel_RFI_Script.RFICreation_ownerLevel();
			// viewerlevel_RFI_Script.Image1();
			// viewerlevel_RFI_Script.RFI_Reassign_seconduser();
			Log.assertThat(viewerlevel_RFI_Script.Logout(), "Logout working Successfully",
					"Logout not working Successfully", driver);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
			CommonMethod.analyzeLog(driver);

		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

}