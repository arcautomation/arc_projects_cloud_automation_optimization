package com.arc.projects.testscripts;

import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectGalleryPage;
import com.arc.projects.pages.ProjectLatestDocumentsPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ProjectsRegistrationPage;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.Log;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class Project_Sorting_Testcases 
{

	public static WebDriver driver;
	
	ProjectsLoginPage projectsLoginPage;
	ProjectDashboardPage projectDashboardPage; 
	ProjectsRegistrationPage projectRegistrationPage;
	FolderPage folderPage;
	ProjectGalleryPage projectGalleryPage;
	ProjectLatestDocumentsPage projectLatestDocumentsPage;
	
	
	@Parameters("browser")
	@BeforeMethod
    public WebDriver beforeTest(String browser) {
        
    	if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        File dest = new File("./drivers/win/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.setExperimentalOption("prefs", prefs);
        driver = new ChromeDriver( options );
        driver.get(PropertyReader.getProperty("SkysiteProdURL"));
 
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
               System.setProperty("webdriver.safari.noinstall", "false"); //To stop uninstall each time
               driver = new SafariDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   
    	return driver;
    	
	}
	
	
	/** TC_001 ("Verify descending order sorting in Project dashboard with PROJECT NAME")*/
	
	@Test(priority = 0, enabled = true, description = "Verify descending order sorting in Project dashboard with PROJECT NAME")
	public void Verify_DescendingOrderSortingByProjectName() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo("Verify descending order sorting in Project dashboard with PROJECT NAME");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			
			Log.assertThat(projectDashboardPage.descendingOrderSortingByProjectName(), "Descending order sorting by Project name is successfull", "Descending order sorting by Project name is unsuccessfull", driver);		
			
			projectDashboardPage.resetToDefaultSortSettings();
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	
	/** TC_002 ("Verify ascending order sorting in Project dashboard with PROJECT NAME")*/
	
	@Test(priority = 1, enabled = true, description = "Verify acending order sorting in Project dashboard with PROJECT NAME")
	public void Verify_AscendingOrderSortingByProjectName() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo("Verify ascending order sorting in Project dashboard with PROJECT NAME");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			
			Log.assertThat(projectDashboardPage.ascendingOrderSortingByProjectName(), "Ascending order sorting by Project name is successfull", "Ascending order sorting by Project name is unsuccessfull", driver);		
			
			projectDashboardPage.resetToDefaultSortSettings();
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_003 ("Verify descending order sorting in Project dashboard with PROJECT NUMBER")*/
	
	@Test(priority = 2, enabled = true, description = "Verify descending order sorting in Project dashboard with PROJECT NUMBER")
	public void Verify_DescendingOrderSortingByProjectNumber() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo("Verify descending order sorting in Project dashboard with PROJECT NUMBER");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			
			Log.assertThat(projectDashboardPage.descendingOrderSortingByProjectNumber(), "Descending order sorting by Project Number is successfull", "Descending order sorting by Project Number is unsuccessfull", driver);		
			
			projectDashboardPage.resetToDefaultSortSettings();
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	
	/** TC_004 ("Verify ascending order sorting in Project dashboard with PROJECT NUMBER")*/
	
	@Test(priority = 3, enabled = true, description = "Verify ascending order sorting in Project dashboard with PROJECT NUMBER")
	public void Verify_AscendingOrderSortingByProjectNumber() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo("Verify ascending order sorting in Project dashboard with PROJECT NUMBER");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			
			Log.assertThat(projectDashboardPage.ascendingOrderSortingByProjectNumber(), "Ascending order sorting by Project Number is successfull", "Ascending order sorting by Project Number is unsuccessfull", driver);		
			
			projectDashboardPage.resetToDefaultSortSettings();
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	

	/** TC_005 ("Verify descending order sorting in Project dashboard with PROJECT OWNER")*/
	
	@Test(priority = 4, enabled = true, description = "Verify descending order sorting in Project dashboard with PROJECT OWNER")
	public void Verify_DescendingOrderSortingByProjectOwner() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo("Verify descending order sorting in Project dashboard with PROJECT OWNER");
			
			String username = PropertyReader.getProperty("USERNAME3");
		    String password = PropertyReader.getProperty("PASSWORD3");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			
			Log.assertThat(projectDashboardPage.descendingOrderSortingByProjectOwner(), "Descending order sorting by Project Owner is successfull", "Descending order sorting by Project Owner is unsuccessfull", driver);		
			
			projectDashboardPage.resetToDefaultSortSettings();
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_006 ("Verify ascending order sorting in Project dashboard with PROJECT OWNER")*/
	
	@Test(priority = 5, enabled = true, description = "Verify ascending order sorting in Project dashboard with PROJECT OWNER")
	public void Verify_AscendingOrderSortingByProjectOwner() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo("Verify ascending order sorting in Project dashboard with PROJECT OWNER");
			
			String username = PropertyReader.getProperty("USERNAME3");
		    String password = PropertyReader.getProperty("PASSWORD3");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 
			
			Log.assertThat(projectDashboardPage.ascendingOrderSortingByProjectOwner(), "Ascending order sorting by Project Owner is successfull", "Ascending order sorting by Project Owner is unsuccessfull", driver);		
			
			projectDashboardPage.resetToDefaultSortSettings();
			
    	}
	
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }


	/** TC_007 ("Verify sorting order set in the Project Dashboard remains the same after Logout and Log In")*/
	
	@Test(priority = 6, enabled = true, description = "Verify sorting order set in the Project Dashboard remains the same after Logout and Log In")
	public void Verify_ProjectSortOrderSaveAfterLoginLogout() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo("Verify sorting order set in the Project Dashboard remains the same after Logout and Log In");
			
			String username = PropertyReader.getProperty("USERNAME3");
		    String password = PropertyReader.getProperty("PASSWORD3");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			ArrayList<String> arr= projectDashboardPage.sortOrderSave();
			projectDashboardPage.Logout_Projects();
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password);
			
			Log.assertThat(projectDashboardPage.sortOrderSaveAfterLogIn(arr), " Project Sorting options selected are saved successfully after Logout and Log In ", " Project Sorting options selected are not saved successfully after Logout and Log In ", driver);	
			
			projectDashboardPage.resetToDefaultSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }	
	
	
	/** TC_008 (" Verify descending order sorting for Folders with FOLDER NAME ")*/
	
	@Test(priority = 7, enabled = true, description = " Verify descending order sorting for Folders with FOLDER NAME ")
	public void Verify_DescendingOrderSortingByFolderName() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Folders with FOLDER NAME ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			
			Log.assertThat(folderPage.descendingOrderSortingByFolderName(), " Descending order sorting by Folder Name is successfull ", " Descending order sorting by Folder Name is unsuccessfull ", driver);		
			
			folderPage.resetToDefaultFolderSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }	
	
	
	/** TC_009 (" Verify ascending order sorting for Folders with FOLDER NAME ")*/
	
	@Test(priority = 8, enabled = true, description = " Verify ascending order sorting for Folders with FOLDER NAME ")
	public void Verify_AscendingOrderSortingByFolderName() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Folders with FOLDER NAME ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			
			Log.assertThat(folderPage.ascendingOrderSortingByFolderName(), " Ascending order sorting by Folder Name is successfull ", " Ascending order sorting by Folder Name is unsuccessfull ", driver);		
			
			folderPage.resetToDefaultFolderSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }	
	
	
	/** TC_010 (" Verify sorting order set in the Folder Level remains the same after Logout and Log In ")*/
	
	@Test(priority = 9, enabled = true, description = " Verify sorting order set in the Folder Level remains the same after Logout and Log In ")
	public void Verify_FolderSortOrderSaveAfterLoginLogout() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify sorting order set in the Folder Level remains the same after Logout and Log In ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			ArrayList<String> arr= folderPage.sortOrderSave();
			folderPage.Logout_Projects();
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password);
			folderPage=projectDashboardPage.enterProject1();
			
			Log.assertThat(folderPage.sortOrderSaveAfterLogIn(arr), " Folder Sorting options selected are saved successfully after Logout and Log In ", " Folder Sorting options selected are not saved successfully after Logout and Log In ", driver);		
			
			folderPage.resetToDefaultFolderSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }	
	
	
	/** TC_011 (" Verify descending order sorting for Folders with FOLDER CREATE DATE ")*/
	
	@Test(priority = 10, enabled = true, description = " Verify descending order sorting for Folders with FOLDER CREATE DATE ")
	public void Verify_DescendingOrderSortingByFolderCreateDate() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Folders with FOLDER CREATE DATE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			
			Log.assertThat(folderPage.descendingOrderSortingByFolderCreateDate(), " Descending order sorting by Folder Create Date is successfull ", " Descending order sorting by Folder Create Date is unsuccessfull ", driver);		
			
			folderPage.resetToDefaultFolderSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_012 (" Verify ascending order sorting for Folders with FOLDER CREATE DATE ")*/
	
	@Test(priority = 11, enabled = true, description = " Verify ascending order sorting for Folders with FOLDER CREATE DATE ")
	public void Verify_AscendingOrderSortingByFolderCreateDate() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Folders with FOLDER CREATE DATE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			
			Log.assertThat(folderPage.ascendingOrderSortingByFolderCreateDate(), " Ascending order sorting by Folder Create Date is successfull ", " Ascending order sorting by Folder Create Date is unsuccessfull ", driver);		
			
			folderPage.resetToDefaultFolderSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	

	/** TC_013 (" Verify descending order sorting for Files with NAME ")*/
	
	@Test(priority = 12, enabled = true, description = " Verify descending order sorting for Files with NAME ")
	public void Verify_DescendingOrderSortingByFileName() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Files with NAME ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			folderPage.enterFolder1();
			
			Log.assertThat(folderPage.descendingOrderSortingByFileName(), " Descending order sorting by File Name is successfull ", " Descending order sorting by File Name is unsuccessfull ", driver);		
			
			folderPage.resetToDefaultFileSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	
	/** TC_014 (" Verify ascending order sorting for Files with NAME ")*/
	
	@Test(priority = 13, enabled = true, description = " Verify ascending order sorting for Files with NAME ")
	public void Verify_AscendingOrderSortingByFileName() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Files with NAME ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			folderPage.enterFolder1();
			
			Log.assertThat(folderPage.ascendingOrderSortingByFileName(), " Ascending order sorting by File Name is successfull ", " Ascending order sorting by File Name is unsuccessfull ", driver);		
			
			folderPage.resetToDefaultFileSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_015 (" Verify descending order sorting for Files with CREATE DATE ")*/
	
	@Test(priority = 14, enabled = true, description = " Verify descending order sorting for Files with CREATE DATE ")
	public void Verify_DescendingOrderSortingByFileCreateDate() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Files with CREATE DATE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			folderPage.enterFolder1();
			folderPage.changeFolderViewGridToList();
			
			Log.assertThat(folderPage.descendingOrderSortingByFileCreateDate(), " Descending order sorting for Files with CREATE DATE is successfull ", " Descending order sorting for Files with CREATE DATE is unsuccessfull ", driver);		
			
			folderPage.changeFolderViewListToGrid();
			folderPage.resetToDefaultFileSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_016 (" Verify ascending order sorting for Files with CREATE DATE ")*/
	
	@Test(priority = 15, enabled = true, description = " Verify ascending order sorting for Files with CREATE DATE ")
	public void Verify_AscendingOrderSortingByFileCreateDate() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Files with CREATE DATE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			folderPage.enterFolder1();
			folderPage.changeFolderViewGridToList();
			
			Log.assertThat(folderPage.ascendingOrderSortingByFileCreateDate(), " Ascending order sorting for Files with CREATE DATE is successfull ", " Ascending order sorting for Files with CREATE DATE is unsuccessfull ", driver);		
			
			folderPage.changeFolderViewListToGrid();
			folderPage.resetToDefaultFileSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_017 (" Verify descending order sorting for Files with REVISION DATE ")*/
	
	@Test(priority = 16, enabled = true, description = " Verify descending order sorting for Files with REVISION DATE ")
	public void Verify_DescendingOrderSortingByFileRevisionDate() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Files with REVISION DATE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			folderPage.enterFolder2();
			folderPage.changeFolderViewGridToList();
			
			Log.assertThat(folderPage.descendingOrderSortingByFileRevisionDate(), " Descending order sorting for Files with REVISION DATE is successfull ", " Descending order sorting for Files with REVISION DATE is unsuccessfull ", driver);		
			
			folderPage.changeFolderViewListToGrid();
			folderPage.resetToDefaultFileSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_018 (" Verify ascending order sorting for Files with REVISION DATE ")*/
	
	@Test(priority = 17, enabled = true, description = " Verify ascending order sorting for Files with REVISION DATE ")
	public void Verify_AscendingOrderSortingByFileRevisionDate() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Files with REVISION DATE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			folderPage.enterFolder2();
			folderPage.changeFolderViewGridToList();
			
			Log.assertThat(folderPage.ascendingOrderSortingByFileRevisionDate(), " Ascending order sorting for Files with REVISION DATE is successfull ", " Ascending order sorting for Files with REVISION DATE is unsuccessfull ", driver);		
			
			folderPage.changeFolderViewListToGrid();
			folderPage.resetToDefaultFileSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_019 (" Verify descending order sorting for Files with DISCIPLINE ")*/
	
	@Test(priority = 18, enabled = true, description = " Verify descending order sorting for Files with DISCIPLINE ")
	public void Verify_DescendingOrderSortingByFileDiscipline() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Files with DISCIPLINE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			folderPage.enterFolder1();
			folderPage.changeFolderViewGridToList();
			
			Log.assertThat(folderPage.descendingOrderSortingByFileDiscipline(), " Descending order sorting for Files with DISCIPLINE is successfull ", " Descending order sorting for Files with DISCIPLINE is unsuccessfull ", driver);		
			
			folderPage.changeFolderViewListToGrid();
			folderPage.resetToDefaultFileSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	

	/** TC_020 (" Verify ascending order sorting for Files with DISCIPLINE ")*/
	
	@Test(priority = 19, enabled = true, description = " Verify ascending order sorting for Files with DISCIPLINE ")
	public void Verify_AscendingOrderSortingByFileDiscipline() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Files with DISCIPLINE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			folderPage.enterFolder1();
			folderPage.changeFolderViewGridToList();
			
			Log.assertThat(folderPage.ascendingOrderSortingByFileDiscipline(), " Ascending order sorting for Files with DISCIPLINE is successfull ", " Ascending order sorting for Files with DISCIPLINE is unsuccessfull ", driver);		
			
			folderPage.changeFolderViewListToGrid();
			folderPage.resetToDefaultFileSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_021 (" Verify descending order sorting for Files with ORDINAL NUMBER ")*/
	
	@Test(priority = 20, enabled = true, description = " Verify descending order sorting for Files with ORDINAL NUMBER ")
	public void Verify_DescendingOrderSortingByFileOrdinalNumber() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Files with ORDINAL NUMBER ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject2();
			folderPage.enterManageFileOrderScreen();
			ArrayList<String> arr= folderPage.getManageFileSortOrder();
			folderPage.rootFolderLevelNavigation();
			folderPage.enterFolder1();
			
			Log.assertThat(folderPage.descendingOrderSortingByFileOrdinalNumber(arr), " Descending order sorting for Files with ORDINAL NUMBER is successfull ", " Descending order sorting for Files with ORDINAL NUMBER is unsuccessfull ", driver);		
			
			folderPage.resetToDefaultFileSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	/** TC_022 (" Verify ascending order sorting for Files with ORDINAL NUMBER ")*/
	
	@Test(priority = 21, enabled = true, description = " Verify ascending order sorting for Files with ORDINAL NUMBER ")
	public void Verify_AscendingOrderSortingByFileOrdinalNumber() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Files with ORDINAL NUMBER ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject2();
			folderPage.enterManageFileOrderScreen();
			ArrayList<String> arr= folderPage.getManageFileSortOrder();
			folderPage.rootFolderLevelNavigation();
			folderPage.enterFolder1();
			
			Log.assertThat(folderPage.ascendingOrderSortingByFileOrdinalNumber(arr), " Ascending order sorting for Files with ORDINAL NUMBER is successfull ", " Ascending order sorting for Files with ORDINAL NUMBER is unsuccessfull ", driver);		
			
			folderPage.resetToDefaultFileSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	

	/** TC_023 (" Verify sorting order set in the File Level remains the same after Logout and Log In ")*/
	
	@Test(priority = 22, enabled = true, description = " Verify sorting order set in the File Level remains the same after Logout and Log In ")
	public void Verify_FileSortOrderSaveAfterLoginLogout() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify sorting order set in the File Level remains the same after Logout and Log In ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			folderPage.enterFolder1();
			ArrayList<String> arr= folderPage.sortOrderSave1();
			folderPage.Logout_Projects();
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password);
			folderPage=projectDashboardPage.enterProject1();
			folderPage.enterFolder1();
			
			Log.assertThat(folderPage.sortOrderSaveAfterLogIn1(arr), " File Sorting options selected are saved successfully after Logout and Log In ", " File Sorting options selected are not saved successfully after Logout and Log In ", driver);		
			
			folderPage.resetToDefaultFileSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }	
	
	
	/** TC_024 (" Verify descending order sorting for Album level in Gallery with NAME ")*/
	
	@Test(priority = 23, enabled = true, description = " Verify descending order sorting for Album level in Gallery with NAME ")
	public void Verify_DescendingOrderSortingByAlbumName() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Album level in Gallery with NAME ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectGalleryPage=folderPage.enterGallery();
			
			Log.assertThat(projectGalleryPage.descendingOrderSortingByAlbumName(), " Descending order sorting by ALbum Name is successfull ", " Descending order sorting by Album Name is unsuccessfull ", driver);		
			
			projectGalleryPage.resetToDefaultAlbumSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_025 (" Verify ascending order sorting for Album level in Gallery with NAME ")*/
	
	@Test(priority = 24, enabled = true, description = " Verify ascending order sorting for Album level in Gallery with NAME ")
	public void Verify_AscendingOrderSortingByAlbumName() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Album level in Gallery with NAME ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectGalleryPage=folderPage.enterGallery();
			
			Log.assertThat(projectGalleryPage.ascendingOrderSortingByAlbumName(), " Ascending order sorting by ALbum Name is successfull ", " Ascending order sorting by Album Name is unsuccessfull ", driver);		
			
			projectGalleryPage.resetToDefaultAlbumSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }


	/** TC_026 (" Verify descending order sorting for Album level in Gallery with CREATE DATE ")*/
	
	@Test(priority = 25, enabled = true, description = " Verify descending order sorting for Album level in Gallery with CREATE DATE ")
	public void Verify_DescendingOrderSortingByAlbumCreateDate() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Album level in Gallery with CREATE DATE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectGalleryPage=folderPage.enterGallery();
			
			Log.assertThat(projectGalleryPage.descendingOrderSortingByAlbumCreateDate(), " Descending order sorting by Album Create Date is successfull ", " Descending order sorting by Album Create date is unsuccessfull ", driver);		
			
			projectGalleryPage.resetToDefaultAlbumSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	
	/** TC_027 (" Verify ascending order sorting for Album level in Gallery with CREATE DATE ")*/
	
	@Test(priority = 26, enabled = true, description = " Verify ascending order sorting for Album level in Gallery with CREATE DATE ")
	public void Verify_AscendingOrderSortingByAlbumCreateDate() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Album level in Gallery with CREATE DATE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectGalleryPage=folderPage.enterGallery();
			
			Log.assertThat(projectGalleryPage.ascendingOrderSortingByAlbumCreateDate(), " Ascending order sorting by Album Create Date is successfull ", " Ascending order sorting by Album Create Date is unsuccessfull ", driver);		
			
			projectGalleryPage.resetToDefaultAlbumSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}		
    }
	

	/** TC_028 (" Verify sorting order set in the Album level in Gallery remains the same after Logout and Log In ")*/
	
	@Test(priority = 27, enabled = true, description = " Verify sorting order set in the Album level in Gallery remains the same after Logout and Log In ")
	public void Verify_AlbumSortOrderSaveAfterLoginLogout() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify sorting order set in the Album level in Gallery remains the same after Logout and Log In ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectGalleryPage=folderPage.enterGallery();			
			ArrayList<String> arr= projectGalleryPage.sortOrderSave();			
			projectGalleryPage.Logout_Projects();			
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password);
			folderPage=projectDashboardPage.enterProject1();
			projectGalleryPage=folderPage.enterGallery();
			Log.assertThat(projectGalleryPage.sortOrderSaveAfterLogIn(arr), " Album Sorting options selected are saved successfully after Logout and Log In ", " Album Sorting options selected are not saved successfully after Logout and Log In ", driver);		
			
			projectGalleryPage.resetToDefaultAlbumSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}		
    }
	
	
	/** TC_029 (" Verify descending order sorting for Photos in Gallery with NAME ")*/
	
	@Test(priority = 28, enabled = true, description = " Verify descending order sorting for Photos in Gallery with NAME ")
	public void Verify_DescendingOrderSortingByPhotoName() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Photos in Gallery with NAME ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectGalleryPage=folderPage.enterGallery();
			projectGalleryPage.enterAlbum();
			
			Log.assertThat(projectGalleryPage.descendingOrderSortingByPhotoName(), " Descending order sorting by Photo Name is successfull ", " Descending order sorting by Photo Name is unsuccessfull ", driver);		
			
			projectGalleryPage.resetToDefaultAlbumSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_030 (" Verify ascending order sorting for Photos in Gallery with NAME ")*/
	
	@Test(priority = 29, enabled = true, description = " Verify ascending order sorting for Photos in Gallery with NAME ")
	public void Verify_AscendingOrderSortingByPhotoName() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Photos in Gallery with NAME ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectGalleryPage=folderPage.enterGallery();
			projectGalleryPage.enterAlbum();
			
			Log.assertThat(projectGalleryPage.descendingOrderSortingByPhotoName(), " Ascending order sorting by Photo Name is successfull ", " Ascending order sorting by Photo Name is unsuccessfull ", driver);		
			
			projectGalleryPage.resetToDefaultAlbumSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_031 (" Verify sorting order set for Photos in Gallery remains the same after Logout and Log In ")*/
	
	@Test(priority = 30, enabled = true, description = " Verify sorting order set for Photos in Gallery remains the same after Logout and Log In ")
	public void Verify_PhotoSortOrderSaveAfterLoginLogout() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify sorting order set for Photos in Gallery remains the same after Logout and Log In ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectGalleryPage=folderPage.enterGallery();	
			projectGalleryPage.enterAlbum();
			ArrayList<String> arr= projectGalleryPage.sortOrderSave1();			
			projectGalleryPage.Logout_Projects();			
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password);
			folderPage=projectDashboardPage.enterProject1();
			projectGalleryPage=folderPage.enterGallery();
			projectGalleryPage.enterAlbum();
			
			Log.assertThat(projectGalleryPage.sortOrderSaveAfterLogIn1(arr), " Photo Sorting options selected are saved successfully after Logout and Log In ", " Photo Sorting options selected are not saved successfully after Logout and Log In ", driver);		
			
			projectGalleryPage.resetToDefaultAlbumSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}		
    }
	
	
	/** TC_032 (" Verify descending order sorting for Photos in Gallery with CREATE DATE ")*/
	
	@Test(priority = 31, enabled = true, description = " Verify descending order sorting for Photos in Gallery with CREATE DATE ")
	public void Verify_DescendingOrderSortingByPhotoCreateDate() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Photos in Gallery with CREATE DATE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectGalleryPage=folderPage.enterGallery();
			projectGalleryPage.enterAlbum();
			projectGalleryPage.changeAlbumViewGridToList();
			
			Log.assertThat(projectGalleryPage.descendingOrderSortingByPhotoCreateDate(), " Descending order sorting by Photo Create Date is successfull ", " Descending order sorting by Photo Create Date is unsuccessfull ", driver);		
			
			projectGalleryPage.changeAlbumViewListToGrid();
			projectGalleryPage.resetToDefaultAlbumSortSettings();
					
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_033 (" Verify ascending order sorting for Photos in Gallery with CREATE DATE ")*/
	
	@Test(priority = 32, enabled = true, description = " Verify ascending order sorting for Photos in Gallery with CREATE DATE ")
	public void Verify_AscendingOrderSortingByPhotoCreateDate() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Photos in Gallery with CREATE DATE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectGalleryPage=folderPage.enterGallery();
			projectGalleryPage.enterAlbum();
			projectGalleryPage.changeAlbumViewGridToList();
			
			Log.assertThat(projectGalleryPage.ascendingOrderSortingByPhotoCreateDate(), " Ascending order sorting by Photo Create Date is successfull ", " Ascending order sorting by Photo Create Date is unsuccessfull ", driver);		
			
			projectGalleryPage.changeAlbumViewListToGrid();
			projectGalleryPage.resetToDefaultAlbumSortSettings();
					
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_034 (" Verify descending order sorting for Files in Latest Documents with NAME ")*/
	
	@Test(priority = 33, enabled = true, description = " Verify descending order sorting for Files in Latest Documents with NAME ")
	public void Verify_DescendingOrderSortingInLatestDocumentsByFileName() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Files in Latest Documents with NAME ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectLatestDocumentsPage=folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
				
			Log.assertThat(projectLatestDocumentsPage.descendingOrderSortingInLatestDocumentsByFileName(), " Descending order sorting for Files in Latest Documents with NAME is successfull ", " Descending order sorting for Files in Latest Documents with NAME is unsuccessfull ", driver);		
				
			projectLatestDocumentsPage.resetToDefaultLatestDocumentsSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_035 (" Verify ascending order sorting for Files in Latest Documents with NAME ")*/
	
	@Test(priority = 34, enabled = true, description = " Verify ascending order sorting for Files in Latest Documents with NAME ")
	public void Verify_AscendingOrderSortingInLatestDocumentsByFileName() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Files in Latest Documents with NAME ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectLatestDocumentsPage=folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
				
			Log.assertThat(projectLatestDocumentsPage.ascendingOrderSortingInLatestDocumentsByFileName(), " Ascending order sorting for Files in Latest Documents with NAME is successfull ", " Ascending order sorting for Files in Latest Documents with NAME is unsuccessfull ", driver);		
				
			projectLatestDocumentsPage.resetToDefaultLatestDocumentsSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_036 (" Verify descending order sorting for Files in Latest Documents with CREATE DATE ")*/
	
	@Test(priority = 35, enabled = true, description = " Verify descending order sorting for Files in Latest Documents with CREATE DATE ")
	public void Verify_DescendingOrderSortingInLatestDocumentsByCreateDate() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Files in Latest Documents with CREATE DATE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectLatestDocumentsPage=folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
			projectLatestDocumentsPage.changeLDViewGridToList();
			
			Log.assertThat(projectLatestDocumentsPage.descendingOrderSortingInLatestDocumentsByCreateDate(), " Descending order sorting for Files in Latest Documents with CREATE DATE is successfull ", " Descending order sorting for Files in Latest Documents with CREATE DATE is unsuccessfull ", driver);		
				
			projectLatestDocumentsPage.changeLDViewListToGrid();
			projectLatestDocumentsPage.resetToDefaultLatestDocumentsSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_037 (" Verify ascending order sorting for Files in Latest Documents with CREATE DATE ")*/
	
	@Test(priority = 36, enabled = true, description = " Verify ascending order sorting for Files in Latest Documents with CREATE DATE ")
	public void Verify_AscendingOrderSortingInLatestDocumentsByCreateDate() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Files in Latest Documents with CREATE DATE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectLatestDocumentsPage=folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
			projectLatestDocumentsPage.changeLDViewGridToList();
			
			Log.assertThat(projectLatestDocumentsPage.ascendingOrderSortingInLatestDocumentsByCreateDate(), " Ascending order sorting for Files in Latest Documents with CREATE DATE is successfull ", " Ascending order sorting for Files in Latest Documents with CREATE DATE is unsuccessfull ", driver);		
				
			projectLatestDocumentsPage.changeLDViewListToGrid();
			projectLatestDocumentsPage.resetToDefaultLatestDocumentsSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}			
    }
	
	/** TC_038 (" Verify descending order sorting for Files in Latest Documents with DISCIPLINE ")*/
	
	@Test(priority = 37, enabled = true, description = " Verify descending order sorting for Files in Latest Documents with DISCIPLINE ")
	public void Verify_DescendingOrderSortingInLatestDocumentsByDiscipline() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Files in Latest Documents with DISCIPLINE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectLatestDocumentsPage=folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
			projectLatestDocumentsPage.changeLDViewGridToList();
			
			Log.assertThat(projectLatestDocumentsPage.descendingOrderSortingInLatestDocumentsByCreateDateDiscipline(), " Descending order sorting for Files in Latest Documents with DISCIPLINE is successfull ", " Descending order sorting for Files in Latest Documents with DISCIPLINE is unsuccessfull ", driver);		
				
			projectLatestDocumentsPage.changeLDViewListToGrid();
			projectLatestDocumentsPage.resetToDefaultLatestDocumentsSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_039 (" Verify ascending order sorting for Files in Latest Documents with DISCIPLINE ")*/
	
	@Test(priority = 38, enabled = true, description = " Verify Ascending order sorting for Files in Latest Documents with DISCIPLINE ")
	public void Verify_AscendingOrderSortingInLatestDocumentsByDiscipline() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Files in Latest Documents with DISCIPLINE ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectLatestDocumentsPage=folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
			projectLatestDocumentsPage.changeLDViewGridToList();
			
			Log.assertThat(projectLatestDocumentsPage.ascendingOrderSortingInLatestDocumentsByCreateDateDiscipline(), " Ascending order sorting for Files in Latest Documents with DISCIPLINE is successfull ", " Ascending order sorting for Files in Latest Documents with DISCIPLINE is unsuccessfull ", driver);		
				
			projectLatestDocumentsPage.changeLDViewListToGrid();
			projectLatestDocumentsPage.resetToDefaultLatestDocumentsSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}
			
    }
	
	
	/** TC_040 (" Verify descending order sorting for Files in Latest Documents with ORDINAL NUMBER ")*/
	
	@Test(priority = 39, enabled = true, description = " Verify descending order sorting for Files in Latest Documents with ORDINAL NUMBER ")
	public void Verify_DescendingOrderSortingInLatestDocumentsByOrdinalNumber() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify descending order sorting for Files in Latest Documents with ORDINAL NUMBER ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			folderPage.enterManageFileOrderScreen();
			ArrayList<String> arr= folderPage.getManageFileSortOrder();
			folderPage.rootFolderLevelNavigation();
			projectLatestDocumentsPage=folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
			
			Log.assertThat(projectLatestDocumentsPage.descendingOrderSortingInLatestDocumentsByCreateDateOrdinalNumber(arr), " Descending order sorting for Files in Latest Documents with ORDINAL NUMBER is successfull ", " Descending order sorting for Files in Latest Documents with ORDINAL NUMBER is unsuccessfull ", driver);		
				
			projectLatestDocumentsPage.resetToDefaultLatestDocumentsSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}		
    }
	
	/** TC_041 (" Verify ascending order sorting for Files in Latest Documents with ORDINAL NUMBER ")*/
	
	@Test(priority = 40, enabled = true, description = " Verify ascending order sorting for Files in Latest Documents with ORDINAL NUMBER ")
	public void Verify_AscendingOrderSortingInLatestDocumentsByOrdinalNumber() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify ascending order sorting for Files in Latest Documents with ORDINAL NUMBER ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			folderPage.enterManageFileOrderScreen();
			ArrayList<String> arr= folderPage.getManageFileSortOrder();
			folderPage.rootFolderLevelNavigation();
			projectLatestDocumentsPage=folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
			
			Log.assertThat(projectLatestDocumentsPage.ascendingOrderSortingInLatestDocumentsByCreateDateOrdinalNumber(arr), " Ascending order sorting for Files in Latest Documents with ORDINAL NUMBER is successfull ", " Ascending order sorting for Files in Latest Documents with ORDINAL NUMBER is unsuccessfull ", driver);		
				
			projectLatestDocumentsPage.resetToDefaultLatestDocumentsSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}		
    }
	
	/** TC_042 (" Verify sorting order set for Latest Documents remains the same after Logout and Log In ")*/
	
	@Test(priority = 41, enabled = true, description = " Verify sorting order set for Latest Documents remains the same after Logout and Log In ")
	public void Verify_LatestDocumentsSortingOrderSaveAfterLoginLogout() throws Throwable
    {
		
		try
    	{
			Log.testCaseInfo(" Verify sorting order set for Latest Documents remains the same after Logout and Log In ");
			
			String username = PropertyReader.getProperty("USERNAME2");
		    String password = PropertyReader.getProperty("PASSWORD2");
		    
		    projectsLoginPage = new ProjectsLoginPage(driver).get();  
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectLatestDocumentsPage=folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
			ArrayList<String> arr= projectLatestDocumentsPage.sortOrderSave();			
			projectLatestDocumentsPage.Logout_Projects();
			projectDashboardPage=projectsLoginPage.loginWithValidCredential(username,password); 			
			folderPage=projectDashboardPage.enterProject1();
			projectLatestDocumentsPage=folderPage.enterLatestDocuments();
			projectLatestDocumentsPage.latestDocumentEntryValidation();
			
			Log.assertThat(projectLatestDocumentsPage.sortOrderSaveAfterLogIn(arr), " Ascending order sorting for Files in Latest Documents with ORDINAL NUMBER is successfull ", " Ascending order sorting for Files in Latest Documents with ORDINAL NUMBER is unsuccessfull ", driver);		
				
			projectLatestDocumentsPage.resetToDefaultLatestDocumentsSortSettings();
			
    	}
		
		catch(Exception e)
        {
			CommonMethod.analyzeLog(driver);
			e.getCause();
        	Log.exception(e, driver);
        }
    	
		finally
    	{
    		Log.endTestCase();
    		driver.close();
    	}		
    }
	
}
