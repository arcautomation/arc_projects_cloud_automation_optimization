package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.ContactsPage;
import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.utils.AnalyzeLog;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

@Listeners(EmailReport.class)

public class Projects_Downloads_Suit {
	
	
	String webSite;
    ITestResult result;
    
    static WebDriver driver;
    ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;
    FolderPage folderPage;
    ContactsPage contactsPage;
    ProjectDashboardPage projects_ExportToCSVScenarios;
    
    @Parameters("browser")
    @BeforeMethod
 public WebDriver beforeTest(String browser) {
        
        if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        	File dest = new File("./drivers/win/chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
            
            prefs.put("safebrowsing.enabled", "false");//this condition should be f@lse every time
 
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            options.setExperimentalOption("prefs", prefs);
            driver = new ChromeDriver( options );
            driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
               System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
               driver = new SafariDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver;
 }
     
    /** TC_006_FileLevelOperations (Select Multiple Files and Download_List View): Verify Select Multiple Files and Download.
     * Scripted By : NARESH  BABU kavuru
     * @throws Exception
     */
     @Test(priority = 0, enabled = true, description = "TC_006_FileLevelOperations (Select Multiple Files and Download_List View): Verify Select Multiple Files and Download.")
     public void verify_Download_BySelectMultipleFiles() throws Exception
     {
     	try
     	{
     		Log.testCaseInfo("TC_006_FileLevelOperations (Select Multiple Files and Download_List View): Verify Select Multiple Files and Download.");
     	//Getting Static data from properties file
   		String uName = PropertyReader.getProperty("NewData_Username");
   		String pWord = PropertyReader.getProperty("NewData_Password");
   		
     		projectsLoginPage = new ProjectsLoginPage(driver).get();  
     		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
     		
     		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
     		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
     		
     		String Foldername = "Fold_"+Generate_Random_Number.generateRandomValue();
     		folderPage.New_Folder_Create(Foldername);//Create a new Folder
     		
     		folderPage.Select_Folder(Foldername);//Select a folder - newly created
     			
     		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
     		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
     		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
     		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
     		folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount);//Calling Upload using do not index
     		folderPage.Select_Folder(Foldername);//Select a folder - newly created
     		
     		String usernamedir=System.getProperty("user.name");
     		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
     		Log.assertThat(folderPage.ValidateDownload_BySelectMultFiles(Sys_Download_Path,Foldername), "Multiple file select and download is working Successfully", "Multiple file select and download is Not working", driver);
     
     	}
         catch(Exception e)
         {
        	 AnalyzeLog.analyzeLog(driver);
         	e.getCause();
         	Log.exception(e, driver);
         }
     	finally
     	{
     		Log.endTestCase();
     		driver.quit();
     	}
     }
   
	
     /** TC_043_Viewer&Markups (Verify Viewer Level sendlink and Download): Verify Viewer Level sendlink and Download.
      * Scripted By : NARESH  BABU kavuru
      * @throws Exception
      * 
      */
      @Test(priority = 1, enabled = true, description = "TC_043_Viewer&Markups (Verify Viewer Level sendlink and Download): Verify Viewer Level sendlink and Download.")
      public void verify_Download_ViewerLevelFile_SendLink() throws Exception
      {
      	try
      	{
      		Log.testCaseInfo("TC_043_Viewer&Markups (Verify Viewer Level sendlink and Download): Verify Viewer Level sendlink and Download.");
      	//Getting Static data from properties file
    		String uName = PropertyReader.getProperty("NewData_Username");
    		String pWord = PropertyReader.getProperty("NewData_Password");
    		
      		projectsLoginPage = new ProjectsLoginPage(driver).get();  
      		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
      		
      		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
      		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project 
      		
      		String Foldername = "Fold_"+Generate_Random_Number.generateRandomValue();
      		folderPage.New_Folder_Create(Foldername);//Create a new Folder
      		
      		folderPage.Select_Folder(Foldername);//Select a folder - newly created
      			
      		File Path_FMProperties = new File(PropertyReader.getProperty("SingleFIle_TestData_Path"));
     		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
      		String CountOfFilesInFolder = PropertyReader.getProperty("FileCou_OneFile");
      		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
      		folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount);//Calling Upload using do not index
      		folderPage.Select_Folder(Foldername);//Select a folder - newly created
      		
      		String usernamedir=System.getProperty("user.name");
     		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
      		Log.assertThat(folderPage.ValidateDownload_ViewerLevel_SendLink(Sys_Download_Path), "Viewer Level sendlink and Download is working Successfully", "Viewer Level sendlink and Download is Not working", driver);
      
      	}
      	catch(Exception e)
      	{
      		AnalyzeLog.analyzeLog(driver);
          	e.getCause();
          	Log.exception(e, driver);
        }
      	finally
      	{
      		Log.endTestCase();
      		driver.quit();
      	}
      }
      
      /** TC_008_FileLevelOperations (Verify User is able to do Send link by select multiple files and Download_List View): Verify User is able to do Send link by select multiple files and Download.
       * Scripted By : NARESH  BABU kavuru
       * @throws Exception
       */
       @Test(priority = 2, enabled = true, description = "TC_008_FileLevelOperations (Verify User is able to do Send link by select multiple files and Download_List View): Verify User is able to do Send link by select multiple files and Download.")
       public void verify_Download_MultipleFile_SendLink() throws Exception
       {
       	try
       	{
       		Log.testCaseInfo("TC_008_FileLevelOperations (Verify User is able to do Send link by select multiple files and Download_List View): Verify User is able to do Send link by select multiple files and Download.");
       	//Getting Static data from properties file
     		String uName = PropertyReader.getProperty("NewData_Username");
     		String pWord = PropertyReader.getProperty("NewData_Password");
     		
       		projectsLoginPage = new ProjectsLoginPage(driver).get();  
       		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
       		
       		String Project_Name = "Prj_"+Generate_Random_Number.generateRandomValue();
       		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
       		
       		String Foldername = "Fold_"+Generate_Random_Number.generateRandomValue();
       		folderPage.New_Folder_Create(Foldername);//Create a new Folder
       		
       		folderPage.Select_Folder(Foldername);//Select a folder - newly created
       			
       		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
         	String FolderPath = Path_FMProperties.getAbsolutePath().toString();
       		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
       		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
       		folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount);//Calling Upload using do not index
       		folderPage.Select_Folder(Foldername);//Select a folder - newly created
       		
       		String usernamedir=System.getProperty("user.name");
         	String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
       		Log.assertThat(folderPage.ValidateDownload_FromSendLink_BySelectMultFiles(Sys_Download_Path,Foldername), 
       	"Multiple file sendlink and Download is working Successfully", "Multiple file sendlink and Download is Not working", driver);
       
       	}
           catch(Exception e)
           {
        	   AnalyzeLog.analyzeLog(driver);
           	e.getCause();
           	Log.exception(e, driver);
           }
       	finally
       	{
       		Log.endTestCase();
       		driver.quit();
       	}
       }
       
       /** TC_004&TC_007_FolderLevelOperations (Folder sendlink and Download sub folder): Verify Folder sendlink and Download sub folder from URL
        * Scripted By : NARESH  BABU kavuru
        * @throws Exception
        */
        @Test(priority = 3, enabled = true, description = "TC_004&TC_007_FolderLevelOperations (Folder sendlink and Download sub folder): Verify Folder sendlink and Download sub folder from URL")
        public void verify_SubFold_Download_FromFoldSendLinkURL() throws Exception
        {
        	try
        	{
        		Log.testCaseInfo("TC_004&TC_007_FolderLevelOperations (Folder sendlink and Download sub folder): Verify Folder sendlink and Download sub folder from URL");
        	//Getting Static data from properties file
      		String uName = PropertyReader.getProperty("User_Folder_Exp");
      		String pWord = PropertyReader.getProperty("Password_Folder_Exp");
      		
        		projectsLoginPage = new ProjectsLoginPage(driver).get();  
        		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
        		
        		String Project_Name = PropertyReader.getProperty("Prj_SubFoldSendLink");
        		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
        		
        		String usernamedir=System.getProperty("user.name");
         		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
        		String Foldername = PropertyReader.getProperty("Fold_SubFoldSendLink");
        		Log.assertThat(folderPage.ValidateDownload_SubFolder_FromFolderSendLink(Sys_Download_Path,Foldername), "Folder sendlink and Download sub Folder is working Successfully", "Folder sendlink and Download sub Folder is Not working", driver);
        
        	}
            catch(Exception e)
            {
            	AnalyzeLog.analyzeLog(driver);
            	e.getCause();
            	Log.exception(e, driver);
            }
        	finally
        	{
        		Log.endTestCase();
        		driver.quit();
        	}
        }
        
        
        /** TC_09_FileLevelOperations (File Download from revision history): Verify File Download from revision history
         * Scripted By : NARESH  BABU kavuru
         * @throws Exception
         * 
         */
         @Test(priority = 4, enabled = true, description = "TC_09_FileLevelOperations (File Download from revision history): Verify File Download from revision history")
         public void verify_File_Download_FromRevisionHistory() throws Exception
         {
         	try
         	{
         		Log.testCaseInfo("TC_09_FileLevelOperations (File Download from revision history): Verify File Download from revision history");
         	//Getting Static data from properties file
       		String uName = PropertyReader.getProperty("User_Folder_Exp");
       		String pWord = PropertyReader.getProperty("Password_Folder_Exp");
       		
         		projectsLoginPage = new ProjectsLoginPage(driver).get();  
         		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
         		
         		String Project_Name = PropertyReader.getProperty("Prj_Revision");
         		folderPage=projectDashboardPage.Validate_SelectAProject(Project_Name);//Select a project
         		
         		String Foldername = PropertyReader.getProperty("Fold_Revision");
         		folderPage.Select_Folder(Foldername);//Select a Folder
         		
         		String usernamedir=System.getProperty("user.name");
         		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
         		Log.assertThat(folderPage.ValidateDownload_File_FromRevisionHistory(Sys_Download_Path), "File Download from revision history is working Successfully", "File Download from revision history is Not working", driver);
         
         	}
             catch(Exception e)
             {
            	 AnalyzeLog.analyzeLog(driver);
             	e.getCause();
             	Log.exception(e, driver);
             }
         	finally
         	{
         		Log.endTestCase();
         		driver.quit();
         	}
         }
         
         /** TC_012_Gallery (Verify Send link by Select multiple photos and download): Verify send link by Select multiple photos and download.
          * Scripted By : NARESH BABU kavuru
          * @throws Exception
          * 
          */
          @Test(priority = 5, enabled = true, description = "TC_012_Gallery (Verify Send link by Select multiple photos and download): Verify send link by Select multiple photos and download.")
          public void verify_MultPhoto_SendLink_Download() throws Exception
          {
          	try
          	{
          		Log.testCaseInfo("TC_012_Gallery (Verify Send link by Select multiple photos and download): Verify send link by Select multiple photos and download.");
          	//Getting Static data from properties file
          		String uName = PropertyReader.getProperty("NewData_Username");
         		String pWord = PropertyReader.getProperty("NewData_Password");
        		
          		projectsLoginPage = new ProjectsLoginPage(driver).get();  
          		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
          		
          		String Project_Name = "Prj_Galery"+Generate_Random_Number.generateRandomValue();
          		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
          		
          		folderPage.Select_Gallery_Folder();//Select Gallery
          		
          		File Path_FMProperties = new File(PropertyReader.getProperty("MultPhoto_TestData_Path"));
         		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
         		String CountOfPhotosInFolder = PropertyReader.getProperty("PhotoCount");
         		int FileCount = Integer.parseInt(CountOfPhotosInFolder);//String to Integer
          		folderPage.UploadPhotos_InGallery(FolderPath, FileCount);//Upload photos to gallery
          		
          		String usernamedir=System.getProperty("user.name");
         		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
          		String Foldername = "Gallery";
          		Log.assertThat(folderPage.ValidateDownload_FromSendLink_BySelectMultPhotos(Sys_Download_Path,Foldername,Project_Name), "Send link by select multiple photos and download is working Successfully", "Send link by select multiple photos and download is Not working", driver);
          
          	}
              catch(Exception e)
              {
            	  AnalyzeLog.analyzeLog(driver);
              	e.getCause();
              	Log.exception(e, driver);
              }
          	finally
          	{
          		Log.endTestCase();
          		driver.quit();
          	}
          }
          
          /**  TC_013_Gallery (Verify multiple photo selection and downlaod): Verify Download by Select multiple photos.
           * Scripted By : NARESH BABU kavuru
           * @throws Exception
           */
           @Test(priority = 6, enabled = true, description = "TC_013_Gallery (Verify multiple photo selection and downlaod): Verify Download by Select multiple photos.")
           public void verify_Select_MultPhoto_Download() throws Exception
           {
           	try
           	{
           		Log.testCaseInfo("TC_013_Gallery (Verify multiple photo selection and downlaod): Verify Download by Select multiple photos.");
           	//Getting Static data from properties file
           		String uName = PropertyReader.getProperty("NewData_Username");
           		String pWord = PropertyReader.getProperty("NewData_Password");
         		
           		projectsLoginPage = new ProjectsLoginPage(driver).get();  
           		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
           		
           		String Project_Name = "Prj_Galery"+Generate_Random_Number.generateRandomValue();
           		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
           		
           		folderPage.Select_Gallery_Folder();//Select Gallery
           		
           		File Path_FMProperties = new File(PropertyReader.getProperty("MultPhoto_TestData_Path"));
           		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
           		String CountOfPhotosInFolder = PropertyReader.getProperty("PhotoCount");
           		int FileCount = Integer.parseInt(CountOfPhotosInFolder);//String to Integer
           		folderPage.UploadPhotos_InGallery(FolderPath, FileCount);//Upload photos to gallery
           		
           		String usernamedir=System.getProperty("user.name");
             		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
           		String Foldername = "Gallery";
           		Log.assertThat(folderPage.ValidateDownload_BySelectMultPhotos(Sys_Download_Path,Foldername,Project_Name), "Download by select multiple photos is working Successfully", "Download by select multiple photos is Not working", driver);
           
           	}
               catch(Exception e)
               {
            	   AnalyzeLog.analyzeLog(driver);
               	e.getCause();
               	Log.exception(e, driver);
               }
           	finally
           	{
           		Log.endTestCase();
           		driver.quit();
           	}
           }
	
           
           /** TC_008_FolderLevelOpearations (Move folder and download destination folder): Verify Move folder and download destination folder.
            * Scripted By : NARESH BABU kavuru
            * @throws Exception
            */
            @Test(priority = 7, enabled = true, description = "TC_008_FolderLevelOpearations (Move folder and download destination folder): Verify Move folder and download destination folder.")
            public void verify_MoveFolder_AndDownloadDestinationFolder() throws Exception
            {
            	try
            	{
            		Log.testCaseInfo("TC_008_FolderLevelOpearations (Move folder and download destination folder): Verify Move folder and download destination folder.");
            	//Getting Static data from properties file
            		String uName = PropertyReader.getProperty("NewData_Username");
            		String pWord = PropertyReader.getProperty("NewData_Password");
          		
            		projectsLoginPage = new ProjectsLoginPage(driver).get();  
            		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
            		
            		String Project_Name = "Prj_MoveFold"+Generate_Random_Number.generateRandomValue();
            		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
            		
            		String Foldername = "SourcFold_"+Generate_Random_Number.generateRandomValue();
            		folderPage.New_Folder_Create(Foldername);//Create a New Folder            		
            		folderPage.Select_Folder(Foldername);//Select a folder - newly created
            		
            		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
            		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
            		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
            		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
            		folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount);//Calling Upload using do not index
           		
            		String Destination_Folder = "DestinationFold_"+Generate_Random_Number.generateRandomValue();
            		folderPage.New_Folder_Create(Destination_Folder);//Create a New Folder - Destination

            		Log.assertThat(folderPage.Move_Folder_ToDestinationFolder(Foldername, Destination_Folder), "Move Folder is working successfully","Move Folder is Not working", driver);
           
            		String usernamedir=System.getProperty("user.name");
             		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
            		Log.assertThat(folderPage.Download_Folder(Sys_Download_Path,Destination_Folder), "Download Destination Folder is working Successfully", "Download Destination Folder is Not working", driver);
            
            	}
                catch(Exception e)
                {
                	AnalyzeLog.analyzeLog(driver);
                	e.getCause();
                	Log.exception(e, driver);
                }
            	finally
            	{
            		Log.endTestCase();
            		driver.quit();
            	}
            }
	
            
            /** TC_009_FolderLevelOpeartions (Move folder and do send link of destination folder): Verify Move folder and do send link of destination folder.
             * Scripted By : NARESH BABU kavuru
             * @throws Exception
             */
             @Test(priority = 8, enabled = true, description = "TC_009_FolderLevelOpeartions (Move folder and do send link of destination folder): Verify Move folder and do send link of destination folder.")
             public void verify_MoveFolder_DestinationFolder_SendLink() throws Exception
             {
             	try
             	{
             		Log.testCaseInfo("TC_009_FolderLevelOpeartions (Move folder and do send link of destination folder): Verify Move folder and do send link of destination folder.");
             	//Getting Static data from properties file
             		String uName = PropertyReader.getProperty("NewData_Username");
             		String pWord = PropertyReader.getProperty("NewData_Password");
           		
             		projectsLoginPage = new ProjectsLoginPage(driver).get();  
             		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
             		
             		String Project_Name = "Prj_MoveFold"+Generate_Random_Number.generateRandomValue();
             		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
             		
             		String Foldername = "SourcFold_"+Generate_Random_Number.generateRandomValue();
             		folderPage.New_Folder_Create(Foldername);//Create a New Folder
             		
             		folderPage.Select_Folder(Foldername);//Select a folder - newly created
             		
             		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
             		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
            		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
            		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
            		folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount);//Calling Upload using do not index
            		
            		String Destination_Folder = "DestinationFold_"+Generate_Random_Number.generateRandomValue();
             		folderPage.New_Folder_Create(Destination_Folder);//Create a New Folder - Destination
 
             		Log.assertThat(folderPage.Move_Folder_ToDestinationFolder(Foldername, Destination_Folder), "Move Folder is working successfully","Move Folder is Not working", driver);
             		String usernamedir=System.getProperty("user.name");
             		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
             		Log.assertThat(folderPage.ValidateDownload_SubFolder_FromFolderSendLink(Sys_Download_Path,Destination_Folder), "SendLink of Destination Folder is working Successfully", "SendLink of Destination Folder is Not working", driver);
             
             	}
                 catch(Exception e)
                 {
                	 AnalyzeLog.analyzeLog(driver);
                 	e.getCause();
                 	Log.exception(e, driver);
                 }
             	finally
             	{
             		Log.endTestCase();
             		driver.quit();
             	}
             }
             
             /** TC_010_FolderLevelOperations (Move exclude LD folder to exclude LD folder then download): Verify Move exclude LD folder to exclude LD folder then download.
              * Scripted By : NARESH BABU kavuru
              * @throws Exception
              * Move �Exclude LD Folder� to �Excluded LD Folder� then Download
              */
              @Test(priority = 9, enabled = true, description = "TC_010_FolderLevelOperations (Move exclude LD folder to exclude LD folder then download): Verify Move exclude LD folder to exclude LD folder then download.")
              public void verify_Move_ExcLdFolder_ToDestinationExcLdFolder_Downlaod() throws Exception
              {
              	try
              	{
              		Log.testCaseInfo("TC_010_FolderLevelOperations (Move exclude LD folder to exclude LD folder then download): Verify Move exclude LD folder to exclude LD folder then download.");
              	//Getting Static data from properties file
              		String uName = PropertyReader.getProperty("NewData_Username");
              		String pWord = PropertyReader.getProperty("NewData_Password");
            		
              		projectsLoginPage = new ProjectsLoginPage(driver).get();  
              		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
              		
              		String Project_Name = "Prj_MoveFold"+Generate_Random_Number.generateRandomValue();
              		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
              		
              		String Foldername = "SourcFold_"+Generate_Random_Number.generateRandomValue();
              		folderPage.New_Folder_Create_ExcludeLD(Foldername);//Create a New Folder Exclude LD
              		
              		folderPage.Select_Folder(Foldername);//Select a folder - newly created
              		
              		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
              		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
             		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
             		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
             		folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount);//Calling Upload using do not index
             		
             		String Destination_Folder = "DestinationFold_"+Generate_Random_Number.generateRandomValue();
              		folderPage.New_Folder_Create_ExcludeLD(Destination_Folder);//Create a New Folder Exclude LD - Destination
  
              		Log.assertThat(folderPage.Move_Folder_ToDestinationFolder(Foldername, Destination_Folder), "Move Folder is working successfully","Move Folder is Not working", driver);
              		
              		Log.assertThat(folderPage.Validate_LD_FolderIsEmpty(), "LD Folder is working successfully","LD Folder is Not working", driver);
              		String usernamedir=System.getProperty("user.name");
             		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
              		Log.assertThat(folderPage.Download_Folder_ValidateInsideZip(Sys_Download_Path,Destination_Folder,FolderPath,FileCount), "Download & Validate ZIP Folder is working Successfully", "Download & Validate ZIP Folder is Not working", driver);
              
              	}
                  catch(Exception e)
                  {
                	  AnalyzeLog.analyzeLog(driver);
                  	e.getCause();
                  	Log.exception(e, driver);
                  }
              	finally
              	{
              		Log.endTestCase();
              		driver.quit();
              	}
              }
              
              /** TC_011_FolderLevelOperations (Move Include LD Folder to Exclude LD Folder and validate then Download): Verify Move Include LD Folder to Exclude LD Folder and validate then Download.
               * Scripted By : NARESH BABU kavuru
               * @throws Exception
               * Move �Exclude LD Folder� to �Excluded LD Folder� then Download
               */
               @Test(priority = 10, enabled = true, description = "TC_011_FolderLevelOperations (Move Include LD Folder to Exclude LD Folder and validate then Download): Verify Move Include LD Folder to Exclude LD Folder and validate then Download.")
               public void verify_Move_IncLdFolder_ToDestinationExcLdFolder_Downlaod() throws Exception
               {
               	try
               	{
               		Log.testCaseInfo("TC_011_FolderLevelOperations (Move Include LD Folder to Exclude LD Folder and validate then Download): Verify Move Include LD Folder to Exclude LD Folder and validate then Download.");
               	//Getting Static data from properties file
               		String uName = PropertyReader.getProperty("NewData_Username");
               		String pWord = PropertyReader.getProperty("NewData_Password");
             		
               		projectsLoginPage = new ProjectsLoginPage(driver).get();  
               		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
               		
               		String Project_Name = "Prj_MoveFold"+Generate_Random_Number.generateRandomValue();
               		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
               		
               		String Foldername = "SourcFold_"+Generate_Random_Number.generateRandomValue();
               		folderPage.New_Folder_Create(Foldername);//Create a New Folder Include LD
               		
               		folderPage.Select_Folder(Foldername);//Select a folder - newly created
               		
               		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
               		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
               		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
               		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
               		folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount);//Calling Upload using do not index
              		
               		String Destination_Folder = "DestinationFold_"+Generate_Random_Number.generateRandomValue();
               		folderPage.New_Folder_Create_ExcludeLD(Destination_Folder);//Create a New Folder Exclude LD - Destination
               		
               		folderPage.Select_Folder(Destination_Folder);//Select a folder - newly created
               		
               		File Path_FMProperties1 = new File(PropertyReader.getProperty("Two_PdfFiles"));
               		String FolderPath1 = Path_FMProperties1.getAbsolutePath().toString();
               		String CountOfFilesInFolder1 = PropertyReader.getProperty("Count_Two_PdfFiles");
               		int FileCount1 = Integer.parseInt(CountOfFilesInFolder1);//String to Integer
               		folderPage.UploadFiles_WithoutIndex(FolderPath1,FileCount1);//Calling Upload using do not index
               		
               		Log.assertThat(folderPage.Move_Folder_ToDestinationFolder(Foldername, Destination_Folder), "Move Folder is working successfully","Move Folder is Not working", driver);
               		
               		Log.assertThat(folderPage.Validate_LD_FolderIsEmpty(), "LD Folder is working successfully","LD Folder is Not working", driver);
               		String usernamedir=System.getProperty("user.name");
                 		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
               		Log.assertThat(folderPage.Download_Folder_ValidateInsideZip(Sys_Download_Path,Destination_Folder,FolderPath,FileCount), "Download & Validate ZIP Folder is working Successfully", "Download & Validate ZIP Folder is Not working", driver);
               
               	}
                   catch(Exception e)
                   {
                	   AnalyzeLog.analyzeLog(driver);
                   	e.getCause();
                   	Log.exception(e, driver);
                   }
               	finally
               	{
               		Log.endTestCase();
               		driver.quit();
               	}
               }
               
               /** TC_012_FolderLevelOperations (Move Exclude LD Folder to Include LD Folder and validate then Download): Verify Move Exclude LD Folder to Include LD Folder and validate then Download.
                * Scripted By : NARESH BABU KAVURU
                * @throws Exception
                * Move �Exclude LD Folder� to �Excluded LD Folder� then Download
                */
                @Test(priority = 11, enabled = true, description = "TC_012_FolderLevelOperations (Move Exclude LD Folder to Include LD Folder and validate then Download): Verify Move Exclude LD Folder to Include LD Folder and validate then Download.")
                public void verify_Move_ExcLdFolder_ToDestinationIncLdFolder_Downlaod() throws Exception
                {
                	try
                	{
                		Log.testCaseInfo("TC_012_FolderLevelOperations (Move Exclude LD Folder to Include LD Folder and validate then Download): Verify Move Exclude LD Folder to Include LD Folder and validate then Download.");
                	//Getting Static data from properties file
                		String uName = PropertyReader.getProperty("NewData_Username");
                		String pWord = PropertyReader.getProperty("NewData_Password");
              		
                		projectsLoginPage = new ProjectsLoginPage(driver).get();  
                		projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS
                		
                		String Project_Name = "Prj_MoveFold"+Generate_Random_Number.generateRandomValue();
                		folderPage=projectDashboardPage.createProject(Project_Name);//Create a new project
                		
                		String Foldername = "SourcFold_"+Generate_Random_Number.generateRandomValue();
                		folderPage.New_Folder_Create_ExcludeLD(Foldername);//Create a New Folder Exclude LD
                		
                		folderPage.Select_Folder(Foldername);//Select a folder - newly created
                		
                		File Path_FMProperties = new File(PropertyReader.getProperty("DonotInd_TestData_Path"));
                		String FolderPath = Path_FMProperties.getAbsolutePath().toString();
                		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
                		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
                		folderPage.UploadFiles_WithoutIndex(FolderPath,FileCount);//Calling Upload using do not index
               		
                		String Destination_Folder = "DestinationFold_"+Generate_Random_Number.generateRandomValue();
                		folderPage.New_Folder_Create(Destination_Folder);//Create a New Folder Include LD - Destination
                		
                		folderPage.Select_Folder(Destination_Folder);//Select a folder - newly created
                		
                		File Path_FMProperties1 = new File(PropertyReader.getProperty("Two_PdfFiles"));
                		String FolderPath1 = Path_FMProperties1.getAbsolutePath().toString();
                		String CountOfFilesInFolder1 = PropertyReader.getProperty("Count_Two_PdfFiles");
                		int FileCount1 = Integer.parseInt(CountOfFilesInFolder1);//String to Integer
                		folderPage.UploadFiles_WithoutIndex(FolderPath1,FileCount1);//Calling Upload using do not index
                		
                		Log.assertThat(folderPage.Move_Folder_ToDestinationFolder(Foldername, Destination_Folder), "Move Folder is working successfully","Move Folder is Not working", driver);
                		
                		//Log.assertThat(folderPage.Validate_LD_FolderIsEmpty(), "LD Folder is working successfully","LD Folder is Not working", driver);
                		String usernamedir=System.getProperty("user.name");
                 		String Sys_Download_Path="C:\\" + "Users\\" + usernamedir + "\\Downloads";
                		Log.assertThat(folderPage.Download_Folder_ValidateInsideZip(Sys_Download_Path,Destination_Folder,FolderPath,FileCount), "Download & Validate ZIP Folder is working Successfully", "Download & Validate ZIP Folder is Not working", driver);
                
                	}
                   catch(Exception e)
                   {
                	   AnalyzeLog.analyzeLog(driver);
                    	e.getCause();
                    	Log.exception(e, driver);
                   }
                	finally
                	{
                		Log.endTestCase();
                		driver.quit();
                	}
                }
                
                  	

}
