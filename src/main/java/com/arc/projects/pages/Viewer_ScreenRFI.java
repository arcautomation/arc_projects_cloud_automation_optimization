package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;




public class Viewer_ScreenRFI extends LoadableComponent<Viewer_ScreenRFI> {
	
	WebDriver driver;
    ProjectDashboardPage projectDashboardPage;
    Viewer_ScreenRFI viewerScreenPage;
   
    static String Stamp_inbox = Generate_Random_Number.getRandomText(3);
    static String Stamp_title = Generate_Random_Number.StampTitle();
    static String Stamp_inbox1 = Generate_Random_Number.getRandomText(3);
    static String Stamp_title1 = Generate_Random_Number.StampTitle();
   
	private boolean isPageLoaded;
	
	@FindBy(linkText = "Process completed")
	WebElement linkProcessCompleted;

	@FindBy(css = "#aPrjAddFolder")
	WebElement btnAddNewFolder;

	@FindBy(xpath = "//*[@id='txtSearchKeyword']")
	WebElement GlobalSearch;

	@FindBy(xpath = "//*[@id='btnSearch']")
	WebElement ButtonSearch;

	// =====ZoomIn Button======================
	@FindBy(xpath = "//i[@class='icon icon-plus']")
	WebElement ZoomIn;

	// =====ZoomOut Button======================
	@FindBy(xpath = "//i[@class='icon icon-minus']")
	WebElement ZoomOut;

	@FindBy(xpath = "//a[@class='leaflet-control-zoom-in leaflet-disabled']")
	WebElement ZoomInDisabled;

	// ========HomeButton======================
	@FindBy(css = ".leaflet-control-zoom-out")
	WebElement Home;

	// ========Full Screen Button===============
	@FindBy(css = ".leaflet-control-zoom-fullscreen.leaflet-bar-part.leaflet-bar-part-bottom.last")
	WebElement FullScreen;

	// ========Pointer Arrow========================
	@FindBy(css = "#pointerToolMenuBtn")
	WebElement OpenPointer_Menu;

	// ========Pointer Arrow_Submenu_Arrow===========
	@FindBy(css = "#panSelectMenuBtn")
	WebElement Pointer_Arrow_submenu;

	// ========Pointer Arrow_Submenu_Multi_select====
	@FindBy(css = ".icon.icon-lg.icon-multi_select")
	WebElement Pointer_Arrow_submenu_MultiSelect;

	// ========OpenLineMenu==========================
	@FindBy(xpath = "(//i[@class='icon icon-lg icon-select'])[1]")
	WebElement pointer_Menu;

	@FindBy(xpath = "//a[@id='lineToolMenuBtn'][1]")
	WebElement OpenLineMenu;

	@FindBy(xpath = "(//i[@class='icon icon-lg icon-line']")
	WebElement DROWlINE;

	@FindBy(xpath = "//i [@class='icon icon-lg icon-arrow']")
	WebElement Arrow;

	@FindBy(xpath = "//a[@data-original-title='Draw with a freehand']")
	WebElement DrowWithFreehand;

	@FindBy(xpath = ".//*[@id='imageViewer']")
	WebElement ImageareA;

	@FindBy(xpath = "//a[@class='leaflet-control-savebar-save']")
	WebElement SaveButton;

	@FindBy(xpath = "//i[@ class='icon icon-lg icon-highlight']")
	WebElement Highlighter;

	// =============== Add a Text/CALLOUT =================

	@FindBy(xpath = "//a[@id='textToolMenuBtn']/i")
	WebElement OpenTextMenu;

	@FindBy(xpath = "//*[@id='textCreate']/a")
	WebElement AddaText;

	@FindBy(css = ".icon.icon-lg.icon-callout2")
	WebElement CALLOUT;

	@FindBy(xpath = "//*[@id='editSheetViewerText']")
	WebElement Textarea;

	@FindBy(xpath = "//*[@id='editSheetViewerText']")
	WebElement CallOutTextArea;

	@FindBy(id = "selectColor")
	WebElement Fontcolour;

	@FindBy(xpath = "//*[@id='selectBackColor']")
	WebElement Fillcolour;

	@FindBy(id = "selectFontSize")
	WebElement Fontsize;

	@FindBy(xpath = "//*[@id='selectLineWeight']")
	WebElement BorderThickness;

	@FindBy(xpath = "//*[@id='selectBackColor']")
	WebElement BackGroundColour;

	@FindBy(xpath = ".//*[@id='selectBorderColor']")
	WebElement BorderColour;

	@FindBy(xpath = "//button[@class='btn btn-primary btn-sm saveLayer']")
	WebElement Text_okButton;

	@FindBy(xpath = "//*[@id='txtMarkupTitle']")
	WebElement MarkUpName;

	@FindBy(xpath = "//*[@id='saving-markup-btn']")
	WebElement MarkName_SaveButton;

	@FindBy(css = ".form-inline.pw-pager")
	WebElement NoOfViewerMaxValuesheet;

	@FindBy(xpath = "(//i [@class='icon icon-step-forward'])[2]")
	WebElement NextPageButton;

	// ============Open shape menu for Cloud Retangle============

	@FindBy(xpath = "//i [@class='icon icon-lg icon-shape_group']")
	WebElement OpenShapeMenu;

	@FindBy(xpath = "(//i[@class='icon icon-lg icon-cloud2'])[1]")
	WebElement DrowCloud;

	// ============Open shape menu for Hyper-link Circle============

	@FindBy(xpath = "html/body/div[1]/div[8]/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li[6]/a")
	WebElement OpenHyperlinkmenu;

	@FindBy(xpath = "html/body/div[1]/div[8]/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li[6]/ul/li[2]/a")
	WebElement Hyperlink_Circle;

	// =========Create Punch=========================

	@FindBy(xpath = "//i[@class='icon icon-lg icon-app-punch']")
	WebElement punchShortcuttools;

	@FindBy(xpath = "//*[@id='closeAddNewPunchStamp']")
	WebElement Stamp;

	@FindBy(xpath = "//button[@class ='btn btn-default btn-block']")
	WebElement AddNewButton;

	@FindBy(xpath = "//input[@id='txtShortDescription']")
	WebElement Stamp_Inbox;

	@FindBy(xpath = "//input[@id='txtTitleDescription']")
	WebElement Stamp_Title;

	@FindBy(xpath = "//i[@class='icon icon-ok']")
	WebElement RightButton;

	@FindBy(xpath = "//div[@id='divPunchAssignee']/div/a")
	WebElement AssignTo;

	@FindBy(xpath = "//*[@id='txtUserSearchKeyword']")
	WebElement SelectUse_Inbox;

	@FindBy(css = "input.pull-left")
	WebElement SelectUser_CheckBox;

	@FindBy(xpath = "//*[@id='divProjectUsers']/div/div[1]/div[2]/a[1]")
	WebElement Search_Button;

	@FindBy(xpath = "//button[@class='btn btn-primary' and @data-bind='click: SelectContactEvent']")
	WebElement SelectUser_Button;

	@FindBy(xpath = "//div[@id='divCC']/div[2]/a")
	WebElement CC_option;

	@FindBy(xpath = "//span [@class='glyphicon glyphicon-calendar icon-lg icon icon-calendar']")
	WebElement CalenderLink;

	@FindBy(xpath = "//div/div[1]/table/thead/tr[1]/th[3]")
	WebElement NextMonthDate;

	@FindBy(xpath = "//div/div[1]/table/tbody/tr[3]/td[4]")
	WebElement pickDate;

	@FindBy(xpath = "//textarea[@data-bind='value: PunchDescription']")
	WebElement Description;

	@FindBy(xpath = "//button[@id='add-local-file']")
	WebElement UploadFileTab;

	@FindBy(xpath = "//*[@id='add-project-file']")
	WebElement Projectfiles;

	@FindBy(xpath = "//*[@id='txtDocSearchKeyword']")
	WebElement SearchProjectfilesInbox;

	@FindBy(xpath = "//i [@class='icon icon-search icon-lg']")
	WebElement ProjectfilesTab_searchButton;

	@FindBy(xpath = "//*[@id='ulDocumentList']/li[1]/div[1]")
	WebElement Folder;

	@FindBy(xpath = "//i[@class='icon icon-file-alt icon-3x']")
	WebElement File_CheckBox;

	@FindBy(xpath = "(//i [@class='icon icon-file-alt icon-3x'])[1]")
	WebElement File_CheckBox1;

	@FindBy(xpath = "//*[@id='punchAttachedFiles']/span[1]")
	WebElement File_Upload_index;

	@FindBy(xpath = "(//i [@class='icon icon-file-alt icon-3x'])[1]")
	WebElement Projectfiles_FileCheckbox;

	// **************************Upload files************************

	@FindBy(xpath = ".//*[@id='add-local-file']")
	WebElement UploadTab;

	@FindBy(xpath = "//input[@type='file' and @name='qqfile']")
	WebElement ChooseFileButton;

	@FindBy(xpath = "//*[@id='btnCreate']")
	WebElement CreatePunch;

	@FindBy(css = ".col-sm-12>small")
	WebElement Aftersave_Email1;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[1]/div[2]/div/span[2]/small[1]")
	WebElement Aftersave_Email2;

	@FindBy(css = ".attachment.doc-view>span")
	WebElement Attachment;

	@FindBy(css = ".btn.btn-info")
	WebElement ClosePunch;

	// ==============Punch Owner Description=====================================

	@FindBy(css = ".punch-stamp.pull-left.punch-open")
	WebElement Shortdescription;

	@FindBy(css = ".col-md-11>div>h4")
	WebElement ShortNumber;

	@FindBy(css = ".col-sm-12>small")
	WebElement Afrerpunch_AssignTo;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[1]/div[2]/div/span[2]/small[1]")
	WebElement Afrerpunch_CCOption;

	@FindBy(xpath = "(//a[@class='attachment doc-view'])[1]")
	WebElement UplodedFileName1;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[3]/div/a[1]")
	WebElement UplodedFile1_download;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[3]/div/a[2]/span")
	WebElement UplodedFileName2;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[3]/div/a[2]")
	WebElement UplodedFile2_download;

	@FindBy(xpath = "//*[@id='imageViewer']/div[1]/div[1]/div/div[2]/img[2]")
	WebElement beforeImagearea;

	@FindBy(css = "circle.leaflet-clickable")
	WebElement afterImagearea;

	@FindBy(css = ".leaflet-marker-icon.icon.icon-rfi.icon-3x.pw-icon-lg-orange.leaflet-zoom-animated.leaflet-clickable")
	WebElement afterImagearea_RFI;

	@FindBy(xpath = "(//button [@class='btn btn-default'])[9]")
	WebElement CloseRFIByowner;

	@FindBy(xpath = "//*[@id='DocViewerHeader']")
	WebElement ClickOtherArea;

	@FindBy(xpath = ".//*[@id='spnPunchCount']")
	WebElement TotalPunch_Display;

	@FindBy(css = ".punch-stamp.punch-stamp-small.punch-open")
	WebElement Display_firstrow;

	@FindBy(css = "(//div [@class='punch-stamp punch-stamp-small punch-open'])[1]")
	WebElement shortNumber;

	@FindBy(xpath = "(//h4[@data-toggle='tooltip']/span)[1]")
	WebElement PunchClose_shortdescription;

	// ================ Punch shortCut=================
	@FindBy(xpath = "(//li[@id='liShowPunchList'])[1]")
	WebElement Punchshortcut;

	@FindBy(xpath = "(//ul[@id='ulPunchList']/li/div[2]/div[4]/span)[1]")
	WebElement Status;

	@FindBy(xpath = "(//ul[@id='ulPunchList']/li/div[2]/div/div)[1]")
	WebElement SheetNO;

	@FindBy(css = "span.punch-title")
	WebElement SheetDescription;

	@FindBy(xpath = "//*[@id='ulPunchList']/li[1]/div[2]/div[3]/div/strong")
	WebElement AttachementStatus;

	@FindBy(xpath = "//*[@id='ulPunchList']/li[1]/div[2]/div[3]/div/strong")
	WebElement Toemail;

	@FindBy(xpath = "//*[@id='txtComment']")
	WebElement Addcomment;

	@FindBy(xpath = "//*[@id='btnSubmit']")
	WebElement Submitbutton;

	@FindBy(xpath = "//*[@id='btnAttachement']")
	WebElement AttachmentLink;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[2]/span")
	WebElement TestDescription;

	@FindBy(xpath = "//span[@class='label label-success pull-right pw-status pw-status-small']")
	WebElement CloseButton;

	@FindBy(css = "#btnRefreshPunch")
	WebElement RefereshButton;

	@FindBy(xpath = "//div/div[2]/div[2]/div[1]/div[8]/div/input")
	WebElement NewcustomAttributeField;

	// ====================================Export Related Elements
	@FindBy(css = "#aPrivateProjects")
	WebElement PrivateProjectsTab;

	@FindBy(css = ".icon.icon-off")
	WebElement LogOut;

	@FindBy(css = "#button-1")
	WebElement YesButton;

	// =========Filename Selection=========================
	@FindBy(xpath = ".preview.document-preview-event")
	WebElement FileName_preview;

	// span[@class='noty_text']
	@FindBy(css = ".noty_text")
	WebElement notificationMsg;

	// ============================================

	@FindBy(css = "#FeedbackClose>span")
	WebElement FeedBackmeesage;

	@FindBy(xpath = "html/body/div[1]/div[3]/div[2]/div/ul/li[1]/div/section[1]")
	WebElement Project;

	@FindBy(xpath = "//*[@id='li_1KPavDU1%40lrrTmrIGviGoA%3d%3d']/div/section[1]")
	WebElement Project_test;

	@FindBy(xpath = "//*[@id='lirfiPunch']")
	WebElement ProjectManagement;

	@FindBy(xpath = "//*[@id='a_showRFIList']")
	WebElement RFI;

	@FindBy(xpath = "//i[@class='icon icon-rfi icon-lg']")
	WebElement revision_RFI;

	@FindBy(xpath = "//*[@class='aPunch']")
	WebElement punchmenu;

	@FindBy(xpath = ".//*[@id='liThirdTab']")
	WebElement AllPunchList;

	@FindBy(xpath = ".//*[@id='ulPunchList']/li[1]")
	WebElement PunchList;

	@FindBy(xpath = "html/body/div[1]/div[3]/div[3]/div[2]/ul/li[3]")
	WebElement Floder;

	@FindBy(xpath = "(//img[@class='img-responsive img-thumbnail' and @alt='drawings'])[1]")
	WebElement ImageClick;

	@FindBy(xpath = "(//img[@class='img-responsive img-thumbnail'and @alt='drawings'])[2]")
	WebElement ImageClick1;

	@FindBy(xpath = "//i[@class='icon icon-app-project icon-2x pulse animated']")
	WebElement ProjectManagement1;

	@FindBy(css = "#a_showRFIList")
	WebElement ProjectManagement_RFI;

	@FindBy(xpath = "//a[@title='All RFI']")
	WebElement RFITab;

	@FindBy(xpath = "(//img [@class='img-responsive img-thumbnail'])[1]")
	WebElement RFI_Firstrow;

	// ============create RFI=====================================

	@FindBy(xpath = "//i[@class='icon icon-rfi icon-lg']")
	WebElement REVISION_DROPDOWN;

	@FindBy(xpath = "//i [@class='icon icon-2x icon-rfi pw-icon-lg-orange']")
	WebElement REVISION_rfiiMAGE;

	@FindBy(xpath = "//i[@class='icon icon-lg icon-rfi']")
	WebElement RFI_Shortcut;

	@FindBy(xpath = "(//i[@class='icon icon-app-contact icon-lg'])[1]")
	WebElement RFI_TO;

	@FindBy(xpath = "(//i[@class='icon icon-app-contact icon-lg'])[2]")
	WebElement RFI_CC;

	@FindBy(xpath = "//*[@id='txtSubject']")
	WebElement RFI_SUBJECT;

	@FindBy(xpath = "//input[@ id='txtHasPotentialCostImpact']")
	WebElement PotentialCostImpact;

	@FindBy(xpath = "//input[@id='txtHasPotentialScheduleImpact']")
	WebElement PotentialScheduleImpact;

	@FindBy(css = "//*[@id='closeRfidiscipline']")
	WebElement RFI_Discipline;

	@FindBy(css = ".//*[@id='txtSheetNoData']")
	WebElement RFI_SheetNo;

	@FindBy(css = "//*[@id='txtSpecification']")
	WebElement RFI_Specification;

	@FindBy(xpath = "//*[@id='txtQuestion']")
	WebElement RFI_Question;

	@FindBy(xpath = "//*[@id='btnCreateRFI']")
	WebElement RFI_CreateButton;

	@FindBy(css = "#reassign_block_read-only>small")
	WebElement RFI_Email_TO;

	@FindBy(css = "#CCRecipient")
	WebElement RFI_Email_CC;

	@FindBy(xpath = "//*[@id='txtAnswerEditable']")
	WebElement AnswerEditBox;

	@FindBy(xpath = "(//i[@class='icon icon-paper-clip icon-lg'])[2]")
	WebElement AddAttachment;

	@FindBy(xpath = "//*[@id='btnCreateRFI']")
	WebElement ButtonRFI;

	@FindBy(xpath = "//*[@id='punchAttachedFiles']/span[1]")
	WebElement AttachementCount;

	@FindBy(xpath = "//*[@id='divEditRFIUI']/div/div[1]/div[1]/div[1]/div[3]/h4/span[2]")
	WebElement Subject;

	@FindBy(css = ".rfi-cont>span")
	WebElement RFI_Questions;

	@FindBy(xpath = "//span[@class='label label-warning pull-right pw-status']")
	WebElement RFI_Status;

	// ==========EmployeeLevel RFI

	@FindBy(xpath = "//*[@id='liRFIContainer']/a/b")
	WebElement RFI_Dropdown;

	@FindBy(xpath = "html/body/div[1]/div[8]/div/div[1]/div/ul[1]/li[5]/div/ul/li[1]/div/div[2]/h4/span")
	WebElement RFI_FirstRow;

	@FindBy(xpath = "html/body/div[1]/div[8]/div/div[1]/div/ul[1]/li[5]/div/ul/li[1]/div/div[2]/h4/span")
	WebElement RFI_SecondRow;

	@FindBy(xpath = "//*[@id='DocViewerHeader']/nav")
	WebElement clickotherside;
	// ============ Employee Level Punch

	@FindBy(xpath = "//i[@class='icon icon-app-punch icon-lg']")
	WebElement Punch_Dropdown;

	@FindBy(xpath = "(//h4 [@data-toggle='tooltip'])[1]")
	WebElement FirstRow;

	@FindBy(xpath = "(//h4 [@data-toggle='tooltip'])[2]")
	WebElement SecondRow;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[1]/div[1]/div[1]/div[3]/h4")
	WebElement Punch_Subject;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[1]/div[1]/div[1]/div[3]/h4")
	WebElement Punch_AssignTo;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[1]/div[2]/div/span[2]/small[1]")
	WebElement Punch_CC;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[2]/span")
	WebElement Punch_Description;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[3]/div/a[1]/span")
	WebElement Punch_Attachment1;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[3]/div/a[2]/span")
	WebElement Punch_Attachment2;

	@FindBy(css = "#txtComment")
	WebElement Punch_AddCommentInbox;

	@FindBy(css = "#btnAttachement")
	WebElement Punch_Attachment;

	@FindBy(css = "#btnSubmit")
	WebElement Punch_SubmitButton;

	@FindBy(xpath = "//span [@class='label label-warning pull-right pw-status pw-status-small']")
	WebElement Punch_EmployeeStatus;

	// ====================Logout====================================
	@FindBy(xpath = "//span [@class='img-circle profile-no-image md']")
	WebElement Profile;

	@FindBy(xpath = "(//a[@onclick='javascript:return confirmlogout(this);'])[2]")
	WebElement Logout;
	// ==============================Reassign========================

	@FindBy(xpath = "//button[@class='btn btn-default btn-small']")
	WebElement ReassignLink;

	@FindBy(xpath = "(//i [@class='icon icon-app-contact icon-lg'])[1]")
	WebElement AssitnTo_Mail;

	@FindBy(xpath = "(//button[@class='btn btn-default btn-sm'])[1]")
	WebElement RFI_SaveButton;

	@FindBy(xpath = "(//span [@class='label label-warning pull-right pw-status'])[1]")
	WebElement DisplayStatus;

	@FindBy(xpath = "(//span[@class='label label-warning pull-right pw-status'])[1]")
	WebElement OpenStatus;

	@FindBy(xpath = "(//span[@class='label label-warning pull-right pw-status'])[2]")
	WebElement OpenStatus_popUp;

	@FindBy(xpath = "(//span [@class='label label-success pull-right pw-status'])[1]")
	WebElement CloseStatus;

	@FindBy(xpath = "(//span [@class='label label-success pull-right pw-status'])[2]")
	WebElement Close_POPUP;

	@FindBy(xpath = "(//span [@class='label label-info pull-right pw-status'])[1]")
	WebElement EmployeeStatus;
	
	@FindBy(xpath = "(//button[@data-original-title='Close'])[1]")
	WebElement closebutton;

	@FindBy(xpath = "//button [@class='btn btn-default' and @data-bind='click: CloseRFI, clickBubble: false, visible: CanClose()']")
	WebElement CloseRFIButton;

	@FindBy(xpath = "//button [@ data-bind='click: ReopenRFI, clickBubble: false, visible: CanReopen()']")
	WebElement ReopenRFI;

	@FindBy(xpath = "//button [@data-bind='click: ForwardRFI, clickBubble: false, visible: CanForwardRFI()']")
	WebElement Forward;

	@FindBy(xpath = "//div/div/div[1]/div[1]/div[2]/button")
	WebElement Closebutton;

	@FindBy(xpath = "(//i [@class='icon icon-app-contact icon-lg'])[1]")
	WebElement Forward_Contact;

	@FindBy(xpath = " //textarea [@id='txtForwardComment']")
	WebElement TextArea;

	@FindBy(xpath = "//button[@data-bind='click: ForwardSendRFI, clickBubble: false, visible: CanForwardSendRFI()']")

	WebElement SendButton;

	/// ============simple execution================================
	@FindBy(xpath = " (//li[@data-projecttype='08G47q6dhw27QxejwQbFIilTlzSNKdtC'])[2]")
	WebElement project;

	@FindBy(xpath = "(//li[@data-pid='xfvWsv4l%40MhNI11BqgMYeg%3d%3d'])[3]")
	WebElement folder;
        
		
       
     	
		//==========================================================
				
		@Override
		protected void load() {
		isPageLoaded = true;
		
					
		}

		@Override
		protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
			}
					
		}
				
				/**
				 * Declaring constructor for initializing web elements using PageFactory class.
				 * @param driver
				 */
		public Viewer_ScreenRFI(WebDriver driver) {
				this.driver = driver;
					PageFactory.initElements(this.driver, this);
					 }
		
		
public boolean ObjectFeedback()
		{
			SkySiteUtils.waitForElement(driver, FeedBackmeesage, 20);
			Log.message("Waiting for FreeTrail Link to be appeared");
			if(FeedBackmeesage.isDisplayed())
				return true;
				else
				return false;
		}



public boolean NavigatonBacktodeshboard(String Project_Name,String Foldername) throws InterruptedException, AWTException, IOException{
	boolean result = false;
	
	SkySiteUtils.waitTill(10000);	
    int Avl_Fold_Count=0;	
	List<WebElement> allElements = driver.findElements(By.linkText("Process completed")); 
	for (WebElement Element : allElements) 
	{ 
		Avl_Fold_Count = Avl_Fold_Count+1; 	
	} 
	Log.message("Available Folder Count is: "+ Avl_Fold_Count);
	 
	if (Avl_Fold_Count==1)		{			
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, linkProcessCompleted, 300);		
		linkProcessCompleted.click();
		Log.message("Clicked on Process Completed Link." + Avl_Fold_Count);
		SkySiteUtils.waitTill(20000);
	}
	else
	{
		SkySiteUtils.waitTill(10000);	
		Log.message("go to dashboard page");		
		SkySiteUtils.waitTill(15000);
		SkySiteUtils.waitForElement(driver, PrivateProjectsTab,120);
		PrivateProjectsTab.click();
		Log.message("Private Projects button has been clicked!!!");
		SkySiteUtils.waitTill(5000);
	    //Getting count of available projects
		int Avl_Projects_Count=0;
		List<WebElement> allElements0 = driver.findElements(By.xpath("//*[contains(@id, 'PName_')]"));
		for (WebElement Element : allElements0)
		{ 
			Avl_Projects_Count =Avl_Projects_Count+1; 	
		}
		Log.message("Available private projects count is: "+Avl_Projects_Count);
		SkySiteUtils.waitTill(5000);
		
		for(int i=1;i<=Avl_Projects_Count;i++)
		{
			String Exp_ProjName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/ul/li["+i+"]/div/section[1]/h4")).getText();
			
	      //Validating - Expected project is selected or not
			if(Exp_ProjName.trim().contentEquals(Project_Name.trim()))
			{  	
				Log.message("Maching Project Found!!");
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/ul/li["+i+"]/div/section[1]/h4")).click();
				Log.message("Clicked on expected project!!");
				SkySiteUtils.waitTill(10000);
				break; 				
			}

		}
		SkySiteUtils.waitTill(5000);
		
		Log.message("Select Particular Project from Dashboard");
		SkySiteUtils.waitForElement(driver, btnAddNewFolder, 120);
		Log.message("Waiting for Create Folder button to be appeared");
		// Getting Folder count after created a new folder
		//int Avl_Fold_Count = 0;
		String actualFolderName = null;
		List<WebElement> allElements1 = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
		for (WebElement Element : allElements1) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		for (int j = 1; j <= Avl_Fold_Count; j++) {
			actualFolderName = driver
					.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
					.getText();
			Log.message("Exp Name:" + Foldername);
			Log.message("Act Name:" + actualFolderName);

			// Validating the new Folder is created or not
			if (Foldername.trim().contentEquals(actualFolderName.trim())) {
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
						.click();// Select a folder
				Log.message("Expected Folder is clicked successfully with name: " + Foldername);
				break;
			}
		}
		SkySiteUtils.waitTill(20000);
	}
	
	return result;

}




public boolean Logout() throws Throwable

{
	boolean result = false;
	Log.message("Entered into Logout Method");
	SkySiteUtils.waitTill(5000);
	Profile.click();
	Log.message("Profile Option Clicked Sucessfully");
	SkySiteUtils.waitTill(6000);  
	Logout.click();
	Log.message("Logout option Clicked Sucessfully");
	SkySiteUtils.waitTill(6000);
	YesButton.click();
	SkySiteUtils.waitTill(5000);	
	String actualTitle = "Sign in - SKYSITE";
	String expectedTitle = driver.getTitle();
	Log.message("expected title is:"+expectedTitle);
	if(actualTitle.equalsIgnoreCase(expectedTitle))
		return true;
	else
		return false;

}



public boolean RFI_Reassign() throws Throwable

{
	boolean result = false;
	Log.message("Reassign Link Clicked Sucessfully");
	SkySiteUtils.waitTill(10000);
	ReassignLink.click();
	SkySiteUtils.waitForElement(driver, AssitnTo_Mail, 60);
	AssitnTo_Mail.click();
	SkySiteUtils.waitTill(5000);
	Log.message("Entered the Value In Select Email field");
	String Select_Email = PropertyReader.getProperty("ReAssignTo");
	Log.message("Entered the Value In Select Email field"+Select_Email);
	SkySiteUtils.waitTill(2000);
	SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
	SelectUse_Inbox.sendKeys(Select_Email);		
	SkySiteUtils.waitForElement(driver, Search_Button, 60);
	SkySiteUtils.waitTill(2000);
	Search_Button.click();
	Log.message("Search Button Clicked Sucessfully");
	SkySiteUtils.waitTill(2000);
	SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
	SelectUser_CheckBox.click();
	Log.message("To Email Checkbox Clicked Sucessfully");
	SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
	SelectUser_Button.click();
	Log.message("Search User Button Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	RFI_SaveButton.click();
	Log.message("Save Button Clicked Sucessfully");
	SkySiteUtils.waitTill(6000);
	YesButton.click();
	Log.message("Yes Button Clicked Sucessfully");
	SkySiteUtils.waitTill(10000);
	SkySiteUtils.waitForElement(driver, RFI_Dropdown, 60);
	RFI_Dropdown.click();
	Log.message("RFI Drop Down Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_FirstRow, 30);
	SkySiteUtils.waitTill(5000);
	RFI_FirstRow.click();
	Log.message("RFI First Row Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, ClickOtherArea, 30);	
	ClickOtherArea.click();
	SkySiteUtils.waitTill(10000);
	afterImagearea_RFI.click();
	Log.message("Image Area Clicked Sucessfully");
	//Log.message(parentHandle);	
	SkySiteUtils.waitTill(20000);	
	return result;
	
}


public boolean RFI_POPUP_validation() throws Throwable

{
	boolean result = false;
	SkySiteUtils.waitTill(10000);
	
   //=================Email To=================
    SkySiteUtils.waitForElement(driver, RFI_Email_TO, 30);	
	String Email_To = RFI_Email_TO.getText();
	Log.message("Email_To Message is: "+ Email_To);
	String Email = PropertyReader.getProperty("Email_AssignTo");			
	if (Email_To.equalsIgnoreCase(Email))
		{
		Log.message("Email To verified Sucessfully ");
				
		}
		else
		{
		Log.message("Email To not verified Sucessfully");
		}
			
			
	//=================CC Option=================
	SkySiteUtils.waitForElement(driver, RFI_Email_CC, 30);	
	String Email_CC = RFI_Email_CC.getText();
	Log.message("Email_To Message is: "+ Email_CC);
	String CCoption = PropertyReader.getProperty("Email_CCOption");			
	if (Email_CC.equalsIgnoreCase(CCoption))
	{
	Log.message("Email To verified Sucessfully ");
				
	}
	else
	{
	Log.message("Email To not verified Sucessfully");
	}
	
	//=================Subject=================
	SkySiteUtils.waitForElement(driver, Subject, 30);	
	String subject = Subject.getText();			
	Log.message("subject Message is: "+ subject);
	String RFI_Subject = PropertyReader.getProperty("RFI_Subject");
			
	if (subject.equalsIgnoreCase(RFI_Subject))
	{
	Log.message("Subject verified Sucessfully ");
				
	}
	else
	{
	Log.message("Subject not verified Sucessfully");
	}
		
	//=================Question=================
	SkySiteUtils.waitForElement(driver, RFI_Questions, 30);	
	String RFI_Questions1 = RFI_Questions.getText();			
	Log.message("Question Message is: "+ RFI_Questions1);
	String RFI_Questions = PropertyReader.getProperty("RFI_Questions");			
	if (RFI_Questions1.equalsIgnoreCase(RFI_Questions))
	{
	Log.message("Question verified Sucessfully ");
				
	}
	else
	{
	Log.message("Question not verified Sucessfully");
	}
	//============OpenStatus==========================		
	SkySiteUtils.waitForElement(driver, RFI_Status, 30);		
	String Status = RFI_Status.getText();			
	Log.message("Status Message is: "+ Status);
	String status = PropertyReader.getProperty("Status");			
	if (Status.equalsIgnoreCase(status))
	{
	Log.message("Status verified Sucessfully ");				
	}
	else
	{
	Log.message("Status not verified Sucessfully");
	}
	SkySiteUtils.waitTill(3000);
	SkySiteUtils.waitForElement(driver, TestDescription, 30);
	String addComment = PropertyReader.getProperty("AddComment");
	Addcomment.sendKeys(addComment);
	Log.message("Entered Value In Add Comment inbox: "+ addComment);
	SkySiteUtils.waitTill(1000);
	Submitbutton.click();
	Log.message("submit button clicked sucessfully");			
			//SkySiteUtils.waitTill(3000);
    SkySiteUtils.waitTill(5000);
	String Commm=driver.findElement(By.xpath("html/body/div[1]/div[19]/div/div/div[2]/div/div[2]/ul/li/p[1]/span")).getText();
	Log.message("Comment name is:"+Commm);
	if(Commm.contains(addComment))
	return true;
	else
	return false;				

	
	
	
}





public boolean RFI_OwnerLevel_validation() throws Throwable

{
	boolean result = false;
	SkySiteUtils.waitTill(10000);
	Log.message("Owner level validation Method ");
	SkySiteUtils.waitForElement(driver, afterImagearea_RFI, 30);
	SkySiteUtils.waitTill(10000);
	afterImagearea_RFI.click();	
	Log.message("After Image Area Click Sucessfully ");
   //=================Email To=================
    SkySiteUtils.waitForElement(driver, RFI_Email_TO, 30);	
	String Email_To = RFI_Email_TO.getText();
	Log.message("Email_To Message is: "+ Email_To);
	String Email = PropertyReader.getProperty("Email_AssignTo");			
	if (Email_To.equalsIgnoreCase(Email))
		{
		Log.message("Email To verified Sucessfully ");
				
		}
		else
		{
		Log.message("Email To not verified Sucessfully");
		}
			
			
	//=================CC Option=================
	SkySiteUtils.waitForElement(driver, RFI_Email_CC, 30);	
	String Email_CC = RFI_Email_CC.getText();
	Log.message("Email_To Message is: "+ Email_CC);
	String CCoption = PropertyReader.getProperty("Email_CCOption");			
	if (Email_CC.equalsIgnoreCase(CCoption))
	{
	Log.message("Email To verified Sucessfully ");
				
	}
	else
	{
	Log.message("Email To not verified Sucessfully");
	}
	
	//=================Subject=================
	SkySiteUtils.waitForElement(driver, Subject, 30);	
	String subject = Subject.getText();			
	Log.message("subject Message is: "+ subject);
	String RFI_Subject = PropertyReader.getProperty("RFI_Subject");
			
	if (subject.equalsIgnoreCase(RFI_Subject))
	{
	Log.message("Subject verified Sucessfully ");
				
	}
	else
	{
	Log.message("Subject not verified Sucessfully");
	}
		
	//=================Question=================
	SkySiteUtils.waitForElement(driver, RFI_Questions, 30);	
	String RFI_Questions1 = RFI_Questions.getText();			
	Log.message("Question Message is: "+ RFI_Questions1);
	String RFI_Questions = PropertyReader.getProperty("RFI_Questions");			
	if (RFI_Questions1.equalsIgnoreCase(RFI_Questions))
	{
	Log.message("Question verified Sucessfully ");
				
	}
	else
	{
	Log.message("Question not verified Sucessfully");
	}
	//============OpenStatus==========================		
	SkySiteUtils.waitForElement(driver, RFI_Status, 30);		
	String Status = RFI_Status.getText();			
	Log.message("Status Message is: "+ Status);
	String status = PropertyReader.getProperty("Status");			
	if (Status.equalsIgnoreCase(status))
	{
	Log.message("Status verified Sucessfully ");				
	}
	else
	{
	Log.message("Status not verified Sucessfully");
	}
	SkySiteUtils.waitTill(3000);
	SkySiteUtils.waitForElement(driver, TestDescription, 30);
	String addComment = PropertyReader.getProperty("AddComment");
	Addcomment.sendKeys(addComment);
	Log.message("Entered Value In Add Comment inbox: "+ addComment);
	SkySiteUtils.waitTill(1000);
	Submitbutton.click();
	Log.message("submit button clicked sucessfully");			
			//SkySiteUtils.waitTill(3000);
    SkySiteUtils.waitTill(5000);
	String Commm=driver.findElement(By.xpath("html/body/div[1]/div[19]/div/div/div[2]/div/div[2]/ul/li/p[1]/span")).getText();
	Log.message("Comment name is:"+Commm);
	if(Commm.contains(addComment))
	return true;
	else
	return false;				

	
	
	
}


public boolean SecondRFI_Validation(String parentHandle) throws Throwable

{
	boolean result = false;
	SkySiteUtils.waitTill(5000);
	
	parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	
	if(revision_RFI.isDisplayed()){
		
		Log.message("Revision File Display Sucessfully");
	}
	
	else{
		Log.message("Revision File Not Displaying Here");
	}
	return result;
	
	

	
}

public boolean RFI_TestDataCreation_ownerLevel1(String parentHandle) throws Throwable

{
	boolean result = false;
	
	SkySiteUtils.waitTill(5000);
	parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);	
	SkySiteUtils.waitTill(5000);
	driver.switchTo().defaultContent();
	Log.message("Switch to Default frame ");
	   
		SkySiteUtils.waitForElement(driver, RFI_Shortcut, 60);
		SkySiteUtils.waitTill(5000);
		RFI_Shortcut.click();		
		SkySiteUtils.waitTill(8000);
		beforeImagearea.click();
		SkySiteUtils.waitTill(8000);	
		Log.message("Waiting for create RFI Menu to be appeared");
		
		//================  To ========================
		SkySiteUtils.waitForElement(driver, RFI_TO, 60);
		RFI_TO.click();
		Log.message(" To Link clicked Sucessfully");
		
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field"+Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);
		SelectUse_Inbox.sendKeys(Select_Email);		
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		//************CC Option---------------------------------------
		SkySiteUtils.waitTill(5000);
		RFI_CC.click();
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);	
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(5000);
		SelectUse_Inbox.sendKeys(SelectEmail);	
		SkySiteUtils.waitForElement(driver, Search_Button, 30);
		SkySiteUtils.waitTill(5000);
		Search_Button.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 30);
		SelectUser_CheckBox.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 30);
		SelectUser_Button.click();	
		//=====Subject==============================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 30);
		String Subject = PropertyReader.getProperty("RFI_Subject");
		RFI_SUBJECT.sendKeys(Subject);	
		/*//=====Discilipline=========================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Discipline, 30);
		String Discipline = PropertyReader.getProperty("RFI_Discipline");
		if (Discipline != "" && Discipline != null) {
		RFI_Discipline.sendKeys(Discipline);
		}
		//========Sheet No==============================
		
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_SheetNo, 30);
		String SheetNO = PropertyReader.getProperty("RFI_SheetNo");
		if (SheetNO != "" && SheetNO != null) {
			RFI_SheetNo.sendKeys(SheetNO);
		}
		//============ Specification====================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Specification, 30);
		String Specification = PropertyReader.getProperty("RFI_SPecification");
		RFI_Specification.sendKeys(Specification);
		*/
		//============ Question====================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Question, 30);
		String question= PropertyReader.getProperty("RFI_Questions");
		RFI_Question.sendKeys(question);
		//===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: "+FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
		Log.message("FileCount: "+FileCount);
		SkySiteUtils.waitTill(5000);
		//UploadFiles(FolderPath, FileCount);	
		SkySiteUtils.waitTill(3000);
		//projectfile_Upload1();		
		//======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitTill(2000);
		RFI_CreateButton.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 30);	
		String message = notificationMsg.getText();
		Log.message("RFI  Message is: "+   message);
		String expectedMessage = "RFI is added successfully";
		if (message.contains(expectedMessage))
		{
			result=true;
			Log.message("RFI is added successfully ");			
		}
		else
		{
			result=false;
			Log.message("RFI is  NOT added successfully ");
		}	
		SkySiteUtils.waitTill(5000);
		if(result==true)
			 return true;
		else
			return false;				

}




public boolean simpleexecutipn() throws Throwable

{
		boolean result = false;

		project.click();
		SkySiteUtils.waitTill(5000);
		folder.click();

		if (result == true)
			return true;
		else
			return false;		

}


public boolean RFI_firstRevision(String parentHandle) throws Throwable

{
	boolean result = false;
	
	SkySiteUtils.waitTill(5000);
	parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);	
	SkySiteUtils.waitTill(5000);
	driver.switchTo().defaultContent();	
		if(result==true)
			 return true;
		else
			return false;				

}





	public boolean MainStatus() throws Throwable

	{
		boolean result = false;
		Log.message("Enter into main status ");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, OpenStatus, 60);
		String Open = OpenStatus.getText();

		Log.message("Open Button Display: " + Open);

		if (Open.equalsIgnoreCase("OPEN")) {
			Log.message("Status Open Button verified Sucessfully ");

		} else {
			Log.message("Status Open not verified Sucessfully");
		}
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, CloseRFIButton, 100);
		CloseRFIButton.click();

		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, REVISION_DROPDOWN, 60);
		REVISION_DROPDOWN.click();
		Log.message("Revision Dropdown click sucessfully");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, REVISION_rfiiMAGE, 60);
		REVISION_rfiiMAGE.click();
		Log.message("Revision image click sucessfully");
		SkySiteUtils.waitTill(10000);
		afterImagearea_RFI.click();
		Log.message("RFI Image Area Clicked Sucessfully");

		SkySiteUtils.waitForElement(driver, CloseStatus, 60);
		String Close = CloseStatus.getText();

		Log.message("Close Button Display: " + CloseStatus);

		if (Close.equalsIgnoreCase("CLOSED")) {
			Log.message("Status CLOSED Button verified Sucessfully ");

		} else {
			Log.message("Status CLOSED not verified Sucessfully");
		}

		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, ReopenRFI, 60);
		ReopenRFI.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();

		SkySiteUtils.waitForElement(driver, REVISION_DROPDOWN, 60);
		REVISION_DROPDOWN.click();
		Log.message("Revision Dropdown click sucessfully");
		SkySiteUtils.waitForElement(driver, REVISION_rfiiMAGE, 60);
		REVISION_rfiiMAGE.click();
		Log.message("Revision image click sucessfully");
		SkySiteUtils.waitTill(10000);
		afterImagearea_RFI.click();
		Log.message("RFI Image Area Clicked Sucessfully");

		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, OpenStatus_popUp, 60);
		String Reopen1 = OpenStatus_popUp.getText();
		Log.message("Open Button Display: " + Reopen1);

		if (Reopen1.equalsIgnoreCase("RE-OPENED")) {
			Log.message("Status RE-OPENED POP UP verified Sucessfully ");

		} else {
			Log.message("Status RE-OPENED  POP UP not verified Sucessfully");
		}

		// ============================ forwaded===============================
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, Forward, 60);
		Forward.click();
		SkySiteUtils.waitForElement(driver, Forward, 60);
		Forward_Contact.click();
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TextArea, 60);
		TextArea.sendKeys("OK Verified");
		SkySiteUtils.waitForElement(driver, SendButton, 60);
		SendButton.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		if (afterImagearea_RFI.isDisplayed())
			return true;
		else
			return false;
	}

public boolean RFI_TestDataCreation_ownerLevel(String parentHandle) throws Throwable

{
		boolean result = false;

		parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitTill(5000);
		Log.message(parentHandle);
		SkySiteUtils.waitTill(5000);
		driver.switchTo().defaultContent();
		Log.message("Switch to Default frame ");

		SkySiteUtils.waitForElement(driver, RFI_Shortcut, 60);
		SkySiteUtils.waitTill(5000);
		RFI_Shortcut.click();
		SkySiteUtils.waitTill(8000);
		beforeImagearea.click();
		SkySiteUtils.waitTill(8000);
		Log.message("Waiting for create RFI Menu to be appeared");

		// ================ To ========================
		SkySiteUtils.waitForElement(driver, RFI_TO, 60);
		RFI_TO.click();
		Log.message(" To Link clicked Sucessfully");

		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(5000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		// ************CC Option---------------------------------------
		SkySiteUtils.waitTill(5000);
		RFI_CC.click();
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(5000);
		SelectUse_Inbox.sendKeys(SelectEmail);
		SkySiteUtils.waitForElement(driver, Search_Button, 30);
		SkySiteUtils.waitTill(5000);
		Search_Button.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 30);
		SelectUser_CheckBox.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 30);
		SelectUser_Button.click();
		// =====Subject==============================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 30);
		String Subject = PropertyReader.getProperty("RFI_Subject");
		RFI_SUBJECT.sendKeys(Subject);
		/*
		 * //=====Discilipline=========================
		 * SkySiteUtils.waitTill(2000); SkySiteUtils.waitForElement(driver,
		 * RFI_Discipline, 30); String Discipline =
		 * PropertyReader.getProperty("RFI_Discipline"); if (Discipline != "" &&
		 * Discipline != null) { RFI_Discipline.sendKeys(Discipline); }
		 * //========Sheet No==============================
		 * 
		 * SkySiteUtils.waitTill(2000); SkySiteUtils.waitForElement(driver,
		 * RFI_SheetNo, 30); String SheetNO =
		 * PropertyReader.getProperty("RFI_SheetNo"); if (SheetNO != "" &&
		 * SheetNO != null) { RFI_SheetNo.sendKeys(SheetNO); } //============
		 * Specification==================== SkySiteUtils.waitTill(2000);
		 * SkySiteUtils.waitForElement(driver, RFI_Specification, 30); String
		 * Specification = PropertyReader.getProperty("RFI_SPecification");
		 * RFI_Specification.sendKeys(Specification);
		 */
		// ============ Question====================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Question, 30);
		String question = PropertyReader.getProperty("RFI_Questions");
		RFI_Question.sendKeys(question);
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);// String to
																// Integer
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(5000);
		UploadFiles(FolderPath, FileCount);
		SkySiteUtils.waitTill(3000);
		projectfile_Upload();
		// ======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitTill(2000);
		RFI_CreateButton.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 30);
		String message = notificationMsg.getText();
		Log.message("RFI  Message is: " + message);
		String expectedMessage = "RFI is added successfully";
		if (message.contains(expectedMessage)) {
			result = true;
			Log.message("RFI is added successfully");
		} else {
			result = false;
			Log.message("RFI is NOT added successfully");
		}
		SkySiteUtils.waitTill(5000);
		if (result == true)
			return true;
		else
			return false;

	}



public boolean RFITestDataCreation_potentialChecked(String parentHandle) throws Throwable

{
	boolean result = false;
	
	parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);	
	SkySiteUtils.waitTill(5000);
	driver.switchTo().defaultContent();
	Log.message("Switch to Default frame ");
	   
		SkySiteUtils.waitForElement(driver, RFI_Shortcut, 60);
		SkySiteUtils.waitTill(5000);
		RFI_Shortcut.click();		
		SkySiteUtils.waitTill(8000);
		beforeImagearea.click();
		SkySiteUtils.waitTill(8000);	
		Log.message("Waiting for create RFI Menu to be appeared");
		
		//================  To ========================
		SkySiteUtils.waitForElement(driver, RFI_TO, 60);
		RFI_TO.click();
		Log.message(" To Link clicked Sucessfully");
		
		
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field"+Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);
		SelectUse_Inbox.sendKeys(Select_Email);	
		Log.message("Entered the Value In Select Email field");
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		//************CC Option---------------------------------------
		SkySiteUtils.waitTill(5000);
		RFI_CC.click();
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);		
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(5000);
		SelectUse_Inbox.sendKeys(SelectEmail);	
		Log.message("Entered the Value In Select  CC Email field");
		SkySiteUtils.waitForElement(driver, Search_Button, 30);
		SkySiteUtils.waitTill(5000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 30);
		SelectUser_CheckBox.click();
		Log.message("Select User Checkbox  Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 30);
		SelectUser_Button.click();	
		Log.message("Select User Button  Clicked Sucessfully");
		//=====Subject==============================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 30);
		String Subject = PropertyReader.getProperty("RFI_Subject");
		RFI_SUBJECT.sendKeys(Subject);
		Log.message("Entered Value In Select field:-"+Subject);
		if(PotentialCostImpact.isDisplayed()){
		SkySiteUtils.waitForElement(driver, PotentialCostImpact, 60);
		PotentialCostImpact.click();
		Log.message("Potential Cost Impact Checkbox Clicked Sucessfully");
		}

        if(PotentialScheduleImpact.isDisplayed()){
		SkySiteUtils.waitForElement(driver, PotentialScheduleImpact,60);
		PotentialScheduleImpact.click();
		Log.message("Potential Schedule Impact Checkbox Clicked Sucessfully");
		}
		//============ Question==========================================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Question, 30);
		String question= PropertyReader.getProperty("RFI_Questions");
		RFI_Question.sendKeys(question);
		Log.message("Entered Question in Question Field");
		//===External And Internal File Upload===========================
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: "+FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
		Log.message("FileCount: "+FileCount);
		SkySiteUtils.waitTill(5000);
		//UploadFiles(FolderPath, FileCount);	
		SkySiteUtils.waitTill(3000);
		//projectfile_Upload();		
		//======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitTill(2000);
		RFI_CreateButton.click();
		Log.message("Create Button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);	
		String message = notificationMsg.getText();
		/*Log.message("RFI  Message is: "+   message);
		String expectedMessage = "RFI annotation updated successfully";
		if (message.contains(expectedMessage))
		{
			result=true;
			Log.message("RFI annotation updated successfully");			
		}
		else
		{
			result=false;
			Log.message("RFI annotation not updated successfully ");
		}	*/
		SkySiteUtils.waitTill(5000);
		if(result==true)
			 return true;
		else
			return false;				

}



public boolean RFI_TestDataCreationLevel(String parentHandle,String Attribute) throws Throwable

{
	boolean result = false;
	
	parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);	
	SkySiteUtils.waitTill(5000);
	driver.switchTo().defaultContent();
	Log.message("Switch to Default frame ");
	   
		SkySiteUtils.waitForElement(driver, RFI_Shortcut, 60);
		SkySiteUtils.waitTill(5000);
		RFI_Shortcut.click();		
		SkySiteUtils.waitTill(8000);
		beforeImagearea.click();
		SkySiteUtils.waitTill(8000);	
		Log.message("Waiting for create RFI Menu to be appeared");
		
		//================  To ========================
		SkySiteUtils.waitForElement(driver, RFI_TO, 60);
		RFI_TO.click();
		Log.message(" To Link clicked Sucessfully");
		
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field"+Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);
		SelectUse_Inbox.sendKeys(Select_Email);		
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		//************CC Option---------------------------------------
		SkySiteUtils.waitTill(5000);
		RFI_CC.click();
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);	
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(5000);
		SelectUse_Inbox.sendKeys(SelectEmail);	
		SkySiteUtils.waitForElement(driver, Search_Button, 30);
		SkySiteUtils.waitTill(5000);
		Search_Button.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 30);
		SelectUser_CheckBox.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 30);
		SelectUser_Button.click();	
		//=====Subject==============================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 30);
		String Subject = PropertyReader.getProperty("RFI_Subject");
		RFI_SUBJECT.sendKeys(Subject);	
		
		//===========================new field created =========
		if(NewcustomAttributeField.isDisplayed()){
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, NewcustomAttributeField, 30);
		//String Attribute = PropertyReader.getProperty("RFI_Subject");
		NewcustomAttributeField.sendKeys(Attribute);
		}
		//============ Question====================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Question, 30);
		String question= PropertyReader.getProperty("RFI_Questions");
		RFI_Question.sendKeys(question);
		//===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: "+FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
		Log.message("FileCount: "+FileCount);
		SkySiteUtils.waitTill(5000);
		UploadFiles(FolderPath, FileCount);	
		SkySiteUtils.waitTill(3000);
		projectfile_Upload();		
		//======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitTill(2000);
		RFI_CreateButton.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 30);	
		String message = notificationMsg.getText();
		Log.message("RFI  Message is: "+   message);
		String expectedMessage = "RFI is added successfully";
		if (message.contains(expectedMessage))
		{
			result=true;
			Log.message("RFI is added successfully ");			
		}
		else
		{
			result=false;
			Log.message("RFI is  NOT added successfully ");
		}	
		SkySiteUtils.waitTill(5000);
		if(result==true)
			 return true;
		else
			return false;				

}

public boolean RFITestDataCreationLevel(String parentHandle,String subject) throws Throwable

{
	boolean result = false;
	
	parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);	
	SkySiteUtils.waitTill(5000);
	driver.switchTo().defaultContent();
	Log.message("Switch to Default frame ");
	   
		SkySiteUtils.waitForElement(driver, RFI_Shortcut, 60);
		SkySiteUtils.waitTill(5000);
		RFI_Shortcut.click();		
		SkySiteUtils.waitTill(8000);
		beforeImagearea.click();
		SkySiteUtils.waitTill(8000);	
		Log.message("Waiting for create RFI Menu to be appeared");
		
		//================  To ========================
		SkySiteUtils.waitForElement(driver, RFI_TO, 60);
		RFI_TO.click();
		Log.message(" To Link clicked Sucessfully");
		
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field"+Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);
		SelectUse_Inbox.sendKeys(Select_Email);		
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		//************CC Option---------------------------------------
		SkySiteUtils.waitTill(5000);
		RFI_CC.click();
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);	
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(5000);
		SelectUse_Inbox.sendKeys(SelectEmail);	
		SkySiteUtils.waitForElement(driver, Search_Button, 30);
		SkySiteUtils.waitTill(5000);
		Search_Button.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 30);
		SelectUser_CheckBox.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 30);
		SelectUser_Button.click();	
		//=====Subject==============================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 60);
		//String Subject = PropertyReader.getProperty("RFI_Subject");
		RFI_SUBJECT.sendKeys(subject);	
		
		
		//============ Question====================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Question, 30);
		String question= PropertyReader.getProperty("RFI_Questions");
		RFI_Question.sendKeys(question);
		//===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: "+FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
		Log.message("FileCount: "+FileCount);
		SkySiteUtils.waitTill(5000);
		UploadFiles(FolderPath, FileCount);	
		SkySiteUtils.waitTill(3000);
		projectfile_Upload();		
		//======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitTill(2000);
		RFI_CreateButton.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 30);	
		String message = notificationMsg.getText();
		Log.message("RFI  Message is: "+   message);
		String expectedMessage = "RFI is added successfully";
		if (message.contains(expectedMessage))
		{
			result=true;
			Log.message("RFI is added successfully ");			
		}
		else
		{
			result=false;
			Log.message("RFI is  NOT added successfully ");
		}	
		SkySiteUtils.waitTill(5000);
		if(result==true)
			 return true;
		else
			return false;				

}

public boolean RFI_ImageClick1() throws Throwable

{
	boolean result = false;
	Log.message("Entered into Owner level test");
	SkySiteUtils.waitTill(10000);					
	SkySiteUtils.waitForElement(driver, ImageClick, 60);
	ImageClick.click();
	Log.message("Image Link clicked.");
	SkySiteUtils.waitTill(10000);
	return result;
}


public boolean RFI_ImageClick2() throws Throwable

{
	boolean result = false;
	Log.message("Entered into Owner level test");
	SkySiteUtils.waitTill(10000);					
	SkySiteUtils.waitForElement(driver, ImageClick1, 60);
	ImageClick1.click();
	Log.message("Image2 Link clicked.");
	SkySiteUtils.waitTill(10000);
	return result;
}


public boolean RFI_RevisionFile(String parentHandle) throws Throwable

{
	boolean result = false;
	
	SkySiteUtils.waitTill(8000);
	parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);	
	SkySiteUtils.waitTill(5000);
	driver.switchTo().defaultContent();
	Log.message("Switch to Default frame ");
	SkySiteUtils.waitForElement(driver, REVISION_DROPDOWN, 60);
	REVISION_DROPDOWN.click();
	Log.message("Revision Dropdown click sucessfully");
	SkySiteUtils.waitForElement(driver, REVISION_rfiiMAGE, 60);
	REVISION_rfiiMAGE.click();
	Log.message("Revision image click sucessfully");
	return result;
	
}

public boolean RFI_TestDataRevisionFile_ownerLevel(String parentHandle) throws Throwable

{
	boolean result = false;
	
	SkySiteUtils.waitTill(8000);
	parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);	
	SkySiteUtils.waitTill(5000);
	driver.switchTo().defaultContent();
	Log.message("Switch to Default frame ");
	   
		SkySiteUtils.waitForElement(driver, RFI_Shortcut, 60);
		SkySiteUtils.waitTill(5000);
		RFI_Shortcut.click();		
		SkySiteUtils.waitTill(8000);
		beforeImagearea.click();
		SkySiteUtils.waitTill(8000);	
		Log.message("Waiting for create RFI Menu to be appeared");
		
		//================  To ========================
		SkySiteUtils.waitForElement(driver, RFI_TO, 60);
		RFI_TO.click();
		Log.message(" To Link clicked Sucessfully");
		
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field"+Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);
		SelectUse_Inbox.sendKeys(Select_Email);		
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		//************CC Option---------------------------------------
		SkySiteUtils.waitTill(5000);
		RFI_CC.click();
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);	
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(5000);
		SelectUse_Inbox.sendKeys(SelectEmail);	
		SkySiteUtils.waitForElement(driver, Search_Button, 30);
		SkySiteUtils.waitTill(5000);
		Search_Button.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 30);
		SelectUser_CheckBox.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 30);
		SelectUser_Button.click();	
		//=====Subject==============================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 30);
		String Subject = PropertyReader.getProperty("RFI_Subject");
		RFI_SUBJECT.sendKeys(Subject);	
		/*//=====Discilipline=========================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Discipline, 30);
		String Discipline = PropertyReader.getProperty("RFI_Discipline");
		if (Discipline != "" && Discipline != null) {
		RFI_Discipline.sendKeys(Discipline);
		}
		//========Sheet No==============================
		
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_SheetNo, 30);
		String SheetNO = PropertyReader.getProperty("RFI_SheetNo");
		if (SheetNO != "" && SheetNO != null) {
			RFI_SheetNo.sendKeys(SheetNO);
		}
		//============ Specification====================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Specification, 30);
		String Specification = PropertyReader.getProperty("RFI_SPecification");
		RFI_Specification.sendKeys(Specification);
		*/
		//============ Question====================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Question, 30);
		String question= PropertyReader.getProperty("RFI_Questions");
		RFI_Question.sendKeys(question);
		//===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: "+FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
		Log.message("FileCount: "+FileCount);
		SkySiteUtils.waitTill(5000);
		UploadFiles(FolderPath, FileCount);	
		SkySiteUtils.waitTill(3000);
		projectfile_Upload();		
		//======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitTill(2000);
		RFI_CreateButton.click();
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 30);	
		String message = notificationMsg.getText();
		Log.message("RFI  Message is: "+   message);
		String expectedMessage = "RFI is added successfully";
		if (message.contains(expectedMessage))
		{
			Log.message("RFI is added successfully ");			
		}
		else
		{
			Log.message("RFI is  NOT added successfully ");
		}	
		
		
		
		
		return result;

}








public boolean PUNCH_Closed_OwnerLevel(String parentHandle) throws Throwable

{
	boolean result = false;	
	
	/*SkySiteUtils.waitTill(10000);					
	SkySiteUtils.waitForElement(driver, ImageClick, 60);
	ImageClick.click();
	Log.message("Image Link clicked.");*/
	SkySiteUtils.waitTill(8000);
	parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);	
	SkySiteUtils.waitTill(5000);
	driver.switchTo().defaultContent();
	Log.message("Switch to Default frame ");
	SkySiteUtils.waitForElement(driver, Punch_Dropdown, 30);	
	Punch_Dropdown.click();
	Log.message("Punch Drop Down Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, FirstRow, 30);
	SkySiteUtils.waitTill(5000);
	FirstRow.click();
	Log.message("Punch First Row Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, ClickOtherArea, 30);	
	ClickOtherArea.click();
	SkySiteUtils.waitTill(10000);
	afterImagearea.click();
	Log.message("Image Area Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	ClosePunch.click();
	Log.message("Close punch Button Clicked Sucessfully");
	SkySiteUtils.waitForElement(driver, YesButton, 30);
	SkySiteUtils.waitTill(2000);
	YesButton.click();
	Log.message("Yes Button Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, notificationMsg, 60);	
	String message1 = notificationMsg.getText();
	Log.message("punch after saving Message is: "+   message1);	
	SkySiteUtils.waitTill(5000);
	String expectedMessage1 = "Punch annotation saved successfully";
	/*if (message1.contains(expectedMessage1))
	{
		Log.message("Punch annotation saved successfully");
		
	}
	else
	{
		Log.message("Punch annotation not saved successfully");
	}
	*/
	
	Punch_Dropdown.click();
	Log.message("Punch Drop Down Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);	
	RefereshButton.click();
	Log.message("Referesh Button Clicked Sucessfully");
	
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, CloseButton, 60);	
	String Close= CloseButton.getText();
	Log.message("Close Button Display: "+  Close);

	if (Close.contains("CLOSED"))
	{
		Log.message("Status CLOSED verified Sucessfully ");
		
	}
	else
	{
		Log.message("Status Closed not verified Sucessfully");
	}
	
	SkySiteUtils.waitTill(5000);
	
	return result;
}




public boolean PUNCH_EmployeeLevel_VALIDATION(String parentHandle) throws Throwable

{
	boolean result = false;	
	
	
	SkySiteUtils.waitTill(10000);
	 parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);	
	SkySiteUtils.waitTill(5000);
	driver.switchTo().defaultContent();
	SkySiteUtils.waitForElement(driver, Punch_Dropdown, 30);
	
	Punch_Dropdown.click();
	Log.message("Punch Drop Down Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, FirstRow, 30);
	SkySiteUtils.waitTill(5000);
	FirstRow.click();
	Log.message("Punch First Row Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, ClickOtherArea, 30);	
	ClickOtherArea.click();
	SkySiteUtils.waitTill(10000);
	afterImagearea.click();
	Log.message("Image Area Clicked Sucessfully");		
	SkySiteUtils.waitTill(20000);
	
	 //=================Subject headline=================
		SkySiteUtils.waitForElement(driver, Punch_Subject, 30);	
		String subject = Punch_Subject.getText();			
		Log.message("subject Message is: "+ subject);
		String punch_Subject = PropertyReader.getProperty("Punch_Subject");
		
		if (subject.equalsIgnoreCase(punch_Subject))
		{
			Log.message("Subject verified Sucessfully ");
			
		}
		else
		{
			Log.message("Subject not verified Sucessfully");
		}
	//=================Email To=================
	SkySiteUtils.waitForElement(driver, Punch_AssignTo, 30);	
	String Email_To = Punch_AssignTo.getText();
	Log.message("Email_To Message is: "+ Email_To);
	String Email = PropertyReader.getProperty("Email_AssignTo");
	
	if (Email_To.equalsIgnoreCase(Email))
	{
		Log.message("Email To verified Sucessfully ");
		
	}
	else
	{
		Log.message("Email To not verified Sucessfully");
	}
	
	
	 //=================CC Option=================
	SkySiteUtils.waitForElement(driver, Punch_CC, 30);	
	String Email_CC = Punch_CC.getText();
	Log.message("Email_To Message is: "+ Email_CC);
	String CCoption = PropertyReader.getProperty("Email_CCOption");
	
	if (Email_CC.equalsIgnoreCase(CCoption))
	{
		Log.message("Email To verified Sucessfully ");
		
	}
	else
	{
		Log.message("Email To not verified Sucessfully");
	}

	//=================================================
	
	//=================Description=================
		SkySiteUtils.waitForElement(driver, Punch_Description, 30);	
		String Description = Punch_Description.getText();
		Log.message("Email_To Message is: "+ Email_CC);
		String desctiption = PropertyReader.getProperty("Desctiption");
		
		if (Description.equalsIgnoreCase(desctiption))
		{
			Log.message("Description verified Sucessfully ");
			
		}
		else
		{
			Log.message("Description not verified Sucessfully");
		}

	
	
	
      	Log.message("Add Attachement first File Name Display "+Punch_Attachment1);
      	
      	Log.message("Add Attachement first File Name Display "+Punch_Attachment2);
      	//============= Entered Value In comment Field=======================
      	String comment = PropertyReader.getProperty("AddComment");
      	Punch_AddCommentInbox.sendKeys(comment);
      	SkySiteUtils.waitTill(3000);
      	//===========Clicked  Button==============================
      	Punch_Attachment.click();
      	SkySiteUtils.waitTill(5000);
	     //===External And Internal File Upload
	    String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: "+FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
		Log.message("FileCount: "+FileCount);
		SkySiteUtils.waitTill(5000);
		UploadFiles(FolderPath, FileCount);	
		SkySiteUtils.waitTill(3000);
		projectfile_Upload();
		SkySiteUtils.waitTill(10000);
		CreatePunch.click();			
		SkySiteUtils.waitTill(10000);
	    Log.message("Upload And Attached Button Clicked Sucessfully");	
	    SkySiteUtils.waitTill(10000);
	    //String Count = AttachementCount.getText();	
	    //Log.message("Attachement Count Verified Sucessfully:"+ Count);
	
	return result;	
	
}

public boolean PUNCH_EmployeeLevel_VALIDATION1(String parentHandle) throws Throwable

{
	boolean result = false;	
	
	/*SkySiteUtils.waitTill(10000);
	ImageClick.click();
	Log.message("Image Link clicked.");*/
	SkySiteUtils.waitTill(10000);
	 parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);	
	SkySiteUtils.waitTill(5000);
	driver.switchTo().defaultContent();
	SkySiteUtils.waitForElement(driver, Punch_Dropdown, 30);
	
	Punch_Dropdown.click();
	Log.message("Punch Drop Down Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, FirstRow, 30);
	SkySiteUtils.waitTill(5000);
	SecondRow.click();
	Log.message("Punch First Row Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, ClickOtherArea, 30);	
	ClickOtherArea.click();
	SkySiteUtils.waitTill(10000);
	afterImagearea.click();
	Log.message("Image Area Clicked Sucessfully");		
	SkySiteUtils.waitTill(20000);
	
	 //=================Subject headline=================
		SkySiteUtils.waitForElement(driver, Punch_Subject, 30);	
		String subject = Punch_Subject.getText();			
		Log.message("subject Message is: "+ subject);
		String punch_Subject = PropertyReader.getProperty("Punch_Subject");
		
		if (subject.equalsIgnoreCase(punch_Subject))
		{
			Log.message("Subject verified Sucessfully ");
			
		}
		else
		{
			Log.message("Subject not verified Sucessfully");
		}
	//=================Email To=================
	SkySiteUtils.waitForElement(driver, Punch_AssignTo, 30);	
	String Email_To = Punch_AssignTo.getText();
	Log.message("Email_To Message is: "+ Email_To);
	String Email = PropertyReader.getProperty("Email_AssignTo");
	
	if (Email_To.equalsIgnoreCase(Email))
	{
		Log.message("Email To verified Sucessfully ");
		
	}
	else
	{
		Log.message("Email To not verified Sucessfully");
	}
	
	
	 //=================CC Option=================
	SkySiteUtils.waitForElement(driver, Punch_CC, 30);	
	String Email_CC = Punch_CC.getText();
	Log.message("Email_To Message is: "+ Email_CC);
	String CCoption = PropertyReader.getProperty("Email_CCOption");
	
	if (Email_CC.equalsIgnoreCase(CCoption))
	{
		Log.message("Email To verified Sucessfully ");
		
	}
	else
	{
		Log.message("Email To not verified Sucessfully");
	}

	//=================================================
	
	//=================Description=================
		SkySiteUtils.waitForElement(driver, Punch_Description, 30);	
		String Description = Punch_Description.getText();
		Log.message("Email_To Message is: "+ Email_CC);
		String desctiption = PropertyReader.getProperty("Desctiption");
		
		if (Description.equalsIgnoreCase(desctiption))
		{
			Log.message("Description verified Sucessfully ");
			
		}
		else
		{
			Log.message("Description not verified Sucessfully");
		}

	
	
	
      	Log.message("Add Attachement first File Name Display "+Punch_Attachment1);
      	
      	Log.message("Add Attachement first File Name Display "+Punch_Attachment2);
      	//============= Entered Value In comment Field=======================
      	String comment = PropertyReader.getProperty("AddComment");
      	Punch_AddCommentInbox.sendKeys(comment);
      	SkySiteUtils.waitTill(3000);
      	//===========Clicked Attachment Button==============================
      	Punch_Attachment.click();
      	SkySiteUtils.waitTill(5000);
	     //===External And Internal File Upload
	    String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: "+FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
		Log.message("FileCount: "+FileCount);
		SkySiteUtils.waitTill(5000);
		UploadFiles(FolderPath, FileCount);	
		SkySiteUtils.waitTill(3000);
		projectfile_Upload1();
		SkySiteUtils.waitTill(10000);
		CreatePunch.click();			
		SkySiteUtils.waitTill(10000);
	    Log.message("Upload And Attached Button Clicked Sucessfully");	
	    SkySiteUtils.waitTill(10000);
	    //String Count = AttachementCount.getText();	
	    //Log.message("Attachement Count Verified Sucessfully:"+ Count);
	
	return result;	
	
}


public boolean CloseRFI(String parentHandle) throws Throwable

{
	boolean result = false;	
	
	SkySiteUtils.waitTill(2000);					
	SkySiteUtils.waitForElement(driver, ImageClick, 30);
	ImageClick.click();
	Log.message("Image Link clicked.");
	SkySiteUtils.waitTill(2000);
	 parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(3000);
	Log.message(parentHandle);
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_Dropdown, 30);
	RFI_Dropdown.click();
	Log.message("RFI Drop Down Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_FirstRow, 30);
	SkySiteUtils.waitTill(5000);
	RFI_FirstRow.click();
	Log.message("RFI First Row Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, ClickOtherArea, 30);	
	ClickOtherArea.click();
	SkySiteUtils.waitTill(10000);
	afterImagearea_RFI.click();
	Log.message("Image Area Clicked Sucessfully");
	//Log.message(parentHandle);	
	SkySiteUtils.waitTill(20000);
	
	CloseRFIByowner.click();
	Log.message("Close RFI Button Clicked Sucessfully");
	return result;

}






public boolean RFI_Closed(String parentHandle) throws Throwable

{
	boolean result = false;	
	
	SkySiteUtils.waitTill(2000);					
	SkySiteUtils.waitForElement(driver, ImageClick, 30);
	ImageClick.click();
	Log.message("Image Link clicked.");
	SkySiteUtils.waitTill(2000);
	 parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(3000);
	Log.message(parentHandle);
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_Dropdown, 30);
	RFI_Dropdown.click();
	Log.message("RFI Drop Down Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_FirstRow, 30);
	SkySiteUtils.waitTill(5000);
	RFI_FirstRow.click();
	Log.message("RFI First Row Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, ClickOtherArea, 30);	
	ClickOtherArea.click();
	SkySiteUtils.waitTill(10000);
	afterImagearea_RFI.click();
	Log.message("Image Area Clicked Sucessfully");
	//Log.message(parentHandle);	
	SkySiteUtils.waitTill(10000);	
	CloseRFIByowner.click();
	Log.message("Closed RFI By Owner Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	return result;
}

public boolean RFI_EMPLOYEEVALIDATION(String parentHandle) throws Throwable

{
	boolean result = false;	
	
	SkySiteUtils.waitTill(5000);					
	
	 parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_Dropdown, 30);
	RFI_Dropdown.click();
	Log.message("RFI Drop Down Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_FirstRow, 30);
	SkySiteUtils.waitTill(5000);
	FirstRow.click();
	Log.message("RFI First Row Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, ClickOtherArea, 30);	
	ClickOtherArea.click();
	SkySiteUtils.waitTill(10000);
	afterImagearea_RFI.click();
	Log.message("Image Area Clicked Sucessfully");
	//Log.message(parentHandle);	
	SkySiteUtils.waitTill(20000);
	
	 //=================Subject headline=================
		SkySiteUtils.waitForElement(driver, Subject, 30);	
		String subject = Subject.getText();			
		Log.message("subject Message is: "+ subject);
		String RFI_Subject = PropertyReader.getProperty("RFI_Subject");
		
		if (subject.equalsIgnoreCase(RFI_Subject))
		{
			Log.message("Subject verified Sucessfully ");
			
		}
		else
		{
			Log.message("Subject not verified Sucessfully");
		}
	//=================Email To=================
	SkySiteUtils.waitForElement(driver, RFI_Email_TO, 30);	
	String Email_To = RFI_Email_TO.getText();
	Log.message("Email_To Message is: "+ Email_To);
	String Email = PropertyReader.getProperty("Email_AssignTo");
	
	if (Email_To.equalsIgnoreCase(Email))
	{
		Log.message("Email To verified Sucessfully ");
		
	}
	else
	{
		Log.message("Email To not verified Sucessfully");
	}
	
	
	 //=================CC Option=================
	SkySiteUtils.waitForElement(driver, RFI_Email_CC, 30);	
	String Email_CC = RFI_Email_CC.getText();
	Log.message("Email_To Message is: "+ Email_CC);
	String CCoption = PropertyReader.getProperty("Email_CCOption");
	
	if (Email_CC.equalsIgnoreCase(CCoption))
	{
		Log.message("Email To verified Sucessfully ");
		
	}
	else
	{
		Log.message("Email To not verified Sucessfully");
	}

	String Answers = PropertyReader.getProperty("Answer");
	AnswerEditBox.sendKeys(Answers);
	Log.message("Entered Ans in inbox Field");
	SkySiteUtils.waitForElement(driver, AddAttachment, 30);	
	AddAttachment.click();	
	Log.message("Add Attachement Clicked Sucessfully");
	
	//===External And Internal File Upload
	String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
	Log.message("FolderPath: "+FolderPath);
	String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
	int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
	Log.message("FileCount: "+FileCount);
	SkySiteUtils.waitTill(5000);
	//Log.assertThat( UploadFiles(FolderPath, FileCount), "Upload file working Successfully","Upload file not working Successfully", driver);
	UploadFiles(FolderPath, FileCount);	
	SkySiteUtils.waitTill(3000);
	//Log.assertThat( projectfile_Upload(), "project internal file working Successfully","project internal file not working Successfully", driver);
	projectfile_Upload();
	SkySiteUtils.waitTill(10000);;
	ButtonRFI.click();
	Log.message("Upload And Attached Button Clicked Sucessfully");	
	//CommonMethod.Fluentwait(AttachementCount, 120, 5);
	SkySiteUtils.waitTill(10000);;
	String Count = AttachementCount.getText();	
	Log.message("Attachement Count Verified Sucessfully:"+ Count);
	
	//=============Reassign Method=======
	SkySiteUtils.waitTill(3000);
	
	//Log.assertThat( RFI_Reassign(), "RFI Reassign working Successfully","Reassign not working Successfully", driver);
 	
	RFI_Reassign();
	
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_Email_TO, 30);	
	String Email1_To = RFI_Email_TO.getText();
	Log.message("Reassign Email  Message is: "+ Email1_To);
	String Email1 = PropertyReader.getProperty("ReAssign");
	
	if (Email_To.equalsIgnoreCase(Email1))
	{
		Log.message("Email To verified Sucessfully ");
		//result=true;
	}
	else
	{
		Log.message("Email To not verified Sucessfully");
		//result= false;
	}
	
	
	SkySiteUtils.waitTill(5000);
	if(afterImagearea_RFI.isDisplayed())
		 return true;
	else
		return false;	
	
	/*//====Notification Message RFI Added Sucessfully validation
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, notificationMsg, 60);	
	String message = notificationMsg.getText();
	Log.message("RFI  Message is: "+   message);
	String expectedMessage = "RFI is added successfully";
	if (message.contains(expectedMessage))
	{
		Log.message("RFI is added successfully ");			
	}
	else
	{
		Log.message("RFI is  NOT added successfully ");
	}	
	
	*/
	//return result;	
	
}


public boolean RFI_EMPLOYEEVALIDATION1(String parentHandle) throws Throwable

{
	boolean result = false;	
	
	SkySiteUtils.waitTill(5000);					
	/*SkySiteUtils.waitForElement(driver, ImageClick, 60);
	ImageClick.click();
	Log.message("Image Link clicked.");
	SkySiteUtils.waitTill(3000);*/
	 parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_Dropdown, 30);
	RFI_Dropdown.click();
	Log.message("RFI Drop Down Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_FirstRow, 30);
	SkySiteUtils.waitTill(5000);
	FirstRow.click();
	Log.message("RFI First Row Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, ClickOtherArea, 30);	
	ClickOtherArea.click();
	SkySiteUtils.waitTill(10000);
	afterImagearea_RFI.click();
	Log.message("Image Area Clicked Sucessfully");
	//Log.message(parentHandle);	
	SkySiteUtils.waitTill(20000);
	
	 //=================Subject headline=================
		SkySiteUtils.waitForElement(driver, Subject, 30);	
		String subject = Subject.getText();			
		Log.message("subject Message is: "+ subject);
		String RFI_Subject = PropertyReader.getProperty("RFI_Subject");
		
		if (subject.equalsIgnoreCase(RFI_Subject))
		{
			Log.message("Subject verified Sucessfully ");
			
		}
		else
		{
			Log.message("Subject not verified Sucessfully");
		}
	//=================Email To=================
	SkySiteUtils.waitForElement(driver, RFI_Email_TO, 30);	
	String Email_To = RFI_Email_TO.getText();
	Log.message("Email_To Message is: "+ Email_To);
	String Email = PropertyReader.getProperty("Email_AssignTo");
	
	if (Email_To.equalsIgnoreCase(Email))
	{
		Log.message("Email To verified Sucessfully ");
		
	}
	else
	{
		Log.message("Email To not verified Sucessfully");
	}
	
	
	 //=================CC Option=================
	SkySiteUtils.waitForElement(driver, RFI_Email_CC, 30);	
	String Email_CC = RFI_Email_CC.getText();
	Log.message("Email_To Message is: "+ Email_CC);
	String CCoption = PropertyReader.getProperty("Email_CCOption");
	
	if (Email_CC.equalsIgnoreCase(CCoption))
	{
		Log.message("Email To verified Sucessfully ");
		
	}
	else
	{
		Log.message("Email To not verified Sucessfully");
	}

	String Answers = PropertyReader.getProperty("Answer");
	AnswerEditBox.sendKeys(Answers);
	Log.message("Entered Ans in inbox Field");
	SkySiteUtils.waitForElement(driver, AddAttachment, 30);	
	AddAttachment.click();	
	Log.message("Add Attachement Clicked Sucessfully");
	
	//===External And Internal File Upload
	String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
	Log.message("FolderPath: "+FolderPath);
	String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
	int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
	Log.message("FileCount: "+FileCount);
	SkySiteUtils.waitTill(5000);
	//Log.assertThat( UploadFiles(FolderPath, FileCount), "Upload file working Successfully","Upload file not working Successfully", driver);
	UploadFiles(FolderPath, FileCount);	
	SkySiteUtils.waitTill(3000);
	//Log.assertThat( projectfile_Upload(), "project internal file working Successfully","project internal file not working Successfully", driver);
	projectfile_Upload();
	SkySiteUtils.waitTill(10000);;
	ButtonRFI.click();
	Log.message("Upload And Attached Button Clicked Sucessfully");	
	//CommonMethod.Fluentwait(AttachementCount, 120, 5);
	SkySiteUtils.waitTill(10000);;
	String Count = AttachementCount.getText();	
	Log.message("Attachement Count Verified Sucessfully:"+ Count);
	
	//=============Reassign Method=======
	SkySiteUtils.waitTill(3000);
	
	//Log.assertThat( RFI_Reassign(), "RFI Reassign working Successfully","Reassign not working Successfully", driver);
 	
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_Email_TO, 30);	
	String Email1_To = RFI_Email_TO.getText();
	Log.message("Reassign Email  Message is: "+ Email1_To);
	String Email1 = PropertyReader.getProperty("ReAssign");
	
	if (Email_To.equalsIgnoreCase(Email1))
	{
		Log.message("Email To verified Sucessfully ");
		//result=true;
	}
	else
	{
		Log.message("Email To not verified Sucessfully");
		//result= false;
	}
	
	
	SkySiteUtils.waitTill(5000);
	if(afterImagearea_RFI.isDisplayed())
		 return true;
	else
		return false;	
	
	
	
}

public boolean RFI_EMPLOYEEVALIDATION2(String parentHandle) throws Throwable

{
	boolean result = false;	
	
	SkySiteUtils.waitTill(5000);					
	/*SkySiteUtils.waitForElement(driver, ImageClick, 60);
	ImageClick.click();
	Log.message("Image Link clicked.");
	SkySiteUtils.waitTill(3000);*/
	 parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_Dropdown, 30);
	RFI_Dropdown.click();
	Log.message("RFI Drop Down Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_FirstRow, 30);
	SkySiteUtils.waitTill(5000);
	SecondRow.click();
	Log.message("RFI First Row Menu Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, ClickOtherArea, 30);	
	ClickOtherArea.click();
	SkySiteUtils.waitTill(10000);
	afterImagearea_RFI.click();
	Log.message("Image Area Clicked Sucessfully");
	//Log.message(parentHandle);	
	SkySiteUtils.waitTill(20000);
	
	 //=================Subject headline=================
		SkySiteUtils.waitForElement(driver, Subject, 30);	
		String subject = Subject.getText();			
		Log.message("subject Message is: "+ subject);
		String RFI_Subject = PropertyReader.getProperty("RFI_Subject");
		
		if (subject.equalsIgnoreCase(RFI_Subject))
		{
			Log.message("Subject verified Sucessfully ");
			
		}
		else
		{
			Log.message("Subject not verified Sucessfully");
		}
	//=================Email To=================
	SkySiteUtils.waitForElement(driver, RFI_Email_TO, 30);	
	String Email_To = RFI_Email_TO.getText();
	Log.message("Email_To Message is: "+ Email_To);
	String Email = PropertyReader.getProperty("Email_AssignTo");
	
	if (Email_To.equalsIgnoreCase(Email))
	{
		Log.message("Email To verified Sucessfully ");
		
	}
	else
	{
		Log.message("Email To not verified Sucessfully");
	}
	
	
	 //=================CC Option=================
	SkySiteUtils.waitForElement(driver, RFI_Email_CC, 30);	
	String Email_CC = RFI_Email_CC.getText();
	Log.message("Email_To Message is: "+ Email_CC);
	String CCoption = PropertyReader.getProperty("Email_CCOption");
	
	if (Email_CC.equalsIgnoreCase(CCoption))
	{
		Log.message("Email To verified Sucessfully ");
		
	}
	else
	{
		Log.message("Email To not verified Sucessfully");
	}

	String Answers = PropertyReader.getProperty("Answer");
	AnswerEditBox.sendKeys(Answers);
	Log.message("Entered Ans in inbox Field");
	SkySiteUtils.waitForElement(driver, AddAttachment, 30);	
	AddAttachment.click();	
	Log.message("Add Attachement Clicked Sucessfully");
	
	//===External And Internal File Upload
	String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
	Log.message("FolderPath: "+FolderPath);
	String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
	int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
	Log.message("FileCount: "+FileCount);
	SkySiteUtils.waitTill(5000);
	//Log.assertThat( UploadFiles(FolderPath, FileCount), "Upload file working Successfully","Upload file not working Successfully", driver);
	UploadFiles(FolderPath, FileCount);	
	SkySiteUtils.waitTill(3000);
	//Log.assertThat( projectfile_Upload(), "project internal file working Successfully","project internal file not working Successfully", driver);
	projectfile_Upload1();
	SkySiteUtils.waitTill(10000);;
	ButtonRFI.click();
	Log.message("Upload And Attached Button Clicked Sucessfully");	
	//CommonMethod.Fluentwait(AttachementCount, 120, 5);
	SkySiteUtils.waitTill(10000);;
	String Count = AttachementCount.getText();	
	Log.message("Attachement Count Verified Sucessfully:"+ Count);
	
	//=============Reassign Method=======
	SkySiteUtils.waitTill(3000);
	
	//Log.assertThat( RFI_Reassign(), "RFI Reassign working Successfully","Reassign not working Successfully", driver);
 	
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, RFI_Email_TO, 30);	
	String Email1_To = RFI_Email_TO.getText();
	Log.message("Reassign Email  Message is: "+ Email1_To);
	String Email1 = PropertyReader.getProperty("ReAssign");
	
	if (Email_To.equalsIgnoreCase(Email1))
	{
		Log.message("Email To verified Sucessfully ");
		//result=true;
	}
	else
	{
		Log.message("Email To not verified Sucessfully");
		//result= false;
	}
	
	
	SkySiteUtils.waitTill(5000);
	if(afterImagearea_RFI.isDisplayed())
		 return true;
	else
		return false;	
	
	/*//====Notification Message RFI Added Sucessfully validation
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, notificationMsg, 60);	
	String message = notificationMsg.getText();
	Log.message("RFI  Message is: "+   message);
	String expectedMessage = "RFI is added successfully";
	if (message.contains(expectedMessage))
	{
		Log.message("RFI is added successfully ");			
	}
	else
	{
		Log.message("RFI is  NOT added successfully ");
	}	
	
	*/
	//return result;	
	
}


public boolean projectfile_Upload() throws Throwable

{
	boolean result = false;
SkySiteUtils.waitTill(3000);
SkySiteUtils.waitForElement(driver, Projectfiles, 30);
Log.message("Waiting for Project Tab button to be appeared");	
Projectfiles.click();
Log.message("Project files Tab Clicked Sucessfully");
SkySiteUtils.waitTill(3000);
//entered the value in searchbox
String Folder1 = PropertyReader.getProperty("FolderName");
SearchProjectfilesInbox.sendKeys(Folder1);
SkySiteUtils.waitTill(2000);
Log.message("Entered the value in project files Inbox field");
//================Search Button Clicked Sucessfully.
SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton, 30);	
ProjectfilesTab_searchButton.click();
SkySiteUtils.waitTill(5000);
Log.message("Search Button Click Sucessfully");
SkySiteUtils.waitForElement(driver, Folder, 60);
Folder.click();
Log.message("Folder Selected And clicked");	
SkySiteUtils.waitForElement(driver, File_CheckBox, 60);	
File_CheckBox.click();
return result;

}


public boolean projectfile_Upload1() throws Throwable

{
	boolean result = false;
SkySiteUtils.waitTill(3000);
SkySiteUtils.waitForElement(driver, Projectfiles, 30);
Log.message("Waiting for Project Tab button to be appeared");	
Projectfiles.click();
Log.message("Project files Tab Clicked Sucessfully");
SkySiteUtils.waitTill(3000);
//entered the value in searchbox
String Folder1 = PropertyReader.getProperty("FolderName");
SearchProjectfilesInbox.sendKeys(Folder1);
SkySiteUtils.waitTill(2000);
Log.message("Entered the value in project files Inbox field");
//================Search Button Clicked Sucessfully.
SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton, 30);	
ProjectfilesTab_searchButton.click();
SkySiteUtils.waitTill(5000);
Log.message("Search Button Click Sucessfully");
SkySiteUtils.waitForElement(driver, Folder, 60);
Folder.click();
Log.message("Folder Selected And clicked");	
SkySiteUtils.waitForElement(driver, File_CheckBox1, 60);	
File_CheckBox1.click();
return result;

}

//=========================Punch Creatiom Script==================


public boolean projectLevel_EmployeePunchValidation() throws Throwable

{
	boolean result = false;
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, Punchshortcut, 20);
	Project.click();
	Log.message("First Project Link Clicked sucessfully");
	SkySiteUtils.waitTill(5000);
	ProjectManagement.click();	
	Log.message("Project Management Link Clicked sucessfully");
	SkySiteUtils.waitTill(5000);
	punchmenu.click();
	Log.message("Punch Menu Clicked sucessfully");
	SkySiteUtils.waitTill(5000);
	AllPunchList.click();
	Log.message("All Punch List  Clicked sucessfully");
	SkySiteUtils.waitTill(5000);
	PunchList.click();
	
	   //=================Stamp Description=================
		SkySiteUtils.waitForElement(driver, Shortdescription, 30);	
		String Descriptipn = Shortdescription.getText();
		Log.message("Short Description Message is1: "+   Descriptipn);
		Log.message("Short Description Message is2: "+   Stamp_inbox);
		if (Descriptipn.equalsIgnoreCase(Stamp_inbox))
		{
			Log.message("Stamp verified Sucessfully ");
			
		}
		else
		{
			Log.message("Stamp not verified Sucessfully");
		}
		//================== Stamp Title===================
		SkySiteUtils.waitTill(1000);
		
		SkySiteUtils.waitForElement(driver, ShortNumber, 30);	
		String Stamp_Title = ShortNumber.getText();
		Log.message("Stamp Title Message is: "+  Stamp_Title);
		;
		if (Stamp_Title.contentEquals(Stamp_title))
		{
			Log.message("Stamp Title verified Sucessfully ");
			
		}
		else
		{
			Log.message("Stamp Title not verified Sucessfully");
		}
		
	
		//=====================Description====================
		
		SkySiteUtils.waitTill(1000);	
		SkySiteUtils.waitForElement(driver, TestDescription, 30);	
		String Description= TestDescription.getText();;
		
		Log.message("Description is1: "+  Description);

		String description = PropertyReader.getProperty("Desctiption");
		Log.message("Description is2: "+  description);
		if (Description.contentEquals(description))
		{
			Log.message("Description verified Sucessfully ");
			
		}
		else
		{
			Log.message("Description not verified Sucessfully");
		}
		
		//==========================Assign To===================================
		
		SkySiteUtils.waitForElement(driver, Afrerpunch_AssignTo, 30);	
		String AfterPunchassign= Afrerpunch_AssignTo.getText();
		
		Log.message("Assign To Email is: "+ AfterPunchassign);
		
		String AssignTo = PropertyReader.getProperty("Email_AssignTo");
		if (AfterPunchassign.contentEquals(AssignTo))
		{
			Log.message("Assign To Email verified Sucessfully ");
			
		}
		else
		{
			Log.message("Assign To Email not verified Sucessfully");
		}
		
		//============================CC Option ======================
		
	    SkySiteUtils.waitTill(1000);	
		SkySiteUtils.waitForElement(driver, Afrerpunch_CCOption, 30);	
		String AfterPunchCCoption= Afrerpunch_CCOption.getText();
		
		Log.message("CC To Email is1: "+  AfterPunchCCoption);
		
		String CCoption = PropertyReader.getProperty("Email_CCOption");
		Log.message("CC To Email is2: "+  CCoption);
		if (CCoption.contentEquals(AfterPunchCCoption))
		{
			Log.message("CC Email verified Sucessfully ");
			
		}
		else
		{
			Log.message("CC Email not verified Sucessfully");
		}
		
		//==============File Display======================
		String FileName1= UplodedFileName1.getText();
		Log.message("Uploded file Name1 Display As: "+FileName1);
		String FileName2= UplodedFileName2.getText();
		Log.message("Uploded file Name2 Display As: "+FileName2);
		
		
	return result;
	
	
	
}

public boolean PunchValidation_RecipientuploadingNewRevision() throws Throwable

{
	boolean result = false;
	
	//====================Punch ShortCut ===============
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, Punchshortcut, 20);
	Log.message("Waiting for punch shortcut to be appeared");
	Punchshortcut.click();	
	
	return result;	
	
	
	
	
	
	
	
}


public boolean ValidationAfterPunchCreation() throws Throwable

{
	boolean result = false;
	Log.message("Enter into validation Part");
	//====================Punch ShortCut ===============
	SkySiteUtils.waitTill(5000);	
	SkySiteUtils.waitForElement(driver, Shortdescription, 20);
	Log.message("Waiting for After Punch Description to be appeared");
	
	//=================Stamp Description=================
	SkySiteUtils.waitForElement(driver, Shortdescription, 30);	
	String Descriptipn = Shortdescription.getText();
	Log.message("Short Description Message is1: "+   Descriptipn);
	Log.message("Short Description Message is2: "+   Stamp_inbox);
	if (Descriptipn.equalsIgnoreCase(Stamp_inbox))
	{
		Log.message("Stamp verified Sucessfully ");
		
	}
	else
	{
		Log.message("Stamp not verified Sucessfully");
	}
	//================== Stamp Title===================
	SkySiteUtils.waitTill(1000);
	
	SkySiteUtils.waitForElement(driver, ShortNumber, 30);	
	String Stamp_Title = ShortNumber.getText();
	Log.message("Stamp Title Message is: "+  Stamp_Title);
	;
	if (Stamp_Title.contentEquals(Stamp_title))
	{
		Log.message("Stamp Title verified Sucessfully ");
		
	}
	else
	{
		Log.message("Stamp Title not verified Sucessfully");
	}
	
	
	//==========================Assign To===================================
	
	SkySiteUtils.waitForElement(driver, Afrerpunch_AssignTo, 30);	
	String AfterPunchassign= Afrerpunch_AssignTo.getText();
	
	Log.message("Assign To Email is: "+ AfterPunchassign);
	
	String AssignTo = PropertyReader.getProperty("Email_AssignTo");
	if (AfterPunchassign.contentEquals(AssignTo))
	{
		Log.message("Assign To Email verified Sucessfully ");
		
	}
	else
	{
		Log.message("Assign To Email not verified Sucessfully");
	}
	
	//============================CC Option ======================
	
    SkySiteUtils.waitTill(1000);	
	SkySiteUtils.waitForElement(driver, Afrerpunch_CCOption, 30);	
	String AfterPunchCCoption= Afrerpunch_CCOption.getText();
	
	Log.message("CC To Email is1: "+  AfterPunchCCoption);
	
	String CCoption = PropertyReader.getProperty("Email_CCOption");
	Log.message("CC To Email is2: "+  CCoption);
	if (CCoption.contentEquals(AfterPunchCCoption))
	{
		Log.message("CC Email verified Sucessfully ");
		
	}
	else
	{
		Log.message("CC Email not verified Sucessfully");
	}
	
	//==============File Display======================
	String FileName1= UplodedFileName1.getText();
	Log.message("Uploded file Name1 Display As: "+FileName1);
	String FileName2= UplodedFileName2.getText();
	Log.message("Uploded file Name2 Display As: "+FileName2);
	
	
	//=====================Description====================
	
	SkySiteUtils.waitTill(1000);	
	SkySiteUtils.waitForElement(driver, TestDescription, 30);	
	String Description= TestDescription.getText();;
	
	Log.message("Description is1: "+  Description);

	String description = PropertyReader.getProperty("Desctiption");
	Log.message("Description is2: "+  description);
	if (Description.contentEquals(description))
	{
		Log.message("Description verified Sucessfully ");
		
	}
	else
	{
		Log.message("Description not verified Sucessfully");
	}
	
	
	return result;	
	
}










/*public boolean AfterPunch_OwnerLevelValidation() throws Throwable

{
	boolean result = false;
	Log.message("After Closed Punch validation ");
	//====================Punch ShortCut ===============
	SkySiteUtils.waitTill(5000);	
	SkySiteUtils.waitForElement(driver, TotalPunch_Display, 20);
	Log.message("Waiting for punch dropdown to be Appeared");
	String afterClosetotalpunch = TotalPunch_Display.getText();
	
	String[] parts = afterClosetotalpunch.split(" ");
	String part1 = parts[0]; // 004
	Log.message("Punch display on the screen:"+ part1);
	SkySiteUtils.waitTill(5000);
	TotalPunch_Display.click();
	Log.message("Total Punch clicked sucessfully ");
	SkySiteUtils.waitForElement(driver, Display_firstrow, 20);
	SkySiteUtils.waitTill(5000);
	Display_firstrow.click();	
	Log.message("Total Punch clicked sucessfully ");
	//=================Stamp Description=================
	SkySiteUtils.waitForElement(driver, shortNumber, 30);	
	String StampNo = shortNumber.getText();
	Log.message("Short Description Message is1: "+   StampNo);
	Log.message("Short Description Message is2: "+   Stamp_inbox);
	if (StampNo.equalsIgnoreCase(Stamp_inbox))
	{
		Log.message("Stamp verified Sucessfully ");
		
	}
	else
	{
		Log.message("Stamp not verified Sucessfully");
	}
	
	//================== Stamp Title===================
	SkySiteUtils.waitTill(1000);
	
	SkySiteUtils.waitForElement(driver, PunchClose_shortdescription, 30);	
	String Stamp_Title = PunchClose_shortdescription.getText();
	Log.message("Stamp Title Message is: "+  Stamp_Title);
	;
	if (Stamp_Title.contentEquals(Stamp_title))
	{
		Log.message("Stamp Title verified Sucessfully ");
		
	}
	else
	{
		Log.message("Stamp Title not verified Sucessfully");
	}
	
	
	
	shortNumber.click();
	Log.message("Sort Message Clicked Sucessfully");
	
	//=====================Description====================
	
	SkySiteUtils.waitTill(1000);	
	SkySiteUtils.waitForElement(driver, CloseButton, 30);	
	String Description= CloseButton.getText();;
	
	Log.message("Description is1: "+  Description);

	//String description = PropertyReader.getProperty("Desctiption");
	
	if (Description.contentEquals("CLOSED"))
	{
		Log.message("Status CLOSED verified Sucessfully ");
		
	}
	else
	{
		Log.message("Status Closed not verified Sucessfully");
	}
	

	
	
	
	
	return result;	
	
}
*/


public boolean AddComment() throws Throwable

{
	boolean result = false;
	Log.message("Enter into Add Method section");
	SkySiteUtils.waitTill(1000);	
	SkySiteUtils.waitForElement(driver, TestDescription, 30);
	String addComment = PropertyReader.getProperty("AddComment");
	Addcomment.sendKeys(addComment);
	SkySiteUtils.waitTill(1000);
	Submitbutton.click();	
	SkySiteUtils.waitForElement(driver, notificationMsg, 60);	
	String message = notificationMsg.getText();
	Log.message("punch after saving Message is: "+   message);
	String expectedMessage = "Comment is added successfully";
	if (message.contains(expectedMessage))
	{
		Log.message("Comment message Verified Sucessfully");
		
	}
	else
	{
		Log.message("Comment message Verified Sucessfully");
	}
	
	
	


	return result;	
}










public boolean MultipleMarkUpSheetValidation() throws Throwable

{
	boolean result = false;
	
	SkySiteUtils.waitTill(5000);					
	SkySiteUtils.waitForElement(driver, ImageClick, 180);
	ImageClick.click();
	Log.message("Image Link clicked.");
	SkySiteUtils.waitTill(8000);
	String parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	
	Log.message(parentHandle);	
	
	driver.switchTo().defaultContent();
	Log.message("Switch to Default frame ");					
	SkySiteUtils.waitTill(5000);
	
	String value1 = NoOfViewerMaxValuesheet.getText();
	Log.message("sheet message is:"+value1);
	SkySiteUtils.waitTill(5000);
	String[] x = value1.split(" ");
	 String avlcount=x[1];
	 int count=Integer.parseInt(avlcount);
	 Log.message("Avl sheet value is:"+count);			
			for (int i = 1; i<= count; i++)
				 
			{				
				SkySiteUtils.waitTill(3000);
				Log.assertThat(TextValidation(), "text Validation working Successfully","Text validation Not  working sucessfully", driver);
				SkySiteUtils.waitTill(5000);
				Log.assertThat(CallOutValidation(), "Call Out Validation working Successfully","Call Out validation not working  sucessfully", driver);
			
				SkySiteUtils.waitTill(10000);
				SkySiteUtils.waitForElement(driver, NextPageButton, 120);
				NextPageButton.click();			
				Log.message("Next Button Page Click Sucessfully");		
			}				
			
			driver.switchTo().window(parentHandle);		
			Log.assertThat(Logout(), "LogOut working Successfully","Logout not working  sucessfully", driver);
			
	return true;
}

public boolean MarkUPValidation() throws Throwable

{
	boolean result = false;
	
	SkySiteUtils.waitTill(10000);	
	
	
	//=============================================
	 viewerScreenPage.UPDATED_RFIROW();
	 
	 SkySiteUtils.waitTill(5000);
	String parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);	
	SkySiteUtils.waitTill(5000);
	 viewerScreenPage.Close_POPUP();
	//Log.message("Switch to Default frame ");					
	SkySiteUtils.waitTill(5000);
	
	String value1 = NoOfViewerMaxValuesheet.getText();
	Log.message("sheet message is:"+value1);
	SkySiteUtils.waitTill(5000);
	String[] x = value1.split(" ");
	 String avlcount=x[1];
	 int count=Integer.parseInt(avlcount);
	 Log.message("Avl sheet value is:"+count);			
			for (int i = 1; i<= count; i++)
				 
			{				
				SkySiteUtils.waitTill(3000);
				//Log.assertThat(TextValidation(), "text Validation working Successfully","Text validation Not  working sucessfully", driver);
				SkySiteUtils.waitTill(5000);
				Log.assertThat(CallOutValidation(), "Call Out Validation working Successfully","Call Out validation not working  sucessfully", driver);
			
				SkySiteUtils.waitTill(10000);
				SkySiteUtils.waitForElement(driver, NextPageButton, 120);
				NextPageButton.click();			
				Log.message("Next Button Page Click Sucessfully");		
			}				
			
					
	
	return true;
}


public boolean Image1() throws Throwable

{
	boolean result = false;
	SkySiteUtils.waitTill(3000);
	SkySiteUtils.waitForElement(driver, ImageClick, 60);
	ImageClick.click();	
	Log.message("Image Link clicked.");
	SkySiteUtils.waitTill(5000);
	
	
	
	return result;

}

public boolean Image2() throws Throwable

{
	boolean result = false;
	Log.message("Image2 validation Started Link clicked.");
	SkySiteUtils.waitTill(3000);
	SkySiteUtils.waitForElement(driver, ImageClick1, 60);
	ImageClick1.click();
	Log.message("Image Link clicked.");
	SkySiteUtils.waitTill(5000);
	
	return result;

}


public boolean PunchValidation_creation1(String parentHandle) throws Throwable

{
	boolean result = false;	
	SkySiteUtils.waitTill(5000);					
	
	 parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	Log.message(parentHandle);	
	
	driver.switchTo().defaultContent();
	Log.message("Switch to Default frame ");
	//*********** Punch Tools***************************************
	//Log.assertThat(Punch_Description(), "  punch  Description Added Successfully","punch  Description  not Added  sucessfully", driver);
	SkySiteUtils.waitTill(1000);
	Punch_Description1();
	//=========External and Internal download Script
	SkySiteUtils.waitTill(1000);
	ExternalAndInternal_Upload2();
	//======== ======add comment==============================	
	SkySiteUtils.waitTill(5000);
	CreatePunch.click();//Click on Upload without index  
	Log.message("Clicked on create button Sucessfully");
	SkySiteUtils.waitForElement(driver, notificationMsg, 60);	
	String message1 = notificationMsg.getText();
	Log.message("punch after saving Message is: "+   message1);	
    String expectedMessage1 = "Punch annotation saved successfully";
	if (message1.contains(expectedMessage1))
	{
		Log.message("Punch save Sucessfully message Verified");
		
	}
	else
	{
		Log.message("Punch  not Saved Sucessfully message Verified");
	}
	
	
	
	
	SkySiteUtils.waitForElement(driver, TotalPunch_Display,60);
	Log.message("Waiting for punch dropdown to be Appeared");
	String afterClosetotalpunch = TotalPunch_Display.getText();
	
	String[] parts = afterClosetotalpunch.split(" ");
	String part1 = parts[0]; // 004
	Log.message("Punch display on the screen:"+ part1);
	SkySiteUtils.waitTill(5000);
	TotalPunch_Display.click();
	Log.message("Total Punch clicked sucessfully ");
	SkySiteUtils.waitForElement(driver, Display_firstrow,60);
	SkySiteUtils.waitTill(5000);
	Display_firstrow.click();	
	Log.message("Total Punch clicked sucessfully ");	
	
	SkySiteUtils.waitTill(5000);	
	afterImagearea.click();
	SkySiteUtils.waitTill(5000);
	
	ValidationAfterPunchCreation();	
	SkySiteUtils.waitTill(3000);	
	AddComment();
	SkySiteUtils.waitTill(3000);
	
	//Log.assertThat(AfterClosed_PunchValidation(), " After Closed validation working Successfully","After Closed validation not working  sucessfully", driver);
	
	//AfterClosed_PunchValidation();
	
	return result;
}
public boolean PunchValidation_creation2(String parentHandle) throws Throwable

{
	boolean result = false;	
	SkySiteUtils.waitTill(5000);					
	parentHandle = driver.getWindowHandle();
	for(String winHandle : driver.getWindowHandles())
	{
		driver.switchTo().window(winHandle);
	}
	SkySiteUtils.waitTill(5000);
	Log.message(parentHandle);	
	SkySiteUtils.waitTill(5000);
	driver.switchTo().defaultContent();
	Log.message("Switch to Default frame ");
	//*********** Punch Tools***************************************
	//Log.assertThat(Punch_Description(), "  punch  Description Added Successfully","punch  Description  not Added  sucessfully", driver);
	SkySiteUtils.waitTill(1000);
	Punch_Description2();
	//=========External and Internal download Script
	SkySiteUtils.waitTill(31000);
	ExternalAndInternal_Upload();
	//======== ======add comment==============================	
		
	SkySiteUtils.waitTill(15000);
	CreatePunch.click();//Click on Upload without index  
	Log.message("Clicked on create button Sucessfully");
	SkySiteUtils.waitForElement(driver, notificationMsg, 60);	
	String message1 = notificationMsg.getText();
	Log.message("punch after saving Message is: "+   message1);	
	SkySiteUtils.waitTill(5000);
	String expectedMessage1 = "Punch annotation saved successfully";
	if (message1.contains(expectedMessage1))
	{
		Log.message("Punch save Sucessfully message Verified");
		
	}
	else
	{
		Log.message("Punch  not Saved Sucessfully message Verified");
	}
	
	
	
	SkySiteUtils.waitTill(5000);	
	SkySiteUtils.waitForElement(driver, TotalPunch_Display, 20);
	Log.message("Waiting for punch dropdown to be Appeared");
	String afterClosetotalpunch = TotalPunch_Display.getText();
	
	String[] parts = afterClosetotalpunch.split(" ");
	String part1 = parts[0]; // 004
	Log.message("Punch display on the screen:"+ part1);
	SkySiteUtils.waitTill(5000);
	TotalPunch_Display.click();
	Log.message("Total Punch clicked sucessfully ");
	SkySiteUtils.waitForElement(driver, Display_firstrow, 20);
	SkySiteUtils.waitTill(5000);
	Display_firstrow.click();	
	Log.message("Total Punch clicked sucessfully ");	
	
	SkySiteUtils.waitTill(5000);	
	afterImagearea.click();
	SkySiteUtils.waitTill(5000);
	
	ValidationAfterPunchCreation();	
	SkySiteUtils.waitTill(5000);	
	AddComment();
	SkySiteUtils.waitTill(5000);
	
	//Log.assertThat(AfterClosed_PunchValidation(), " After Closed validation working Successfully","After Closed validation not working  sucessfully", driver);
	
	//AfterClosed_PunchValidation();
	
	return result;
	

}














public boolean UploadFiles(String FolderPath, int FileCount) throws InterruptedException, AWTException, IOException
{
	boolean result = false;
	//SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitTill(10000);
	Log.message("Waiting for upload file button to be appeared");
	//SkySiteUtils.waitTill(5000);
	ChooseFileButton.click();
	SkySiteUtils.waitTill(10000);
	//=================================
	//Writing File names into a text file for using in AutoIT Script
			BufferedWriter output;		
			randomFileName rn=new randomFileName();
			
			String Sys_Download_Path= "./File_Download_Location/"+ rn.nextFileName()+".txt" ;
	  		Log.message("system download location2"+Sys_Download_Path);
			String tmpFileName = Sys_Download_Path;
			
			//String tmpFileName="c:/"+rn.nextFileName()+".txt";							
			output = new BufferedWriter(new FileWriter(tmpFileName,true));					
			String expFilename=null;
			
			File[] files = new File(FolderPath).listFiles();				
			for(File file : files)
			{
				if(file.isFile()) 
				{
					expFilename=file.getName();//Getting File Names into a variable
					Log.message("Expected File name is:"+expFilename);
					output.append('"' + expFilename + '"');
					output.append(" ");
					SkySiteUtils.waitTill(1000);	
				}
			}

			output.flush();
			output.close();
				
			Log.message("waiting AutoIT Script!!");
			SkySiteUtils.waitTill(30000);
			
			
		     //====================Executing .exe autoIt file
			String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
			File dest = new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile").toString());
			String path = dest.getAbsolutePath();
			Runtime.getRuntime().exec(AutoIt_ExeFile_Path +" "+ path+" "+tmpFileName );
			Log.message("AutoIT Script Executed!!");			
			SkySiteUtils.waitTill(20000);
			
			SkySiteUtils.waitForElement(driver, CreatePunch, 30);
			Log.message("Waiting for Cloud Account button to be appeared");
			SkySiteUtils.waitTill(5000);
		       //====================Delete the temp file=====================
			try
			{
				File file = new File(tmpFileName);
				if(file.delete()){
				Log.message(file.getName() + " is deleted!");
				}else{
					Log.message("Delete operation is failed.");
					}
			}
			catch(Exception e)
			{
				Log.message("Exception occured!!!"+e);	
			}
					
	return result;
	
	
	
}



public boolean ExternalAndInternal_Upload2() throws InterruptedException, AWTException, IOException, NumberFormatException
{
	boolean result = false;
	
	SkySiteUtils.waitTill(5000);
	String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
	Log.message("FolderPath: "+FolderPath);
	String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
	int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
	Log.message("FileCount: "+FileCount);
	SkySiteUtils.waitTill(5000);
	UploadFiles(FolderPath, FileCount);	
	
	//===========project file upload
	
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, Projectfiles, 30);
	Log.message("Waiting for Project Tab button to be appeared");	
	Projectfiles.click();
	Log.message("Project files Tab Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	//entered the value in searchbox
	String Folder1 = PropertyReader.getProperty("FolderName");
	SearchProjectfilesInbox.sendKeys(Folder1);
	SkySiteUtils.waitTill(2000);
	Log.message("Entered the value in project files Inbox field");
	//================Search Button Clicked Sucessfully.
	SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton, 30);	
	ProjectfilesTab_searchButton.click();
	SkySiteUtils.waitTill(5000);
	Log.message("Search Button Click Sucessfully");
	SkySiteUtils.waitForElement(driver, Folder, 60);
	Folder.click();
	Log.message("Folder Selected And clicked");	
	SkySiteUtils.waitForElement(driver, File_CheckBox1, 60);	
	File_CheckBox1.click();	
	
	
	
	return result;
}
public boolean ExternalAndInternal_Upload() throws InterruptedException, AWTException, IOException, NumberFormatException
{
	boolean result = false;
	
	SkySiteUtils.waitTill(5000);
	String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
	Log.message("FolderPath: "+FolderPath);
	String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
	int FileCount = Integer.parseInt(CountOfFilesInFolder);//String to Integer
	Log.message("FileCount: "+FileCount);
	SkySiteUtils.waitTill(5000);
	UploadFiles(FolderPath, FileCount);	
	
	//===========project file upload
	
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, Projectfiles, 30);
	Log.message("Waiting for Project Tab button to be appeared");	
	Projectfiles.click();
	Log.message("Project files Tab Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	//entered the value in searchbox
	String Folder1 = PropertyReader.getProperty("FolderName");
	SearchProjectfilesInbox.sendKeys(Folder1);
	SkySiteUtils.waitTill(2000);
	Log.message("Entered the value in project files Inbox field");
	//================Search Button Clicked Sucessfully.
	SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton, 30);	
	ProjectfilesTab_searchButton.click();
	SkySiteUtils.waitTill(5000);
	Log.message("Search Button Click Sucessfully");
	SkySiteUtils.waitForElement(driver, Folder, 60);
	Folder.click();
	Log.message("Folder Selected And clicked");	
	SkySiteUtils.waitForElement(driver, File_CheckBox, 60);	
	File_CheckBox.click();	
	
	
	
	return result;
}


public boolean Punch_Description1() throws InterruptedException, AWTException, IOException 
{
	boolean result = false;
	
	SkySiteUtils.waitForElement(driver, punchShortcuttools, 60);
	SkySiteUtils.waitTill(5000);
	punchShortcuttools.click();
	
	SkySiteUtils.waitTill(5000);
	beforeImagearea.click();
	SkySiteUtils.waitTill(5000);	
	Log.message("Waiting for create punch page to be appeared");	
	Stamp.click();	
	SkySiteUtils.waitTill(5000);	
	SkySiteUtils.waitForElement(driver, AddNewButton, 60);
	Log.message("Stamp List Box button Clicked");
	SkySiteUtils.waitTill(2000);
	AddNewButton.click();
	Log.message("Add New Link Button Clicked");
	SkySiteUtils.waitForElement(driver, Stamp_Inbox, 60);
	SkySiteUtils.waitTill(2000);
	//String Stamp_inbox1 = Generate_Random_Number.getRandomText(3);
	Stamp_Inbox.sendKeys(Stamp_inbox);
	Log.message("Entered Value In Stamp Inbox");
	SkySiteUtils.waitForElement(driver, Stamp_Title, 60);
	//String Stamp_title1 = Generate_Random_Number.StampTitle();
	SkySiteUtils.waitTill(2000);
	Stamp_Title.sendKeys(Stamp_title1);
	SkySiteUtils.waitTill(5000);
	RightButton.click();
	Log.message("Right Button Clicked Sucessfully");
	//================ Assign To ========================
	SkySiteUtils.waitForElement(driver, AssignTo, 60);
	AssignTo.click();
	Log.message("Assign To Link clicked Sucessfully");		
	Log.message("Entered the Value In Select Email field");
	String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
	Log.message("Entered the Value In Select Email field:---   "+Select_Email);
	SkySiteUtils.waitTill(2000);
	SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);
	SelectUse_Inbox.sendKeys(Select_Email);
	
	SkySiteUtils.waitForElement(driver, Search_Button, 60);
	SkySiteUtils.waitTill(2000);
	Search_Button.click();
	Log.message("Search Button Clicked Sucessfully");
	SkySiteUtils.waitTill(2000);
	SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
	SelectUser_CheckBox.click();
	Log.message("Assign To Email Checkbox Clicked Sucessfully");
	SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
	SelectUser_Button.click();
	Log.message("Search User Button Clicked Sucessfully");
	//************CC Option---------------------------------------
	SkySiteUtils.waitTill(5000);
	CC_option.click();
	Log.message("Cc Option Button Clicked Sucessfully");
	SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);	
	Log.message("Entered the Value In Select Email field");
	String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
	SkySiteUtils.waitTill(5000);
	SelectUse_Inbox.sendKeys(SelectEmail);	
	SkySiteUtils.waitForElement(driver, Search_Button, 60);
	SkySiteUtils.waitTill(5000);
	Search_Button.click();
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
	SelectUser_CheckBox.click();
	SkySiteUtils.waitTill(2000);
	SkySiteUtils.waitForElement(driver, SelectUser_Button, 30);
	SelectUser_Button.click();	
	//=========Date Selection Script==================
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, CalenderLink, 30);
	CalenderLink.click();	
	SkySiteUtils.waitForElement(driver, NextMonthDate, 30);
	NextMonthDate.click();
	SkySiteUtils.waitForElement(driver, pickDate, 30);
	pickDate.click();
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, Description,60);
	String description = PropertyReader.getProperty("Desctiption");	
	Description.sendKeys(description);
	SkySiteUtils.waitTill(3000);
	return result;	
	
}


public boolean Punch_Description2() throws InterruptedException, AWTException, IOException 
{
	boolean result = false;
	
	SkySiteUtils.waitForElement(driver, punchShortcuttools, 60);
	SkySiteUtils.waitTill(5000);
	punchShortcuttools.click();
	
	SkySiteUtils.waitTill(5000);
	beforeImagearea.click();
	SkySiteUtils.waitTill(5000);	
	Log.message("Waiting for create punch page to be appeared");	
	Stamp.click();	
	SkySiteUtils.waitTill(5000);	
	SkySiteUtils.waitForElement(driver, AddNewButton, 60);
	Log.message("Stamp List Box button Clicked");
	SkySiteUtils.waitTill(2000);
	AddNewButton.click();
	Log.message("Add New Link Button Clicked");
	
	SkySiteUtils.waitTill(2000);
	//String Stamp_inbox1 = Generate_Random_Number.getRandomText(3);
	SkySiteUtils.waitForElement(driver, Stamp_Inbox, 60);
	Stamp_Inbox.sendKeys(Stamp_inbox1);
	Log.message("Entered Value In Stamp Inbox");
	SkySiteUtils.waitForElement(driver, Stamp_Title, 60);
	//String Stamp_title1 = Generate_Random_Number.StampTitle();
	SkySiteUtils.waitTill(2000);
	Stamp_Title.sendKeys(Stamp_title1);
	SkySiteUtils.waitTill(5000);
	RightButton.click();
	Log.message("Right Button Clicked Sucessfully");
	//================ Assign To ========================
	SkySiteUtils.waitForElement(driver, AssignTo, 60);
	AssignTo.click();
	Log.message("Assign To Link clicked Sucessfully");		
	Log.message("Entered the Value In Select Email field");
	String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
	Log.message("Entered the Value In Select Email field:---   "+Select_Email);
	SkySiteUtils.waitTill(2000);
	SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);
	SelectUse_Inbox.sendKeys(Select_Email);
	
	SkySiteUtils.waitForElement(driver, Search_Button, 60);
	SkySiteUtils.waitTill(2000);
	Search_Button.click();
	Log.message("Search Button Clicked Sucessfully");
	SkySiteUtils.waitTill(2000);
	SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
	SelectUser_CheckBox.click();
	Log.message("Assign To Email Checkbox Clicked Sucessfully");
	SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
	SelectUser_Button.click();
	Log.message("Search User Button Clicked Sucessfully");
	//************CC Option---------------------------------------
	SkySiteUtils.waitTill(5000);
	CC_option.click();
	Log.message("Cc Option Button Clicked Sucessfully");
	SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);	
	Log.message("Entered the Value In Select Email field");
	String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
	SkySiteUtils.waitTill(5000);
	SelectUse_Inbox.sendKeys(SelectEmail);	
	SkySiteUtils.waitForElement(driver, Search_Button, 60);
	SkySiteUtils.waitTill(5000);
	Search_Button.click();
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
	SelectUser_CheckBox.click();
	SkySiteUtils.waitTill(2000);
	SkySiteUtils.waitForElement(driver, SelectUser_Button, 30);
	SelectUser_Button.click();	
	//=========Date Selection Script==================
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, CalenderLink, 30);
	CalenderLink.click();	
	SkySiteUtils.waitForElement(driver, NextMonthDate, 30);
	NextMonthDate.click();
	SkySiteUtils.waitForElement(driver, pickDate, 30);
	pickDate.click();
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, Description,60);
	String description = PropertyReader.getProperty("Desctiption");	
	Description.sendKeys(description);
	SkySiteUtils.waitTill(3000);
	return result;	
	
}




public boolean ProjectFiles_Internal() 
{
	boolean result = false;
	
	SkySiteUtils.waitTill(10000);
	
	SkySiteUtils.waitForElement(driver, punchShortcuttools, 60);	
	punchShortcuttools.click();
	//*********** Punch viewer cordinate creation***************************************
	Actions builder = new Actions(driver); 
	Actions drawAction = builder.moveToElement(ImageareA,862,261).click(ImageareA);
	SkySiteUtils.waitTill(5000);
	drawAction.build().perform();
	drawAction.click().release();    
	SkySiteUtils.waitTill(10000);	
	Log.message("Waiting for create punch page to be appeared");	
	Stamp.click();	
	SkySiteUtils.waitTill(5000);	
	SkySiteUtils.waitForElement(driver, AddNewButton, 60);
	Log.message("Stamp List Box button Clicked");
	SkySiteUtils.waitTill(2000);
	AddNewButton.click();
	Log.message("Add New Link Button Clicked");
	SkySiteUtils.waitForElement(driver, Stamp_Inbox, 60);
	SkySiteUtils.waitTill(2000);
	String Stamp_inbox = Generate_Random_Number.getRandomText(3);
	Stamp_Inbox.sendKeys(Stamp_inbox);
	Log.message("Entered Value In Stamp Inbox");
	SkySiteUtils.waitForElement(driver, Stamp_Title, 60);
	String Stamp_title = Generate_Random_Number.StampTitle();
	SkySiteUtils.waitTill(2000);
	Stamp_Title.sendKeys(Stamp_title);
	SkySiteUtils.waitTill(5000);
	RightButton.click();
	Log.message("Right Button Clicked Sucessfully");
	//================ Assign To ========================
	SkySiteUtils.waitForElement(driver, AssignTo, 60);
	AssignTo.click();
	Log.message("Assign To Link clicked Sucessfully");		
	Log.message("Entered the Value In Select Email field");
	String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
	Log.message("Entered the Value In Select Email field"+Select_Email);
	SkySiteUtils.waitTill(2000);
	SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 30);
	SelectUse_Inbox.sendKeys(Select_Email);
	
	SkySiteUtils.waitForElement(driver, Search_Button, 60);
	SkySiteUtils.waitTill(2000);
	            Search_Button.click();
	Log.message("Search Button Clicked Sucessfully");
	SkySiteUtils.waitTill(2000);
	SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
	SelectUser_CheckBox.click();
	Log.message("Assign To Email Checkbox Clicked Sucessfully");
	SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
	SelectUser_Button.click();
	Log.message("Search User Button Clicked Sucessfully");
	//************CC Option---------------------------------------
	SkySiteUtils.waitTill(5000);
	CC_option.click();
	Log.message("Cc Option Button Clicked Sucessfully");
	SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);	
	Log.message("Entered the Value In Select Email field");
	String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
	SkySiteUtils.waitTill(5000);
	SelectUse_Inbox.sendKeys(SelectEmail);	
	SkySiteUtils.waitForElement(driver, Search_Button, 60);
	SkySiteUtils.waitTill(5000);
	 Search_Button.click();
	 SkySiteUtils.waitTill(8000);
	SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
	SelectUser_CheckBox.click();
	SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
	SelectUser_Button.click();	
	//=========Date Selection Script==================
	SkySiteUtils.waitForElement(driver, CalenderLink, 60);
	CalenderLink.click();	
	SkySiteUtils.waitForElement(driver, NextMonthDate, 60);
	NextMonthDate.click();
	SkySiteUtils.waitForElement(driver, pickDate, 60);
	pickDate.click();	
	SkySiteUtils.waitForElement(driver, Description, 60);
	String description = PropertyReader.getProperty("Desctiption");	
	Description.sendKeys(description);
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver, Projectfiles, 60);
	Log.message("Waiting for Project Tab button to be appeared");	
	Projectfiles.click();
	Log.message("Project files Tab Clicked Sucessfully");
	SkySiteUtils.waitTill(5000);
	//entered the value in
	SearchProjectfilesInbox.sendKeys("aaa");
	SkySiteUtils.waitTill(5000);
	Log.message("Entered the value in project files Inbox field");
	//================Search Button Clicked Sucessfully.
	SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton, 60);	
	ProjectfilesTab_searchButton.click();
	SkySiteUtils.waitTill(5000);
	Log.message("Search Button Click Sucessfully");
	SkySiteUtils.waitForElement(driver, Folder, 60);
	Folder.click();
	Log.message("Folder Selected And clicked");	
	SkySiteUtils.waitForElement(driver, File_CheckBox, 60);	
	File_CheckBox.click();	
	
	SkySiteUtils.waitTill(5000);
	CreatePunch.click();//Click on Upload without index  
	Log.message("Clicked on create button Sucessfully");
	SkySiteUtils.waitTill(10000);
	String message = notificationMsg.getText();
	Log.message("punch after saving Message is: "+   message);	
	
	
	String expectedMessage = "Punch annotation saved successfully";
	if (message.contains(expectedMessage))
	{
		Log.message("Punch save Sucessfully message Verified");
		
	}
	else
	{
		Log.message("Punch  not Saved Sucessfully message Verified");
	}
	return result;
	
	
}









				
				
public ProjectDashboardPage viewerPageSimpleExecution() throws IOException
				{
						
					SkySiteUtils.waitTill(8000);
					/*if(FeedBackmeesage.isDisplayed())
					{
						FeedBackmeesage.click();
					}*/
					
					//Log.message("Feedback Alert Clicked Sucessfully");					
					//CommonMethod.Fluentwait(Project_test, 100, 2);
					SkySiteUtils.waitForElement(driver, Project_test, 30);
					Project_test.click();
					Log.message("Project Button Clicked");
					SkySiteUtils.waitTill(2000);
					Floder.click();
					Log.message("Folder Button clicked.");
					SkySiteUtils.waitTill(2000);					
					SkySiteUtils.waitForElement(driver, ImageClick, 30);
					ImageClick.click();
					Log.message("Image Link clicked.");
					SkySiteUtils.waitTill(2000);
					/*String parentHandle = driver.getWindowHandle();
					for(String winHandle : driver.getWindowHandles())
					{
						driver.switchTo().window(winHandle);
					}
					SkySiteUtils.waitTill(3000);
					Log.message(parentHandle);*/
					return new ProjectDashboardPage(driver).get();
					
				}



public boolean DeleteProject(String Prj_Name)
{
	boolean result = false;
	
	SkySiteUtils.waitTill(5000);
	GlobalSearch.sendKeys(Prj_Name);
	Log.message("Entered Value In Global Search field");
	ButtonSearch.click();		
	Log.message("Button Search Clicked Sucessfully");
	
	int Avl_Projects_Count=0;
	List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'PName_')]"));
	for (WebElement Element : allElements)
	{ 
		Avl_Projects_Count =Avl_Projects_Count+1; 	
	}
	Log.message("Available private projects count is: "+Avl_Projects_Count);
	SkySiteUtils.waitTill(5000);
	
	for(int i=1;i<=Avl_Projects_Count;i++)
	{
		String Exp_ProjName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/ul/li["+i+"]/div/section[1]/h4")).getText();
		
//Validating - Expected project is selected or not
		if(Exp_ProjName.trim().contentEquals(Prj_Name.trim()))
		{  	
			Log.message("Maching Project Found!!");
			driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/ul/li["+i+"]/div/section[1]/h4")).click();
			Log.message("Clicked on expected project!!");
			SkySiteUtils.waitTill(10000);
			break; 				
		}

	}
	
	return result;	
	
}
				
public boolean ToolBarSelection_And_Validation() throws Throwable
				
				{
					boolean result = false;
					SkySiteUtils.waitTill(10000);					
					SkySiteUtils.waitForElement(driver, ImageClick, 30);
					ImageClick.click();
					Log.message("Image Link clicked.");
					SkySiteUtils.waitTill(8000);
					String parentHandle = driver.getWindowHandle();
					for(String winHandle : driver.getWindowHandles())
					{
						driver.switchTo().window(winHandle);
					}
					SkySiteUtils.waitTill(5000);
					Log.message(parentHandle);	
					SkySiteUtils.waitTill(5000);
					driver.switchTo().defaultContent();
					Log.message("Switch to Default frame ");					
					SkySiteUtils.waitTill(5000);
					//TextValidation();
					Log.assertThat(TextValidation(), "text Validation working Successfully","Text validation Not  working sucessfully", driver);
					//CallOutValidation();
					Log.assertThat(CallOutValidation(), "Call Out Validation working Successfully","Call Out validation not working  sucessfully", driver);
					driver.switchTo().window(parentHandle);
				    
				    Log.assertThat(Logout(), "Logout working Successfully","Logout not working  sucessfully", driver);
					//ArrowValidation();
					//Log.assertThat(ArrowValidation(), "Arrow Validation working Successfully", "Arrow working not sucessfully", driver);
					//RetangleHightlighter();	
					//Log.assertThat(RetangleHightlighter(), "Retangular Validation working Successfully", "Retangular not validation working sucessfully", driver);
					//CloudRetangleValidation() ;
					//Log.assertThat(CloudRetangleValidation(), "Cloud Retangle Validation working Successfully", "Cloud not Retangle validation working sucessfully", driver);									
					//HyperlinkCircleValidation();
					//Log.assertThat(HyperlinkCircleValidation(), "Hyperlink Validation working Successfully", "Hyperlink validation not working sucessfully", driver);
					//FreeHandValidation();
					//Log.assertThat(	FreeHandValidation(), "Freehand Validation working Successfully", "Free hand validation not working sucessfully", driver);
					
					
					
					return true;
				}
				



//===========Cloud Retangle Tools=========================

public boolean CloudRetangleValidation() throws Throwable

{boolean result = true;
Log.message("Enter Into Cloud Retangle Validation Method");
SkySiteUtils.waitTill(3000);
SkySiteUtils.waitForElement(driver, OpenShapeMenu, 30);
OpenShapeMenu.click();
Log.message("Open shape Menu Button clicked sucessfully");
SkySiteUtils.waitTill(5000);
SkySiteUtils.waitForElement(driver,DrowCloud, 30);
//==== FreeHand tool-Baar===================== 
SkySiteUtils.waitTill(5000);
DrowCloud.click();
Log.message("Drow Cloud Button clicked sucessfully ");
SkySiteUtils.waitTill(5000);
Actions builder = new Actions(driver); 
Actions drawAction = builder.moveToElement(ImageareA,862,261).clickAndHold().moveToElement(ImageareA, 969, 439);
SkySiteUtils.waitTill(3000);
drawAction.build().perform();
drawAction.click().release();    
SkySiteUtils.waitTill(10000);

 return true;
	
}


//=====================Text tools=============================
public boolean TextValidation() throws Throwable

{
	boolean result = false;

Log.message("Enter Into Text Validation Method");
SkySiteUtils.waitTill(5000);
SkySiteUtils.waitForElement(driver, OpenTextMenu, 30);
OpenTextMenu.click();
Log.message("Open Text menu Button clicked sucessfully");
SkySiteUtils.waitTill(5000);
SkySiteUtils.waitForElement(driver,AddaText, 30);
Log.message("Waiting to click on text tab.");
SkySiteUtils.waitTill(5000);
driver.findElement(By.xpath("html/body/header/nav")).click();  

//==== FreeHand tool-Baar===================== 
SkySiteUtils.waitTill(5000);
AddaText.click();
Log.message("Add a text note Button clicked sucessfully ");
SkySiteUtils.waitTill(5000);
Actions builder = new Actions(driver); 
Actions drawAction = builder.moveToElement(ImageareA,862,261).click(ImageareA);
SkySiteUtils.waitTill(5000);
drawAction.build().perform();
drawAction.click().release();    
SkySiteUtils.waitTill(10000);

//============== for Pop Validation Message Change========================

String Entertext  = Generate_Random_Number.RandamName_text();
SkySiteUtils.waitTill(5000);

//===========Enter Text Into text area.
SkySiteUtils.waitForElement(driver, Textarea, 60);
Textarea.sendKeys(Entertext);
Log.message("Entered Value In TeXt Area"+Entertext);

//select font Colour from font Colour drop down.
SkySiteUtils.waitTill(5000);
Select oSelect = new Select(Fontcolour);
oSelect.selectByVisibleText("Yellow");
Log.message("Selected Value From Font Colour Drop Down");
SkySiteUtils.waitTill(5000);

//select font size from font-size  drop down.
Select oSelect1 = new Select(Fontsize);
oSelect1.selectByVisibleText("20");
Log.message("Selected Value From Font Size Drop Down");
SkySiteUtils.waitTill(5000);

//select Backgroung colour from Back Ground Colour drop down.
Select oSelect2 = new Select(BackGroundColour);
oSelect2.selectByVisibleText("Red");
Log.message("Selected Value From BackGround Colour Drop Down");

SkySiteUtils.waitTill(5000);
//select Border colour from Border colour  drop down.
Select oSelect3 = new Select(BorderColour);
oSelect3.selectByVisibleText("Red");
Log.message("Selected Value From Border Colour Drop Down");
SkySiteUtils.waitTill(5000);
SkySiteUtils.waitForElement(driver, Text_okButton, 30);
Text_okButton.click();
Log.message("Ok Button Clicked sucessfully");
SkySiteUtils.waitTill(10000);

//FOR PARTICULAR AREA SCREENSHOT
File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
BufferedImage  fullImg = ImageIO.read(screenshot);
org.openqa.selenium.Point point = ImageareA.getLocation();
//Get width and height of the element
int eleWidth = ImageareA.getSize().getWidth();
int eleHeight = ImageareA.getSize().getHeight();
Log.message("Width of the screenshot "+eleWidth);
Log.message("Width of the screenshot "+eleHeight);
BufferedImage eleScreenshot= fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
ImageIO.write(eleScreenshot, "png", screenshot);
//Log.message("bufferImage "+eleScreenshot);
String FileLocation1 =PropertyReader.getProperty("ImageLocation1");
File screenshotLocation = new File(FileLocation1 +".jpg");
FileUtils.copyFile(screenshot, screenshotLocation);

SkySiteUtils.waitTill(5000);
SaveButton.click();
Log.message("Save Button Clicked Sucessfully");
SkySiteUtils.waitTill(10000);
SkySiteUtils.waitForElement(driver, MarkUpName, 30);
String Markupname = Generate_Random_Number.RandamName();
SkySiteUtils.waitTill(5000);
MarkUpName.sendKeys(Markupname);
Log.message("Mark UP Name Value Entered Sucessfully");
SkySiteUtils.waitForElement(driver, MarkName_SaveButton, 180);
MarkName_SaveButton.click();
SkySiteUtils.waitTill(5000);
Log.message("Mark Up Pop UP  Save Button Clicked Sucessfully");
SkySiteUtils.waitForElement(driver, notificationMsg, 30);
String message = notificationMsg.getText();
Log.message("Message after saving call-out markup is: "+message);
String expectedMessage = "Markup saved successfully";
if (message.contentEquals(expectedMessage)){
	Log.message("Mark Up Saved Sucessfully message Verified");
	
}
else{
	Log.message("Mark Up not Saved Sucessfully message Verified");
}

SkySiteUtils.waitTill(7000);

/*File screenshot1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
BufferedImage  fullImg1 = ImageIO.read(screenshot);
org.openqa.selenium.Point point1 = ImageareA.getLocation();
//Get width and height of the element
int eleWidth1 = ImageareA.getSize().getWidth();
int eleHeight1 = ImageareA.getSize().getHeight();
Log.message("Width of the screenshot "+eleWidth1);
Log.message("Width of the screenshot "+eleHeight1);
BufferedImage eleScreenshot1= fullImg1.getSubimage(point1.getX(), point1.getY(),eleWidth1, eleHeight1);
ImageIO.write(eleScreenshot1, "png", screenshot1);*/
//Log.message("bufferImage "+eleScreenshot1);
String FileLocation2 =PropertyReader.getProperty("ImageLocation2");
File screenshotLocation1 = new File(FileLocation2+".jpg");
FileUtils.copyFile(screenshot, screenshotLocation1);
Log.message("After Save Button Screenshot Taken Sucessfully");
SkySiteUtils.waitTill(5000);

//===Image Comparision Method=========================
String File1 =PropertyReader.getProperty("ImageLocation1")+".jpg";
String File2 =PropertyReader.getProperty("ImageLocation2")+".jpg";
SkySiteUtils.waitTill(5000);
result = CommonMethod.Imagecomparision(File1,File2);

if(result==true)
return true;
else
	return false;
	
}


//===================Callout Tools =================================
public boolean Delete_MarkUP() throws Throwable

{
	boolean result = true;
	Log.message("Enter Into call out Validation Method");
	//=====open Text menu tols click===========================
	SkySiteUtils.waitTill(3000);
	SkySiteUtils.waitForElement(driver, OpenTextMenu, 30);
	OpenTextMenu.click();
	Log.message("Open Text menu Button clicked sucessfully");
	SkySiteUtils.waitTill(5000);
	SkySiteUtils.waitForElement(driver,AddaText, 30);
	Log.message("Waiting to click on text tab.");

	SkySiteUtils.waitTill(5000);
	driver.findElement(By.xpath("html/body/header/nav")).click();  

	//====  call out option clicked===================== 
	SkySiteUtils.waitTill(5000);
	CALLOUT.click();
	Log.message("Call Out  Button clicked sucessfully ");
	SkySiteUtils.waitTill(5000);
	//************* Image Area Selection***************************************

	/*SkySiteUtils.waitTill(10000);
	beforeImagearea.click();*/
	Actions builder = new Actions(driver); 
	Actions drawAction = builder.moveToElement(ImageareA,697,419).click(ImageareA);
	SkySiteUtils.waitTill(3000);
	drawAction.build().perform();
	drawAction.click().release();    
	SkySiteUtils.waitTill(10000);
	return result;
	
}





//===================Callout Tools =================================
public boolean CallOutValidation() throws Throwable

{
	boolean result = true;
Log.message("Enter Into call out Validation Method");

//=====open Text menu tols click===========================
SkySiteUtils.waitTill(3000);
SkySiteUtils.waitForElement(driver, OpenTextMenu, 30);
OpenTextMenu.click();
Log.message("Open Text menu Button clicked sucessfully");
SkySiteUtils.waitTill(5000);
SkySiteUtils.waitForElement(driver,AddaText, 30);
Log.message("Waiting to click on text tab.");

SkySiteUtils.waitTill(5000);
driver.findElement(By.xpath("html/body/header/nav")).click();  

//====  call out option clicked===================== 
SkySiteUtils.waitTill(5000);
CALLOUT.click();
Log.message("Call Out  Button clicked sucessfully ");
SkySiteUtils.waitTill(5000);
//************* Image Area Selection***************************************

/*SkySiteUtils.waitTill(10000);
beforeImagearea.click();*/
Actions builder = new Actions(driver); 
Actions drawAction = builder.moveToElement(ImageareA,697,419).click(ImageareA);
SkySiteUtils.waitTill(3000);
drawAction.build().perform();
drawAction.click().release();    
SkySiteUtils.waitTill(10000);
//String Entertext =PropertyReader.getProperty("CallOut_EnterText"); 
SkySiteUtils.waitTill(5000);
//========================Enter Text Into text area.============================
SkySiteUtils.waitForElement(driver, CallOutTextArea, 60);
String Entercallout  = Generate_Random_Number.RandamName_CallOut();
CallOutTextArea.sendKeys(Entercallout);
Log.message("Entered Value In TeXt Area:---"+Entercallout);
//========================select font colour from font colour drop down.===========
SkySiteUtils.waitTill(5000);
Select oSelect = new Select(Fontcolour);
oSelect.selectByVisibleText("Yellow");
Log.message("Selected Value From Font Colour Drop Down");
//==========================select fill colour from fill colour  drop down.===
SkySiteUtils.waitTill(5000);
Select oSelect1 = new Select(Fillcolour);
oSelect1.selectByVisibleText("Red");
Log.message("Selected Value From fill colour Drop Down");
//==========================select Border Thickness  drop down.=================
SkySiteUtils.waitTill(5000);
Select oSelect2 = new Select(BorderThickness);
oSelect2.selectByIndex(4);
Log.message("Selected Value From Border Thickness Drop Down:");
//============================select font size from  drop down=============
SkySiteUtils.waitTill(5000);
Select oSelect3 = new Select(Fontsize);
oSelect3.selectByVisibleText("20");
Log.message("Selected Value From font Size Drop Down");
//===================select Border colour from Border colour  drop down.============
SkySiteUtils.waitTill(5000);
Select oSelect4 = new Select(BorderColour);
oSelect4.selectByVisibleText("Red");
Log.message("Selected Value From Border Colour Drop Down");
SkySiteUtils.waitTill(5000);
SkySiteUtils.waitForElement(driver, Text_okButton, 30);
Text_okButton.click();
Log.message("Ok Button Clicked sucessfully");
SkySiteUtils.waitTill(20000);

//*************First Screenshot Function********************************************
File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
BufferedImage  fullImg = ImageIO.read(screenshot);
org.openqa.selenium.Point point = ImageareA.getLocation();

//*************** Get width and height of the element************************
int eleWidth = ImageareA.getSize().getWidth();
int eleHeight = ImageareA.getSize().getHeight();
Log.message("Width of the screenshot "+eleWidth);
Log.message("Width of the screenshot "+eleHeight);
BufferedImage eleScreenshot= fullImg.getSubimage(point.getX(), point.getY(),
    eleWidth, eleHeight);
ImageIO.write(eleScreenshot, "png", screenshot);
String FileLocation1 =PropertyReader.getProperty("ImageLocation1");
File screenshotLocation = new File(FileLocation1+".jpg");
FileUtils.copyFile(screenshot, screenshotLocation);
//************** Save Button clicked***************
SkySiteUtils.waitTill(3000);
SaveButton.click();
Log.message("Save Button Clicked Sucessfully");
SkySiteUtils.waitForElement(driver, MarkUpName, 30);
String Markupname = Generate_Random_Number.RandamName();
SkySiteUtils.waitTill(3000);
MarkUpName.sendKeys(Markupname);
Log.message("Mark UP Name Value Entered Sucessfully");
SkySiteUtils.waitForElement(driver, MarkName_SaveButton, 180);
MarkName_SaveButton.click();
SkySiteUtils.waitTill(3000);
Log.message("Mark Up Pop UP  Save Button Clicked Sucessfully");
SkySiteUtils.waitForElement(driver, notificationMsg, 30);

String message = notificationMsg.getText();
Log.message("Message after saving call-out markup is: "+message);
String expectedMessage = "Markup saved successfully";
  if (message.contentEquals(expectedMessage)){
	   Log.message("Mark Up Saved Sucessfully message Verified");
	
       }
  else
  {
	Log.message("Mark Up not Saved Sucessfully message Verified");
     }

  SkySiteUtils.waitTill(7000);

 /* File screenshot1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
  BufferedImage  fullImg1 = ImageIO.read(screenshot);
  org.openqa.selenium.Point point1 = ImageareA.getLocation();
  //Get width and height of the element
  int eleWidth1 = ImageareA.getSize().getWidth();
  int eleHeight1 = ImageareA.getSize().getHeight();
  Log.message("Width of the screenshot "+eleWidth1);
  Log.message("Width of the screenshot "+eleHeight1);
  BufferedImage eleScreenshot1= fullImg1.getSubimage(point1.getX(), point1.getY(),eleWidth1, eleHeight1);
  ImageIO.write(eleScreenshot1, "png", screenshot1);*/
  //Log.message("bufferImage "+eleScreenshot1);
  String FileLocation2 =PropertyReader.getProperty("ImageLocation2");
  File screenshotLocation1 = new File(FileLocation2+".jpg");
  FileUtils.copyFile(screenshot, screenshotLocation1);
  Log.message("After Save Button Screenshot Taken Sucessfully");
  SkySiteUtils.waitTill(5000);

  //===Image Comparision Method=========================
  String File1 =PropertyReader.getProperty("ImageLocation1")+".jpg";
  String File2 =PropertyReader.getProperty("ImageLocation2")+".jpg";
  SkySiteUtils.waitTill(5000);
  result = CommonMethod.Imagecomparision(File1,File2);


if(result==true)
 return true;
else
	return false;
	
}

//===================Hyper-link Circle Tools =================================
public boolean HyperlinkCircleValidation() throws Throwable

{boolean result = true;
Log.message("Enter Into Hyperlink Circle Method");
SkySiteUtils.waitTill(3000);
SkySiteUtils.waitForElement(driver, OpenHyperlinkmenu, 30);
OpenHyperlinkmenu.click();
Log.message("Open HyperLink Button clicked sucessfully");
SkySiteUtils.waitTill(5000);
SkySiteUtils.waitForElement(driver,Hyperlink_Circle, 30);
//==== FreeHand tool-Baar===================== 
SkySiteUtils.waitTill(5000);
Hyperlink_Circle.click();
Log.message("HyperLink Circle Button clicked sucessfully ");
SkySiteUtils.waitTill(5000);
Actions builder = new Actions(driver); 

Actions drawAction = builder.moveToElement(ImageareA,862,261).clickAndHold().moveToElement(ImageareA, 969, 439);

SkySiteUtils.waitTill(3000);
drawAction.build().perform();
drawAction.click().release();    
SkySiteUtils.waitTill(10000);

File screenshot1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
BufferedImage  fullImg1 = ImageIO.read(screenshot1);
org.openqa.selenium.Point point1 = ImageareA.getLocation();
// Get width and height of the element
int eleWidth1 = ImageareA.getSize().getWidth();
int eleHeight1 = ImageareA.getSize().getHeight();
Log.message("Width of the screenshot "+eleWidth1);
Log.message("Width of the screenshot "+eleHeight1);
BufferedImage eleScreenshot1= fullImg1.getSubimage(point1.getX(), point1.getY(), eleWidth1, eleHeight1);
ImageIO.write(eleScreenshot1, "png", screenshot1);
Log.message("bufferImage "+eleScreenshot1);

String FileLocation1 =PropertyReader.getProperty("ImageLocation1");
File screenshotLocation1 = new File(FileLocation1+".jpg");
FileUtils.copyFile(screenshot1, screenshotLocation1);
Log.message("After Save Button Screenshot Taken Sucessfully");
SkySiteUtils.waitTill(5000);



 return true;
	
}


public boolean FreeHandValidation() throws Throwable

{boolean result = true;
Log.message("Enter Into Freehand Method");
SkySiteUtils.waitTill(3000);
SkySiteUtils.waitForElement(driver, OpenLineMenu, 30);
//==== Open line Menu ToolBaar Click	=======	
OpenLineMenu.click();
Log.message("Open Line Button clicked sucessfully ");
SkySiteUtils.waitTill(3000);
SkySiteUtils.waitForElement(driver,DrowWithFreehand, 30);
//==== FreeHand tool-Baar===================== 
SkySiteUtils.waitTill(3000);
DrowWithFreehand.click();
Log.message("Freehand Button clicked sucessfully ");	
Actions builder = new Actions(driver); 
Actions drawAction = builder.moveToElement(ImageareA,862,261).clickAndHold().moveToElement(ImageareA, 969, 439).clickAndHold(ImageareA);
SkySiteUtils.waitTill(3000);
drawAction.perform();
  
SkySiteUtils.waitTill(10000);

File screenshot1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
BufferedImage  fullImg1 = ImageIO.read(screenshot1);
org.openqa.selenium.Point point1 = ImageareA.getLocation();
// Get width and height of the element
int eleWidth1 = ImageareA.getSize().getWidth();
int eleHeight1 = ImageareA.getSize().getHeight();
Log.message("Width of the screenshot "+eleWidth1);
Log.message("Width of the screenshot "+eleHeight1);
BufferedImage eleScreenshot1= fullImg1.getSubimage(point1.getX(), point1.getY(), eleWidth1, eleHeight1);
ImageIO.write(eleScreenshot1, "png", screenshot1);
Log.message("bufferImage "+eleScreenshot1);

String FileLocation1 =PropertyReader.getProperty("ImageLocation1");
File screenshotLocation1 = new File(FileLocation1+".jpg");
FileUtils.copyFile(screenshot1, screenshotLocation1);
Log.message("After Save Button Screenshot Taken Sucessfully");
SkySiteUtils.waitTill(5000);



 return true;
	
}


//=========Arrow Related Test Cases==========================
public boolean ArrowValidation() throws Throwable

{
	boolean result = false;	
	Log.message("Enter Into open line menumethod");
	SkySiteUtils.waitTill(3000);
	SkySiteUtils.waitForElement(driver, OpenLineMenu, 30);	
	//==== Open line Menu ToolBaar Click	=======	
	OpenLineMenu.click();	
	Log.message("Open Line Button clicked sucessfully ");
	Thread.sleep(3000);
	SkySiteUtils.waitForElement(driver,Arrow, 30);
	//==== Arrow Tool-Baar===================== 	
	Thread.sleep(2000);	
	Arrow.click();	
	Log.message("Arrow Button clicked sucessfully ");
	SkySiteUtils.waitTill(3000);
	//Action class for selecting the Image And Clicking the particular Area As per X and Y Axis	
	Actions builder = new Actions(driver);  
	builder.moveToElement(ImageareA, 785,322);
	builder.clickAndHold();
	builder.moveToElement(ImageareA, 1130, 261);
	builder.clickAndHold();
	builder.release();
	builder.build().perform();
	Thread.sleep(10000);
	//builder.click();
	Thread.sleep(10000);	
	
  //=========First File Comparison==============================================
    
    File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    BufferedImage  fullImg = ImageIO.read(screenshot);

    org.openqa.selenium.Point point = ImageareA.getLocation();

    // Get width and height of the element
    int eleWidth = ImageareA.getSize().getWidth();
    int eleHeight = ImageareA.getSize().getHeight();
    Log.message("Width of the screenshot "+eleWidth);
    Log.message("Width of the screenshot "+eleHeight);
    BufferedImage eleScreenshot= fullImg.getSubimage(point.getX(), point.getY(),
        eleWidth, eleHeight);
    ImageIO.write(eleScreenshot, "png", screenshot);
    Log.message("bufferImage "+eleScreenshot);
    //============File Comparision=======================================
    String FileLocation1 =PropertyReader.getProperty("ImageLocation1");
    File screenshotLocation = new File(FileLocation1+".jpg");
    FileUtils.copyFile(screenshot, screenshotLocation);   
    
    SkySiteUtils.waitTill(3000);
/*    SaveButton.click();
    Log.message("Save Button Clicked Sucessfully");
    //============= second screenshot for file comparision
    File screenshot2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    BufferedImage  fullImg2 = ImageIO.read(screenshot2);
    org.openqa.selenium.Point point2 = ImageareA.getLocation();
    
    // Get width and height of the element
    int eleWidth2 = ImageareA.getSize().getWidth();
    int eleHeight2 = ImageareA.getSize().getHeight();
    Log.message("Width of the screenshot "+eleWidth2);
    Log.message("Width of the screenshot "+eleHeight2);
    BufferedImage eleScreenshot2= fullImg2.getSubimage(point2.getX(), point2.getY(),
        eleWidth2, eleHeight2);
    ImageIO.write(eleScreenshot2, "png", screenshot2);
    Log.message("bufferImage "+eleScreenshot2);
    String FileLocation2 =PropertyReader.getProperty("ImageLocation2");
    File screenshotLocation2 = new File(FileLocation2 +".jpg");
    FileUtils.copyFile(screenshot2, screenshotLocation2);    
   
    Log.message("Screenshot save In specified Area ");
    String FileName1 = PropertyReader.getProperty("ImageLocation1"); 
    String FileName2 = PropertyReader.getProperty("ImageLocation2");    
   CommonMethod.Imagecomparision(FileName1, FileName2);
  */
    SkySiteUtils.waitTill(8000);	
	
	return true;
}

//=================highlighter related Testcases==================================================
public boolean RetangleHightlighter() throws Throwable

{
	boolean result = false;	
	Log.message("Enter Into Retangle Hightlighter menu");
	SkySiteUtils.waitTill(3000);
	//==== ZoomIn button validation	=======	
	Highlighter.click();
	Log.message("Highlighter toolbaar Button Clicked Sucessfully");
	Actions builder = new Actions(driver);    
    Actions drawAction = builder.moveToElement(ImageareA,862,261).clickAndHold().moveToElement(ImageareA, 969, 439);    
    SkySiteUtils.waitTill(3000);
    drawAction.build().perform();
    //drawAction.click().release();    
    SkySiteUtils.waitTill(10000);
    File screenshot1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    BufferedImage  fullImg1 = ImageIO.read(screenshot1);
    org.openqa.selenium.Point point1 = ImageareA.getLocation();
    // Get width and height of the element
    int eleWidth1 = ImageareA.getSize().getWidth();
    int eleHeight1 = ImageareA.getSize().getHeight();
    Log.message("Width of the screenshot "+eleWidth1);
    Log.message("Width of the screenshot "+eleHeight1);
    BufferedImage eleScreenshot1= fullImg1.getSubimage(point1.getX(), point1.getY(), eleWidth1, eleHeight1);
    ImageIO.write(eleScreenshot1, "png", screenshot1);
    Log.message("bufferImage "+eleScreenshot1);

    String FileLocation1 =PropertyReader.getProperty("ImageLocation1");
    File screenshotLocation1 = new File(FileLocation1+".jpg");
    FileUtils.copyFile(screenshot1, screenshotLocation1);
    Log.message("After Save Button Screenshot Taken Sucessfully");
    SkySiteUtils.waitTill(5000);

 
	return true;
}


//=================Add A Text Test-cases==================================================
public boolean Add_A_Text() throws Throwable

{
	boolean result = false;	
	Log.message("Enter Into Add A text  menu");
	SkySiteUtils.waitTill(5000);
	//==== ZoomIn button validation	=======	
	Highlighter.click();
	Log.message("Highlighter toolbaar Button Clicked Sucessfully");		
	
   Actions builder = new Actions(driver);  
   Actions drawAction = builder.moveToElement(ImageareA,862,261).clickAndHold().moveToElement(ImageareA, 969, 439).clickAndHold();
   drawAction.build().perform();
   //drawAction.release();
  
   SkySiteUtils.waitTill(10000);
   File screenshot1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
   BufferedImage  fullImg1 = ImageIO.read(screenshot1);
   org.openqa.selenium.Point point1 = ImageareA.getLocation();
   // Get width and height of the element
   int eleWidth1 = ImageareA.getSize().getWidth();
   int eleHeight1 = ImageareA.getSize().getHeight();
   Log.message("Width of the screenshot "+eleWidth1);
   Log.message("Width of the screenshot "+eleHeight1);
   BufferedImage eleScreenshot1= fullImg1.getSubimage(point1.getX(), point1.getY(), eleWidth1, eleHeight1);
   ImageIO.write(eleScreenshot1, "png", screenshot1);
   Log.message("bufferImage "+eleScreenshot1);

   String FileLocation1 =PropertyReader.getProperty("ImageLocation1");
   File screenshotLocation1 = new File(FileLocation1+".jpg");
   FileUtils.copyFile(screenshot1, screenshotLocation1);
   Log.message("After Save Button Screenshot Taken Sucessfully");
   SkySiteUtils.waitTill(5000);


	return true;
}




/*public ProjectDashboardPage LogOut()
	
	{
		
		SkySiteUtils.waitForElement(driver, LogOut, 60);			
		LogOut.click();	
		Log.message("Logout button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, YesButton, 30);
		Log.message("Waiting for logout Button Present");
		YesButton.click();
		Log.message("Yes Button Clicked Sucessfully ");
		return new ProjectDashboardPage(driver).get();		
		
		
	}*/


	
public boolean ZoomINValidation() throws Throwable

{
	boolean result = false;
	
	Log.message("Enter Into zoom method");	
	//==== ZoomIn button validation	=======	
	ZoomIn.click();
	
	Log.message("Zoom In Button clicked sucessfully -First time.");
	SkySiteUtils.waitTill(3000);
	ZoomIn.click();
	Log.message("Zoom In Button clicked sucessfully -Second time.");
	SkySiteUtils.waitTill(3000);
	ZoomIn.click();
	Log.message("Zoom In Button clicked sucessfully -Third time.");	
	//ZoomOut Button Clicked Sucessfully.
	SkySiteUtils.waitTill(3000);
	if(ZoomInDisabled.isDisplayed())
		return true;
	else
		return false;
}



public boolean RFITAB() throws Throwable

{
boolean result = false;
SkySiteUtils.waitTill(3000);
SkySiteUtils.waitForElement(driver, RFITab, 30);
RFITab.click();
Log.message("RFI Tab Sucessfully");	


return result;


}

public boolean UPDATED_RFIROW() throws Throwable

{
boolean result = false;
SkySiteUtils.waitTill(10000);
SkySiteUtils.waitForElement(driver, RFI_Firstrow, 30);
RFI_Firstrow.click();
SkySiteUtils.waitTill(10000);
Log.message("RFI FirstRow Clicked Sucessfully");


return result;


}

public boolean Close_POPUP() throws Throwable

{
boolean result = false;
SkySiteUtils.waitTill(5000);
SkySiteUtils.waitForElement(driver, Closebutton, 60);
Closebutton.click();
Log.message("RFI close Button Clicked Sucessfully");
SkySiteUtils.waitTill(10000);
return result;


}




public boolean CloseThePOPUSaveMarkUP() throws Throwable

{
boolean result = false;
SkySiteUtils.waitTill(10000);
SkySiteUtils.waitForElement(driver, RFI_Firstrow, 30);
RFI_Firstrow.click();
SkySiteUtils.waitTill(10000);
Log.message("RFI FirstRow Clicked Sucessfully");	
return result;


}


public boolean ProjectManagement() throws Throwable

{
boolean result = false;
SkySiteUtils.waitTill(10000);
SkySiteUtils.waitForElement(driver, ProjectManagement, 60);
ProjectManagement1.click();
SkySiteUtils.waitTill(5000);
Log.message("project Management Link Click Sucessfully");	
SkySiteUtils.waitForElement(driver, ProjectManagement_RFI, 60);
ProjectManagement_RFI.click();
SkySiteUtils.waitTill(10000);
Log.message("RFI Link Click Sucessfully");	

return result;


}
public boolean ZoomOUTValidation() throws Throwable

{
	boolean result = false;
	
	Log.message("Enter Into zoom out method");	
	//==== ZoomIn button validation	=======	
	ZoomOut.click();
	
	Log.message("Zoom Out Button clicked sucessfully -First time.");
	SkySiteUtils.waitTill(3000);
	ZoomOut.click();
	Log.message("Zoom Out Button clicked sucessfully -Second time.");
	SkySiteUtils.waitTill(3000);
	ZoomOut.click();
	Log.message("Zoom out Button clicked sucessfully -Third time.");	
	//ZoomOut Button Clicked Sucessfully.
	SkySiteUtils.waitTill(3000);
	if(driver.findElement(By.xpath("//a[@class='leaflet-control-zoom-out leaflet-disabled']")).isDisplayed())
		return true;
	else
		return false;
}	
	
	

	
public boolean HomeButton() throws Throwable

{
	boolean result = false;	
	SkySiteUtils.waitTill(3000);
    ZoomOut.click();
	
	Log.message("Zoom Out Button clicked sucessfully -First time.");
	return result;
	
}}