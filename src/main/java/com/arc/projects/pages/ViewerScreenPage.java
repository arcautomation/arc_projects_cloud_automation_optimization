package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.Assert;
import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;

public class ViewerScreenPage extends LoadableComponent<ViewerScreenPage> {

	WebDriver driver;
	ProjectDashboardPage projectDashboardPage;

	ViewerScreenPage viewerScreenPage;
	static String Stamp_inbox = Generate_Random_Number.getRandomText(3);
	static String Stamp_title = Generate_Random_Number.MyDate();
	static String Stamp_inbox1 = Generate_Random_Number.getRandomText1(3);
	static String Stamp_title1 = Generate_Random_Number.MyDate();
	private boolean isPageLoaded;

	// ========== Status Validation==========
	@FindBy(xpath = "//*[@id='btn-Load-attachment']")
	WebElement Addattachment;

	@FindBy(xpath = "//span [@class='label label-warning pull-right pw-status']")
	WebElement OpenText;

	@FindBy(xpath = "(//div [@class='row has-tooltip'])[1]")
	WebElement RFIFirstRow;

	@FindBy(xpath = "(//button [@class='btn btn-default'])[9]")
	WebElement CloseRFI;

	@FindBy(xpath = "//button [@data-bind='click: Cancel']")
	WebElement ClosePOPUP;

	@FindBy(xpath = "(//i [@class='icon icon-2x icon-rfi pw-icon-lg-green'])[1]")
	WebElement FirstRFIDropwon_Green;

	@FindBy(xpath = "//div [@class='leaflet-marker-icon icon icon-rfi icon-3x pw-icon-lg-green leaflet-zoom-animated leaflet-clickable']")
	WebElement viewerImage_Green;

	@FindBy(xpath = "(//button [@class='btn btn-default'])[7]")
	WebElement VoidRFI;

	@FindBy(xpath = "(//button [@class='btn btn-default'])[6]")
	WebElement REOPENRFI;

	@FindBy(xpath = "//span [@class='label label-success pull-right pw-status']")
	WebElement CloseText;

	@FindBy(xpath = "//textarea [@id='txtAnswerEditable']")
	WebElement EmployeeTextArea;

	@FindBy(xpath = "(//button [@class='btn btn-default'])[3]")
	WebElement REPLYButton;
	// ========================================================
	@FindBy(linkText = "Process completed")
	WebElement linkProcessCompleted;

	@FindBy(xpath = "(//i [@class='icon icon-ellipsis-horizontal media-object icon-lg'])[1]")
	WebElement FirstRevisionFileLink;

	@FindBy(xpath = "(//i [@class='icon icon-ellipsis-horizontal media-object icon-lg'])[2]")
	WebElement SecondRevisionFileLink;

	@FindBy(xpath = "(//a [@class='btnDeletedoc'])[1]")
	WebElement Revision_Delete1;

	@FindBy(xpath = "(//a [@class='btnDeletedoc'])[2]")
	WebElement Revision_Delete2;

	@FindBy(css = "#aPrjAddFolder")
	WebElement btnAddNewFolder;

	@FindBy(xpath = "//*[@id='txtSearchKeyword']")
	WebElement GlobalSearch;

	@FindBy(xpath = "//*[@id='btnSearch']")
	WebElement ButtonSearch;

	// =====ZoomIn Button======================
	@FindBy(xpath = "//i[@class='icon icon-plus']")
	WebElement ZoomIn;
	// =====ZoomOut Button======================
	@FindBy(xpath = "//i[@class='icon icon-minus']")
	WebElement ZoomOut;

	@FindBy(xpath = "//a[@class='leaflet-control-zoom-in leaflet-disabled']")
	WebElement ZoomInDisabled;

	// ========HomeButton======================
	@FindBy(css = ".leaflet-control-zoom-out")
	WebElement Home;

	// ========Full Screen Button===============
	@FindBy(css = ".leaflet-control-zoom-fullscreen.leaflet-bar-part.leaflet-bar-part-bottom.last")
	WebElement FullScreen;

	// ========Pointer Arrow========================
	@FindBy(css = "#pointerToolMenuBtn")
	WebElement OpenPointer_Menu;

	// ========Pointer Arrow_Submenu_Arrow===========
	@FindBy(css = "#panSelectMenuBtn")
	WebElement Pointer_Arrow_submenu;

	// ========Pointer Arrow_Submenu_Multi_select====
	@FindBy(css = ".icon.icon-lg.icon-multi_select")
	WebElement Pointer_Arrow_submenu_MultiSelect;

	// ========OpenLineMenu==========================
	@FindBy(xpath = "(//i[@class='icon icon-lg icon-select'])[1]")
	WebElement pointer_Menu;

	@FindBy(xpath = "//a[@id='lineToolMenuBtn'][1]")
	WebElement OpenLineMenu;

	@FindBy(xpath = "(//i[@class='icon icon-lg icon-line']")
	WebElement DROWlINE;

	@FindBy(xpath = "//i [@class='icon icon-lg icon-arrow']")
	WebElement Arrow;

	@FindBy(xpath = "//a[@data-original-title='Draw with a freehand']")
	WebElement DrowWithFreehand;

	@FindBy(xpath = ".//*[@id='imageViewer']")
	WebElement ImageareA;

	@FindBy(xpath = "//a[@class='leaflet-control-savebar-save']")
	WebElement SaveButton;

	@FindBy(xpath = "//i[@ class='icon icon-lg icon-highlight']")
	WebElement Highlighter;

	// =============== Add a Text/CALLOUT =================

	@FindBy(xpath = "//a[@data-original-title='Open text menu']")
	WebElement OpenTextMenu;

	@FindBy(xpath = "//a[@data-original-title='Add a text']")
	WebElement AddaText;

	@FindBy(css = ".icon.icon-lg.icon-callout2")
	WebElement CALLOUT;

	@FindBy(xpath = "//*[@id='editSheetViewerText']")
	WebElement Textarea;

	@FindBy(xpath = "//*[@id='editSheetViewerText']")
	WebElement CallOutTextArea;

	@FindBy(id = "selectColor")
	WebElement Fontcolour;

	@FindBy(xpath = "//*[@id='selectBackColor']")
	WebElement Fillcolour;

	@FindBy(id = "selectFontSize")
	WebElement Fontsize;

	@FindBy(xpath = "//*[@id='selectLineWeight']")
	WebElement BorderThickness;

	@FindBy(xpath = "//*[@id='selectBackColor']")
	WebElement BackGroundColour;

	@FindBy(xpath = ".//*[@id='selectBorderColor']")
	WebElement BorderColour;

	@FindBy(xpath = "//button[@class='btn btn-primary btn-sm saveLayer']")
	WebElement Text_okButton;

	@FindBy(xpath = "//*[@id='txtMarkupTitle']")
	WebElement MarkUpName;

	@FindBy(xpath = "//*[@id='saving-markup-btn']")
	WebElement MarkName_SaveButton;

	@FindBy(css = ".form-inline.pw-pager")
	WebElement NoOfViewerMaxValuesheet;

	@FindBy(xpath = "(//i [@class='icon icon-step-forward'])[2]")
	WebElement NextPageButton;
	
	@FindBy(xpath = "//button [@class='btn btn-default btn-sm pull-left deleteLayer']")
	WebElement DeleteButton;
	
	//button [@class='btn btn-default btn-sm pull-left deleteLayer']

	// ============Open shape menu for Cloud Retangle============

	@FindBy(xpath = "//i [@class='icon icon-lg icon-shape_group']")
	WebElement OpenShapeMenu;

	@FindBy(xpath = "(//i[@class='icon icon-lg icon-cloud2'])[1]")
	WebElement DrowCloud;

	// ============Open shape menu for Hyper-link Circle============

	@FindBy(xpath = "html/body/div[1]/div[8]/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li[6]/a")
	WebElement OpenHyperlinkmenu;

	@FindBy(xpath = "html/body/div[1]/div[8]/div/div[2]/div/div/div[1]/div[2]/div[2]/div/div/ul/li[6]/ul/li[2]/a")
	WebElement Hyperlink_Circle;

	// =========Create Punch=========================
	@FindBy(xpath = "//*[@id='createNewPunch']")
	WebElement Projectlevel_Punchlink;

	@FindBy(css = ".leaflet-draw-draw-punch")
	WebElement punchShortcuttools;

	@FindBy(xpath = "//*[@id='closeAddNewPunchStamp']")
	WebElement Stamp;

	@FindBy(xpath = "//button[@class ='btn btn-default btn-block']")
	WebElement AddNewButton;

	@FindBy(xpath = "//*[contains(text(),'Add new')]")
	WebElement AddNew_Button;

	@FindBy(xpath = "//input[@id='txtShortDescription']")
	WebElement Stamp_Inbox;

	@FindBy(css = "#txtTitleDescription")
	WebElement Stamp_Title;

	@FindBy(xpath = "//i[@class='icon icon-ok']")
	WebElement RightButton;

	@FindBy(xpath = "//*[@id='divPunchAssignee']/div/a")
	WebElement AssignTo;

	@FindBy(xpath = "//*[@id='txtUserSearchKeyword']")
	WebElement SelectUse_Inbox;

	@FindBy(xpath = "//input [@class='pull-left']")
	WebElement SelectUser_CheckBox;

	@FindBy(xpath = "//*[@id='divProjectUsers']/div/div[1]/div[2]/a[1]")
	WebElement Search_Button;

	@FindBy(xpath = "//button[contains(text(),'Select user')]")
	WebElement SelectUser_Button;

	@FindBy(xpath = "//*[@id='divCC']/div[2]/a/i")
	WebElement CC_option;

	@FindBy(xpath = "//span [@class='glyphicon glyphicon-calendar icon-lg icon icon-calendar']")
	WebElement CalenderLink;

	@FindBy(xpath = "(//th [@class='next'])[1]")
	WebElement NextMonthDate;

	@FindBy(xpath = "//div/div[1]/table/tbody/tr[3]/td[4]")
	WebElement pickDate;

	@FindBy(xpath = "(//td[@class='day active today'])[1]")
	WebElement pickDate1;

	@FindBy(xpath = "//textarea[@data-bind='value: PunchDescription']")
	WebElement Description;

	@FindBy(xpath = "//button[@id='add-local-file']")
	WebElement UploadFileTab;

	@FindBy(xpath = "//*[@id='add-project-file']")
	WebElement ProjectTab;

	@FindBy(xpath = "//*[@id='txtDocSearchKeyword']")
	WebElement ProjectInbox;

	@FindBy(xpath = "(//i [@class='icon icon-search icon-lg'])[1]")
	WebElement SearchButton;

	@FindBy(xpath = "(//i [@class='icon icon-3x icon-folder-close'])[1]")
	WebElement FolderSelection;

	@FindBy(xpath = "(//input [@type='checkbox'  and @data-bind='checked: IsChecked'])[1]")
	WebElement file_Checkox;

	@FindBy(css = "#add-project-file")
	WebElement Projectfiles;

	@FindBy(css = "#txtDocSearchKeyword")
	WebElement SearchProjectfilesInbox;

	@FindBy(css = ".icon.icon-search.icon-lg")
	WebElement ProjectfilesTab_searchButton;

	@FindBy(css = ".icon.icon-3x.icon-folder-close")
	WebElement Folder;

	@FindBy(xpath = "(//i [@class='icon icon-file-alt icon-3x'])[1]")
	WebElement File_CheckBox;

	@FindBy(xpath = "(//i [@class='icon icon-file-alt icon-3x'])[1]")
	WebElement File_CheckBox1;

	@FindBy(xpath = "//*[@id='punchAttachedFiles']/span[1]")
	WebElement File_Upload_index;

	@FindBy(xpath = "(//i [@class='icon icon-file-alt icon-3x'])[1]")
	WebElement Projectfiles_FileCheckbox;

	// **************************Upload files************************

	@FindBy(xpath = ".//*[@id='add-local-file']")
	WebElement UploadTab;

	@FindBy(css = ".btn.btn-primary.set-files")
	WebElement selectfiles;

	@FindBy(xpath = "//*[@id='btnSelectFiles']")
	WebElement ChooseFileButton;

	@FindBy(xpath = "//*[@id='btnCreate']")
	WebElement CreatePunch;

	@FindBy(css = ".col-sm-12>small")
	WebElement Aftersave_Email1;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[1]/div[2]/div/span[2]/small[1]")
	WebElement Aftersave_Email2;

	@FindBy(css = ".attachment.doc-view>span")
	WebElement Attachment;

	@FindBy(css = ".btn.btn-info")
	WebElement ClosePunch;

	// ==============Punch Owner
	// Description=====================================

	@FindBy(css = ".punch-stamp.pull-left.punch-open")
	WebElement Shortdescription;

	@FindBy(css = ".col-md-11>div>h4")
	WebElement ShortNumber;

	@FindBy(css = ".col-sm-12>small")
	WebElement Afrerpunch_AssignTo;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[1]/div[2]/div/span[2]/small[1]")
	WebElement Afrerpunch_CCOption;

	@FindBy(xpath = "(//a[@class='attachment doc-view'])[1]")
	WebElement UplodedFileName1;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[3]/div/a[1]")
	WebElement UplodedFile1_download;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[3]/div/a[2]/span")
	WebElement UplodedFileName2;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[3]/div/a[2]")
	WebElement UplodedFile2_download;

	@FindBy(xpath = "//*[@id='imageViewer']/div[1]/div[1]/div/div[2]/img[2]")
	WebElement beforeImagearea;

	@FindBy(css = "circle.leaflet-clickable")
	WebElement afterImagearea;

	@FindBy(xpath = "//*[@id='photo-upload-button']")
	WebElement UPLoadPhotoButton;

	@FindBy(xpath = "//div[@class='leaflet-marker-icon icon icon-photo-rounded icon-3x leaflet-zoom-animated leaflet-clickable']")
	WebElement AfterPhotoImAGE;

	@FindBy(xpath = "//a [@class='close']")
	WebElement CloseImage;

	@FindBy(xpath = "//div[@class='leaflet-marker-pane']/div")
	WebElement afterImagearea_RFI;

	@FindBy(xpath = "//*[contains(text(),'Close RFI')]")
	WebElement CloseRFIByowner;

	// ===================================================

	@FindBy(xpath = "(//button [@id='photo-upload-button'])[1]")
	WebElement UploadPhoto_Button;

	@FindBy(xpath = "//*[@id='btnSelectFiles']")
	WebElement Choosefile_Button;

	@FindBy(xpath = "//button [@id='btnViewerFileUpload']")
	WebElement Upload_Button;

	@FindBy(xpath = "//input [@id='photoAnnName']")
	WebElement PhotoInbox;

	@FindBy(xpath = "(//button[@type='button'])[17]")
	WebElement Save_Button;

	@FindBy(xpath = "//i [@class='icon-lg icon-photo_mgmt']")
	WebElement PhotoDrodown;

	@FindBy(xpath = "(//i [@class='icon icon-user'])[1]")
	WebElement FirstRowClick;

	@FindBy(xpath = "//span [@class='edit-content']")
	WebElement ContentsText;

	// ==================================================

	@FindBy(xpath = "//*[@id='DocViewerHeader']")
	WebElement ClickOtherArea;

	@FindBy(xpath = ".//*[@id='spnPunchCount']")
	WebElement TotalPunch_Display;

	@FindBy(xpath = "//i [@class='icon icon-app-punch icon-lg' ]")
	WebElement DropDown;

	@FindBy(css = ".punch-stamp.punch-stamp-small.punch-open")
	WebElement Display_firstrow;

	@FindBy(css = "(//div [@class='punch-stamp punch-stamp-small punch-open'])[1]")
	WebElement shortNumber;

	@FindBy(xpath = "(//h4[@data-toggle='tooltip']/span)[1]")
	WebElement PunchClose_shortdescription;

	// ================ Punch shortCut=================
	@FindBy(xpath = "//li[@id='liPunchContainer']/a/i")
	WebElement PunchDropDown_Button;

	@FindBy(xpath = "(//div [@class='punch-stamp punch-stamp-small punch-open'])[1]")
	WebElement PunchFirstRow;

	@FindBy(xpath = "(//li[@id='liShowPunchList'])[1]")
	WebElement Punchshortcut;

	@FindBy(xpath = "(//ul[@id='ulPunchList']/li/div[2]/div[4]/span)[1]")
	WebElement Status;

	@FindBy(xpath = "(//ul[@id='ulPunchList']/li/div[2]/div/div)[1]")
	WebElement SheetNO;

	@FindBy(css = "span.punch-title")
	WebElement SheetDescription;

	@FindBy(xpath = "//*[@id='ulPunchList']/li[1]/div[2]/div[3]/div/strong")
	WebElement AttachementStatus;

	@FindBy(xpath = "//*[@id='ulPunchList']/li[1]/div[2]/div[3]/div/strong")
	WebElement Toemail;

	@FindBy(xpath = "//*[@id='txtComment']")
	WebElement Addcomment;

	@FindBy(xpath = "//*[@id='btnSubmit']")
	WebElement Submitbutton;

	@FindBy(xpath = "//*[@id='btnAttachement']")
	WebElement AttachmentLink;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[2]/span")
	WebElement TestDescription;

	@FindBy(xpath = "//span[@class='label label-success pull-right pw-status pw-status-small']")
	WebElement CloseButton;

	@FindBy(css = "#btnRefreshPunch")
	WebElement RefereshButton;

	@FindBy(xpath = "//div/div[2]/div[2]/div[1]/div[8]/div/input")
	WebElement NewcustomAttributeField;

	// ====================================
	// Export Related Elements
	@FindBy(css = "#aPrivateProjects")
	WebElement PrivateProjectsTab;

	@FindBy(css = ".icon.icon-off")
	WebElement LogOut;

	@FindBy(xpath = "//*[@id='button-1']")
	WebElement YesButton;

	// =========Filename Selection=========================
	@FindBy(xpath = ".preview.document-preview-event")
	WebElement FileName_preview;

	// span[@class='noty_text']
	@FindBy(css = ".noty_text")
	WebElement notificationMsg;

	// ============================================

	@FindBy(css = "#FeedbackClose>span")
	WebElement FeedBackmeesage;

	@FindBy(xpath = "html/body/div[1]/div[3]/div[2]/div/ul/li[1]/div/section[1]")
	WebElement Project;

	@FindBy(xpath = "//*[@id='li_1KPavDU1%40lrrTmrIGviGoA%3d%3d']/div/section[1]")
	WebElement Project_test;

	@FindBy(xpath = "//*[@id='lirfiPunch']")
	WebElement ProjectManagement;

	@FindBy(xpath = "//*[@id='a_showRFIList']")
	WebElement RFI;

	@FindBy(xpath = "//i[@class='icon icon-rfi icon-lg']")
	WebElement revision_RFI;

	@FindBy(xpath = "//*[@class='aPunch']")
	WebElement punchmenu;

	@FindBy(xpath = ".//*[@id='liThirdTab']")
	WebElement AllPunchList;

	@FindBy(xpath = ".//*[@id='ulPunchList']/li[1]")
	WebElement PunchList;

	@FindBy(xpath = "html/body/div[1]/div[3]/div[3]/div[2]/ul/li[3]")
	WebElement Floder;

	// @FindBy(css="a.preview.document-preview-event")
	// WebElement ImageClick;

	@FindBy(xpath = "(//img[@class='img-responsive img-thumbnail' and @alt='drawings'])[1]")
	WebElement ImageClick;

	@FindBy(xpath = "(//img[@class='img-responsive img-thumbnail'and @alt='drawings'])[2]")
	WebElement ImageClick1;

	@FindBy(xpath = "//i[@class='icon icon-app-project icon-2x pulse animated']")
	WebElement ProjectManagement1;

	@FindBy(css = "#a_showRFIList")
	WebElement ProjectManagement_RFI;

	@FindBy(xpath = "//a[@title='All RFI']")
	WebElement RFITab;

	@FindBy(xpath = "(//img [@class='img-responsive img-thumbnail'])[1]")
	WebElement RFI_Firstrow;

	// ============create RFI=====================================

	@FindBy(xpath = "//i[@class='icon icon-rfi icon-lg']")
	WebElement REVISION_DROPDOWN;

	@FindBy(xpath = "//i [@class='icon icon-2x icon-rfi pw-icon-lg-orange']")
	WebElement REVISION_rfiiMAGE;

	// =================================================

	@FindBy(xpath = "//a [@class='leaflet-draw-draw-photo']")
	WebElement PHOTO_shortcut;

	// =================================================
	
	
	
	@FindBy(xpath="//i [@class='icon icon-chevron-right icon-lg']")
	WebElement rightArrow;

	//@FindBy(xpath = "//i [@class='icon icon-lg icon-rfi']")
	@FindBy(xpath="//*[@class='icon icon-lg icon-rfi']")
	WebElement RFI_Shortcut;

	@FindBy(xpath = "(//i[@class='icon icon-app-contact icon-lg'])[1]")
	WebElement RFI_TO;

	@FindBy(xpath = "(//i[@class='icon icon-app-contact icon-lg'])[2]")
	WebElement RFI_CC;

	@FindBy(xpath = "//*[@id='txtSubject']")
	WebElement RFI_SUBJECT;

	@FindBy(xpath = "//input[@ id='txtHasPotentialCostImpact']")
	WebElement PotentialCostImpact;

	@FindBy(xpath = "//input[@id='txtHasPotentialScheduleImpact']")
	WebElement PotentialScheduleImpact;

	@FindBy(css = "//*[@id='closeRfidiscipline']")
	WebElement RFI_Discipline;

	@FindBy(css = ".//*[@id='txtSheetNoData']")
	WebElement RFI_SheetNo;

	@FindBy(css = "//*[@id='txtSpecification']")
	WebElement RFI_Specification;

	@FindBy(xpath = "//*[@id='txtQuestion']")
	WebElement RFI_Question;

	@FindBy(xpath = "//*[@id='btnCreateRFI']")
	WebElement RFI_CreateButton;

	@FindBy(css = "#reassign_block_read-only>small")
	WebElement RFI_Email_TO;

	@FindBy(css = "#CCRecipient")
	WebElement RFI_Email_CC;

	@FindBy(xpath = "//*[@id='txtAnswerEditable']")
	WebElement AnswerEditBox;

	@FindBy(xpath = "(//i[@class='icon icon-paper-clip icon-lg'])[2]")
	WebElement AddAttachment;

	@FindBy(xpath = "//*[@id='btnCreateRFI']")
	WebElement ButtonRFI;

	@FindBy(xpath = "//*[@id='punchAttachedFiles']/span[1]")
	WebElement AttachementCount;

	@FindBy(xpath = "//span [@data-bind='text: Subject, attr: { title: Subject }']")
	WebElement Subject;

	@FindBy(xpath = "//span[@data-bind='html: Question']")
	WebElement RFI_Questions;

	@FindBy(xpath = "(//span [@class='yellow-txt'])[1]")
	WebElement RFI_Status;

	// ==========EmployeeLevel RFI =================================

	@FindBy(xpath = "//*[@id='liRFIContainer']/a/b")
	WebElement RFI_Dropdown;

	@FindBy(xpath = "html/body/div[1]/div[8]/div/div[1]/div/ul[1]/li[5]/div/ul/li[1]/div/div[2]/h4/span")
	WebElement RFI_FirstRow;

	@FindBy(xpath = "html/body/div[1]/div[8]/div/div[1]/div/ul[1]/li[5]/div/ul/li[1]/div/div[2]/h4/span")
	WebElement RFI_SecondRow;

	@FindBy(xpath = "//*[@id='DocViewerHeader']/nav")
	WebElement clickotherside;
	// ============ Employee Level Punch=============================

	@FindBy(xpath = "//i[@class='icon icon-app-punch icon-lg']")
	WebElement Punch_Dropdown;

	@FindBy(xpath = "(//h4 [@data-toggle='tooltip'])[1]")
	WebElement FirstRow;

	@FindBy(xpath = "(//h4 [@data-toggle='tooltip'])[2]")
	WebElement SecondRow;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[1]/div[1]/div[1]/div[3]/h4")
	WebElement Punch_Subject;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[1]/div[1]/div[1]/div[3]/h4")
	WebElement Punch_AssignTo;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[1]/div[2]/div/span[2]/small[1]")
	WebElement Punch_CC;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[2]/span")
	WebElement Punch_Description;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[3]/div/a[1]/span")
	WebElement Punch_Attachment1;

	@FindBy(xpath = "//*[@id='divEditPunch']/div/div[2]/div/div[1]/ul/li[3]/div/a[2]/span")
	WebElement Punch_Attachment2;

	@FindBy(css = "#txtComment")
	WebElement Punch_AddCommentInbox;

	@FindBy(css = "#btnAttachement")
	WebElement Punch_Attachment;

	@FindBy(css = "#btnSubmit")
	WebElement Punch_SubmitButton;

	@FindBy(xpath = "//span [@class='label label-warning pull-right pw-status pw-status-small']")
	WebElement Punch_EmployeeStatus;

	// ====================Logout====================================
	@FindBy(xpath = "//span[@ class='img-circle profile-no-image md']/img")
	WebElement Profile;

	@FindBy(css = ".icon.icon-off")
	WebElement Logout;
	// ==============================Reassign========================

	@FindBy(xpath = "//button[@class='btn btn-default btn-small']")
	WebElement ReassignLink;

	@FindBy(xpath = "//input[@id='txtReassignTOData']/following-sibling::a")
	WebElement AssitnTo_Mail;

    @FindBy(xpath = "(//button[@class='btn btn-default btn-sm'])[1]")
	WebElement RFI_SaveButton;

	@FindBy(xpath = "(//span [@class='label label-warning pull-right pw-status'])[1]")
	WebElement DisplayStatus;

	@FindBy(xpath = "(//span[@class='label label-warning pull-right pw-status'])[1]")
	WebElement OpenStatus;

	@FindBy(xpath = "(//span[@class='label label-warning pull-right pw-status'])[2]")
	WebElement OpenStatus_popUp;

	@FindBy(xpath = "(//span [@class='label label-success pull-right pw-status'])[1]")
	WebElement CloseStatus;

	@FindBy(xpath = "(//span [@class='label label-success pull-right pw-status'])[2]")
	WebElement Close_POPUP;

	@FindBy(xpath = "(//span [@class='label label-info pull-right pw-status'])[1]")
	WebElement EmployeeStatus;

	@FindBy(xpath = "(//button[@data-original-title='Close'])[1]")
	WebElement closebutton;

	@FindBy(xpath = "//button [@class='btn btn-default' and @data-bind='click: CloseRFI, clickBubble: false, visible: CanClose()']")
	WebElement CloseRFIButton;

	@FindBy(xpath = "//button [@ data-bind='click: ReopenRFI, clickBubble: false, visible: CanReopen()']")
	WebElement ReopenRFI;

	@FindBy(xpath = "//button [@data-bind='click: ForwardRFI, clickBubble: false, visible: CanForwardRFI()']")
	WebElement Forward;

	@FindBy(xpath = "//div/div/div[1]/div[1]/div[2]/button")
	WebElement Closebutton;

	@FindBy(xpath = "(//i [@class='icon icon-app-contact icon-lg'])[1]")
	WebElement Forward_Contact;

	@FindBy(xpath = " //textarea [@id='txtForwardComment']")
	WebElement TextArea;

	@FindBy(xpath = "//button[@data-bind='click: ForwardSendRFI, clickBubble: false, visible: CanForwardSendRFI()']")
	WebElement SendButton;

	/// ============simple execution================================
	@FindBy(xpath = "(//li [@id='li_1KPavDU1%40loQsPvNDPQp2A%3d%3d'])[1]")
	WebElement project;

	@FindBy(xpath = "html/body/div[1]/div[3]/div[3]/div[2]/ul/li[3]")
	WebElement folder;

	// ==========================================================

	@Override
	protected void load() {
		isPageLoaded = true;

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}

	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public ViewerScreenPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public boolean ObjectFeedback() {
		SkySiteUtils.waitForElement(driver, FeedBackmeesage, 20);
		Log.message("Waiting for FreeTrail Link to be appeared");
		if (FeedBackmeesage.isDisplayed())
			return true;
		else
			return false;
	}

	public boolean NavigatonBacktodeshboard(String Project_Name, String Foldername)
			throws InterruptedException, AWTException, IOException {
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, linkProcessCompleted, 180);
		int Avl_Fold_Count = 0;
		List<WebElement> allElements = driver.findElements(By.linkText("Process completed"));
		for (WebElement Element : allElements) {
			Avl_Fold_Count = Avl_Fold_Count + 1;
		}
		Log.message("Available Folder Count is: " + Avl_Fold_Count);

		if (linkProcessCompleted.isDisplayed() && Avl_Fold_Count == 1) {
			// SkySiteUtils.waitTill(3000);
			SkySiteUtils.waitForElement(driver, linkProcessCompleted, 180);
			linkProcessCompleted.click();
			Log.message("Clicked on Process Completed Link." + Avl_Fold_Count);
			// SkySiteUtils.waitTill(3000);
		} else {

			Log.message("go to dashboard page");
			// SkySiteUtils.waitTill(10000);
			SkySiteUtils.waitForElement(driver, GlobalSearch, 60);
			SkySiteUtils.waitTill(10000);
			GlobalSearch.sendKeys(Project_Name);
			Log.message("Entered Value In search Inbox to Search project ");
			SkySiteUtils.waitForElement(driver, ButtonSearch, 100);
			ButtonSearch.click();
			Log.message("Button search Clicked Sucessfully");

			SkySiteUtils.waitTill(2000);
			// Getting count of available projects
			int Avl_Projects_Count = 0;
			List<WebElement> allElements0 = driver.findElements(By.xpath("//*[contains(@id, 'PName_')]"));
			for (WebElement Element : allElements0) {
				Avl_Projects_Count = Avl_Projects_Count + 1;
			}
			Log.message("Available private projects count is: " + Avl_Projects_Count);
			SkySiteUtils.waitTill(10000);

			for (int i = 1; i <= Avl_Projects_Count; i++) {
				String Exp_ProjName = driver
						.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/ul/li[" + i + "]/div/section[1]/h4"))
						.getText();

				// Validating - Expected project is selected or not
				if (Exp_ProjName.trim().contentEquals(Project_Name.trim())) {
					Log.message("Maching Project Found!!");
					driver.findElement(
							By.xpath("html/body/div[1]/div[3]/div[2]/div/ul/li[" + i + "]/div/section[1]/h4")).click();
					Log.message("Clicked on expected project!!");
					SkySiteUtils.waitTill(10000);
					break;
				}

			}
			SkySiteUtils.waitTill(10000);

			Log.message("Select Particular Project from Dashboard");
			SkySiteUtils.waitForElement(driver, btnAddNewFolder, 60);
			Log.message("Waiting for Create Folder button to be appeared");
			// Getting Folder count after created a new folder
			// int Avl_Fold_Count = 0;
			String actualFolderName = null;
			List<WebElement> allElements1 = driver.findElements(By.xpath("//*[contains(@id, 'li_Fld_')]"));
			for (WebElement Element : allElements1) {
				Avl_Fold_Count = Avl_Fold_Count + 1;
			}
			Log.message("Available Folder Count is: " + Avl_Fold_Count);

			for (int j = 1; j <= Avl_Fold_Count; j++) {
				actualFolderName = driver
						.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
						.getText();
				Log.message("Exp Name:" + Foldername);
				Log.message("Act Name:" + actualFolderName);

				// Validating the new Folder is created or not
				if (Foldername.trim().contentEquals(actualFolderName.trim())) {
					driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li[" + j + "]/section[2]/h4"))
							.click();// Select a folder
					Log.message("Expected Folder is clicked successfully with name: " + Foldername);
					break;
				}
			}
			SkySiteUtils.waitTill(10000);
		}

		return result;

	}

	public boolean DeleteFirstRevisionfile(String parentHandle) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Log.message(parentHandle);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, FirstRevisionFileLink, 60);
		FirstRevisionFileLink.click();
		Log.message("First Revision File Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Revision_Delete1, 60);
		Revision_Delete1.click();
		Log.message("Delete Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		Log.message("Yes Button Clicked Sucessfully");
		return result;

	}

	public boolean DeleteSecondRevisionfile() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SecondRevisionFileLink.click();
		Log.message("Second Revision File Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Revision_Delete2, 60);
		Revision_Delete2.click();
		Log.message("Delete Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		Log.message("Yes Button Clicked Sucessfully");
		return result;

	}

	public boolean Logout() throws Throwable

	{
		boolean result = false;
		Log.message("Entered into Logout Method");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Profile, 120);
		SkySiteUtils.waitTill(1000);
		Profile.click();
		Log.message("Profile Option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Logout, 120);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", Logout);    
		
		/*SkySiteUtils.waitForElement(driver, Logout, 120);
		SkySiteUtils.waitTill(1000);
		Logout.click();*/
		Log.message("Logout option Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, YesButton,120);
		SkySiteUtils.waitTill(1000);
		YesButton.click();
		String actualTitle = "Sign in - SKYSITE";
		String expectedTitle = driver.getTitle();
		SkySiteUtils.waitTill(10000);
		Log.message("expected title is:" + expectedTitle);
		if (actualTitle.equalsIgnoreCase(expectedTitle))
			return result = true;
		else
			return result = false;

	}

	public boolean ReassignRFI_new() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFIFirstRow, 120);
		RFIFirstRow.click();
		Log.message("RFI List First Row  Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, ReassignLink, 120);
		ReassignLink.click();
		Log.message("Reassign Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, AssitnTo_Mail, 60);
		AssitnTo_Mail.click();
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 120);
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("user_1");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		SkySiteUtils.waitTill(2000);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 120);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SkySiteUtils.waitTill(2000);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, RFI_SaveButton, 60);
		RFI_SaveButton.click();
		Log.message("Save Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		Log.message("Yes Button Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		return result;

	}

	public boolean RFI_Reassign() throws Throwable

	{
		boolean result = false;
		Log.message("Reassign Link Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		ReassignLink.click();
		SkySiteUtils.waitForElement(driver, AssitnTo_Mail, 60);
		AssitnTo_Mail.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("ReAssignTo");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		RFI_SaveButton.click();
		Log.message("Save Button Clicked Sucessfully");
		SkySiteUtils.waitTill(6000);
		YesButton.click();
		Log.message("Yes Button Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_Dropdown, 60);
		RFI_Dropdown.click();
		Log.message("RFI Drop Down Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_FirstRow, 60);
		SkySiteUtils.waitTill(10000);
		RFI_FirstRow.click();
		Log.message("RFI First Row Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ClickOtherArea, 60);
		ClickOtherArea.click();
		SkySiteUtils.waitTill(10000);
		afterImagearea_RFI.click();
		Log.message("Image Area Clicked Sucessfully");
		// Log.message(parentHandle);
		SkySiteUtils.waitTill(20000);
		return result;

	}

	public boolean RFI_POPUP_validation() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		// =================Email To=================
		SkySiteUtils.waitForElement(driver, RFI_Email_TO, 60);
		String Email_To = RFI_Email_TO.getText();
		Log.message("Email_To Message is: " + Email_To);
		String Email = PropertyReader.getProperty("Email_AssignTo");
		if (Email_To.equalsIgnoreCase(Email)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}
		// =================CC Option=================
		SkySiteUtils.waitForElement(driver, RFI_Email_CC, 60);
		String Email_CC = RFI_Email_CC.getText();
		Log.message("Email_To Message is: " + Email_CC);
		String CCoption = PropertyReader.getProperty("Email_CCOption");
		if (Email_CC.equalsIgnoreCase(CCoption)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}
		// =================Subject=================
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject = Subject.getText();
		Log.message("subject Message is: " + subject);
		String RFI_Subject = PropertyReader.getProperty("RFI_Subject");
		if (subject.equalsIgnoreCase(RFI_Subject)) {
			Log.message("Subject verified Sucessfully ");

		} else {
			Log.message("Subject not verified Sucessfully");
		}
		// =================Question=================
		SkySiteUtils.waitForElement(driver, RFI_Questions, 60);
		String RFI_Questions1 = RFI_Questions.getText();
		Log.message("Question Message is: " + RFI_Questions1);
		String RFI_Questions = PropertyReader.getProperty("RFI_Questions");
		if (RFI_Questions1.equalsIgnoreCase(RFI_Questions)) {
			Log.message("Question verified Sucessfully ");

		} else {
			Log.message("Question not verified Sucessfully");
		}
		// ============OpenStatus==========================
		SkySiteUtils.waitForElement(driver, RFI_Status, 60);
		String Status = RFI_Status.getText();
		Log.message("Status Message is: " + Status);
		String status = PropertyReader.getProperty("Status");
		if (Status.equalsIgnoreCase(status)) {
			Log.message("Status verified Sucessfully ");
		} else {
			Log.message("Status not verified Sucessfully");
		}
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, TestDescription, 60);
		String addComment = PropertyReader.getProperty("AddComment");
		Addcomment.sendKeys(addComment);
		Log.message("Entered Value In Add Comment inbox: " + addComment);
		SkySiteUtils.waitTill(1000);
		Submitbutton.click();
		Log.message("submit button clicked sucessfully");
		// SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitTill(10000);
		String Commm = driver
				.findElement(By.xpath("html/body/div[1]/div[19]/div/div/div[2]/div/div[2]/ul/li/p[1]/span")).getText();
		Log.message("Comment name is:" + Commm);
		if (Commm.contains(addComment))
			return result= true;
		else
			return result = false;

	}

	public void RFI_OwnerLevel_validation() throws Throwable

	{
		//boolean result = false;
		SkySiteUtils.waitTill(10000);
		Log.message("Owner level validation Method ");	
		SkySiteUtils.waitForElement(driver, afterImagearea_RFI, 120);
		SkySiteUtils.waitTill(10000);
		afterImagearea_RFI.click();	
		SkySiteUtils.waitForElement(driver, RFI_Email_TO, 600);
		SkySiteUtils.waitTill(1000);
		Log.message("After Image Area Click Sucessfully ");
		// =================Email To=================
		SkySiteUtils.waitForElement(driver, RFI_Email_TO, 60);
		String Email_To = RFI_Email_TO.getText();
		SkySiteUtils.waitTill(1000);
		Log.message("Email_To Message is: " + Email_To);
		String Email = PropertyReader.getProperty("Email_AssignTo_emp");
		if (Email_To.equalsIgnoreCase(Email)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}

		// =================CC Option=================
		SkySiteUtils.waitForElement(driver, RFI_Email_CC, 60);
		String Email_CC = RFI_Email_CC.getText();
		SkySiteUtils.waitTill(1000);
		Log.message("Email_To Message is: " + Email_CC);
		String CCoption = PropertyReader.getProperty("Email_CCOption");
		if (Email_CC.equalsIgnoreCase(CCoption)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}

		// =================Subject=================
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject = Subject.getText();
		SkySiteUtils.waitTill(500);
		Log.message("subject Message is: " + subject);
		String RFI_Subject = PropertyReader.getProperty("RFI_Subject");

		if (subject.equalsIgnoreCase(RFI_Subject)) {
			Log.message("Subject verified Sucessfully ");

		} else {
			Log.message("Subject not verified Sucessfully");
		}

		// =================Question=================
		SkySiteUtils.waitForElement(driver, RFI_Questions, 60);
		String RFI_Questions1 = RFI_Questions.getText();
		SkySiteUtils.waitTill(500);
		Log.message("Question Message is: " + RFI_Questions1);
		String RFI_Questions = PropertyReader.getProperty("RFI_Questions");
		if (RFI_Questions1.equalsIgnoreCase(RFI_Questions)) {
			Log.message("Question verified Sucessfully ");

		} else {
			Log.message("Question not verified Sucessfully");
		}
		// ============OpenStatus==========================

		SkySiteUtils.waitForElement(driver, RFI_Status, 60);
		String Status = RFI_Status.getText();
		Log.message("Status Message is: " + Status);
		String status = PropertyReader.getProperty("Status");
		if (Status.equalsIgnoreCase(status)) {
			Log.message("Status verified Sucessfully ");
		} else {
			Log.message("Status not verified Sucessfully");
		}
		SkySiteUtils.waitForElement(driver, TestDescription, 60);
		String addComment = PropertyReader.getProperty("AddComment");
		Addcomment.sendKeys(addComment);
		Log.message("Entered Value In Add Comment inbox: " + addComment);
		SkySiteUtils.waitTill(500);
		Submitbutton.click();
		Log.message("submit button clicked sucessfully");
		// SkySiteUtils.waitTill(6000);
		/*SkySiteUtils.waitForElement(driver, CloseRFIButton, 60);
		if (CloseRFIButton.isDisplayed())
			return true;
		else
			return false;*/

		/*
		 * String Commm = driver.findElement(By.xpath(
		 * "html/body/div[1]/div[50]/div/div/div[2]/div/div[2]/ul/li/p[1]")).getText();
		 * Log.message("Comment name is:" + Commm); if (Commm.contains(addComment))
		 * return true; else return false;
		 */

	}
	/**public method to switch to new tab
	 * 
	 * 
	 * 
	 * 
	 * @param parentHandle
	 * @return
	 * @throws Throwable
	 */
	
	public void swichToMainWindow() throws Throwable

	{
		Robot rb = new Robot();
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_W);
		rb.keyRelease(KeyEvent.VK_CONTROL);
		SkySiteUtils.waitTill(10000);
		//driver.close();
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);

		}
		Log.message("switched to child window");

		driver.switchTo().defaultContent();

	}	
		
	

	public boolean SecondRFI_Validation() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		String MainWindow = driver.getWindowHandle();
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);
		}
		if (revision_RFI.isDisplayed()) {

			Log.message("Revision File Display Sucessfully");
		}
		else {
			Log.message("Revision File Not Displaying Here");
		}
		SkySiteUtils.waitTill(10000);
		driver.close();
		SkySiteUtils.waitTill(10000);
		driver.switchTo().window(MainWindow);
		return result;
	    }

	public void RFI_TestDataCreation_ownerLevel1() throws Throwable

	{
		//boolean result = false;
		SkySiteUtils.waitTill(10000);
		String MainWindow = driver.getWindowHandle();
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);
		}
		Log.message(MainWindow);
		driver.switchTo().defaultContent();
		Log.message("Switch to default content ");
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, RFI_Shortcut,120);		
		Actions act=new Actions(driver);
		act.moveToElement(RFI_Shortcut).click(RFI_Shortcut).build().perform();
		Log.message("create rfi icon is clicked");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, beforeImagearea, 120);
		SkySiteUtils.waitTill(10000);
		beforeImagearea.click();
		SkySiteUtils.waitTill(8000);
		Log.message("Waiting for create RFI Menu to be appeared");
		// ================ To ========================
		SkySiteUtils.waitForElement(driver, RFI_TO, 60);
		RFI_TO.click();
		Log.message(" To Link clicked Sucessfully");
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		// ************CC Option---------------------------------------
		SkySiteUtils.waitTill(10000);
		RFI_CC.click();
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(10000);
		SelectUse_Inbox.sendKeys(SelectEmail);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(10000);
		Search_Button.click();
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		// =====Subject==============================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 60);
		String Subject = PropertyReader.getProperty("RFI_Subject");
		RFI_SUBJECT.sendKeys(Subject);
		// ============ Question====================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		RFI_Question.sendKeys(question);		
		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			Log.message("Attachment Button Clicked Sucessfully");
		}
		SkySiteUtils.waitTill(8000);
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(1000);
		//UploadFiles(FolderPath, FileCount);
		SkySiteUtils.waitTill(2000);
		//projectfile_Upload1();
		// ======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitTill(2000);
		RFI_CreateButton.click();
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String message = notificationMsg.getText();
		Log.message("RFI  Message is: " + message);
		String expectedMessage = "RFI is added successfully";
		if (message.contains(expectedMessage)) {
			//result = true;
			Log.message("RFI is added successfully ");
		} else {
			//result = false;
			Log.message("RFI is  NOT added successfully ");
		}
		//Owner level validation
		RFI_OwnerLevel_validation();
		
		SkySiteUtils.waitTill(10000);
		driver.close();
		SkySiteUtils.waitTill(10000);
		driver.switchTo().window(MainWindow);
		SkySiteUtils.waitTill(10000);
		/*if (result == true)
			return true;
		else
			return false;
*/
	}

	

	public boolean RFI_firstRevision(String parentHandle) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}		
		Log.message(parentHandle);
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		if (result == true)
			return true;
		else
			return false;
	    }

	public boolean StatusValidation() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, OpenText, 60);
		String Open = OpenText.getText();
		Log.message("Open Button Display: " + Open);
		if (Open.equalsIgnoreCase("OPEN")) {
			Log.message("Status Open  verified Sucessfully ");
		} else {
			Log.message("Status Open not verified Sucessfully");
		}
		SkySiteUtils.waitForElement(driver, CloseRFI, 100);
		CloseRFI.click();
		Log.message("Close RFI Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		Log.message("Yes Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, REVISION_DROPDOWN, 60);
		REVISION_DROPDOWN.click();
		Log.message("Drop down Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, FirstRFIDropwon_Green, 60);
		FirstRFIDropwon_Green.click();
		Log.message("Drop down Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, viewerImage_Green, 60);
		viewerImage_Green.click();
		Log.message("viewer page Image RFI Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, CloseText, 60);
		String Close = CloseText.getText();
		Log.message("Close text Display: " + Close);
		if (Close.equalsIgnoreCase("CLOSE")) {
			Log.message("Status Close  verified Sucessfully ");
		} else {
			Log.message("Status Close not verified Sucessfully");
		}
		SkySiteUtils.waitForElement(driver, REOPENRFI, 60);
		REOPENRFI.click();
		Log.message("REOPEN RFI Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, CloseText, 60);
		String Reopen = OpenText.getText();
		Log.message("Reopen text Display: " + Reopen);
		if (Reopen.equalsIgnoreCase("REOPEN")) {
			Log.message("Status Reopen  verified Sucessfully ");
		} else {
			Log.message("Status reopen not verified Sucessfully");
		}

		return result;

	    }

	public boolean EmployeeLevelStatus_Validation() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, REVISION_DROPDOWN, 60);
		REVISION_DROPDOWN.click();
		Log.message("Drop down Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, EmployeeTextArea, 60);
		String text = PropertyReader.getProperty("EmployeeTextArea");
		EmployeeTextArea.sendKeys(text);
		SkySiteUtils.waitForElement(driver, REPLYButton, 60);
		REPLYButton.click();
		Log.message("Replay Button Clicked Sucessfully");
		REVISION_DROPDOWN.click();
		Log.message("Drop down Button Clicked Sucessfully");
		return result;

	}

	public boolean MainStatus() throws Throwable

	{
		boolean result = false;
		Log.message("Enter into main status ");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, OpenStatus, 60);
		String Open = OpenStatus.getText();
		Log.message("Open Button Display: " + Open);
		if (Open.equalsIgnoreCase("OPEN")) {
			Log.message("Status Open Button verified Sucessfully ");

		} else {
			Log.message("Status Open not verified Sucessfully");
		}
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, CloseRFIButton, 100);
		CloseRFIButton.click();
		Log.message("Close RFI Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		Log.message("Yes Button Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, REVISION_DROPDOWN, 60);
		REVISION_DROPDOWN.click();
		Log.message("Revision Dropdown click sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, REVISION_rfiiMAGE, 60);
		REVISION_rfiiMAGE.click();
		Log.message("Revision image click sucessfully");
		SkySiteUtils.waitTill(10000);
		afterImagearea_RFI.click();
		Log.message("RFI Image Area Clicked Sucessfully");

		SkySiteUtils.waitForElement(driver, CloseStatus, 60);
		String Close = CloseStatus.getText();
		Log.message("Close Button Display: " + CloseStatus);
		if (Close.equalsIgnoreCase("CLOSED")) {
			Log.message("Status CLOSED Button verified Sucessfully ");

		} else {
			Log.message("Status CLOSED not verified Sucessfully");
		}
		SkySiteUtils.waitForElement(driver, ReopenRFI, 60);
		ReopenRFI.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		SkySiteUtils.waitForElement(driver, REVISION_DROPDOWN, 60);
		REVISION_DROPDOWN.click();
		Log.message("Revision Dropdown click sucessfully");
		SkySiteUtils.waitForElement(driver, REVISION_rfiiMAGE, 60);
		REVISION_rfiiMAGE.click();
		Log.message("Revision image click sucessfully");
		SkySiteUtils.waitTill(10000);
		afterImagearea_RFI.click();
		Log.message("RFI Image Area Clicked Sucessfully");
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, OpenStatus_popUp, 60);
		String Reopen1 = OpenStatus_popUp.getText();
		Log.message("Open Button Display: " + Reopen1);
		if (Reopen1.equalsIgnoreCase("RE-OPENED")) {
			Log.message("Status RE-OPENED POP UP verified Sucessfully ");

		} else {
			Log.message("Status RE-OPENED  POP UP not verified Sucessfully");
		}

		// ============================ forwaded===============================
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, Forward, 60);
		Forward.click();
		SkySiteUtils.waitForElement(driver, Forward, 60);
		Forward_Contact.click();
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, TextArea, 60);
		TextArea.sendKeys("OK Verified");
		SkySiteUtils.waitForElement(driver, SendButton, 60);
		SendButton.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		YesButton.click();
		if (afterImagearea_RFI.isDisplayed())
			return true;
		else
			return false;
	}
	
	
	
	public boolean RFI_TestDataCreation_ownerLevel2() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		String parentHandle = driver.getWindowHandle();
		String MainWindow = driver.getWindowHandle();
		Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
		driver.switchTo().window(childwindow);
		}
		driver.switchTo().defaultContent();
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitTill(10000);
		/*if (rightArrow.isDisplayed()){
			SkySiteUtils.waitTill(2000);
			rightArrow.click();
		}
		SkySiteUtils.waitTill(6000);*/
		SkySiteUtils.waitForElement(driver, RFI_Shortcut,120);		
		Actions act=new Actions(driver);
		act.moveToElement(RFI_Shortcut).click(RFI_Shortcut).build().perform();
		Log.message("create rfi icon is clicked");
		SkySiteUtils.waitTill(8000);	
		SkySiteUtils.waitForElement(driver, beforeImagearea, 120);
		//SkySiteUtils.waitTill(10000);
		beforeImagearea.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Waiting for create RFI Menu to be appeared");
		// ================ To ========================
		SkySiteUtils.waitForElement(driver, RFI_TO, 120);
		RFI_TO.click();
		SkySiteUtils.waitTill(2000);
		Log.message(" To Link clicked Sucessfully");
		Log.message("Entered the Value In Select Email field");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitTill(3000);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 120);
		SkySiteUtils.waitTill(1000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");		
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 120);
		SkySiteUtils.waitTill(2000);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 120);
		SkySiteUtils.waitTill(1000);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		// ************CC Option---------------------------------------
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, RFI_CC, 60);
		RFI_CC.click();
		SkySiteUtils.waitTill(1000);
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(2000);
		SelectUse_Inbox.sendKeys(SelectEmail);
		Log.message("Entered the value in CC Inbox");
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("Select Checkbox of cc field");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Select User Button clicked sucessfully");
		// =====Subject==============================
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 60);
		String Subject = PropertyReader.getProperty("RFI_Subject");
		RFI_SUBJECT.sendKeys(Subject);
		Log.message("Entered the Value in RFI Subject field");
		// ============ Question====================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		RFI_Question.sendKeys(question);
		Log.message("Entered the Value in RFI question field");
		SkySiteUtils.waitTill(2000);
		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			Log.message("Attachment Button Clicked Sucessfully");
		}
		SkySiteUtils.waitTill(8000);
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(1000);
		UploadFiles(FolderPath, FileCount);
		//projectfile_Upload();
		// ======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitForElement(driver, RFI_CreateButton, 120);
		RFI_CreateButton.click();

		SkySiteUtils.waitForElement(driver, notificationMsg, 120);
		String message = notificationMsg.getText();
		Log.message("RFI  Message is: " + message);
		String expectedMessage = "RFI is added successfully";
		if (message.contains(expectedMessage)) {
			result = true;
			Log.message("RFI is added successfully");
		} else {
			result = false;
			Log.message("RFI is NOT added successfully");
		}
		
		driver.close();			    		
		driver.switchTo().window(parentHandle);
		SkySiteUtils.waitTill(1000);
		if (result == true)
			return true;
		else
			return false;

	}


	public void RFI_TestDataCreation_ownerLevel() throws Throwable

	{
		
		SkySiteUtils.waitTill(10000);		
	    String  MainWindow = driver.getWindowHandle();
	    Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);			
		}
		driver.switchTo().defaultContent();
		Log.message("Switch to default content ");
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, RFI_Shortcut,120);		
		Actions act=new Actions(driver);
		act.moveToElement(RFI_Shortcut).click(RFI_Shortcut).build().perform();
		Log.message("create rfi icon is clicked");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, beforeImagearea, 120);
		SkySiteUtils.waitTill(10000);
		beforeImagearea.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Waiting for create RFI Menu to be appeared");
		// ================ To ========================
		SkySiteUtils.waitForElement(driver, RFI_TO, 120);
		RFI_TO.click();
		SkySiteUtils.waitTill(2000);
		Log.message(" To Link clicked Sucessfully");
		Log.message("Entered the Value In Select Email field");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitTill(3000);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 120);
		SkySiteUtils.waitTill(1000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");		
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 120);
		SkySiteUtils.waitTill(2000);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 120);
		SkySiteUtils.waitTill(1000);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		// ************CC Option---------------------------------------
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, RFI_CC, 60);
		RFI_CC.click();
		SkySiteUtils.waitTill(1000);
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(2000);
		SelectUse_Inbox.sendKeys(SelectEmail);
		Log.message("Entered the value in CC Inbox");
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("Select Checkbox of cc field");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Select User Button clicked sucessfully");
		// =====Subject==============================
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 60);
		String Subject = PropertyReader.getProperty("RFI_Subject");
		SkySiteUtils.waitTill(1000);
		RFI_SUBJECT.sendKeys(Subject);
		Log.message("Entered the Value in RFI Subject field");
		// ============ Question====================		
		SkySiteUtils.waitForElement(driver, RFI_Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		SkySiteUtils.waitTill(1000);
		RFI_Question.sendKeys(question);
		Log.message("Entered the Value in RFI question field");
		SkySiteUtils.waitTill(2000);
		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			Log.message("Attachment Button Clicked Sucessfully");
		}
		SkySiteUtils.waitTill(8000);
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(1000);
		//UploadFiles(FolderPath, FileCount);
		//projectfile_Upload();
		// ======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitForElement(driver, RFI_CreateButton, 120);
		RFI_CreateButton.click();

		SkySiteUtils.waitForElement(driver, notificationMsg, 120);
		String message = notificationMsg.getText();
		Log.message("RFI  Message is: " + message);
		String expectedMessage = "RFI is added successfully";
		
		RFI_OwnerLevel_validation();
		
		driver.close();	
		SkySiteUtils.waitTill(10000);
		driver.switchTo().window(MainWindow);
		SkySiteUtils.waitTill(10000);		
	
		

	}
	
	
	public void RFI_TestDataCreation_ownerLevel_Status() throws Throwable

	{
		
		SkySiteUtils.waitTill(10000);		
	    String  MainWindow = driver.getWindowHandle();
	    Log.message(MainWindow);
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);			
		}
		driver.switchTo().defaultContent();
		Log.message("Switch to default content ");
		SkySiteUtils.waitTill(10000);
		if (rightArrow.isDisplayed())
		{
			SkySiteUtils.waitTill(2000);
			rightArrow.click();
		}
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, RFI_Shortcut,120);		
		Actions act=new Actions(driver);
		act.moveToElement(RFI_Shortcut).click(RFI_Shortcut).build().perform();
		Log.message("create rfi icon is clicked");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, beforeImagearea, 120);
		SkySiteUtils.waitTill(10000);
		beforeImagearea.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Waiting for create RFI Menu to be appeared");
		// ================ To ========================
		SkySiteUtils.waitForElement(driver, RFI_TO, 120);
		RFI_TO.click();
		SkySiteUtils.waitTill(2000);
		Log.message(" To Link clicked Sucessfully");
		Log.message("Entered the Value In Select Email field");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitTill(3000);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 120);
		SkySiteUtils.waitTill(1000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");		
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 120);
		SkySiteUtils.waitTill(2000);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 120);
		SkySiteUtils.waitTill(1000);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		// ************CC Option---------------------------------------
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, RFI_CC, 60);
		RFI_CC.click();
		SkySiteUtils.waitTill(1000);
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(2000);
		SelectUse_Inbox.sendKeys(SelectEmail);
		Log.message("Entered the value in CC Inbox");
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("Select Checkbox of cc field");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Select User Button clicked sucessfully");
		// =====Subject==============================
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 60);
		String Subject = PropertyReader.getProperty("RFI_Subject");
		SkySiteUtils.waitTill(1000);
		RFI_SUBJECT.sendKeys(Subject);
		Log.message("Entered the Value in RFI Subject field");
		// ============ Question====================		
		SkySiteUtils.waitForElement(driver, RFI_Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		SkySiteUtils.waitTill(1000);
		RFI_Question.sendKeys(question);
		Log.message("Entered the Value in RFI question field");
		SkySiteUtils.waitTill(2000);
		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			Log.message("Attachment Button Clicked Sucessfully");
		}
		SkySiteUtils.waitTill(8000);
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(1000);
		//UploadFiles(FolderPath, FileCount);
		//projectfile_Upload();
		// ======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitForElement(driver, RFI_CreateButton, 120);
		RFI_CreateButton.click();

		SkySiteUtils.waitForElement(driver, notificationMsg, 120);
		String message = notificationMsg.getText();
		Log.message("RFI  Message is: " + message);
		String expectedMessage = "RFI is added successfully";
		
		//SkySiteUtils.waitTill(10000);
		driver.close();	
		SkySiteUtils.waitTill(10000);
		driver.switchTo().window(MainWindow);
		SkySiteUtils.waitTill(10000);
	/*	if (message.contains(expectedMessage)) {
			//result = true;
			Log.message("RFI is added successfully");
		} else {
			//result = false;
			Log.message("RFI is NOT added successfully");
		}
		return result;*/
		

	}

	public boolean PhotoAnnotation(String parentHandle) throws Throwable

	{	boolean result = false;
		parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Log.message(parentHandle);
		driver.switchTo().defaultContent();
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitForElement(driver, PHOTO_shortcut, 60);		
		Actions act=new Actions(driver);
		act.moveToElement(PHOTO_shortcut).click(PHOTO_shortcut).build().perform();
		Log.message("Photo Annotation icon is clicked");	
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, beforeImagearea, 60);
		beforeImagearea.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Click viewer Page Image Area");
		if (result == true)
			return true;
		else
			return false;

	}
	
	
	
	
	public void UploadFiles_Photo()throws InterruptedException, AWTException, IOException {
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, UPLoadPhotoButton, 60);
		if (UPLoadPhotoButton.isDisplayed()) {
			UPLoadPhotoButton.click();
			Log.message("Photo Upload Button Clicked Sucessfully");
		}
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Choosefile_Button, 60);
		Choosefile_Button.click();
		SkySiteUtils.waitTill(10000);	
		// ====================Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath_new");
		File dest = new File(PropertyReader.getProperty("Upload_PhotoFile").toString());
		String path = dest.getAbsolutePath();
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + path);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(30000);
		SkySiteUtils.waitForElement(driver, Upload_Button, 60);
		Upload_Button.click();// Click on Upload without index
		Log.message("Upload Button Clicked Sucessfully");
		//SkySiteUtils.waitForElement(driver, CreatePunch, 60);
		//Log.message("Waiting for Cloud Account button to be appeared");
		
		// ====================Delete the temp file=====================
	 
	}


	public boolean UploadPhoto(String FolderPath, int FileCount) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, UPLoadPhotoButton, 60);
		if (UPLoadPhotoButton.isDisplayed()) {
			UPLoadPhotoButton.click();
			Log.message("Photo Upload Button Clicked Sucessfully");
		}
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Choosefile_Button, 60);
		Choosefile_Button.click();
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String Sys_Download_Path = "./File_UPload_Location/" + rn.nextFileName() + ".txt";
		Log.message("system download location2" + Sys_Download_Path);
		String tmpFileName = Sys_Download_Path;
		// String tmpFileName="c:/"+rn.nextFileName()+".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();
		Log.message("waiting AutoIT Script!!");
		SkySiteUtils.waitTill(10000);
		// ====================Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		File dest = new File(PropertyReader.getProperty("Upload_PhotoFile").toString());
		String path = dest.getAbsolutePath();
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + path + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(15000);
		//SkySiteUtils.waitForElement(driver, CreatePunch, 60);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(2000);
		// ====================Delete the temp file=====================
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		SkySiteUtils.waitTill(10000);
		Upload_Button.click();// Click on Upload without index
		Log.message("Upload Button Clicked Sucessfully");
		if (result == true)
			return true;
		else
			return false;

	}

	public boolean untitledphotovalidation() throws Throwable {

		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, PhotoInbox, 60);
		PhotoInbox.clear();
		Log.message("Clear the Photo Untitled Inbox");
		SkySiteUtils.waitForElement(driver, PhotoInbox, 60);
		String PhotoGalarry ="Photo_" + Generate_Random_Number.generateRandomValue();
		SkySiteUtils.waitTill(10000);
		PhotoInbox.sendKeys(PhotoGalarry);
		Log.message("Enter The Value In Untitled Inbox");
		SkySiteUtils.waitForElement(driver, Save_Button, 60);
		SkySiteUtils.waitTill(10000);
		Save_Button.click();
		Log.message("Save Button Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		String ContentName = ContentsText.getText();
		if (PhotoGalarry.contains(ContentName)) {
			//result = true;
			Log.message("Photo name updated successfully");
		} else {
			//result = false;
			Log.message("Photo name not updated successfully ");
		}
		SkySiteUtils.waitForElement(driver, PhotoDrodown, 60);
		SkySiteUtils.waitTill(10000);
		PhotoDrodown.click();
		Log.message("drop down Button Clicked  successfully ");
		SkySiteUtils.waitForElement(driver, FirstRowClick, 60);
		SkySiteUtils.waitTill(10000);
		FirstRowClick.click();
		Log.message("First Row Clicked Clicked  successfully ");
		
		SkySiteUtils.waitForElement(driver, ClickOtherArea, 30);
		ClickOtherArea.click();
		
		SkySiteUtils.waitForElement(driver, AfterPhotoImAGE, 60);
		if (AfterPhotoImAGE.isDisplayed()) {
			SkySiteUtils.waitTill(10000);
			AfterPhotoImAGE.click();
			Log.message("After Image area Clicked  successfully ");
		}
		// SkySiteUtils.waitForElement(driver, CloseImage, 60);
		// CloseImage.click();
		// Log.message("Close Link Clicked successfully ");
		if (result == true)
			return true;
		else
			return false;
	}

	public void RFITestDataCreation_potentialChecked() throws Throwable

	{
		
		String MainWindow = driver.getWindowHandle();
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);
		}
		Log.message(MainWindow);
		driver.switchTo().defaultContent();
		Log.message("Switch to default content ");
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, RFI_Shortcut,120);		
		Actions act=new Actions(driver);
		act.moveToElement(RFI_Shortcut).click(RFI_Shortcut).build().perform();
		Log.message("create rfi icon is clicked");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, beforeImagearea, 120);
		SkySiteUtils.waitTill(10000);		
		beforeImagearea.click();
		Log.message("Waiting for create RFI Menu to be appeared");
		// ================ To ========================
		SkySiteUtils.waitForElement(driver, RFI_TO, 120);
		RFI_TO.click();
		Log.message(" To Link clicked Sucessfully");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		SelectUse_Inbox.sendKeys(Select_Email);
		Log.message("Entered the Value In Select Email field");
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(3000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");		
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SkySiteUtils.waitTill(3000);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		// ************CC Option---------------------------------------
		SkySiteUtils.waitTill(10000);
		RFI_CC.click();
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(10000);
		SelectUse_Inbox.sendKeys(SelectEmail);
		Log.message("Entered the Value In Select  CC Email field");
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(10000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("Select User Checkbox  Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Select User Button  Clicked Sucessfully");
		// =====Subject==============================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 60);
		String Subject = PropertyReader.getProperty("RFI_Subject");
		RFI_SUBJECT.sendKeys(Subject);
		Log.message("Entered Value In Select field:-" + Subject);
		if (PotentialCostImpact.isDisplayed()) {
			SkySiteUtils.waitForElement(driver, PotentialCostImpact, 60);
			PotentialCostImpact.click();
			Log.message("Potential Cost Impact Checkbox Clicked Sucessfully");
		}
		if (PotentialScheduleImpact.isDisplayed()) {
			SkySiteUtils.waitForElement(driver, PotentialScheduleImpact, 60);
			PotentialScheduleImpact.click();
			Log.message("Potential Schedule Impact Checkbox Clicked Sucessfully");
		}
		// ============ Question==========================================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		RFI_Question.sendKeys(question);
		Log.message("Entered Question in Question Field");
		SkySiteUtils.waitTill(1000);
		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			SkySiteUtils.waitTill(10000);
		}
		// ===External And Internal File Upload===========================
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		Log.message("FileCount: " + FileCount);		
		//UploadFiles(FolderPath, FileCount);
		//SkySiteUtils.waitTill(2000);
		//projectfile_Upload();
		// ======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitForElement(driver, RFI_CreateButton,120);
		RFI_CreateButton.click();
		Log.message("Create Button Clicked Sucessfully");

		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String message = notificationMsg.getText();
		/*
		 * Log.message("RFI  Message is: "+ message); String expectedMessage =
		 * "RFI annotation updated successfully"; if (message.contains(expectedMessage))
		 * { result=true; Log.message("RFI annotation updated successfully"); } else {
		 * result=false; Log.message("RFI annotation not updated successfully "); }
		 * 
		 * 
		 */
		
		RFI_OwnerLevel_validation();
		SkySiteUtils.waitTill(10000);
		driver.close();
		SkySiteUtils.waitTill(10000);
		driver.switchTo().window(MainWindow);
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, afterImagearea_RFI,120);
		
	}

	public boolean RFI_TestDataCreationLevel( String Attribute) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		String MainWindow = driver.getWindowHandle();
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);
		}
		Log.message(MainWindow);
		driver.switchTo().defaultContent();
		Log.message("Switch to Default frame ");	
		SkySiteUtils.waitTill(10000);
		if (rightArrow.isDisplayed()){
			SkySiteUtils.waitTill(2000);
			rightArrow.click();
		}
		SkySiteUtils.waitForElement(driver, RFI_Shortcut,120);		
		Actions act=new Actions(driver);
		act.moveToElement(RFI_Shortcut).click(RFI_Shortcut).build().perform();
		Log.message("create rfi icon is clicked");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Shortcut, 120);
		SkySiteUtils.waitTill(2000);
		beforeImagearea.click();
		SkySiteUtils.waitForElement(driver, RFI_TO, 120);
		Log.message("Waiting for create RFI Menu to be appeared");
		// ================ To ========================
		SkySiteUtils.waitTill(2000);
		RFI_TO.click();
		Log.message(" To Link clicked Sucessfully");
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		// ************CC Option---------------------------------------
		SkySiteUtils.waitForElement(driver, RFI_CC, 120);
		RFI_CC.click();
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(10000);
		SelectUse_Inbox.sendKeys(SelectEmail);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(10000);
		Search_Button.click();
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		// =====Subject==============================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 60);
		String Subject = PropertyReader.getProperty("RFI_Subject");
		RFI_SUBJECT.sendKeys(Subject);
		// ===========================new field created =========
		if (NewcustomAttributeField.isDisplayed()) {
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, NewcustomAttributeField, 60);
			// String Attribute = PropertyReader.getProperty("RFI_Subject");
			NewcustomAttributeField.sendKeys(Attribute);
		}
		// ============ Question====================
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, RFI_Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		RFI_Question.sendKeys(question);
		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			SkySiteUtils.waitTill(8000);
		}
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		Log.message("FileCount: " + FileCount);
		UploadFiles(FolderPath, FileCount);
		projectfile_Upload();
		// ======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitTill(1000);
		RFI_CreateButton.click();
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String message = notificationMsg.getText();
		Log.message("RFI  Message is: " + message);
		String expectedMessage = "RFI is added successfully";
		if (message.contains(expectedMessage)) {
			result = true;
			Log.message("RFI is added successfully ");
		} else {
			result = false;
			Log.message("RFI is  NOT added successfully ");
		}
		
		SkySiteUtils.waitTill(10000);
		driver.close();	
		SkySiteUtils.waitTill(10000);
		driver.switchTo().window(MainWindow);
		SkySiteUtils.waitTill(10000);
		if (result == true)
			return true;
		else
			return false;

	}

	public boolean RFITestDataCreationLevel( String subject) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		String MainWindow = driver.getWindowHandle();
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);
		}
		
		//Log.message(parentHandle);		
		driver.switchTo().defaultContent();
		Log.message("Switch to Default frame ");
		
		/*SkySiteUtils.waitTill(10000);
		if (rightArrow.isDisplayed()){
			SkySiteUtils.waitTill(2000);
			rightArrow.click();
		}*/
		SkySiteUtils.waitForElement(driver, RFI_Shortcut,120);
		SkySiteUtils.waitTill(2000);				
		Actions act=new Actions(driver);
		act.moveToElement(RFI_Shortcut).click(RFI_Shortcut).build().perform();
		Log.message("create rfi icon is clicked");
		SkySiteUtils.waitTill(15000);
		SkySiteUtils.waitForElement(driver, beforeImagearea, 120);
		beforeImagearea.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Waiting for create RFI Menu to be appeared");
		// ================ To ========================
		SkySiteUtils.waitForElement(driver, RFI_TO, 60);
		RFI_TO.click();
		Log.message(" To Link clicked Sucessfully");
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		// ************CC Option---------------------------------------
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_CC, 120);
		RFI_CC.click();
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(1000);
		SelectUse_Inbox.sendKeys(SelectEmail);
		SkySiteUtils.waitForElement(driver, Search_Button, 120);
		Search_Button.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 120);
		SelectUser_Button.click();
		SkySiteUtils.waitTill(2000);
		// =====Subject==============================
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 60);
		// String Subject = PropertyReader.getProperty("RFI_Subject");
		RFI_SUBJECT.sendKeys(subject);
		// ============ Question====================
		SkySiteUtils.waitForElement(driver, RFI_Question, 120);
		String question = PropertyReader.getProperty("RFI_Questions");
		RFI_Question.sendKeys(question);
		if (Addattachment.isDisplayed()) {
			Addattachment.click();
		}
		SkySiteUtils.waitTill(8000);
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		Log.message("FileCount: " + FileCount);
		UploadFiles(FolderPath, FileCount);
		//projectfile_Upload();
		// ======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitForElement(driver, RFI_CreateButton, 100);
		RFI_CreateButton.click();
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String message = notificationMsg.getText();
		Log.message("RFI  Message is: " + message);
		String expectedMessage = "RFI is added successfully";
		if (message.contains(expectedMessage)) {
			result = true;
			Log.message("RFI is added successfully ");
		} else {
			result = false;
			Log.message("RFI is  NOT added successfully ");
		}
		SkySiteUtils.waitTill(10000);
		driver.close();	
		SkySiteUtils.waitTill(10000);
		driver.switchTo().window(MainWindow);
		SkySiteUtils.waitTill(10000);
		if (result == true)
			return true;
		else
			return false;

	}

	public boolean RFI_ImageClick1() throws Throwable

	{
		boolean result = false;
		Log.message("Entered into Owner level test");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ImageClick, 60);
		ImageClick.click();
		Log.message("Image Link clicked.");
		SkySiteUtils.waitTill(10000);
		return result;
	}

	public boolean RFI_ImageClick2() throws Throwable

	{	boolean result = false;
		Log.message("Entered into Owner level test");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ImageClick1, 60);
		ImageClick1.click();
		Log.message("Image2 Link clicked.");
		SkySiteUtils.waitTill(10000);
		return result;
	}

	public void RFI_RevisionFile() throws Throwable

	{
		
		SkySiteUtils.waitTill(6000);
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Log.message(parentHandle);
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitForElement(driver, REVISION_DROPDOWN, 120);
		REVISION_DROPDOWN.click();
		SkySiteUtils.waitTill(10000);
		Log.message(" Revision file Dropdown click sucessfully");
		SkySiteUtils.waitForElement(driver, REVISION_rfiiMAGE, 60);
		REVISION_rfiiMAGE.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Revision file image click sucessfully");
		
		RFI_OwnerLevel_validation();
		SkySiteUtils.waitTill(10000);
		driver.switchTo().window(parentHandle);
		SkySiteUtils.waitTill(10000);
}

	public boolean RFI_TestDataRevisionFile_ownerLevel(String parentHandle) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(8000);
		parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitTill(10000);
		Log.message(parentHandle);
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitForElement(driver, RFI_Shortcut, 60);
		SkySiteUtils.waitTill(10000);
		RFI_Shortcut.click();
		SkySiteUtils.waitTill(8000);
		beforeImagearea.click();
		SkySiteUtils.waitTill(8000);
		Log.message("Waiting for create RFI Menu to be appeared");
		// ================ To ========================
		SkySiteUtils.waitForElement(driver, RFI_TO, 60);
		RFI_TO.click();
		Log.message(" To Link clicked Sucessfully");
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		// ************CC Option---------------------------------------
		SkySiteUtils.waitTill(10000);
		RFI_CC.click();
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(10000);
		SelectUse_Inbox.sendKeys(SelectEmail);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(10000);
		Search_Button.click();
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		// =====Subject==============================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_SUBJECT, 60);
		String Subject = PropertyReader.getProperty("RFI_Subject");
		RFI_SUBJECT.sendKeys(Subject);
		if (Addattachment.isDisplayed()) {
			Addattachment.click();
			SkySiteUtils.waitTill(8000);
		}
		/*
		 * //=====Discilipline========================= SkySiteUtils.waitTill(2000);
		 * SkySiteUtils.waitForElement(driver, RFI_Discipline, 60); String Discipline =
		 * PropertyReader.getProperty("RFI_Discipline"); if (Discipline != "" &&
		 * Discipline != null) { RFI_Discipline.sendKeys(Discipline); } //========Sheet
		 * No==============================
		 * 
		 * SkySiteUtils.waitTill(2000); SkySiteUtils.waitForElement(driver, RFI_SheetNo,
		 * 60); String SheetNO = PropertyReader.getProperty("RFI_SheetNo"); if (SheetNO
		 * != "" && SheetNO != null) { RFI_SheetNo.sendKeys(SheetNO); } //============
		 * Specification==================== SkySiteUtils.waitTill(2000);
		 * SkySiteUtils.waitForElement(driver, RFI_Specification, 60); String
		 * Specification = PropertyReader.getProperty("RFI_SPecification");
		 * RFI_Specification.sendKeys(Specification);
		 */
		// ============ Question====================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, RFI_Question, 60);
		String question = PropertyReader.getProperty("RFI_Questions");
		RFI_Question.sendKeys(question);
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(10000);
		UploadFiles(FolderPath, FileCount);
		SkySiteUtils.waitTill(6000);
		projectfile_Upload();
		// ======RFI Create Button Clicked Sucessfully=============
		SkySiteUtils.waitTill(2000);
		RFI_CreateButton.click();
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String message = notificationMsg.getText();
		Log.message("RFI  Message is: " + message);
		String expectedMessage = "RFI is added successfully";
		if (message.contains(expectedMessage)) {
			Log.message("RFI is added successfully ");
		} else {
			Log.message("RFI is  NOT added successfully ");
		}

		return result;

	}

	public boolean PUNCH_Closed_OwnerLevel(String parentHandle) throws Throwable

	{
		boolean result = false;		
		SkySiteUtils.waitTill(8000);
		parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitTill(10000);
		Log.message(parentHandle);
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitForElement(driver, Punch_Dropdown, 60);
		Punch_Dropdown.click();
		Log.message("Punch Drop Down Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, FirstRow, 60);
		SkySiteUtils.waitTill(10000);
		FirstRow.click();
		Log.message("Punch First Row Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ClickOtherArea, 60);
		ClickOtherArea.click();
		SkySiteUtils.waitTill(10000);
		afterImagearea.click();
		Log.message("Image Area Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		ClosePunch.click();
		Log.message("Close punch Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, YesButton, 60);
		SkySiteUtils.waitTill(2000);
		YesButton.click();
		Log.message("Yes Button Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String message1 = notificationMsg.getText();
		Log.message("punch after saving Message is: " + message1);
		SkySiteUtils.waitTill(10000);
		String expectedMessage1 = "Punch annotation saved successfully";
		/*
		 * if (message1.contains(expectedMessage1)) {
		 * Log.message("Punch annotation saved successfully");
		 * 
		 * } else { Log.message("Punch annotation not saved successfully"); }
		 */

		Punch_Dropdown.click();
		Log.message("Punch Drop Down Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		RefereshButton.click();
		Log.message("Referesh Button Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, CloseButton, 60);
		String Close = CloseButton.getText();
		Log.message("Close Button Display: " + Close);
		if (Close.contains("CLOSED")) {
			Log.message("Status CLOSED verified Sucessfully ");

		} else {
			Log.message("Status Closed not verified Sucessfully");
		}

		SkySiteUtils.waitTill(10000);
		return result;
	}

	public boolean PUNCH_EmployeeLevel_VALIDATION(String parentHandle) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitTill(10000);
		Log.message(parentHandle);
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, Punch_Dropdown, 60);
		Punch_Dropdown.click();
		Log.message("Punch Drop Down Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, FirstRow, 60);
		SkySiteUtils.waitTill(10000);
		FirstRow.click();
		Log.message("Punch First Row Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ClickOtherArea, 60);
		ClickOtherArea.click();
		SkySiteUtils.waitTill(10000);
		afterImagearea.click();
		Log.message("Image Area Clicked Sucessfully");
		SkySiteUtils.waitTill(20000);
		// =================Subject headline=================
		SkySiteUtils.waitForElement(driver, Punch_Subject, 60);
		String subject = Punch_Subject.getText();
		Log.message("subject Message is: " + subject);
		String punch_Subject = PropertyReader.getProperty("Punch_Subject");

		if (subject.equalsIgnoreCase(punch_Subject)) {
			Log.message("Subject verified Sucessfully ");

		} else {
			Log.message("Subject not verified Sucessfully");
		}
		// =================Email To=================
		SkySiteUtils.waitForElement(driver, Punch_AssignTo, 60);
		String Email_To = Punch_AssignTo.getText();
		Log.message("Email_To Message is: " + Email_To);
		String Email = PropertyReader.getProperty("Email_AssignTo_emp");
		if (Email_To.equalsIgnoreCase(Email)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}
		// =================CC Option=================
		SkySiteUtils.waitForElement(driver, Punch_CC, 60);
		String Email_CC = Punch_CC.getText();
		Log.message("Email_To Message is: " + Email_CC);
		String CCoption = PropertyReader.getProperty("Email_CCOption");
		if (Email_CC.equalsIgnoreCase(CCoption)) {
			Log.message("Email cc  verified Sucessfully ");

		} else {
			Log.message("Email cc  not verified Sucessfully");
		}
		// =================Description=================
		SkySiteUtils.waitForElement(driver, Punch_Description, 60);
		String Description = Punch_Description.getText();
		Log.message("Email_To Message is: " + Email_CC);
		String desctiption = PropertyReader.getProperty("Desctiption");
		if (Description.equalsIgnoreCase(desctiption)) 
		{
			Log.message("Description verified Sucessfully ");
		} 
		else {
			Log.message("Description not verified Sucessfully");
			  }

		Log.message("Add Attachement first File Name Display " + Punch_Attachment1);

		Log.message("Add Attachement first File Name Display " + Punch_Attachment2);
		// ============= Entered Value In comment Field=======================
		String comment = PropertyReader.getProperty("AddComment");
		Punch_AddCommentInbox.sendKeys(comment);
		SkySiteUtils.waitTill(6000);
		// ===========Clicked Attachment Button==============================
		Punch_Attachment.click();
		SkySiteUtils.waitTill(10000);
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(3000);
		UploadFiles(FolderPath, FileCount);
		projectfile_Upload();
		SkySiteUtils.waitTill(10000);
		CreatePunch.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Upload And Attached Button Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);		
		return result;

	}

	public boolean PUNCH_EmployeeLevel_VALIDATION1(String parentHandle) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Log.message(parentHandle);

		driver.switchTo().defaultContent();
		SkySiteUtils.waitForElement(driver, Punch_Dropdown, 60);
		Punch_Dropdown.click();
		Log.message("Punch Drop Down Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, FirstRow, 60);
		SkySiteUtils.waitTill(10000);
		SecondRow.click();
		Log.message("Punch First Row Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ClickOtherArea, 60);
		ClickOtherArea.click();
		SkySiteUtils.waitTill(10000);
		afterImagearea.click();
		Log.message("Image Area Clicked Sucessfully");
		SkySiteUtils.waitTill(20000);
		// =================Subject headline=================
		SkySiteUtils.waitForElement(driver, Punch_Subject, 60);
		String subject = Punch_Subject.getText();
		Log.message("subject Message is: " + subject);
		String punch_Subject = PropertyReader.getProperty("Punch_Subject");
		if (subject.equalsIgnoreCase(punch_Subject)) {
			Log.message("Subject verified Sucessfully ");

		} else {
			Log.message("Subject not verified Sucessfully");
		}
		// =================Email To=================
		SkySiteUtils.waitForElement(driver, Punch_AssignTo, 60);
		String Email_To = Punch_AssignTo.getText();
		Log.message("Email_To Message is: " + Email_To);
		String Email = PropertyReader.getProperty("Email_AssignTo");
		if (Email_To.equalsIgnoreCase(Email)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}
		// =================CC Option=================
		SkySiteUtils.waitForElement(driver, Punch_CC, 60);
		String Email_CC = Punch_CC.getText();
		Log.message("Email_To Message is: " + Email_CC);
		String CCoption = PropertyReader.getProperty("Email_CCOption");
		if (Email_CC.equalsIgnoreCase(CCoption)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}
		// =================Description=================
		SkySiteUtils.waitForElement(driver, Punch_Description, 60);
		String Description = Punch_Description.getText();
		Log.message("Email_To Message is: " + Email_CC);
		String desctiption = PropertyReader.getProperty("Desctiption");
		if (Description.equalsIgnoreCase(desctiption)) {
			Log.message("Description verified Sucessfully ");

		} else {
			Log.message("Description not verified Sucessfully");
		}
		Log.message("Add Attachement first File Name Display " + Punch_Attachment2);
		// ============= Entered Value In comment Field=======================
		String comment = PropertyReader.getProperty("AddComment");
		Punch_AddCommentInbox.sendKeys(comment);
		SkySiteUtils.waitTill(6000);
		// ===========Clicked Attachment Button==============================
		Punch_Attachment.click();
		SkySiteUtils.waitTill(10000);
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);// String to
																// Integer
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(10000);
		UploadFiles(FolderPath, FileCount);
		//SkySiteUtils.waitTill(6000);
		projectfile_Upload1();
		SkySiteUtils.waitTill(10000);
		CreatePunch.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Upload And Attached Button Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		// String Count = AttachementCount.getText();
		// Log.message("Attachement Count Verified Sucessfully:"+ Count);

		return result;

	}

	public boolean CloseRFI(String parentHandle) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, ImageClick, 60);
		ImageClick.click();
		Log.message("Image Link clicked.");
		SkySiteUtils.waitTill(2000);
		parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitTill(6000);
		Log.message(parentHandle);
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_Dropdown, 60);
		RFI_Dropdown.click();
		Log.message("RFI Drop Down Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_FirstRow, 60);
		SkySiteUtils.waitTill(10000);
		RFI_FirstRow.click();
		Log.message("RFI First Row Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ClickOtherArea, 60);
		ClickOtherArea.click();
		SkySiteUtils.waitTill(10000);
		afterImagearea_RFI.click();
		Log.message("Image Area Clicked Sucessfully");
		// Log.message(parentHandle);
		SkySiteUtils.waitTill(20000);
		CloseRFIByowner.click();
		Log.message("Close RFI Button Clicked Sucessfully");
		return result;

	}

	public boolean RFI_Closed() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(2000);
		/*SkySiteUtils.waitForElement(driver, ImageClick, 60);
		ImageClick.click();
		Log.message("Image Link clicked.");
		SkySiteUtils.waitTill(2000);*/
		String MainWindow = driver.getWindowHandle();
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);
		}
		SkySiteUtils.waitTill(6000);
		Log.message(MainWindow);
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_Dropdown, 60);
		RFI_Dropdown.click();
		Log.message("RFI Drop Down Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_FirstRow, 60);
		SkySiteUtils.waitTill(10000);
		RFI_FirstRow.click();
		Log.message("RFI First Row Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ClickOtherArea, 60);
		ClickOtherArea.click();
		SkySiteUtils.waitTill(10000);
		afterImagearea_RFI.click();
		Log.message("Image Area Clicked Sucessfully");
		// Log.message(parentHandle);
		SkySiteUtils.waitTill(10000);
		CloseRFIByowner.click();
		Log.message("Closed RFI By Owner Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		driver.close();
		SkySiteUtils.waitTill(10000);
		driver.switchTo().window(MainWindow);
		SkySiteUtils.waitTill(10000);
		return result;
	}

	public void RFI_EMPLOYEEVALIDATION() throws Throwable

	{
		//boolean result = false;
		SkySiteUtils.waitTill(10000);
		String MainWindow = driver.getWindowHandle();		
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitTill(10000);
		Log.message(MainWindow);
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_Dropdown, 60);
		RFI_Dropdown.click();
		Log.message("RFI Drop Down Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_FirstRow, 60);
		SkySiteUtils.waitTill(10000);
		FirstRow.click();
		Log.message("RFI First Row Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ClickOtherArea, 60);
		ClickOtherArea.click();
		SkySiteUtils.waitTill(10000);		
		JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", afterImagearea_RFI);    
		Log.message("Click on Image Area");		
		// Log.message(parentHandle);
		SkySiteUtils.waitTill(20000);
		// =================Subject headline=================
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject = Subject.getText();
		Log.message("subject Message is: " + subject);
		String RFI_Subject = PropertyReader.getProperty("RFI_Subject");

		if (subject.equalsIgnoreCase(RFI_Subject)) {
			Log.message("Subject verified Sucessfully ");

		} else {
			Log.message("Subject not verified Sucessfully");
		}
		// =================Email To=================
		SkySiteUtils.waitForElement(driver, RFI_Email_TO, 60);
		String Email_To = RFI_Email_TO.getText();
		Log.message("Email_To Message is: " + Email_To);
		String email = PropertyReader.getProperty("Email_AssignTo_emp");

		if (Email_To.equalsIgnoreCase(email)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}

		SkySiteUtils.waitTill(2000);

		SkySiteUtils.waitForElement(driver, RFI_Email_CC, 60);
		String Email_CC = RFI_Email_CC.getText();
		Log.message("Email_To Message is: " + Email_CC);
		String CCoption = PropertyReader.getProperty("Email_CCOption");

		if (Email_CC.equalsIgnoreCase(CCoption)) {
			Log.message("Email CC To verified Sucessfully ");

		} else {
			Log.message("Email CC To not verified Sucessfully");
		}

		String Answers = PropertyReader.getProperty("Answer");
		SkySiteUtils.waitForElement(driver, AnswerEditBox, 60);
		AnswerEditBox.sendKeys(Answers);
		Log.message("Entered Ans in inbox Field");
		SkySiteUtils.waitForElement(driver, AddAttachment, 60);
		AddAttachment.click();
		Log.message("Add Attachement Clicked Sucessfully");
		SkySiteUtils.waitTill(8000);
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);// String to
																// Integer
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(10000);
		// Log.assertThat( UploadFiles(FolderPath, FileCount), "Upload file
		// working Successfully","Upload file not working Successfully",
		// driver);
		UploadFiles(FolderPath, FileCount);
		SkySiteUtils.waitTill(6000);
		// Log.assertThat( projectfile_Upload(), "project internal file working
		// Successfully","project internal file not working Successfully",
		// driver);
		//projectfile_Upload();
		SkySiteUtils.waitTill(10000);
		
		ButtonRFI.click();
		Log.message("Upload And Attached Button Clicked Sucessfully");
		// CommonMethod.Fluentwait(AttachementCount, 120, 5);
		SkySiteUtils.waitTill(10000);
		
		String Count = AttachementCount.getText();
		Log.message("Attachement Count Verified Sucessfully:" + Count);

		// =============Reassign Method=======
		SkySiteUtils.waitTill(6000);

		// Log.assertThat( RFI_Reassign(), "RFI Reassign working
		// Successfully","Reassign not working Successfully", driver);

		RFI_Reassign();

		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_Email_TO, 60);
		String Email1_To = RFI_Email_TO.getText();
		Log.message("Reassign Email  Message is: " + Email1_To);
		String Email1 = PropertyReader.getProperty("ReAssign");

		if (Email_To.equalsIgnoreCase(Email1)) {
			Log.message("Email To verified Sucessfully ");
			// result=true;
		} else {
			Log.message("Email To not verified Sucessfully");
			// result= false;
		}

		SkySiteUtils.waitTill(10000);
		
		driver.close();
		SkySiteUtils.waitTill(10000);		
		
		driver.switchTo().window(MainWindow);
		SkySiteUtils.waitTill(10000);
		/*if (afterImagearea_RFI.isDisplayed())
			return true;
		else
			return false;*/

		/*
		 * //====Notification Message RFI Added Sucessfully validation
		 * SkySiteUtils.waitTill(10000); SkySiteUtils.waitForElement(driver,
		 * notificationMsg, 60); String message = notificationMsg.getText();
		 * Log.message("RFI  Message is: "+ message); String expectedMessage =
		 * "RFI is added successfully"; if (message.contains(expectedMessage)) {
		 * Log.message("RFI is added successfully "); } else {
		 * Log.message("RFI is  NOT added successfully "); }
		 * 
		 */
		// return result;

	}

	public void RFI_EMPLOYEEVALIDATION1() throws Throwable

	{
		//boolean result = false;
		SkySiteUtils.waitTill(10000);
		
		 String MainWindow = driver.getWindowHandle();
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);
		}
		SkySiteUtils.waitTill(10000);
		Log.message(MainWindow);
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_Dropdown, 60);
		RFI_Dropdown.click();
		Log.message("RFI Drop Down Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_FirstRow, 60);
		SkySiteUtils.waitTill(10000);
		FirstRow.click();
		Log.message("RFI First Row Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ClickOtherArea, 60);
		ClickOtherArea.click();
		SkySiteUtils.waitTill(10000);
		afterImagearea_RFI.click();
		Log.message("Image Area Clicked Sucessfully");
		// Log.message(parentHandle);
		SkySiteUtils.waitTill(20000);
		// =================Subject headline=================
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject = Subject.getText();
		Log.message("subject Message is: " + subject);
		String RFI_Subject = PropertyReader.getProperty("RFI_Subject");

		if (subject.equalsIgnoreCase(RFI_Subject)) {
			Log.message("Subject verified Sucessfully ");

		} else {
			Log.message("Subject not verified Sucessfully");
		}
		// =================Email To=================
		SkySiteUtils.waitForElement(driver, RFI_Email_TO, 60);
		String Email_To = RFI_Email_TO.getText();
		Log.message("Email_To Message is: " + Email_To);
		String Email = PropertyReader.getProperty("Email_AssignTo");

		if (Email_To.equalsIgnoreCase(Email)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}

		// =================CC Option=================
		SkySiteUtils.waitForElement(driver, RFI_Email_CC, 60);
		String Email_CC = RFI_Email_CC.getText();
		Log.message("Email_To Message is: " + Email_CC);
		String CCoption = PropertyReader.getProperty("Email_CCOption");

		if (Email_CC.equalsIgnoreCase(CCoption)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}

		String Answers = PropertyReader.getProperty("Answer");
		AnswerEditBox.sendKeys(Answers);
		Log.message("Entered Ans in inbox Field");
		SkySiteUtils.waitForElement(driver, AddAttachment, 60);
		AddAttachment.click();
		Log.message("Add Attachement Clicked Sucessfully");
		SkySiteUtils.waitTill(8000);
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(10000);
		// Log.assertThat( UploadFiles(FolderPath, FileCount), "Upload file
		// working Successfully","Upload file not working Successfully",
		// driver);
		UploadFiles(FolderPath, FileCount);
		SkySiteUtils.waitTill(6000);
		// Log.assertThat( projectfile_Upload(), "project internal file working
		// Successfully","project internal file not working Successfully",
		// driver);
		projectfile_Upload();
		SkySiteUtils.waitTill(10000);	
		ButtonRFI.click();
		Log.message("Upload And Attached Button Clicked Sucessfully");
		// CommonMethod.Fluentwait(AttachementCount, 120, 5);
		SkySiteUtils.waitTill(10000);
		String Count = AttachementCount.getText();
		Log.message("Attachement Count Verified Sucessfully:" + Count);
		// =============Reassign Method=======
		SkySiteUtils.waitTill(6000);
		// Log.assertThat( RFI_Reassign(), "RFI Reassign working
		// Successfully","Reassign not working Successfully", driver);
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_Email_TO, 60);
		String Email1_To = RFI_Email_TO.getText();
		Log.message("Reassign Email  Message is: " + Email1_To);
		String Email1 = PropertyReader.getProperty("ReAssign");
		if (Email_To.equalsIgnoreCase(Email1)) {
			Log.message("Email To verified Sucessfully ");
			// result=true;
		} else {
			Log.message("Email To not verified Sucessfully");
			// result= false;
		}
		
		driver.close();
		SkySiteUtils.waitTill(10000);
		driver.switchTo().window(MainWindow);
		SkySiteUtils.waitTill(10000);
		/*if (afterImagearea_RFI.isDisplayed())
			return true;
		else
			return false;*/

	}

	public void RFI_EMPLOYEEVALIDATION2() throws Throwable

	{
		//boolean result = false;
		SkySiteUtils.waitTill(10000);		
		String MainWindow = driver.getWindowHandle();
		for (String childwindow : driver.getWindowHandles()) {
			driver.switchTo().window(childwindow);
		}
		SkySiteUtils.waitTill(10000);
		Log.message(MainWindow);
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_Dropdown, 60);
		RFI_Dropdown.click();
		Log.message("RFI Drop Down Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_FirstRow, 60);
		SkySiteUtils.waitTill(10000);
		SecondRow.click();
		Log.message("RFI First Row Menu Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ClickOtherArea, 60);
		ClickOtherArea.click();
		SkySiteUtils.waitTill(10000);
		afterImagearea_RFI.click();
		Log.message("Image Area Clicked Sucessfully");
		// Log.message(parentHandle);
		SkySiteUtils.waitTill(20000);
		// =================Subject headline=================
		SkySiteUtils.waitForElement(driver, Subject, 60);
		String subject = Subject.getText();
		Log.message("subject Message is: " + subject);
		String RFI_Subject = PropertyReader.getProperty("RFI_Subject");
		if (subject.equalsIgnoreCase(RFI_Subject)) {
			Log.message("Subject verified Sucessfully ");

		} else {
			Log.message("Subject not verified Sucessfully");
		}
		// =================Email To=================
		SkySiteUtils.waitForElement(driver, RFI_Email_TO, 60);
		String Email_To = RFI_Email_TO.getText();
		Log.message("Email_To Message is: " + Email_To);
		String Email = PropertyReader.getProperty("Email_AssignTo");
		if (Email_To.equalsIgnoreCase(Email)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}
		// =================CC Option=================
		SkySiteUtils.waitForElement(driver, RFI_Email_CC, 60);
		String Email_CC = RFI_Email_CC.getText();
		Log.message("Email_To Message is: " + Email_CC);
		String CCoption = PropertyReader.getProperty("Email_CCOption");
		if (Email_CC.equalsIgnoreCase(CCoption)) {
			Log.message("Email To verified Sucessfully ");

		} else {
			Log.message("Email To not verified Sucessfully");
		}
		String Answers = PropertyReader.getProperty("Answer");
		AnswerEditBox.sendKeys(Answers);
		Log.message("Entered Ans in inbox Field");
		SkySiteUtils.waitForElement(driver, AddAttachment, 60);
		AddAttachment.click();
		Log.message("Add Attachement Clicked Sucessfully");
		SkySiteUtils.waitTill(8000);
		// ===External And Internal File Upload
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(10000);
		// Log.assertThat( UploadFiles(FolderPath, FileCount), "Upload file
		// working Successfully","Upload file not working Successfully",
		// driver);
		UploadFiles(FolderPath, FileCount);
		//SkySiteUtils.waitTill(6000);
		// Log.assertThat( projectfile_Upload(), "project internal file working
		// Successfully","project internal file not working Successfully",
		// driver);
		//projectfile_Upload1();
		SkySiteUtils.waitTill(10000);	
		ButtonRFI.click();
		Log.message("Upload And Attached Button Clicked Sucessfully");
		// CommonMethod.Fluentwait(AttachementCount, 120, 5);
		SkySiteUtils.waitTill(10000);		
		String Count = AttachementCount.getText();
		Log.message("Attachement Count Verified Sucessfully:" + Count);
		// =============Reassign Method=======
		// Log.assertThat( RFI_Reassign(), "RFI Reassign working
		// Successfully","Reassign not working Successfully", driver);

		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_Email_TO, 60);
		String Email1_To = RFI_Email_TO.getText();
		Log.message("Reassign Email  Message is: " + Email1_To);
		String Email1 = PropertyReader.getProperty("ReAssign");

		if (Email_To.equalsIgnoreCase(Email1)) {
			Log.message("Email To verified Sucessfully ");
			// result=;
		} else {
			Log.message("Email To not verified Sucessfully");
			// result= false;
		}

		driver.close();
		SkySiteUtils.waitTill(10000);
		driver.switchTo().window(MainWindow);
		
		SkySiteUtils.waitTill(10000);
		/*if (afterImagearea_RFI.isDisplayed())
			return true;
		else
			return false;
*/
		/*
		 * //====Notification Message RFI Added Sucessfully validation
		 * SkySiteUtils.waitTill(10000); SkySiteUtils.waitForElement(driver,
		 * notificationMsg, 60); String message = notificationMsg.getText();
		 * Log.message("RFI  Message is: "+ message); String expectedMessage =
		 * "RFI is added successfully"; if (message.contains(expectedMessage)) {
		 * Log.message("RFI is added successfully "); } else {
		 * Log.message("RFI is  NOT added successfully "); }
		 * 
		 */
		// return result;

	}

	public boolean projectfile_Upload() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ProjectTab, 60);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ProjectTab);
		Log.message("Project  Tab Clicked Sucessfully");
		// entered the value in searchbox
		String Folder1 = PropertyReader.getProperty("FolderName");
		SkySiteUtils.waitForElement(driver, ProjectInbox, 120);
		SkySiteUtils.waitTill(2000);
		ProjectInbox.sendKeys(Folder1);
		
		Log.message("Entered the value in project files Inbox field:---" + Folder1);
		// ================Search Button Clicked Sucessfully.
		SkySiteUtils.waitForElement(driver, SearchButton, 120);
		JavascriptExecutor executor1 = (JavascriptExecutor) driver;
		executor1.executeScript("arguments[0].click();", SearchButton);
		Log.message("Search Button Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Folder, 120);
		FolderSelection.click();
		SkySiteUtils.waitTill(1000);
		Log.message("Folder Selected And clicked");
		SkySiteUtils.waitForElement(driver, File_CheckBox, 120);
		file_Checkox.click();
		SkySiteUtils.waitTill(1000);
		Log.message("File Checkbox Click Sucessfully");
		SkySiteUtils.waitTill(3000);
		return result;

	}

	public boolean projectfile_Upload11() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, Projectfiles, 60);
		Log.message("Waiting for Project Tab button to be appeared");
		Projectfiles.click();
		Log.message("Project files Tab Clicked Sucessfully");
		// entered the value in searchbox
		String Folder1 = PropertyReader.getProperty("FolderName");
		SkySiteUtils.waitForElement(driver, SearchProjectfilesInbox, 60);
		SkySiteUtils.waitTill(10000);
		SearchProjectfilesInbox.sendKeys(Folder1);
		Log.message("Entered the value in project files Inbox field");
		// ================Search Button Clicked Sucessfully.
		SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton, 60);
		SkySiteUtils.waitTill(10000);
		ProjectfilesTab_searchButton.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Search Button Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Folder, 120);
		SkySiteUtils.waitTill(2000);
		Folder.click();
		Log.message("Folder Selected And clicked");
		SkySiteUtils.waitForElement(driver, File_CheckBox, 120);
		File_CheckBox.click();
		return result;

	}

	public boolean ClosePOPUP() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		if (ClosePOPUP.isDisplayed()) {
			ClosePOPUP.click();
		}
		return result;
	}

	public boolean projectfile_Upload1() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, Projectfiles, 60);
		Log.message("Waiting for Project Tab button to be appeared");
		Projectfiles.click();
		Log.message("Project files Tab Clicked Sucessfully");
		// entered the value in searchbox
		String Folder1 = PropertyReader.getProperty("FolderName");
		SkySiteUtils.waitTill(2000);
		SearchProjectfilesInbox.sendKeys(Folder1);
		Log.message("Entered the value in project files Inbox field");		
		// ================Search Button Clicked Sucessfully.
		SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton, 60);
		SkySiteUtils.waitTill(10000);
		ProjectfilesTab_searchButton.click();		
		Log.message("Search Button Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Folder, 60);
		SkySiteUtils.waitTill(2000);
		Folder.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Folder Selected And clicked");
		SkySiteUtils.waitForElement(driver, File_CheckBox1,120);
		File_CheckBox1.click();
		return result;

	}

	// =========================Punch Creatiom Script==================

	public boolean projectLevel_EmployeePunchValidation() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Punchshortcut, 20);
		Project.click();
		Log.message("First Project Link Clicked sucessfully");
		SkySiteUtils.waitTill(10000);
		ProjectManagement.click();
		Log.message("Project Management Link Clicked sucessfully");
		SkySiteUtils.waitTill(10000);
		punchmenu.click();
		Log.message("Punch Menu Clicked sucessfully");
		SkySiteUtils.waitTill(10000);
		AllPunchList.click();
		Log.message("All Punch List  Clicked sucessfully");
		SkySiteUtils.waitTill(10000);
		PunchList.click();
		// =================Stamp Description=================
		SkySiteUtils.waitForElement(driver, Shortdescription, 60);
		String Descriptipn = Shortdescription.getText();
		Log.message("Short Description Message is1: " + Descriptipn);
		Log.message("Short Description Message is2: " + Stamp_inbox);
		if (Descriptipn.equalsIgnoreCase(Stamp_inbox)) {
			Log.message("Stamp verified Sucessfully ");

		} else {
			Log.message("Stamp not verified Sucessfully");
		}
		// ================== Stamp Title===================
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, ShortNumber, 60);
		String Stamp_Title = ShortNumber.getText();
		Log.message("Stamp Title Message is: " + Stamp_Title);
		if (Stamp_Title.contentEquals(Stamp_title)) {
			Log.message("Stamp Title verified Sucessfully ");

		} else {
			Log.message("Stamp Title not verified Sucessfully");
		}

		// =====================Description====================

		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, TestDescription, 60);
		String Description = TestDescription.getText();
		Log.message("Description is1: " + Description);
		String description = PropertyReader.getProperty("Desctiption");
		Log.message("Description is2: " + description);
		if (Description.contentEquals(description)) {
			Log.message("Description verified Sucessfully ");

		} else {
			Log.message("Description not verified Sucessfully");
		}
		// ==========================AssignTo=======================
		SkySiteUtils.waitForElement(driver, Afrerpunch_AssignTo, 60);
		String AfterPunchassign = Afrerpunch_AssignTo.getText();
		Log.message("Assign To Email is: " + AfterPunchassign);
		String AssignTo = PropertyReader.getProperty("Email_AssignTo");
		if (AfterPunchassign.contentEquals(AssignTo)) {
			Log.message("Assign To Email verified Sucessfully ");

		} else {
			Log.message("Assign To Email not verified Sucessfully");
		}
		// ============================CC Option ======================
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, Afrerpunch_CCOption, 60);
		String AfterPunchCCoption = Afrerpunch_CCOption.getText();
		Log.message("CC To Email is1: " + AfterPunchCCoption);
		String CCoption = PropertyReader.getProperty("Email_CCOption");
		Log.message("CC To Email is2: " + CCoption);
		if (CCoption.contentEquals(AfterPunchCCoption)) {
			Log.message("CC Email verified Sucessfully ");

		} else {
			Log.message("CC Email not verified Sucessfully");
		}
		// ==============File Display======================
		String FileName1 = UplodedFileName1.getText();
		Log.message("Uploded file Name1 Display As: " + FileName1);
		String FileName2 = UplodedFileName2.getText();
		Log.message("Uploded file Name2 Display As: " + FileName2);
		return result;

	}

	public boolean PunchValidation_RecipientuploadingNewRevision() throws Throwable

	{
		boolean result = false;
		// ====================Punch ShortCut ===============
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Punchshortcut, 20);
		Log.message("Waiting for punch shortcut to be appeared");
		Punchshortcut.click();
		return result;

	}

	public boolean PunchfirstrowValidation() throws Throwable

	{
		boolean result = false;
		// ====================Punch ShortCut ===============
		SkySiteUtils.waitTill(10000);
		Log.message("Login into first row click method");
		SkySiteUtils.waitForElement(driver, DropDown, 60);
		DropDown.click();
		Log.message("Punch Drop down Clicked Sucessfully ");
		SkySiteUtils.waitForElement(driver, Display_firstrow, 60);
		Display_firstrow.click();
		SkySiteUtils.waitForElement(driver, afterImagearea, 60);
		afterImagearea.click();
		Log.message("Viewer Image Area  Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		return result;

	}

	public boolean ValidationAfterPunchCreation() throws Throwable

	{
		boolean result = false;
		Log.message("Enter into validation Part");
		// ====================Punch ShortCut ===============
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Shortdescription, 60);
		Log.message("Waiting for After Punch Description to be appeared");
		// =================Stamp Description=================
		SkySiteUtils.waitForElement(driver, Shortdescription, 60);
		String Descriptipn = Shortdescription.getText();
		Log.message("Short Description Message is1: " + Descriptipn);
		Log.message("Short Description Message is2: " + Stamp_inbox);
		if (Descriptipn.equalsIgnoreCase(Stamp_inbox)) {
			Log.message("Stamp verified Sucessfully ");

		} else {
			Log.message("Stamp not verified Sucessfully");
		}
		// ================== Stamp Title===================
		SkySiteUtils.waitTill(1000);

		SkySiteUtils.waitForElement(driver, ShortNumber, 60);
		String Stamp_Title = ShortNumber.getText();
		Log.message("Stamp Title Message is: " + Stamp_Title);

		if (Stamp_Title.contentEquals(Stamp_title)) {
			Log.message("Stamp Title verified Sucessfully ");

		} else {
			Log.message("Stamp Title not verified Sucessfully");
		}

		// ==========================Assign
		// To===================================

		SkySiteUtils.waitForElement(driver, Afrerpunch_AssignTo, 60);
		String AfterPunchassign = Afrerpunch_AssignTo.getText();
		Log.message("Assign To Email is: " + AfterPunchassign);
		String AssignTo = PropertyReader.getProperty("Email_AssignTo");
		if (AfterPunchassign.contentEquals(AssignTo)) {
			Log.message("Assign To Email verified Sucessfully ");

		} else {
			Log.message("Assign To Email not verified Sucessfully");
		}
		// ============================CC Option ======================
		SkySiteUtils.waitTill(100);
		SkySiteUtils.waitForElement(driver, Afrerpunch_CCOption, 60);
		String AfterPunchCCoption = Afrerpunch_CCOption.getText();
		Log.message("CC To Email is1: " + AfterPunchCCoption);
		String CCoption = PropertyReader.getProperty("Email_CCOption");
		Log.message("CC To Email is2: " + CCoption);
		if (CCoption.contentEquals(AfterPunchCCoption)) {
			Log.message("CC Email verified Sucessfully ");

		} else {
			Log.message("CC Email not verified Sucessfully");
		}

		// ==============File Display======================
		String FileName1 = UplodedFileName1.getText();
		Log.message("Uploded file Name1 Display As: " + FileName1);
		String FileName2 = UplodedFileName2.getText();
		Log.message("Uploded file Name2 Display As: " + FileName2);
		// =====================Description====================
		SkySiteUtils.waitTill(100);
		SkySiteUtils.waitForElement(driver, TestDescription, 60);
		String Description = TestDescription.getText();		
		String description = PropertyReader.getProperty("Desctiption");
		Log.message("Description is2: " + description);
		if (Description.contentEquals(description)) {
			Log.message("Description verified Sucessfully ");

		} else {
			Log.message("Description not verified Sucessfully");
		}

		SkySiteUtils.waitTill(1000);
		return result;

	}

	/*
	 * public boolean AfterPunch_OwnerLevelValidation() throws Throwable
	 * 
	 * { boolean result = false; Log.message("After Closed Punch validation ");
	 * //====================Punch ShortCut ===============
	 * SkySiteUtils.waitTill(10000); SkySiteUtils.waitForElement(driver,
	 * TotalPunch_Display, 20);
	 * Log.message("Waiting for punch dropdown to be Appeared"); String
	 * afterClosetotalpunch = TotalPunch_Display.getText();
	 * 
	 * String[] parts = afterClosetotalpunch.split(" "); String part1 = parts[0]; //
	 * 004 Log.message("Punch display on the screen:"+ part1);
	 * SkySiteUtils.waitTill(10000); TotalPunch_Display.click();
	 * Log.message("Total Punch clicked sucessfully ");
	 * SkySiteUtils.waitForElement(driver, Display_firstrow, 20);
	 * SkySiteUtils.waitTill(10000); Display_firstrow.click();
	 * Log.message("Total Punch clicked sucessfully "); //=================Stamp
	 * Description================= SkySiteUtils.waitForElement(driver, shortNumber,
	 * 60); String StampNo = shortNumber.getText();
	 * Log.message("Short Description Message is1: "+ StampNo);
	 * Log.message("Short Description Message is2: "+ Stamp_inbox); if
	 * (StampNo.equalsIgnoreCase(Stamp_inbox)) {
	 * Log.message("Stamp verified Sucessfully ");
	 * 
	 * } else { Log.message("Stamp not verified Sucessfully"); }
	 * 
	 * //================== Stamp Title===================
	 * SkySiteUtils.waitTill(1000);
	 * 
	 * SkySiteUtils.waitForElement(driver, PunchClose_shortdescription, 60); String
	 * Stamp_Title = PunchClose_shortdescription.getText();
	 * Log.message("Stamp Title Message is: "+ Stamp_Title); ; if
	 * (Stamp_Title.contentEquals(Stamp_title)) {
	 * Log.message("Stamp Title verified Sucessfully ");
	 * 
	 * } else { Log.message("Stamp Title not verified Sucessfully"); }
	 * 
	 * 
	 * 
	 * shortNumber.click(); Log.message("Sort Message Clicked Sucessfully");
	 * 
	 * //=====================Description====================
	 * 
	 * SkySiteUtils.waitTill(1000); SkySiteUtils.waitForElement(driver, CloseButton,
	 * 60); String Description= CloseButton.getText();;
	 * 
	 * Log.message("Description is1: "+ Description);
	 * 
	 * //String description = PropertyReader.getProperty("Desctiption");
	 * 
	 * if (Description.contentEquals("CLOSED")) {
	 * Log.message("Status CLOSED verified Sucessfully ");
	 * 
	 * } else { Log.message("Status Closed not verified Sucessfully"); }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * return result;
	 * 
	 * }
	 */

	public boolean AddComment() throws Throwable

	{
		boolean result = false;
		Log.message("Enter into Add Method section");
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, TestDescription, 60);
		String addComment = PropertyReader.getProperty("AddComment");
		Addcomment.sendKeys(addComment);
		SkySiteUtils.waitTill(1000);
		Submitbutton.click();
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String message = notificationMsg.getText();
		Log.message("punch after saving Message is: " + message);
		String expectedMessage = "Comment is added successfully";
		if (message.contains(expectedMessage)) {
			Log.message("Comment message Verified Sucessfully");

		} else {
			Log.message("Comment message Verified Sucessfully");
		}

		return result;
	}

	public boolean MultipleMarkUpSheetValidation() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ImageClick, 180);
		ImageClick.click();
		Log.message("Image Link clicked.");
		SkySiteUtils.waitTill(10000);
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitTill(2000);
		Log.message(parentHandle);
		SkySiteUtils.waitTill(2000);
		driver.switchTo().defaultContent();
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitTill(10000);
		String value1 = NoOfViewerMaxValuesheet.getText();
		Log.message("sheet message is:" + value1);
		SkySiteUtils.waitTill(10000);
		String[] x = value1.split(" ");
		String avlcount = x[1];
		int count = Integer.parseInt(avlcount);
		Log.message("Avl sheet value is:" + count);
		for (int i = 1; i <= count; i++)

		{

			Log.assertThat(TextValidation(), "text Validation working Successfully","Text validation Not  working sucessfully", driver);

			//Log.assertThat(CallOutValidation(), "Call Out Validation working Successfully",
					//"Call Out validation not working  sucessfully", driver);
			SkySiteUtils.waitTill(10000);
			SkySiteUtils.waitForElement(driver, NextPageButton, 120);
			NextPageButton.click();
			Log.message("Next Button Page Click Sucessfully");
		}

		driver.switchTo().window(parentHandle);
		Log.assertThat(Logout(), "LogOut working Successfully", "Logout not working  sucessfully", driver);
		return result;
	}

	public boolean MarkUPValidation() throws Throwable

	{
		boolean result = false;

		SkySiteUtils.waitTill(10000);
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Log.message(parentHandle);
		SkySiteUtils.waitForElement(driver, Closebutton, 120);
		Closebutton.click();
		Log.message("RFI close Button Clicked Sucessfully");
		// Log.message("Switch to Default frame ");
		SkySiteUtils.waitTill(1000);
		String value1 = NoOfViewerMaxValuesheet.getText();
		Log.message("sheet message is:" + value1);
		SkySiteUtils.waitTill(10000);
		String[] x = value1.split(" ");
		String avlcount = x[1];
		int count = Integer.parseInt(avlcount);
		Log.message("Avl sheet value is:" + count);
		for (int i = 1; i <= count; i++)

		{
			SkySiteUtils.waitTill(6000);
			Log.assertThat(TextValidation(), "text Validation working Successfully",
					"Text validation Not working sucessfully", driver);
			SkySiteUtils.waitTill(10000);
			//Log.assertThat(CallOutValidation(), "Call Out Validation working Successfully",
					//"Call Out validation not working  sucessfully", driver);
			SkySiteUtils.waitTill(10000);
			SkySiteUtils.waitForElement(driver, NextPageButton, 120);
			NextPageButton.click();
			Log.message("Next Button Page Click Sucessfully");
		}

		return true;
	}

	public boolean Image1() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		Log.message("Enter Into image Link Method");
		SkySiteUtils.waitForElement(driver, ImageClick, 120);		
		ImageClick.click();
		Log.message("Image Link clicked.");
		SkySiteUtils.waitTill(25000);
		return result;

	}

	public boolean Image2() throws Throwable

	{
		boolean result = false;
		Log.message("Image2 validation Started Link clicked.");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ImageClick1,120);		
		ImageClick1.click();
		Log.message("Image Link clicked.");
		SkySiteUtils.waitTill(25000);
		return result;

	}

	public boolean PunchValidation_creation1(String parentHandle) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);

		parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Log.message(parentHandle);
		driver.switchTo().defaultContent();
		Log.message("Switch to Default frame ");
		// *********** Punch Tools*****************
		SkySiteUtils.waitTill(1000);
		Punch_Description1();
		// =========External and Internal download Script
		SkySiteUtils.waitTill(1000);
		ExternalAndInternal_Upload2();
		// ======== ======add comment==========
		SkySiteUtils.waitTill(10000);
		CreatePunch.click();// Click on Upload without index
		Log.message("Clicked on create button Sucessfully");
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String message1 = notificationMsg.getText();
		Log.message("punch after saving Message is: " + message1);
		SkySiteUtils.waitTill(10000);
		String expectedMessage1 = "Punch annotation saved successfully";
		if (message1.contains(expectedMessage1)) {
			Log.message("Punch save Sucessfully message Verified");

		} else {
			Log.message("Punch  not Saved Sucessfully message Verified");
		}

		SkySiteUtils.waitTill(500);
		SkySiteUtils.waitForElement(driver, TotalPunch_Display, 60);
		Log.message("Waiting for punch dropdown to be Appeared");
		String afterClosetotalpunch = TotalPunch_Display.getText();
		String[] parts = afterClosetotalpunch.split(" ");
		String part1 = parts[0];
		Log.message("Punch display on the screen:" + part1);
		SkySiteUtils.waitTill(500);
		TotalPunch_Display.click();
		Log.message("Total Punch clicked sucessfully ");
		SkySiteUtils.waitForElement(driver, Display_firstrow, 60);
		SkySiteUtils.waitTill(500);
		Display_firstrow.click();
		Log.message("Total Punch clicked sucessfully ");
		SkySiteUtils.waitTill(10000);
		if (afterImagearea.isDisplayed()) {
			afterImagearea.click();
		}
		
		ValidationAfterPunchCreation();
		SkySiteUtils.waitTill(2000);
		AddComment();

		return result;
	}

	public boolean PunchValidation_creation2(String parentHandle) throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Log.message(parentHandle);
		driver.switchTo().defaultContent();
		Log.message("Switch to Default frame ");
		// *********** Punch Tools***************************************
		// Log.assertThat(Punch_Description(), " punch Description Added
		// Successfully","punch Description not Added sucessfully", driver);
		SkySiteUtils.waitTill(1000);
		Punch_Description2();
		// =========External and Internal download Script
		ExternalAndInternal_Upload();
		// ======== ======add comment==============================
		SkySiteUtils.waitTill(10000);
		CreatePunch.click();// Click on Upload without index
		Log.message("Clicked on create button Sucessfully");
		SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		String message1 = notificationMsg.getText();
		Log.message("punch after saving Message is: " + message1);
		SkySiteUtils.waitTill(10000);
		String expectedMessage1 = "Punch annotation saved successfully";
		if (message1.contains(expectedMessage1)) {
			Log.message("Punch save Sucessfully message Verified");

		} else {
			Log.message("Punch  not Saved Sucessfully message Verified");
		}
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, TotalPunch_Display, 20);
		Log.message("Waiting for punch dropdown to be Appeared");
		String afterClosetotalpunch = TotalPunch_Display.getText();
		String[] parts = afterClosetotalpunch.split(" ");
		String part1 = parts[0]; // 004
		Log.message("Punch display on the screen:" + part1);
		SkySiteUtils.waitTill(10000);
		TotalPunch_Display.click();
		Log.message("Total Punch clicked sucessfully ");
		SkySiteUtils.waitForElement(driver, Display_firstrow, 20);
		SkySiteUtils.waitTill(10000);
		Display_firstrow.click();
		Log.message("Total Punch clicked sucessfully ");
		SkySiteUtils.waitTill(10000);
		afterImagearea.click();
		SkySiteUtils.waitTill(10000);
		ValidationAfterPunchCreation();
		SkySiteUtils.waitTill(10000);
		AddComment();
		SkySiteUtils.waitTill(10000);

		// Log.assertThat(AfterClosed_PunchValidation(), " After Closed
		// validation working Successfully","After Closed validation not working
		// sucessfully", driver);

		// AfterClosed_PunchValidation();

		return result;

	}

	public void UploadFiles(String FolderPath, int FileCount)
			throws InterruptedException, AWTException, IOException {

		SkySiteUtils.waitForElement(driver, selectfiles, 60);
		SkySiteUtils.waitTill(2000);
		Log.message("Waiting for upload file button to be appeared");
		// SkySiteUtils.waitTill(10000);
		selectfiles.click();
		SkySiteUtils.waitTill(10000);
		// =================================
		// Writing File names into a text file for using in AutoIT Script
		/*BufferedWriter output;
		randomFileName rn = new randomFileName();

		String Sys_Download_Path = "./File_Download_Location/" + rn.nextFileName() + ".txt";
		Log.message("system download location2" + Sys_Download_Path);
		String tmpFileName = Sys_Download_Path;
		// String tmpFileName="c:/"+rn.nextFileName()+".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;

		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
												// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();

		Log.message("waiting AutoIT Script!!");
		SkySiteUtils.waitTill(15000);*/

		// ====================Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath_new");
		File dest = new File(PropertyReader.getProperty("Upload_TestData_viewer_SingleFile").toString());
		String path = dest.getAbsolutePath();
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + path);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(12000);
		SkySiteUtils.waitForElement(driver, CreatePunch, 60);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(10000);
		// ====================Delete the temp file=====================
	 
	}

	public boolean ExternalAndInternal_Upload2()
			throws InterruptedException, AWTException, IOException, NumberFormatException {
		boolean result = false;

		SkySiteUtils.waitTill(10000);
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);// String to
																// Integer
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(1000);
		UploadFiles(FolderPath, FileCount);
		// ===========project file upload==========================

		SkySiteUtils.waitForElement(driver, Projectfiles, 60);
		SkySiteUtils.waitTill(2000);
		Log.message("Waiting for Project Tab button to be appeared");
		Projectfiles.click();
		Log.message("Project files Tab Clicked Sucessfully");
		SkySiteUtils.waitTill(1000);
		// entered the value in searchbox
		String Folder1 = PropertyReader.getProperty("FolderName");
		SkySiteUtils.waitTill(10000);
		SearchProjectfilesInbox.sendKeys(Folder1);
		Log.message("Entered the value in project files Inbox field");
		// ================Search Button Clicked Sucessfully.
		SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton, 120);
		SkySiteUtils.waitTill(2000);
		ProjectfilesTab_searchButton.click();
		Log.message("Search Button Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Folder, 60);
		SkySiteUtils.waitTill(2000);
		Folder.click();
		Log.message("Folder Selected And clicked");
		SkySiteUtils.waitForElement(driver, File_CheckBox1, 60);
		File_CheckBox1.click();

		return result;
	}

	public boolean ExternalAndInternal_Upload()
			throws InterruptedException, AWTException, IOException, NumberFormatException {
		boolean result = false;

		SkySiteUtils.waitTill(10000);
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);// String to
																// Integer
		Log.message("FileCount: " + FileCount);
		SkySiteUtils.waitTill(2000);
		UploadFiles(FolderPath, FileCount);

		// ===========project file upload

		SkySiteUtils.waitForElement(driver, Projectfiles, 60);
		SkySiteUtils.waitTill(2000);
		Log.message("Waiting for Project Tab button to be appeared");
		Projectfiles.click();
		Log.message("Project files Tab Clicked Sucessfully");

		// entered the value in searchbox
		String Folder1 = PropertyReader.getProperty("FolderName");
		SkySiteUtils.waitTill(10000);
		SearchProjectfilesInbox.sendKeys(Folder1);

		Log.message("Entered the value in project files Inbox field");
		// ================Search Button Clicked Sucessfully.
		SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton, 60);
		SkySiteUtils.waitTill(2000);
		ProjectfilesTab_searchButton.click();
		Log.message("Search Button Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Folder, 60);
		SkySiteUtils.waitTill(1000);
		Folder.click();
		Log.message("Folder Selected And clicked");
		SkySiteUtils.waitForElement(driver, File_CheckBox, 60);
		File_CheckBox.click();

		return result;
	}

	public boolean Punch_Description1() throws InterruptedException, AWTException, IOException {
		boolean result = false;

		SkySiteUtils.waitForElement(driver, punchShortcuttools, 60);
		SkySiteUtils.waitTill(30000);
		punchShortcuttools.click();
		SkySiteUtils.waitTill(10000);
		beforeImagearea.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Waiting for create punch page to be appeared");
		Stamp.click();
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, AddNewButton, 60);
		Log.message("Stamp List Box button Clicked");
		SkySiteUtils.waitTill(2000);
		AddNewButton.click();
		Log.message("Add New Link Button Clicked");
		SkySiteUtils.waitForElement(driver, Stamp_Inbox, 60);
		SkySiteUtils.waitTill(2000);
		// String Stamp_inbox1 = Generate_Random_Number.getRandomText(3);
		Stamp_Inbox.sendKeys(Stamp_inbox);
		Log.message("Entered Value In Stamp Inbox");
		SkySiteUtils.waitForElement(driver, Stamp_Title, 60);
		// String Stamp_title1 = Generate_Random_Number.StampTitle();
		SkySiteUtils.waitTill(2000);
		Stamp_Title.sendKeys(Stamp_title1);
		SkySiteUtils.waitTill(10000);
		RightButton.click();
		Log.message("Right Button Clicked Sucessfully");
		// ================ Assign To ========================
		SkySiteUtils.waitForElement(driver, AssignTo, 60);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", AssignTo);
		Log.message("Assign To Link clicked Sucessfully");
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field:---   " + Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		SelectUse_Inbox.sendKeys(Select_Email);

		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("Assign To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		// ************CC Option---------------------------------------
		SkySiteUtils.waitForElement(driver, CC_option, 120);

		// SkySiteUtils.waitForElement(driver, Stamp, 120);
		JavascriptExecutor executor1 = (JavascriptExecutor) driver;
		executor1.executeScript("arguments[0].click();", CC_option);
		// CC_option.click();
		SkySiteUtils.waitTill(3000);
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(10000);
		SelectUse_Inbox.sendKeys(SelectEmail);
		Log.message("Entered The Value In Inbox Field");
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(10000);
		Search_Button.click();
		Log.message("Search Button clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("Select Checkbox ");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Select User Button Clicked Sucessfully ");
		// =========Date Selection Script==================
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, CalenderLink, 60);
		CalenderLink.click();
		Log.message("Calander Linked Clicked Sucessfully ");
		//SkySiteUtils.waitForElement(driver, NextMonthDate, 60);
		//NextMonthDate.click();
		//Log.message("Next Month Date  Clicked Sucessfully ");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, pickDate, 60);
		JavascriptExecutor executor2 = (JavascriptExecutor) driver;
		executor2.executeScript("arguments[0].click();", pickDate);
		Log.message(" Date  Clicked Sucessfully ");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Description, 60);
		String description = PropertyReader.getProperty("Desctiption");
		Description.sendKeys(description);
		Log.message("Entered The Value In description field");
		SkySiteUtils.waitTill(6000);
		if (Addattachment.isDisplayed()) {

			Addattachment.click();
			SkySiteUtils.waitTill(8000);
		}
		return result;

	}

	public boolean PunchCreation() throws InterruptedException, AWTException, IOException {
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Projectlevel_Punchlink, 120);
		Projectlevel_Punchlink.click();
		Log.message("Punch Link clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, Stamp, 120);
		Log.message("Stamp Clicked sucessfully");
		// Stamp.click();
		SkySiteUtils.waitForElement(driver, Stamp, 120);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", Stamp);
		SkySiteUtils.waitForElement(driver, AddNew_Button, 60);
		Log.message("Stamp List Box button Clicked");

		SkySiteUtils.waitForElement(driver, AddNew_Button, 120);
		JavascriptExecutor executor3 = (JavascriptExecutor) driver;
		executor3.executeScript("arguments[0].click();", AddNew_Button);
		// AddNew_Button.click();
		Log.message("Add New Link Button Clicked");
		String Stamp_text = Generate_Random_Number.getRandomText(3);
		SkySiteUtils.waitForElement(driver, Stamp_Inbox, 120);
		SkySiteUtils.waitTill(10000);
		Stamp_Inbox.sendKeys(Stamp_text);
		Log.message("Entered Value In Stamp Inbox:-" + Stamp_inbox);
		SkySiteUtils.waitForElement(driver, Stamp_Title, 60);
		String Stamp_value = Generate_Random_Number.StampTitle();
		SkySiteUtils.waitTill(10000);
		Stamp_Title.sendKeys(Stamp_value);
		RightButton.click();
		Log.message("Right Button Clicked Sucessfully");
		// ================ Assign To ========================
		SkySiteUtils.waitForElement(driver, AssignTo, 120);
		SkySiteUtils.waitTill(10000);
		AssignTo.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Assign To Link clicked Sucessfully");
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field:---   " + Select_Email);
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SkySiteUtils.waitTill(2000);
		SelectUser_CheckBox.click();
		SkySiteUtils.waitTill(2000);
		Log.message("Assign To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		JavascriptExecutor executor0 = (JavascriptExecutor) driver;
		executor0.executeScript("arguments[0].click();", SelectUser_Button);
		Log.message("Select User Button Clicked Sucessfully");

		// ************CC Option---------------------------------------
		SkySiteUtils.waitForElement(driver, CC_option, 120);
		JavascriptExecutor executor1 = (JavascriptExecutor) driver;
		executor1.executeScript("arguments[0].click();", CC_option);
		SkySiteUtils.waitTill(3000);
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 120);
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(10000);
		SelectUse_Inbox.sendKeys(SelectEmail);
		Log.message("Entered the Value In Select Email field :---" + SelectEmail);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(10000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("Select Checkbox Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		JavascriptExecutor executor4 = (JavascriptExecutor) driver;
		executor4.executeScript("arguments[0].click();", SelectUser_Button);
		SkySiteUtils.waitTill(3000);
		Log.message("Select User Button Clicked Sucessfully");
		// =========Date Selection Script==================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, CalenderLink, 60);
		CalenderLink.click();
		//SkySiteUtils.waitForElement(driver, NextMonthDate, 120);
		//NextMonthDate.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, pickDate1, 120);
		pickDate1.click();
		// ======================================================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, Description, 120);
		String description = PropertyReader.getProperty("Punch_Subject");
		Description.sendKeys(description);
		if (Addattachment.isDisplayed()) {

			Addattachment.click();
		}

		SkySiteUtils.waitForElement(driver, CreatePunch, 120);
		CreatePunch.click();
		SkySiteUtils.waitTill(6000);
		return result;

	}

	public boolean Punch_Description2() throws InterruptedException, AWTException, IOException {
		boolean result = false;

		SkySiteUtils.waitForElement(driver, punchShortcuttools, 60);
		SkySiteUtils.waitTill(30000);
		punchShortcuttools.click();
		SkySiteUtils.waitTill(10000);
		beforeImagearea.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Waiting for create punch page to be appeared");
		Stamp.click();
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, AddNewButton, 120);
		Log.message("Stamp List Box button Clicked");
		AddNewButton.click();
		Log.message("Add New Link Button Clicked");
		SkySiteUtils.waitTill(2000);
		// String Stamp_inbox1 = Generate_Random_Number.getRandomText(3);
		SkySiteUtils.waitForElement(driver, Stamp_Inbox, 60);
		Stamp_Inbox.sendKeys(Stamp_inbox1);
		Log.message("Entered Value In Stamp Inbox");
		SkySiteUtils.waitForElement(driver, Stamp_Title, 60);
		// String Stamp_title1 = Generate_Random_Number.StampTitle();
		SkySiteUtils.waitTill(2000);
		Stamp_Title.sendKeys(Stamp_title1);
		SkySiteUtils.waitTill(2000);
		RightButton.click();
		Log.message("Right Button Clicked Sucessfully");
		// ================ Assign To ========================
		SkySiteUtils.waitForElement(driver, AssignTo, 60);
		SkySiteUtils.waitTill(10000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", AssignTo);
		Log.message("Assign To Link clicked Sucessfully");

		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field:---   " + Select_Email);

		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		SkySiteUtils.waitTill(10000);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(3000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("Assign To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 120);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		// ************CC Option---------------------------------------
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, CC_option, 120);
		JavascriptExecutor executor1 = (JavascriptExecutor) driver;
		executor1.executeScript("arguments[0].click();", CC_option);
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(10000);
		SelectUse_Inbox.sendKeys(SelectEmail);
		Log.message("Entered the Value In Select Email field :---" + SelectEmail);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(3000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("Select Checkbox Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Select User Button Clicked Sucessfully");
		// =========Date Selection Script==================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, CalenderLink, 60);
		CalenderLink.click();
		//SkySiteUtils.waitForElement(driver, NextMonthDate, 120);
		//NextMonthDate.click();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, pickDate1, 120);
		pickDate1.click();
		// ======================================================
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, Description, 120);
		String description = PropertyReader.getProperty("Punch_Subject");
		Description.sendKeys(description);
		if (Addattachment.isDisplayed()) {

			Addattachment.click();
		}

		SkySiteUtils.waitForElement(driver, CreatePunch, 120);
		CreatePunch.click();
		SkySiteUtils.waitTill(3000);
		return result;

	}

	public boolean ProjectFiles_Internal() {
		boolean result = false;

		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, punchShortcuttools, 60);
		punchShortcuttools.click();
		// *********** Punch viewer cordinate
		// creation***************************************
		Actions builder = new Actions(driver);
		Actions drawAction = builder.moveToElement(ImageareA, 862, 261).click(ImageareA);
		SkySiteUtils.waitTill(10000);
		drawAction.build().perform();
		drawAction.click().release();
		SkySiteUtils.waitTill(10000);
		Log.message("Waiting for create punch page to be appeared");
		Stamp.click();
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, AddNewButton, 60);
		Log.message("Stamp List Box button Clicked");
		SkySiteUtils.waitTill(2000);
		AddNewButton.click();
		Log.message("Add New Link Button Clicked");
		SkySiteUtils.waitForElement(driver, Stamp_Inbox, 60);
		SkySiteUtils.waitTill(2000);
		String Stamp_inbox = Generate_Random_Number.getRandomText(3);
		Stamp_Inbox.sendKeys(Stamp_inbox);
		Log.message("Entered Value In Stamp Inbox");
		SkySiteUtils.waitForElement(driver, Stamp_Title, 60);
		String Stamp_title = Generate_Random_Number.StampTitle();
		SkySiteUtils.waitTill(2000);
		Stamp_Title.sendKeys(Stamp_title);
		SkySiteUtils.waitTill(10000);
		RightButton.click();
		Log.message("Right Button Clicked Sucessfully");
		// ================ Assign To ========================
		SkySiteUtils.waitForElement(driver, AssignTo, 60);
		JavascriptExecutor executor1 = (JavascriptExecutor) driver;
		executor1.executeScript("arguments[0].click();", AssignTo);
		Log.message("Assign To Link clicked Sucessfully");
		Log.message("Entered the Value In Select Email field");
		String Select_Email = PropertyReader.getProperty("SelectUserAssignTo_Email");
		Log.message("Entered the Value In Select Email field" + Select_Email);
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		SelectUse_Inbox.sendKeys(Select_Email);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(2000);
		Search_Button.click();
		Log.message("Search Button Clicked Sucessfully");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		Log.message("Assign To Email Checkbox Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		Log.message("Search User Button Clicked Sucessfully");
		// ************CC Option---------------------------------------
		SkySiteUtils.waitForElement(driver, CC_option, 120);
		// CC_option.click();
		JavascriptExecutor executor2 = (JavascriptExecutor) driver;
		executor2.executeScript("arguments[0].click();", CC_option);
		Log.message("Cc Option Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, SelectUse_Inbox, 60);
		Log.message("Entered the Value In Select Email field");
		String SelectEmail = PropertyReader.getProperty("SelectUserCCOption_Email");
		SkySiteUtils.waitTill(10000);
		SelectUse_Inbox.sendKeys(SelectEmail);
		SkySiteUtils.waitForElement(driver, Search_Button, 60);
		SkySiteUtils.waitTill(10000);
		Search_Button.click();
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, SelectUser_CheckBox, 60);
		SelectUser_CheckBox.click();
		SkySiteUtils.waitForElement(driver, SelectUser_Button, 60);
		SelectUser_Button.click();
		// =========Date Selection Script==================
		SkySiteUtils.waitForElement(driver, CalenderLink, 60);
		CalenderLink.click();
		SkySiteUtils.waitForElement(driver, NextMonthDate, 60);
		NextMonthDate.click();
		SkySiteUtils.waitForElement(driver, pickDate, 60);
		pickDate.click();
		SkySiteUtils.waitForElement(driver, Description, 60);
		String description = PropertyReader.getProperty("Desctiption");
		Description.sendKeys(description);
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Projectfiles, 60);
		Log.message("Waiting for Project Tab button to be appeared");
		Projectfiles.click();
		Log.message("Project files Tab Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		// entered the value in
		SearchProjectfilesInbox.sendKeys("aaa");
		SkySiteUtils.waitTill(10000);
		Log.message("Entered the value in project files Inbox field");
		// ================Search Button Clicked Sucessfully.
		SkySiteUtils.waitForElement(driver, ProjectfilesTab_searchButton, 60);
		ProjectfilesTab_searchButton.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Search Button Click Sucessfully");
		SkySiteUtils.waitForElement(driver, Folder, 60);
		Folder.click();
		Log.message("Folder Selected And clicked");
		SkySiteUtils.waitForElement(driver, File_CheckBox, 60);
		File_CheckBox.click();
		SkySiteUtils.waitTill(10000);
		CreatePunch.click();// Click on Upload without index
		Log.message("Clicked on create button Sucessfully");
		SkySiteUtils.waitTill(10000);
		String message = notificationMsg.getText();
		Log.message("punch after saving Message is: " + message);

		String expectedMessage = "Punch annotation saved successfully";
		if (message.contains(expectedMessage)) {
			Log.message("Punch save Sucessfully message Verified");

		} else {
			Log.message("Punch  not Saved Sucessfully message Verified");
		}
		return result;

	}

	public ProjectDashboardPage viewerPageSimpleExecution() throws IOException {

		SkySiteUtils.waitTill(8000);
		/*
		 * if(FeedBackmeesage.isDisplayed()) { FeedBackmeesage.click(); }
		 */

		// Log.message("Feedback Alert Clicked Sucessfully");
		// CommonMethod.Fluentwait(Project_test, 100, 2);
		SkySiteUtils.waitForElement(driver, Project_test, 60);
		Project_test.click();
		Log.message("Project Button Clicked");
		SkySiteUtils.waitTill(2000);
		Floder.click();
		Log.message("Folder Button clicked.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, ImageClick, 60);
		ImageClick.click();
		Log.message("Image Link clicked.");
		SkySiteUtils.waitTill(2000);

		return new ProjectDashboardPage(driver).get();

	}

	public boolean DeleteProject(String Prj_Name) {
		boolean result = false;

		SkySiteUtils.waitTill(10000);
		GlobalSearch.sendKeys(Prj_Name);
		Log.message("Entered Value In Global Search field");
		ButtonSearch.click();
		Log.message("Button Search Clicked Sucessfully");

		int Avl_Projects_Count = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'PName_')]"));
		for (WebElement Element : allElements) {
			Avl_Projects_Count = Avl_Projects_Count + 1;
		}
		Log.message("Available private projects count is: " + Avl_Projects_Count);
		SkySiteUtils.waitTill(10000);

		for (int i = 1; i <= Avl_Projects_Count; i++) {
			String Exp_ProjName = driver
					.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/ul/li[" + i + "]/div/section[1]/h4"))
					.getText();

			// Validating - Expected project is selected or not
			if (Exp_ProjName.trim().contentEquals(Prj_Name.trim())) {
				Log.message("Maching Project Found!!");
				driver.findElement(By.xpath("html/body/div[1]/div[3]/div[2]/div/ul/li[" + i + "]/div/section[1]/h4"))
						.click();
				Log.message("Clicked on expected project!!");
				SkySiteUtils.waitTill(10000);
				break;
			}

		}

		return result;

	}

	public boolean ToolBarSelection_And_Validation() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ImageClick, 60);
		ImageClick.click();
		Log.message("Image Link clicked.");
		SkySiteUtils.waitTill(8000);
		String parentHandle = driver.getWindowHandle();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Log.message(parentHandle);
		SkySiteUtils.waitTill(10000);
		driver.switchTo().defaultContent();
		Log.message("Switch to Default frame ");
		SkySiteUtils.waitTill(10000);
		// TextValidation();
		Log.assertThat(TextValidation(), "text Validation working Successfully",
				"Text validation Not  working sucessfully", driver);
		// CallOutValidation();
		//Log.assertThat(CallOutValidation(), "Call Out Validation working Successfully",
				//"Call Out validation not working  sucessfully", driver);
		driver.switchTo().window(parentHandle);
		Log.assertThat(Logout(), "Logout working Successfully", "Logout not working  sucessfully", driver);
		// ArrowValidation();
		// Log.assertThat(ArrowValidation(), "Arrow Validation working
		// Successfully", "Arrow working not sucessfully", driver);
		// RetangleHightlighter();
		// Log.assertThat(RetangleHightlighter(), "Retangular Validation working
		// Successfully", "Retangular not validation working sucessfully",
		// driver);
		// CloudRetangleValidation() ;
		// Log.assertThat(CloudRetangleValidation(), "Cloud Retangle Validation
		// working Successfully", "Cloud not Retangle validation working
		// sucessfully", driver);
		// HyperlinkCircleValidation();
		// Log.assertThat(HyperlinkCircleValidation(), "Hyperlink Validation
		// working Successfully", "Hyperlink validation not working
		// sucessfully", driver);
		// FreeHandValidation();
		// Log.assertThat( FreeHandValidation(), "Freehand Validation working
		// Successfully", "Free hand validation not working sucessfully",
		// driver);

		return true;
	}

	// ===========Cloud Retangle Tools=========================

	public boolean CloudRetangleValidation() throws Throwable

	{
		boolean result = true;
		Log.message("Enter Into Cloud Retangle Validation Method");
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, OpenShapeMenu, 60);
		OpenShapeMenu.click();
		Log.message("Open shape Menu Button clicked sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, DrowCloud, 60);
		// ==== FreeHand tool-Baar=====================
		SkySiteUtils.waitTill(10000);
		DrowCloud.click();
		Log.message("Drow Cloud Button clicked sucessfully ");
		SkySiteUtils.waitTill(10000);
		Actions builder = new Actions(driver);
		Actions drawAction = builder.moveToElement(ImageareA, 862, 261).clickAndHold().moveToElement(ImageareA, 969,
				439);
		SkySiteUtils.waitTill(6000);
		drawAction.build().perform();
		drawAction.click().release();
		SkySiteUtils.waitTill(10000);
		return true;

	}

	// =====================Text tools=============================
	public boolean TextValidation() throws Throwable

	{
		boolean result = false;
		Log.message("Enter Into Text Validation Method");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, OpenTextMenu, 60);
		
		Actions act=new Actions(driver);
		act.moveToElement(OpenTextMenu).click(OpenTextMenu).build().perform();
		//Log.message("create rfi icon is clicked");
		//OpenTextMenu.click();
		Log.message("Open Text menu Button clicked sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, AddaText, 60);
		Log.message("Waiting to click on text tab.");
		SkySiteUtils.waitTill(10000);
		driver.findElement(By.xpath("html/body/header/nav")).click();
		// ==== FreeHand tool-Baar=====================
		SkySiteUtils.waitTill(10000);
		Actions act1=new Actions(driver);
		act1.moveToElement(AddaText).click(AddaText).build().perform();
		//AddaText.click();
		Log.message("Add a text note Button clicked sucessfully ");
		SkySiteUtils.waitTill(10000);
		Actions builder = new Actions(driver);
		Actions drawAction = builder.moveToElement(ImageareA, 862, 261).click(ImageareA);
		SkySiteUtils.waitTill(10000);
		drawAction.build().perform();
		drawAction.click().release();
		SkySiteUtils.waitTill(10000);
		// ============== for Pop Validation Message Change========================
		String Entertext = Generate_Random_Number.RandamName_text();
		SkySiteUtils.waitTill(10000);
		// ===========Enter Text Into text area.
		SkySiteUtils.waitForElement(driver, Textarea, 60);
		Textarea.sendKeys(Entertext);
		Log.message("Entered Value In TeXt Area" + Entertext);
		// select font Colour from font Colour drop down.
		SkySiteUtils.waitForElement(driver, Fontcolour, 60);
		Select oSelect = new Select(Fontcolour);
		oSelect.selectByVisibleText("Yellow");
		Log.message("Selected Value From Font Colour Drop Down");
		SkySiteUtils.waitForElement(driver, Fontsize, 60);
		// select font size from font-size drop down.
		Select oSelect1 = new Select(Fontsize);
		oSelect1.selectByVisibleText("20");
		Log.message("Selected Value From Font Size Drop Down");
		SkySiteUtils.waitForElement(driver, BackGroundColour, 60);
		// select Backgroung colour from Back Ground Colour drop down.
		Select oSelect2 = new Select(BackGroundColour);
		oSelect2.selectByVisibleText("Red");
		Log.message("Selected Value From BackGround Colour Drop Down");
		SkySiteUtils.waitForElement(driver, BorderColour, 60);
		// select Border colour from Border colour drop down.
		Select oSelect3 = new Select(BorderColour);
		oSelect3.selectByVisibleText("Red");
		Log.message("Selected Value From Border Colour Drop Down");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Text_okButton, 60);
		Text_okButton.click();
		Log.message("Ok Button Clicked sucessfully");
		SkySiteUtils.waitTill(10000);

		// FOR PARTICULAR AREA SCREENSHOT
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg = ImageIO.read(screenshot);
		org.openqa.selenium.Point point = ImageareA.getLocation();
		// Get width and height of the element
		int eleWidth = ImageareA.getSize().getWidth();
		int eleHeight = ImageareA.getSize().getHeight();
		Log.message("Width of the screenshot " + eleWidth);
		Log.message("Width of the screenshot " + eleHeight);
		BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
		ImageIO.write(eleScreenshot, "png", screenshot);
		String FileLocation1 = PropertyReader.getProperty("ImageLocation1");
		File screenshotLocation = new File(FileLocation1 + ".jpg");
		FileUtils.copyFile(screenshot, screenshotLocation);

		SkySiteUtils.waitForElement(driver, SaveButton, 60);
		SaveButton.click();
		Log.message("Save Button Clicked Sucessfully");

		SkySiteUtils.waitForElement(driver, MarkUpName, 60);
		String Markupname = Generate_Random_Number.RandamName();
		SkySiteUtils.waitTill(10000);
		MarkUpName.sendKeys(Markupname);
		Log.message("Mark UP Name Value Entered Sucessfully");
		SkySiteUtils.waitForElement(driver, MarkName_SaveButton, 180);
		MarkName_SaveButton.click();
		SkySiteUtils.waitTill(10000);
		Log.message("Mark Up Pop UP  Save Button Clicked Sucessfully");
		//SkySiteUtils.waitForElement(driver, notificationMsg, 60);
		//String message = notificationMsg.getText();
		//Log.message("Message after saving call-out markup is: " + message);
		/*String expectedMessage = "Markup saved successfully";
		if (message.contentEquals(expectedMessage)) {
			Log.message("Mark Up Saved Sucessfully message Verified");

		} else {
			Log.message("Mark Up not Saved Sucessfully message Verified");
		}*/

		SkySiteUtils.waitTill(10000);

		/*
		 * File screenshot1 =
		 * ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); BufferedImage
		 * fullImg1 = ImageIO.read(screenshot); org.openqa.selenium.Point point1 =
		 * ImageareA.getLocation(); //Get width and height of the element int eleWidth1
		 * = ImageareA.getSize().getWidth(); int eleHeight1 =
		 * ImageareA.getSize().getHeight();
		 * Log.message("Width of the screenshot "+eleWidth1);
		 * Log.message("Width of the screenshot "+eleHeight1); BufferedImage
		 * eleScreenshot1= fullImg1.getSubimage(point1.getX(), point1.getY(),eleWidth1,
		 * eleHeight1); ImageIO.write(eleScreenshot1, "png", screenshot1);
		 */
		// Log.message("bufferImage "+eleScreenshot1);
		String FileLocation2 = PropertyReader.getProperty("ImageLocation2");
		File screenshotLocation1 = new File(FileLocation2 + ".jpg");
		FileUtils.copyFile(screenshot, screenshotLocation1);
		Log.message("After Save Button Screenshot Taken Sucessfully");
		SkySiteUtils.waitTill(10000);
		// ===Image Comparision Method=========================
		String File1 = PropertyReader.getProperty("ImageLocation1") + ".jpg";
		String File2 = PropertyReader.getProperty("ImageLocation2") + ".jpg";
		SkySiteUtils.waitTill(10000);
		result = CommonMethod.Imagecomparision(File1, File2);

		if (result == true)
			return true;
		else
			return false;

	}

	// ===================Callout Tools =================================
	public boolean Delete_MarkUP() throws Throwable

	{
		boolean result = true;
		Log.message("Enter Into call out Validation Method");
		
		SkySiteUtils.waitTill(10000);
		Actions builder = new Actions(driver);
		Actions drawAction = builder.moveToElement(ImageareA, 862, 261).click(ImageareA);
		SkySiteUtils.waitTill(10000);
		drawAction.build().perform();
		drawAction.click().release();
		SkySiteUtils.waitTill(10000);
		
		// =====open Text menu tols click===========================
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, DeleteButton, 60);
		DeleteButton.click();
		/*Log.message("Open Text menu Button clicked sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, AddaText, 60);
		Log.message("Waiting to click on text tab.");
		SkySiteUtils.waitTill(10000);
		driver.findElement(By.xpath("html/body/header/nav")).click();
		// ==== call out option clicked=====================
		SkySiteUtils.waitTill(10000);
		CALLOUT.click();
		Log.message("Call Out  Button clicked sucessfully ");
		SkySiteUtils.waitTill(10000);
		// ************* Image Area
		// Selection***************************************
		
		 * SkySiteUtils.waitTill(10000); beforeImagearea.click();
		 
		Actions builder = new Actions(driver);
		Actions drawAction = builder.moveToElement(ImageareA, 697, 419).click(ImageareA);
		SkySiteUtils.waitTill(6000);
		drawAction.build().perform();
		drawAction.click().release();
		SkySiteUtils.waitTill(10000);*/
		return result;

	}

	// ===================Callout Tools =================================
	public boolean CallOutValidation() throws Throwable

	{
		boolean result = true;
		Log.message("Enter Into call out Validation Method");
		// =====open Text menu tols click===========================
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, OpenTextMenu, 60);
		OpenTextMenu.click();
		Log.message("Open Text menu Button clicked sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, AddaText, 60);
		Log.message("Waiting to click on text tab.");
		SkySiteUtils.waitTill(10000);
		driver.findElement(By.xpath("html/body/header/nav")).click();
		// ==== call out option clicked=====================
		SkySiteUtils.waitForElement(driver, CALLOUT, 60);
		CALLOUT.click();
		Log.message("Call Out  Button clicked sucessfully ");
		SkySiteUtils.waitTill(10000);
		// ************* Image Area
		// Selection***************************************

		/*
		 * SkySiteUtils.waitTill(10000); beforeImagearea.click();
		 */
		Actions builder = new Actions(driver);
		Actions drawAction = builder.moveToElement(ImageareA, 697, 419).click(ImageareA);
		SkySiteUtils.waitTill(6000);
		drawAction.build().perform();
		drawAction.click().release();
		SkySiteUtils.waitTill(10000);
		// String Entertext =PropertyReader.getProperty("CallOut_EnterText");
		SkySiteUtils.waitTill(10000);
		// ========================Enter Text Into text
		// area.============================
		SkySiteUtils.waitForElement(driver, CallOutTextArea, 60);
		String Entercallout = Generate_Random_Number.RandamName_CallOut();
		CallOutTextArea.sendKeys(Entercallout);
		Log.message("Entered Value In TeXt Area:---" + Entercallout);
		// ========================select font colour from font colour drop
		// down.===========
		SkySiteUtils.waitForElement(driver, Fontcolour, 60);
		Select oSelect = new Select(Fontcolour);
		oSelect.selectByVisibleText("Yellow");
		Log.message("Selected Value From Font Colour Drop Down");
		// ==========================select fill colour from fill colour drop
		// down.===
		SkySiteUtils.waitForElement(driver, Fillcolour, 60);
		Select oSelect1 = new Select(Fillcolour);
		oSelect1.selectByVisibleText("Red");
		Log.message("Selected Value From fill colour Drop Down");
		// ==========================select Border Thickness drop
		// down.=================
		SkySiteUtils.waitForElement(driver, BorderThickness, 60);
		Select oSelect2 = new Select(BorderThickness);
		oSelect2.selectByIndex(4);
		Log.message("Selected Value From Border Thickness Drop Down:");
		// ============================select font size from drop
		// down=============
		SkySiteUtils.waitForElement(driver, Fontsize, 60);
		Select oSelect3 = new Select(Fontsize);
		oSelect3.selectByVisibleText("20");
		Log.message("Selected Value From font Size Drop Down");
		// ===================select Border colour from Border colour drop
		// down.============
		SkySiteUtils.waitForElement(driver, BorderColour, 60);
		Select oSelect4 = new Select(BorderColour);
		oSelect4.selectByVisibleText("Red");
		Log.message("Selected Value From Border Colour Drop Down");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, Text_okButton, 60);
		Text_okButton.click();
		Log.message("Ok Button Clicked sucessfully");
		SkySiteUtils.waitTill(10000);

		// *************First Screenshot
		// Function********************************************
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg = ImageIO.read(screenshot);
		org.openqa.selenium.Point point = ImageareA.getLocation();

		// *************** Get width and height of the
		// element************************
		int eleWidth = ImageareA.getSize().getWidth();
		int eleHeight = ImageareA.getSize().getHeight();
		Log.message("Width of the screenshot " + eleWidth);
		Log.message("Width of the screenshot " + eleHeight);
		BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
		ImageIO.write(eleScreenshot, "png", screenshot);
		String FileLocation1 = PropertyReader.getProperty("ImageLocation1");
		File screenshotLocation = new File(FileLocation1 + ".jpg");
		FileUtils.copyFile(screenshot, screenshotLocation);
		// ************** Save Button clicked***************
		SkySiteUtils.waitForElement(driver, SaveButton, 60);
		SaveButton.click();
		Log.message("Save Button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, MarkUpName, 60);
		String Markupname = Generate_Random_Number.RandamName();
		SkySiteUtils.waitTill(6000);
		MarkUpName.sendKeys(Markupname);
		Log.message("Mark UP Name Value Entered Sucessfully");
		SkySiteUtils.waitForElement(driver, MarkName_SaveButton, 180);
		MarkName_SaveButton.click();
		SkySiteUtils.waitTill(600);
		Log.message("Mark Up Pop UP  Save Button Clicked Sucessfully");
		/*
		 * SkySiteUtils.waitForElement(driver, notificationMsg, 60); String message =
		 * notificationMsg.getText();
		 * Log.message("Message after saving call-out markup is: " + message); String
		 * expectedMessage = "Markup saved successfully"; if
		 * (message.contentEquals(expectedMessage)) {
		 * Log.message("Mark Up Saved Sucessfully message Verified");
		 * 
		 * } else { Log.message("Mark Up not Saved Sucessfully message Verified"); }
		 * 
		 * SkySiteUtils.waitTill(7000);
		 * 
		 * 
		 * File screenshot1 =
		 * ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); BufferedImage
		 * fullImg1 = ImageIO.read(screenshot); org.openqa.selenium.Point point1 =
		 * ImageareA.getLocation(); //Get width and height of the element int eleWidth1
		 * = ImageareA.getSize().getWidth(); int eleHeight1 =
		 * ImageareA.getSize().getHeight();
		 * Log.message("Width of the screenshot "+eleWidth1);
		 * Log.message("Width of the screenshot "+eleHeight1); BufferedImage
		 * eleScreenshot1= fullImg1.getSubimage(point1.getX(), point1.getY(),eleWidth1,
		 * eleHeight1); ImageIO.write(eleScreenshot1, "png", screenshot1);
		 */
		// Log.message("bufferImage "+eleScreenshot1);
		String FileLocation2 = PropertyReader.getProperty("ImageLocation2");
		File screenshotLocation1 = new File(FileLocation2 + ".jpg");
		FileUtils.copyFile(screenshot, screenshotLocation1);
		Log.message("After Save Button Screenshot Taken Sucessfully");
		SkySiteUtils.waitTill(10000);

		// ===Image Comparision Method=========================
		String File1 = PropertyReader.getProperty("ImageLocation1") + ".jpg";
		String File2 = PropertyReader.getProperty("ImageLocation2") + ".jpg";
		SkySiteUtils.waitTill(10000);
		result = CommonMethod.Imagecomparision(File1, File2);

		if (result == true)
			return true;
		else
			return false;

	}

	// ===================Hyper-link Circle Tools
	// =================================
	public boolean HyperlinkCircleValidation() throws Throwable

	{
		boolean result = true;
		Log.message("Enter Into Hyperlink Circle Method");
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, OpenHyperlinkmenu, 60);
		OpenHyperlinkmenu.click();
		Log.message("Open HyperLink Button clicked sucessfully");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Hyperlink_Circle, 60);
		// ==== FreeHand tool-Baar=====================
		SkySiteUtils.waitTill(10000);
		Hyperlink_Circle.click();
		Log.message("HyperLink Circle Button clicked sucessfully ");
		SkySiteUtils.waitTill(10000);
		Actions builder = new Actions(driver);
		Actions drawAction = builder.moveToElement(ImageareA, 862, 261).clickAndHold().moveToElement(ImageareA, 969,
				439);

		SkySiteUtils.waitTill(6000);
		drawAction.build().perform();
		drawAction.click().release();
		SkySiteUtils.waitTill(10000);
		File screenshot1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg1 = ImageIO.read(screenshot1);
		org.openqa.selenium.Point point1 = ImageareA.getLocation();
		// Get width and height of the element
		int eleWidth1 = ImageareA.getSize().getWidth();
		int eleHeight1 = ImageareA.getSize().getHeight();
		Log.message("Width of the screenshot " + eleWidth1);
		Log.message("Width of the screenshot " + eleHeight1);
		BufferedImage eleScreenshot1 = fullImg1.getSubimage(point1.getX(), point1.getY(), eleWidth1, eleHeight1);
		ImageIO.write(eleScreenshot1, "png", screenshot1);
		Log.message("bufferImage " + eleScreenshot1);
		String FileLocation1 = PropertyReader.getProperty("ImageLocation1");
		File screenshotLocation1 = new File(FileLocation1 + ".jpg");
		FileUtils.copyFile(screenshot1, screenshotLocation1);
		Log.message("After Save Button Screenshot Taken Sucessfully");
		SkySiteUtils.waitTill(10000);

		return true;

	}

	public boolean FreeHandValidation() throws Throwable

	{
		boolean result = true;
		Log.message("Enter Into Freehand Method");
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, OpenLineMenu, 60);
		// ==== Open line Menu ToolBaar Click =======
		OpenLineMenu.click();
		Log.message("Open Line Button clicked sucessfully ");
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, DrowWithFreehand, 60);
		// ==== FreeHand tool-Baar=====================
		SkySiteUtils.waitTill(6000);
		DrowWithFreehand.click();
		Log.message("Freehand Button clicked sucessfully ");
		Actions builder = new Actions(driver);
		Actions drawAction = builder.moveToElement(ImageareA, 862, 261).clickAndHold()
				.moveToElement(ImageareA, 969, 439).clickAndHold(ImageareA);
		SkySiteUtils.waitTill(6000);
		drawAction.perform();
		SkySiteUtils.waitTill(10000);
		File screenshot1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg1 = ImageIO.read(screenshot1);
		org.openqa.selenium.Point point1 = ImageareA.getLocation();
		// Get width and height of the element
		int eleWidth1 = ImageareA.getSize().getWidth();
		int eleHeight1 = ImageareA.getSize().getHeight();
		Log.message("Width of the screenshot " + eleWidth1);
		Log.message("Width of the screenshot " + eleHeight1);
		BufferedImage eleScreenshot1 = fullImg1.getSubimage(point1.getX(), point1.getY(), eleWidth1, eleHeight1);
		ImageIO.write(eleScreenshot1, "png", screenshot1);
		Log.message("bufferImage " + eleScreenshot1);

		String FileLocation1 = PropertyReader.getProperty("ImageLocation1");
		File screenshotLocation1 = new File(FileLocation1 + ".jpg");
		FileUtils.copyFile(screenshot1, screenshotLocation1);
		Log.message("After Save Button Screenshot Taken Sucessfully");
		SkySiteUtils.waitTill(10000);

		return true;

	}

	// =========Arrow Related Test Cases==========================
	public boolean ArrowValidation() throws Throwable

	{
		boolean result = false;
		Log.message("Enter Into open line menumethod");
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, OpenLineMenu, 60);
		// ==== Open line Menu ToolBaar Click =======
		OpenLineMenu.click();
		Log.message("Open Line Button clicked sucessfully ");
		Thread.sleep(6000);
		SkySiteUtils.waitForElement(driver, Arrow, 60);
		// ==== Arrow Tool-Baar=====================
		Thread.sleep(2000);
		Arrow.click();
		Log.message("Arrow Button clicked sucessfully ");
		SkySiteUtils.waitTill(6000);
		// Action class for selecting the Image And Clicking the particular Area
		// As per X and Y Axis
		Actions builder = new Actions(driver);
		builder.moveToElement(ImageareA, 785, 322);
		builder.clickAndHold();
		builder.moveToElement(ImageareA, 1160, 261);
		builder.clickAndHold();
		builder.release();
		builder.build().perform();
		Thread.sleep(10000);
		// builder.click();
		Thread.sleep(10000);

		// =========First File
		// Comparison==============================================

		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg = ImageIO.read(screenshot);

		org.openqa.selenium.Point point = ImageareA.getLocation();

		// Get width and height of the element
		int eleWidth = ImageareA.getSize().getWidth();
		int eleHeight = ImageareA.getSize().getHeight();
		Log.message("Width of the screenshot " + eleWidth);
		Log.message("Width of the screenshot " + eleHeight);
		BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
		ImageIO.write(eleScreenshot, "png", screenshot);
		Log.message("bufferImage " + eleScreenshot);
		// ============File Comparision=======================================
		String FileLocation1 = PropertyReader.getProperty("ImageLocation1");
		File screenshotLocation = new File(FileLocation1 + ".jpg");
		FileUtils.copyFile(screenshot, screenshotLocation);

		SkySiteUtils.waitTill(6000);
		/*
		 * SaveButton.click(); Log.message("Save Button Clicked Sucessfully");
		 * //============= second screenshot for file comparision File screenshot2 =
		 * ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); BufferedImage
		 * fullImg2 = ImageIO.read(screenshot2); org.openqa.selenium.Point point2 =
		 * ImageareA.getLocation();
		 * 
		 * // Get width and height of the element int eleWidth2 =
		 * ImageareA.getSize().getWidth(); int eleHeight2 =
		 * ImageareA.getSize().getHeight();
		 * Log.message("Width of the screenshot "+eleWidth2);
		 * Log.message("Width of the screenshot "+eleHeight2); BufferedImage
		 * eleScreenshot2= fullImg2.getSubimage(point2.getX(), point2.getY(), eleWidth2,
		 * eleHeight2); ImageIO.write(eleScreenshot2, "png", screenshot2);
		 * Log.message("bufferImage "+eleScreenshot2); String FileLocation2
		 * =PropertyReader.getProperty("ImageLocation2"); File screenshotLocation2 = new
		 * File(FileLocation2 +".jpg"); FileUtils.copyFile(screenshot2,
		 * screenshotLocation2);
		 * 
		 * Log.message("Screenshot save In specified Area "); String FileName1 =
		 * PropertyReader.getProperty("ImageLocation1"); String FileName2 =
		 * PropertyReader.getProperty("ImageLocation2");
		 * CommonMethod.Imagecomparision(FileName1, FileName2);
		 */
		SkySiteUtils.waitTill(8000);

		return true;
	}

	// =================highlighter related
	// Testcases==================================================
	public boolean RetangleHightlighter() throws Throwable

	{
		boolean result = false;
		Log.message("Enter Into Retangle Hightlighter menu");
		SkySiteUtils.waitTill(6000);
		// ==== ZoomIn button validation =======
		Highlighter.click();
		Log.message("Highlighter toolbaar Button Clicked Sucessfully");
		Actions builder = new Actions(driver);
		Actions drawAction = builder.moveToElement(ImageareA, 862, 261).clickAndHold().moveToElement(ImageareA, 969,
				439);
		SkySiteUtils.waitTill(6000);
		drawAction.build().perform();
		// drawAction.click().release();
		SkySiteUtils.waitTill(10000);
		File screenshot1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg1 = ImageIO.read(screenshot1);
		org.openqa.selenium.Point point1 = ImageareA.getLocation();
		// Get width and height of the element
		int eleWidth1 = ImageareA.getSize().getWidth();
		int eleHeight1 = ImageareA.getSize().getHeight();
		Log.message("Width of the screenshot " + eleWidth1);
		Log.message("Width of the screenshot " + eleHeight1);
		BufferedImage eleScreenshot1 = fullImg1.getSubimage(point1.getX(), point1.getY(), eleWidth1, eleHeight1);
		ImageIO.write(eleScreenshot1, "png", screenshot1);
		Log.message("bufferImage " + eleScreenshot1);

		String FileLocation1 = PropertyReader.getProperty("ImageLocation1");
		File screenshotLocation1 = new File(FileLocation1 + ".jpg");
		FileUtils.copyFile(screenshot1, screenshotLocation1);
		Log.message("After Save Button Screenshot Taken Sucessfully");
		SkySiteUtils.waitTill(10000);

		return true;
	}

	// =================Add A Text
	// Test-cases==================================================
	public boolean Add_A_Text() throws Throwable

	{
		boolean result = false;
		Log.message("Enter Into Add A text  menu");
		SkySiteUtils.waitTill(10000);
		// ==== ZoomIn button validation =======
		Highlighter.click();
		Log.message("Highlighter toolbaar Button Clicked Sucessfully");

		Actions builder = new Actions(driver);
		Actions drawAction = builder.moveToElement(ImageareA, 862, 261).clickAndHold()
				.moveToElement(ImageareA, 969, 439).clickAndHold();
		drawAction.build().perform();
		// drawAction.release();

		SkySiteUtils.waitTill(10000);
		File screenshot1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg1 = ImageIO.read(screenshot1);
		org.openqa.selenium.Point point1 = ImageareA.getLocation();
		// Get width and height of the element
		int eleWidth1 = ImageareA.getSize().getWidth();
		int eleHeight1 = ImageareA.getSize().getHeight();
		Log.message("Width of the screenshot " + eleWidth1);
		Log.message("Width of the screenshot " + eleHeight1);
		BufferedImage eleScreenshot1 = fullImg1.getSubimage(point1.getX(), point1.getY(), eleWidth1, eleHeight1);
		ImageIO.write(eleScreenshot1, "png", screenshot1);
		Log.message("bufferImage " + eleScreenshot1);

		String FileLocation1 = PropertyReader.getProperty("ImageLocation1");
		File screenshotLocation1 = new File(FileLocation1 + ".jpg");
		FileUtils.copyFile(screenshot1, screenshotLocation1);
		Log.message("After Save Button Screenshot Taken Sucessfully");
		SkySiteUtils.waitTill(10000);

		return true;
	}

	/*
	 * public ProjectDashboardPage LogOut()
	 * 
	 * {
	 * 
	 * SkySiteUtils.waitForElement(driver, LogOut, 60); LogOut.click();
	 * Log.message("Logout button Clicked Sucessfully");
	 * SkySiteUtils.waitForElement(driver, YesButton, 60);
	 * Log.message("Waiting for logout Button Present"); YesButton.click();
	 * Log.message("Yes Button Clicked Sucessfully "); return new
	 * ProjectDashboardPage(driver).get();
	 * 
	 * 
	 * }
	 */
	
	
	
	public void SwitchToChield(String parentHandle) throws Throwable

	{	
	 parentHandle = driver.getWindowHandle();
	parentHandle = driver.getWindowHandle();
	for (String winHandle : driver.getWindowHandles()) {
		driver.switchTo().window(winHandle);
	}		
	Log.message(parentHandle);
	SkySiteUtils.waitTill(10000);
	driver.switchTo().defaultContent();
	Log.message("Switch to Default frame ");
	}
	
	
	public void SwitchToparent(String parentHandle) throws Throwable

	{	
	
		
			driver.switchTo().window(parentHandle);

		
		Log.message("switched to Parent Window");

		driver.switchTo().defaultContent();
			
	Log.message(parentHandle);
	SkySiteUtils.waitTill(10000);
	
	}
	
	public void CloseParticularTab() throws Throwable

	{	
	
		driver.close();			
	
	
	
	}


	public boolean ZoomINValidation() throws Throwable

	{
		boolean result = false;

		Log.message("Enter Into zoom method");
		// ==== ZoomIn button validation =======
		ZoomIn.click();

		Log.message("Zoom In Button clicked sucessfully -First time.");
		SkySiteUtils.waitTill(6000);
		ZoomIn.click();
		Log.message("Zoom In Button clicked sucessfully -Second time.");
		SkySiteUtils.waitTill(6000);
		ZoomIn.click();
		Log.message("Zoom In Button clicked sucessfully -Third time.");
		// ZoomOut Button Clicked Sucessfully.
		SkySiteUtils.waitTill(6000);
		if (ZoomInDisabled.isDisplayed())
			return true;
		else
			return false;
	}

	public boolean RFIFirstPosition() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, REVISION_DROPDOWN, 60);
		REVISION_DROPDOWN.click();
		Log.message("Drop Down Link Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, RFI_Firstrow, 60);
		RFI_Firstrow.click();
		Log.message("RFI First Row Clicked  Sucessfully");
		SkySiteUtils.waitForElement(driver, REVISION_rfiiMAGE, 60);
		REVISION_rfiiMAGE.click();
		Log.message("RFI image Link Clicked  Sucessfully");

		return result;

	}

	public boolean RFITAB() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFITab, 60);
		RFITab.click();
		Log.message("RFI Tab Sucessfully");

		return result;

	}

	public void UPDATED_RFIROW() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, RFI_Firstrow, 120);
		RFI_Firstrow.click();
		SkySiteUtils.waitTill(10000);
		Log.message("RFI FirstRow Clicked Sucessfully");
		
		/*String parentHandle1 = driver.getWindowHandle();
		Log.message(parentHandle1);
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		*/

	   }

	public boolean Close_POPUP() throws Throwable

	{
		boolean result = false;
		String parentHandle1 = driver.getWindowHandle();
		Log.message(parentHandle1);
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, Closebutton, 60);
		Closebutton.click();
		Log.message("RFI close Button Clicked Sucessfully");
		SkySiteUtils.waitTill(10000);
		return result;

	}

	public boolean CloseThePOPUSaveMarkUP() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, RFI_Firstrow, 60);
		RFI_Firstrow.click();
		SkySiteUtils.waitTill(10000);
		Log.message("RFI FirstRow Clicked Sucessfully");
		return result;

	}

	public boolean ProjectManagement() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, ProjectManagement, 60);
		ProjectManagement1.click();
		SkySiteUtils.waitTill(10000);
		Log.message("project Management Link Click Sucessfully");
		SkySiteUtils.waitForElement(driver, ProjectManagement_RFI, 60);
		ProjectManagement_RFI.click();
		SkySiteUtils.waitTill(10000);
		Log.message("RFI Link Click Sucessfully");

		return result;

	}

	public boolean ZoomOUTValidation() throws Throwable

	{
		boolean result = false;

		Log.message("Enter Into zoom out method");
		// ==== ZoomIn button validation =======
		ZoomOut.click();

		Log.message("Zoom Out Button clicked sucessfully -First time.");
		SkySiteUtils.waitTill(6000);
		ZoomOut.click();
		Log.message("Zoom Out Button clicked sucessfully -Second time.");
		SkySiteUtils.waitTill(6000);
		ZoomOut.click();
		Log.message("Zoom out Button clicked sucessfully -Third time.");
		// ZoomOut Button Clicked Sucessfully.
		SkySiteUtils.waitTill(6000);
		if (driver.findElement(By.xpath("//a[@class='leaflet-control-zoom-out leaflet-disabled']")).isDisplayed())
			return true;
		else
			return false;
	}

	public boolean HomeButton() throws Throwable

	{
		boolean result = false;
		SkySiteUtils.waitTill(6000);
		ZoomOut.click();

		Log.message("Zoom Out Button clicked sucessfully -First time.");
		return result;

	}

	/**
	 * Method generate random markup name By Trinanjwan
	 */
	public String Random_Markupname() {

		String str = "Markup";
		Random r = new Random(System.currentTimeMillis());
		String abc = str + String.valueOf((10000 + r.nextInt(20000)));
		return abc;

	}

	/**
	 * Method to delete markup at beginning of the execution Scripted by Trinanjwan
	 */

	@FindBy(css = "#spnMarkupCount")
	WebElement Markuplist;

	@FindBy(css = ".icon-trash")
	List<WebElement> Markupdelete;

	@FindBy(css = ".icon-trash")
	WebElement Markupdelete1;

	@FindBy(css = "#button-1")
	WebElement deleteconfirmation;

	public void markupdeletebegin() {
		{

			if (Markuplist.isDisplayed()) {
				Markuplist.click();
				Log.message("Clicked on mark up list");
				SkySiteUtils.waitTill(15000);
				int sizeoflist = Markupdelete.size();

				Log.message("size is " + sizeoflist);
				SkySiteUtils.waitTill(10000);
				for (int i = 1; i <= sizeoflist; i++) {

					Markupdelete1.click();
					Log.message("markup delete is clicked");
					SkySiteUtils.waitTill(10000);

					if (deleteconfirmation.isDisplayed()) {
						Actions act = new Actions(driver);
						act.moveToElement(deleteconfirmation).click(deleteconfirmation).build().perform();
					}
					Log.message("Added markup deleted successfully");

				}
			} else {
				Log.message("no markup is present");
			}

		}
	}

	/**
	 * Method to create line, freehand, rectangle annotation in the viewer Scripted
	 * By Trinanjwan
	 */

	@FindBy(css = "#lineToolMenuBtn")
	WebElement drawlinetool;

	@FindBy(css = ".leaflet-draw-draw-line")
	WebElement lineannotation;

	@FindBy(css = ".leaflet-control-savebar-save")
	WebElement savebtn;

	@FindBy(css = "#txtMarkupTitle")
	WebElement markupfield;

	@FindBy(css = "#saving-markup-btn")
	WebElement savemarkupbtn;

	@FindBy(id = "ulMarkupList")
	List<WebElement> markuplist1;

	public boolean drawAnnotation(String markupname, String filepath1, WebElement parentlocator, WebElement childlocator) throws FindFailed, InterruptedException, IOException {

		SkySiteUtils.waitTill(10000);
		// method to delete existing markup
		this.markupdeletebegin();

		SkySiteUtils.waitTill(10000);

		parentlocator.click();
		Log.message("Annotation option is clicked");
		SkySiteUtils.waitTill(6000);
		Actions act = new Actions(driver);
		act.moveByOffset(50, 100);
		
		
		act.moveToElement(childlocator).clickAndHold(childlocator).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("Annotation option is selected now");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\Startpoint.png";
		String Screenshot2 = filepath + "\\Endpoint.png";
		String Screenshot3 = filepath + "\\Clickscreenshot.png";
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		Pattern p3 = new Pattern(Screenshot3);
		
		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("Annotation is drawn on the viewer");

		SkySiteUtils.waitTill(10000);

		savebtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(10000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("Save mark upbtn is clicked");
		SkySiteUtils.waitTill(15000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkup.png";
		String savedscreenshot2 = filepath1 + "\\Aftersavingmarkup.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");

		String markuplocator = "//span[text()='%s']";
		SkySiteUtils.waitTill(6000);
		Markuplist.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(10000);
		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(6000);

		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		// showallmarkup.click();
		Log.message("show all markup clicked");
	
		SkySiteUtils.waitTill(10000);
		s.click(p3);
		SkySiteUtils.waitTill(10000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after re-loading");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	
	/**
	 * Method to create arrow annotation in the viewer
	 * Scripted by Trinanjwan
	 */
	
	
	


	public boolean drawAnnotationarrow(String markupname, String filepath1, WebElement parentlocator, WebElement childlocator) throws FindFailed, InterruptedException, IOException {

		SkySiteUtils.waitTill(10000);
		// method to delete existing markup
		this.markupdeletebegin();

		SkySiteUtils.waitTill(10000);

		parentlocator.click();
		Log.message("Annotation option is clicked");
		SkySiteUtils.waitTill(6000);
		Actions act = new Actions(driver);
		act.moveToElement(childlocator).clickAndHold(childlocator).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("Annotation option is selected now");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\Startpoint.png";
		String Screenshot2 = filepath + "\\Endpoint.png";
		String Screenshot3 = filepath + "\\Clickscreenshot.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3 = new Pattern(Screenshot3);

	

		s.drag(p1);

		s.dropAt(p2);
		Log.message("Annotation is drawn on the viewer");

		SkySiteUtils.waitTill(10000);

		savebtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(10000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("Save mark upbtn is clicked");
		SkySiteUtils.waitTill(15000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkup.png";
		String savedscreenshot2 = filepath1 + "\\Aftersavingmarkup.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");

		String markuplocator = "//span[text()='%s']";
		SkySiteUtils.waitTill(6000);
		Markuplist.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(10000);
		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(6000);

		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		// showallmarkup.click();
		Log.message("show all markup clicked");
		
		SkySiteUtils.waitTill(10000);
		s.click(p3);
		SkySiteUtils.waitTill(10000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after re-loading");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}


	/**
	 * Method to delete screenshot | By Trinanjwan
	 * 
	 * @param Screenshotpath
	 * @return
	 */
	public void delteScreenShot(String Screenshotpath) {
		Log.message("Searching screenshot downloaded");
		File file = new File(Screenshotpath);
		File[] Filearray = file.listFiles();
		if (file.isDirectory()) {
			for (File Myfile : Filearray) {
				if (Myfile.getName().contains(".png")) {
					Myfile.delete();
					Log.message("File is deleted successfully");
				}
			}
		}

	}

	/**
	 * Method to delete the markup added | By Trinanjwan
	 * 
	 * @param markup
	 */
	public void deletemarkup(String markup) {

		SkySiteUtils.waitForElement(driver, Markuplist, 20);
		Markuplist.click();
		Log.message("Markup dropdown clicked");
		SkySiteUtils.waitTill(10000);
		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markup)) {
				Markupdelete1.click();
				SkySiteUtils.waitTill(10000);
				Actions act = new Actions(driver);
				act.moveToElement(deleteconfirmation).click(deleteconfirmation).build().perform();

			} else
				Log.message("Mark up is not present in the list");
	}

	/**
	 * Method to create and validate text/call out annotation in the viewer Scripted By
	 * Trinanjwan
	 */

	@FindBy(css = "#textToolMenuBtn")
	WebElement texttoolmenu;

	@FindBy(css = ".leaflet-draw-draw-text")
	WebElement drawtextmenu;

	@FindBy(css = "#editSheetViewerText")
	WebElement textannotationfield;

	@FindBy(css = "#selectFontSize")
	WebElement textfontsize;

	@FindBy(css = "button.btn-sm:nth-child(2)")
	WebElement textokbutton;

	public boolean drawAnnotationtext(String markupname, String filepath1, WebElement parentlocator, WebElement childlocator)
			throws FindFailed, InterruptedException, IOException {

		SkySiteUtils.waitTill(10000);
		// method to delete existing markup
		this.markupdeletebegin();

		SkySiteUtils.waitTill(10000);
		parentlocator.click();
		Log.message("Annotation option is clicked");
		SkySiteUtils.waitTill(6000);
		Actions act = new Actions(driver);
		act.moveToElement(childlocator).clickAndHold(childlocator).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("Annotation option is selected now");
		SkySiteUtils.waitTill(6000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\Startpoint.png";
		String Screenshot2 = filepath + "\\Endpoint.png";
		String Screenshot3 = filepath + "\\Clickscreenshot.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3 = new Pattern(Screenshot3);

		// s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("Annotation is drawn on the viewer");
		SkySiteUtils.waitTill(10000);

		SkySiteUtils.waitForElement(driver, textannotationfield, 10);
		Log.message("The annotation field is visible now");
		textannotationfield.clear();
		SkySiteUtils.waitTill(10000);
		Log.message("The annotation field is clear now");
		SkySiteUtils.waitTill(10000);
		String textdata = PropertyReader.getProperty("Textannotationdata");
		textannotationfield.sendKeys(textdata);
		Log.message("The is entered in the field.");
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, textfontsize, 15);
		Select textfontsize = new Select(driver.findElement(By.cssSelector("#selectFontSize")));
		textfontsize.selectByValue("36");
		SkySiteUtils.waitForElement(driver, textokbutton, 15);
		textokbutton.click();
		Log.message("Clicked on the OK button");
		SkySiteUtils.waitTill(10000);
		savebtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(10000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("Save mark upbtn is clicked");
		SkySiteUtils.waitTill(10000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkup.png";
		String savedscreenshot2 = filepath1 + "\\Aftersavingmarkup.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");

		String markuplocator = "//span[text()='%s']";
		SkySiteUtils.waitTill(6000);
		Markuplist.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(10000);
		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(6000);

		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		// showallmarkup.click();
		Log.message("show all markup clikced");
		
		SkySiteUtils.waitTill(10000);
		s.click(p3);
		SkySiteUtils.waitTill(10000);

		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after re-loading");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	/**
	 * Method to create and validate rectangular highlight annotation in the viewer Scripted By
	 * Trinanjwan
	 */

	@FindBy(css = ".leaflet-draw-draw-rectHighlighter")
	WebElement recthighselection;
	
	
	public boolean drawAnnotationrecthigh(String markupname, String filepath1)
			throws FindFailed, InterruptedException, IOException {

		SkySiteUtils.waitTill(10000);
		// method to delete existing markup
		this.markupdeletebegin();

		SkySiteUtils.waitTill(10000);
		Actions act = new Actions(driver);
		act.moveToElement(recthighselection).click(recthighselection).build().perform();
		SkySiteUtils.waitTill(10000);
		Log.message("Rectangular highlight Annotation option is selected now");
		SkySiteUtils.waitTill(10000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\Startpoint.png";
		String Screenshot2 = filepath + "\\Endpoint.png";
		String Screenshot3 = filepath + "\\Clickscreenshot.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3 = new Pattern(Screenshot3);

		//s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		
		Log.message("Rectangular highlight Annotation is drawn on the viewer");

		SkySiteUtils.waitTill(10000);

		savebtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(10000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("Save mark upbtn is clicked");
		SkySiteUtils.waitTill(15000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkup.png";
		String savedscreenshot2 = filepath1 + "\\Aftersavingmarkup.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");

		String markuplocator = "//span[text()='%s']";
		SkySiteUtils.waitTill(6000);
		Markuplist.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(10000);
		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(6000);

		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		// showallmarkup.click();
		Log.message("show all markup clicked");
		
		SkySiteUtils.waitTill(10000);
		s.click(p3);
		SkySiteUtils.waitTill(10000);

		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after re-loading");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
	
	
	/**
	 * Method to create and validate note annotation in the viewer Scripted By
	 * Trinanjwan
	 * 
	 * Modified By Trinanjwan on 19-Nov-2018
	 */

	
	@FindBy(css = ".leaflet-draw-draw-note")
	WebElement noteselection;

	
	@FindBy(css = "#editNoteText")
	WebElement editnotefield;

	
	

	public boolean drawAnnotationnote(String markupname, String filepath1)throws FindFailed, InterruptedException, IOException {

		SkySiteUtils.waitTill(10000);
		// method to delete existing markup
		this.markupdeletebegin();

		SkySiteUtils.waitTill(10000);
		texttoolmenu.click();
		Log.message("Annotation option is clicked");
		SkySiteUtils.waitTill(6000);
		Actions act = new Actions(driver);
		act.moveToElement(noteselection).clickAndHold(noteselection).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("Annotation option is selected now");
		SkySiteUtils.waitTill(6000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\Startpoint.png";
		String Screenshot2 = filepath + "\\Endpoint.png";
		String Screenshot3 = filepath + "\\Clickscreenshot.png";
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);
		
		Pattern p3 = new Pattern(Screenshot3);

		s.click(p1);
		Log.message("Text Annotation is drawn on the viewer");
		SkySiteUtils.waitTill(10000);

		SkySiteUtils.waitForElement(driver, editnotefield, 10);
		Log.message("The field is visible now");
		editnotefield.clear();
		SkySiteUtils.waitTill(10000);
		Log.message("The field is clear now");
		SkySiteUtils.waitTill(10000);
		String textdata = PropertyReader.getProperty("Textannotationdata");
		editnotefield.sendKeys(textdata);
		Log.message("The text is entered in the field.");
		SkySiteUtils.waitForElement(driver, textokbutton, 15);
		textokbutton.click();
		Log.message("Clicked on the OK button");
		SkySiteUtils.waitTill(10000);
		savebtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(10000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("Save mark upbtn is clicked");
		SkySiteUtils.waitTill(10000);
		s.click(p3);
		SkySiteUtils.waitTill(10000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkup.png";
		String savedscreenshot2 = filepath1 + "\\Aftersavingmarkup.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");

		String markuplocator = "//span[text()='%s']";
		SkySiteUtils.waitTill(6000);
		Markuplist.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(10000);
		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(6000);

		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		// showallmarkup.click();
		Log.message("show all markup clikced");
		
		SkySiteUtils.waitTill(10000);
		s.click(p3);
		SkySiteUtils.waitTill(10000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after re-loading");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}
	
	

	/**
	 * Method to validate deletion of mark up | Scripted By Trinanjwan
	 */
	
	public boolean deletemarkupvalidation(String markup, String filepath1) throws FindFailed, InterruptedException, IOException {

		
		
		SkySiteUtils.waitForElement(driver, Markuplist, 40);
		Markuplist.click();
		Log.message("Markup dropdown clicked");
		SkySiteUtils.waitTill(10000);
		Markupdelete1.click();
		SkySiteUtils.waitTill(10000);
		Actions act = new Actions(driver);
		act.moveToElement(deleteconfirmation).click(deleteconfirmation).build().perform();
		Log.message("Clicked on delete confirmation");
		SkySiteUtils.waitTill(10000);
	
		if(!Markuplist.isDisplayed())
			return true;
		else 
			return false;


}
	
	
	/**
	 * Method for verification hyperlink_Same folder, different file | Scripted By Trinanjwan
	 */
	
	
	
	@FindBy(css = "#linkToolMenuBtn")
	WebElement hyperlinkparent;

	
	@FindBy(css = ".leaflet-draw-draw-linkRect")
	WebElement hyperlinkchild;

	
	@FindBy(css = ".panel > li:nth-child(2) > div:nth-child(2) > a:nth-child(1) > div:nth-child(3)")
	WebElement hyperlinkfolderlink;

	@FindBy(xpath = "//a[text()='A-TH-0070.pdf']")
	WebElement hyperlinkdestinationfilelink;
	
	@FindBy(css = ".done")
	WebElement Okbuttonhyperlinkpopover;
	
	
	@FindBy(css = ".leaflet-control-nav-home")
	WebElement Homebutton;
	
	@FindBy(css = "#linkImgDiv")
	WebElement Imagethumbnailhyperlink;
	
	@FindBy(xpath = "//span[@data-docname=\"A-TH-0070.pdf\"]")
	WebElement Destinationfileheading;
	
	@FindBy(xpath = "//span[@data-docname='A-TH-0070.pdf']")
	WebElement destinationfilelink;
	
	@FindBy(css = ".revision-view-event")
	WebElement sourcefilelink;
	
	

	public boolean hyperlink_samefolderdiffifle(String markupname, String filepath1)throws FindFailed, InterruptedException, IOException {

		SkySiteUtils.waitTill(10000);
		// method to delete existing markup
		this.markupdeletebegin();

		SkySiteUtils.waitTill(10000);
		hyperlinkparent.click();
		Log.message("Annotation option is clicked");
		SkySiteUtils.waitTill(6000);
		Actions act = new Actions(driver);
		act.moveToElement(hyperlinkchild).clickAndHold(hyperlinkchild).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("The hyperlink annotation is selected now");
		
		
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\Startpoint.png";
		String Screenshot2 = filepath + "\\Endpoint.png";
		String Screenshot3 = filepath + "\\Clickscreenshot.png";
		String Screenshot4 = filepath + "\\Hyperlinkclick.png";
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		Pattern p3 = new Pattern(Screenshot3);

		Pattern p4 = new Pattern(Screenshot4);

		s.drag(p1);

		s.dropAt(p2);
		
		SkySiteUtils.waitTill(7000);
		
		hyperlinkfolderlink.click();
		Log.message("Clicked on hyperlink folder link move inside");
		
		SkySiteUtils.waitForElement(driver, hyperlinkdestinationfilelink, 30);
		Log.message("The file link is available");
		hyperlinkdestinationfilelink.click();
		Log.message("The destination file is selected now");
		SkySiteUtils.waitForElement(driver, Okbuttonhyperlinkpopover, 30);
		Okbuttonhyperlinkpopover.click();
		Log.message("Clicked on OK button");	
		Log.message("Annotation is drawn on the viewer");
		SkySiteUtils.waitTill(10000);
		savebtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(10000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("Save mark up btn is clicked");
	//	SkySiteUtils.waitTill(14000);
	//	savebtn.click();
	//	Log.message("Save button is clicked again to dismiss the dropdown");
		SkySiteUtils.waitTill(10000);
		Homebutton.click();
		Log.message("Clicked on Home button to place the file properly in the viewer");
		SkySiteUtils.waitTill(10000);
		s.click(p4);
		SkySiteUtils.waitForElement(driver, Imagethumbnailhyperlink, 30);
		Log.message("The thumbnail in the hyperlink pop over is displayed now");
		Imagethumbnailhyperlink.click();
		Log.message("Clicked on hyperlink thumbnail");
		SkySiteUtils.waitTill(25000);
		Actions ac=new Actions(driver);
		
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", sourcefilelink);
		Log.message("Source file link is clicked");
		SkySiteUtils.waitTill(25000);
		String actualdest=destinationfilelink.getText();
		Log.message("Actual is:"+actualdest);
		String expecteddest="(R-1) A-TH-0070.pdf";
		Log.message("Expected is:"+expecteddest);
		Log.message("Returned back to the source file");
		if(actualdest.contentEquals(expecteddest))
			return true;
		else 
			return false;
		

	}
	
	
	/**
	 * Method for verification hyperlink_different folder, different file | Scripted By Trinanjwan
	 */
	
	
	
	@FindBy(xpath = "//h4[text()='Folder 2']")
	WebElement hyperlinkfolderlink1;

	@FindBy(xpath = "//a[@title='A1.1.1.pdf']")
	WebElement hyperlinkdestinationfilelink1;
	
	@FindBy(xpath = "//span[@data-docname='A1.1.1.pdf']")
	WebElement destinationfilelink1;
	
	
	
	
	
	public boolean hyperlink_difffolderdiffifle(String markupname, String filepath1)throws FindFailed, InterruptedException, IOException {

		SkySiteUtils.waitTill(10000);
		// method to delete existing markup
		this.markupdeletebegin();

		SkySiteUtils.waitTill(10000);
		hyperlinkparent.click();
		Log.message("Annotation option is clicked");
		SkySiteUtils.waitTill(6000);
		Actions act = new Actions(driver);
		act.moveToElement(hyperlinkchild).clickAndHold(hyperlinkchild).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("The hyperlink annotation is selected now");
		
		
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\Startpoint.png";
		String Screenshot2 = filepath + "\\Endpoint.png";
		String Screenshot3 = filepath + "\\Clickscreenshot.png";
		String Screenshot4 = filepath + "\\Hyperlinkclick.png";
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		Pattern p3 = new Pattern(Screenshot3);

		Pattern p4 = new Pattern(Screenshot4);

		s.drag(p1);

		s.dropAt(p2);
		
		SkySiteUtils.waitTill(7000);
		
		hyperlinkfolderlink1.click();
		Log.message("Clicked on hyperlink folder link move inside");
		
		SkySiteUtils.waitForElement(driver, hyperlinkdestinationfilelink1, 30);
		Log.message("The file link is available");
		hyperlinkdestinationfilelink1.click();
		Log.message("The destination file is selected now");
		SkySiteUtils.waitForElement(driver, Okbuttonhyperlinkpopover, 30);
		Okbuttonhyperlinkpopover.click();
		Log.message("Clicked on OK button");	
		Log.message("Annotation is drawn on the viewer");
		SkySiteUtils.waitTill(10000);
		savebtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(10000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("Save mark up btn is clicked");
	//	SkySiteUtils.waitTill(14000);
	//	savebtn.click();
	//	Log.message("Save button is clicked again to dismiss the dropdown");
		SkySiteUtils.waitTill(10000);
		Homebutton.click();
		Log.message("Clicked on Home button to place the file properly in the viewer");
		SkySiteUtils.waitTill(10000);
		s.click(p4);
		SkySiteUtils.waitForElement(driver, Imagethumbnailhyperlink, 30);
		Log.message("The thumbnail in the hyperlink pop over is displayed now");
		Imagethumbnailhyperlink.click();
		Log.message("Clicked on hyperlink thumbnail");
		SkySiteUtils.waitTill(25000);
		Actions ac=new Actions(driver);
		
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", sourcefilelink);
		Log.message("Source file link is clicked");
		SkySiteUtils.waitTill(25000);
		String actualdest=destinationfilelink1.getText();
		Log.message("Actual is:"+actualdest);
		String expecteddest="(R-1) A1.1.1.pdf";
		Log.message("Expected is:"+expecteddest);
		Log.message("Returned back to the source file");
		if(actualdest.contentEquals(expecteddest))
			return true;
		else 
			return false;
		

	}
	
	
	

	/**
	 * Method for verification of hyperlink different folder (Parent -> Child) & Different file | Scripted By Trinanjwan
	 */

	
	@FindBy(xpath = "//h4[text()='Parent']")
	WebElement hyperlinkfolderparent1;
	
	@FindBy(xpath = "//h4[text()='Child']")
	WebElement hyperlinkfolderchild1;

	@FindBy(xpath = "//a[@title='2.PDF']")
	WebElement hyperlinkdestinationfilelink2;
	
	@FindBy(xpath = "//span[@data-docname='2.PDF']")
	WebElement destinationfilelink2;
	
	
	
	public boolean hyperlink_difffolderdiffifleparenttochild(String markupname, String filepath1)throws FindFailed, InterruptedException, IOException {

		SkySiteUtils.waitTill(10000);
		// method to delete existing markup
		this.markupdeletebegin();

		SkySiteUtils.waitTill(10000);
		hyperlinkparent.click();
		Log.message("Annotation option is clicked");
		SkySiteUtils.waitTill(6000);
		Actions act = new Actions(driver);
		act.moveToElement(hyperlinkchild).clickAndHold(hyperlinkchild).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("The hyperlink annotation is selected now");
		
		
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\Startpoint.png";
		String Screenshot2 = filepath + "\\Endpoint.png";
		String Screenshot3 = filepath + "\\Clickscreenshot.png";
		String Screenshot4 = filepath + "\\Hyperlinkclick.png";
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		Pattern p3 = new Pattern(Screenshot3);

		Pattern p4 = new Pattern(Screenshot4);

		s.drag(p1);

		s.dropAt(p2);
		
		SkySiteUtils.waitTill(7000);
		
		hyperlinkfolderparent1.click();
		Log.message("Clicked on parent folder link to move inside");
		
		SkySiteUtils.waitForElement(driver, hyperlinkfolderchild1, 30);
		Log.message("The child folder is visible now");
		
		hyperlinkfolderchild1.click();
		Log.message("Clicked on child folder link to move inside");
		
		
		SkySiteUtils.waitForElement(driver, hyperlinkdestinationfilelink2, 30);
		Log.message("The file link is available");
		hyperlinkdestinationfilelink2.click();
		Log.message("The destination file is selected now");
		SkySiteUtils.waitForElement(driver, Okbuttonhyperlinkpopover, 30);
		Okbuttonhyperlinkpopover.click();
		Log.message("Clicked on OK button");	
		Log.message("Annotation is drawn on the viewer");
		SkySiteUtils.waitTill(10000);
		savebtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(10000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("Save mark up btn is clicked");
	//	SkySiteUtils.waitTill(14000);
	//	savebtn.click();
	//	Log.message("Save button is clicked again to dismiss the dropdown");
		SkySiteUtils.waitTill(10000);
		Homebutton.click();
		Log.message("Clicked on Home button to place the file properly in the viewer");
		SkySiteUtils.waitTill(10000);
		s.click(p4);
		SkySiteUtils.waitForElement(driver, Imagethumbnailhyperlink, 30);
		Log.message("The thumbnail in the hyperlink pop over is displayed now");
		Imagethumbnailhyperlink.click();
		Log.message("Clicked on hyperlink thumbnail");
		SkySiteUtils.waitTill(25000);
		Actions ac=new Actions(driver);
		
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", sourcefilelink);
		Log.message("Source file link is clicked");
		SkySiteUtils.waitTill(25000);
		String actualdest=destinationfilelink2.getText();
		Log.message("Actual is:"+actualdest);
		String expecteddest="(R-1) 2.PDF";
		Log.message("Expected is:"+expecteddest);
		Log.message("Returned back to the source file");
		if(actualdest.contentEquals(expecteddest))
			return true;
		else 
			return false;

	}

	
	/**
	 * Method for verification of hyperlink different folder (Child -> Parent) & Different file | Scripted By Trinanjwan
	 */
	
	@FindBy(xpath = "//a[@title='1.PDF']")
	WebElement hyperlinkdestinationfilelink3;
	
	@FindBy(xpath = "//span[@data-docname='1.PDF']")
	WebElement destinationfilelink3;
	
	public boolean hyperlink_difffolderdiffiflechildtoparent(String markupname, String filepath1)throws FindFailed, InterruptedException, IOException {

		SkySiteUtils.waitTill(10000);
		// method to delete existing markup
		this.markupdeletebegin();

		SkySiteUtils.waitTill(10000);
		hyperlinkparent.click();
		Log.message("Annotation option is clicked");
		SkySiteUtils.waitTill(6000);
		Actions act = new Actions(driver);
		act.moveToElement(hyperlinkchild).clickAndHold(hyperlinkchild).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("The hyperlink annotation is selected now");
		
		
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\Startpoint.png";
		String Screenshot2 = filepath + "\\Endpoint.png";
		String Screenshot3 = filepath + "\\Clickscreenshot.png";
		String Screenshot4 = filepath + "\\Hyperlinkclick.png";
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		Pattern p3 = new Pattern(Screenshot3);

		Pattern p4 = new Pattern(Screenshot4);

		s.drag(p1);

		s.dropAt(p2);
		
		SkySiteUtils.waitTill(7000);
		
		hyperlinkfolderparent1.click();
		Log.message("Clicked on parent folder link to move inside");
		
		
		SkySiteUtils.waitForElement(driver, hyperlinkdestinationfilelink3, 30);
		Log.message("The file link is available");
		hyperlinkdestinationfilelink3.click();
		Log.message("The destination file is selected now");
		SkySiteUtils.waitForElement(driver, Okbuttonhyperlinkpopover, 30);
		Okbuttonhyperlinkpopover.click();
		Log.message("Clicked on OK button");	
		Log.message("Annotation is drawn on the viewer");
		SkySiteUtils.waitTill(10000);
		savebtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(10000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("Save mark up btn is clicked");
	//	SkySiteUtils.waitTill(14000);
	//	savebtn.click();
	//	Log.message("Save button is clicked again to dismiss the dropdown");
		SkySiteUtils.waitTill(10000);
		Homebutton.click();
		Log.message("Clicked on Home button to place the file properly in the viewer");
		SkySiteUtils.waitTill(10000);
		s.click(p4);
		SkySiteUtils.waitForElement(driver, Imagethumbnailhyperlink, 30);
		Log.message("The thumbnail in the hyperlink pop over is displayed now");
		Imagethumbnailhyperlink.click();
		Log.message("Clicked on hyperlink thumbnail");
		SkySiteUtils.waitTill(25000);
		Actions ac=new Actions(driver);
		
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", sourcefilelink);
		Log.message("Source file link is clicked");
		SkySiteUtils.waitTill(25000);
		String actualdest=destinationfilelink3.getText();
		Log.message("Actual is:"+actualdest);
		String expecteddest="(R-1) 1.PDF";
		Log.message("Expected is:"+expecteddest);
		Log.message("Returned back to the source file");
		if(actualdest.contentEquals(expecteddest))
			return true;
		else 
			return false;

	}
	
	@FindBy(css = ".leaflet-control-zoom-in")
	WebElement zoominplusicon;
	
	
	
	
	
	
	/**
	 * Method to create line, freehand, rectangle annotation in the viewer after zoom in the document
	 * By Trinanjwan
	 */


	public boolean drawAnnotationzoomin(String markupname, String filepath1, WebElement parentlocator, WebElement childlocator) throws FindFailed, InterruptedException, IOException {

		SkySiteUtils.waitTill(10000);
		// method to delete existing markup
		this.markupdeletebegin();

		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, zoominplusicon, 40);
		Log.message("Zoom in button is displayed now");
		zoominplusicon.click();
		Log.message("Zoom in button is clicked first time");
		SkySiteUtils.waitTill(7000);
		zoominplusicon.click();
		Log.message("Zoom in button is clicked second time");
		SkySiteUtils.waitTill(7000);
		
		parentlocator.click();
		Log.message("Annotation option is clicked");
		SkySiteUtils.waitTill(6000);
		Actions act = new Actions(driver);
		act.moveByOffset(50, 100);
		
		
		act.moveToElement(childlocator).clickAndHold(childlocator).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("Annotation option is selected now");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\Startpointzoom.png";
		String Screenshot2 = filepath + "\\Endpointzoom.png";
		String Screenshot3 = filepath + "\\Clickscreenshotzoom.png";
		
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);

		Pattern p3 = new Pattern(Screenshot3);
		
		s.click(p1);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("Annotation is drawn on the viewer");

		SkySiteUtils.waitTill(10000);

		savebtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(10000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("Save mark upbtn is clicked");
		SkySiteUtils.waitTill(15000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkup.png";
		String savedscreenshot2 = filepath1 + "\\Aftersavingmarkup.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");

		String markuplocator = "//span[text()='%s']";
		SkySiteUtils.waitTill(6000);
		Markuplist.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(10000);
		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(6000);

		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		// showallmarkup.click();
		Log.message("show all markup clicked");
	
		SkySiteUtils.waitTill(10000);
		s.click(p3);
		SkySiteUtils.waitTill(10000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after re-loading");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}

	
	
	/**
	 * Method to create arrow annotation in the viewer after zoom in the document
	 * Scripted by Trinanjwan
	 */


	public boolean drawAnnotationarrowzoomin(String markupname, String filepath1, WebElement parentlocator, WebElement childlocator) throws FindFailed, InterruptedException, IOException {

		SkySiteUtils.waitTill(10000);
		// method to delete existing markup
		this.markupdeletebegin();

		
		SkySiteUtils.waitTill(10000);
		SkySiteUtils.waitForElement(driver, zoominplusicon, 40);
		Log.message("Zoom in button is displayed now");
		zoominplusicon.click();
		Log.message("Zoom in button is clicked first time");
		SkySiteUtils.waitTill(7000);
		zoominplusicon.click();
		Log.message("Zoom in button is clicked second time");
		SkySiteUtils.waitTill(7000);

		parentlocator.click();
		Log.message("Annotation option is clicked");
		SkySiteUtils.waitTill(6000);
		Actions act = new Actions(driver);
		act.moveToElement(childlocator).clickAndHold(childlocator).build().perform();
		SkySiteUtils.waitTill(2000);
		act.click().build().perform();
		Log.message("Annotation option is selected now");
		SkySiteUtils.waitTill(2000);
		File fis = new File(PropertyReader.getProperty("Screenshotpath"));
		String filepath = fis.getAbsolutePath().toString();
		String Screenshot1 = filepath + "\\Startpointzoom.png";
		String Screenshot2 = filepath + "\\Endpointzoom.png";
		String Screenshot3 = filepath + "\\Clickscreenshotzoom.png";
		Screen s = new Screen();

		Pattern p1 = new Pattern(Screenshot1);

		Pattern p2 = new Pattern(Screenshot2);
		
		Pattern p3 = new Pattern(Screenshot3);

		s.drag(p1);

		s.dropAt(p2);
		Log.message("Annotation is drawn on the viewer");

		SkySiteUtils.waitTill(10000);

		savebtn.click();
		Log.message("Save button is clicked");
		SkySiteUtils.waitTill(10000);
		markupfield.sendKeys(markupname);
		Log.message("Enter markup name is " + markupname);
		SkySiteUtils.waitForElement(driver, savemarkupbtn, 40);
		savemarkupbtn.click();
		Log.message("Save mark upbtn is clicked");
		SkySiteUtils.waitTill(15000);
		// File fis1=new File(PropertyReader.getProperty("SaveScreenshot"));
		// String filepath1=fis1.getAbsolutePath().toString();
		String savedscreenshot1 = filepath1 + "\\Beforesavingmarkup.png";
		String savedscreenshot2 = filepath1 + "\\Aftersavingmarkup.png";
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler, new File(savedscreenshot1));

		Log.message("Screenshot taken of saved markup");

		String markuplocator = "//span[text()='%s']";
		SkySiteUtils.waitTill(6000);
		Markuplist.click();
		Log.message("markup dropdown clicked");
		SkySiteUtils.waitTill(10000);
		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		Log.message("save markup clicked and image drawn disappears");
		SkySiteUtils.waitTill(6000);

		for (WebElement markupitem : markuplist1)
			if (markupitem.getText().contains(markupname))
				driver.findElement(By.xpath(String.format(markuplocator, markupname))).click();

		// showallmarkup.click();
		Log.message("show all markup clicked");
		
		SkySiteUtils.waitTill(10000);
		s.click(p3);
		SkySiteUtils.waitTill(10000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(srcFiler1, new File(savedscreenshot2));
		Log.message("Screenshot taken after re-loading");

		BufferedImage imgA = ImageIO.read(new File(savedscreenshot1));

		// Thread.sleep(2000);

		BufferedImage imgB = ImageIO.read(new File(savedscreenshot2));

		// Thread.sleep(2000);

		Log.message("Checking the height and width of both the images are same?");

		if (imgA.getWidth() != imgB.getWidth() && imgA.getHeight() != imgB.getHeight()) {
			Log.message("image height width are different");
			return false;
		}

		int width = imgA.getWidth();
		int height = imgA.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

					Log.message("Image are different");
					return false;
				}
			}

		}

		return true;
	}


	
}