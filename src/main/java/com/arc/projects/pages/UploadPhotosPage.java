package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import com.arc.projects.pages.ProjectGalleryPage;

public class UploadPhotosPage extends LoadableComponent<UploadPhotosPage> {
	
	WebDriver driver;
	private boolean isPageLoaded;
	
	/** Identifying web elements using FindBy annotation*/

	@FindBy(id = "btnSelectFiles")
	WebElement buttonChooseFiles;
	
	@FindBy(xpath = "//*[@id=\"btnFileUpload\"]")
	WebElement buttonPhotoUpload;
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, buttonChooseFiles, 20);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
	}
		
		
		/**
		 * Declaring constructor for initializing web elements using PageFactory
		 * class.
		 * 
		 * @param driver
		 * @return
		 */
		public UploadPhotosPage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(this.driver, this);
		}
		

		/** 
		 * Method for Uploading Photos
		 * Scripted By: Ranadeep
		 * @return 
		 */
	public PhotoAttributesPage UploadPhoto(String filepath, String tempfilepath) throws AWTException, IOException
	{
		
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, buttonChooseFiles, 30);	  
		buttonChooseFiles.click();
		Log.message("Clicked on Choose Files button"); 
		SkySiteUtils.waitTill(3000);
		
		//Writing File names into a text file for using in AutoIT Script		                  
        BufferedWriter output;                  
        randomFileName rn=new randomFileName();
        String tmpFileName=tempfilepath+rn.nextFileName()+".txt";                
        output = new BufferedWriter(new FileWriter(tmpFileName,true));    

                     
        String expFilename=null;
        File[] files = new File(filepath).listFiles();
                     
        for(File file : files)
        {
              if(file.isFile()) 
              {
                  expFilename=file.getName();//Getting File Names into a variable
                  Log.message("Expected File name is:"+expFilename);  
                  output.append('"' + expFilename + '"');
                  output.append(" ");
                  SkySiteUtils.waitTill(5000);     
  
              }      
        }      
        
        output.flush();
        output.close();     

//        String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
//		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
//		Log.message("AutoIT Script Executed!!");
//        
        
        String AutoItexe_Path = PropertyReader.getProperty("AutoITPath");
        //Executing .exe autoIt file            
        Runtime.getRuntime().exec(AutoItexe_Path+" "+ filepath+" "+tmpFileName );                  
       
        Log.message("AutoIT Script Executed!!");                           
        SkySiteUtils.waitTill(30000);
                     
        try
        {
              File file = new File(tmpFileName);
              if(file.delete())
              {
                     Log.message(file.getName() + " is deleted!");
              }      
              else
              {
                     Log.message("Delete operation is failed.");
              }
        }
        catch(Exception e)
        {                    
              Log.message("Exception occured!!!"+e);
        }                    
        SkySiteUtils.waitTill(8000);
        
        buttonPhotoUpload.click();
		Log.message("Clicked on Upload button"); 
		SkySiteUtils.waitTill(5000);
		
		return new PhotoAttributesPage(driver).get();

}
	


}

