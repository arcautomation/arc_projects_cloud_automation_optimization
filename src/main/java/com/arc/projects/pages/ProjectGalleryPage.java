package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import com.arc.projects.pages.ProjectGalleryPage;
import com.arc.projects.pages.ProjectActivitiesPage;

public class ProjectGalleryPage extends LoadableComponent<ProjectGalleryPage> {
	
	WebDriver driver;
	private boolean isPageLoaded;


	/** Identifying web elements using FindBy annotation*/

	@FindBy(css = "button.btn:nth-child(5)")
	WebElement buttonUploadPhotos;
	
	@FindBy(css = "#liActivityMain > a:nth-child(1) > span:nth-child(3)")
	WebElement btnProjectActivity;
	
	@FindBy(css = "#liActivity > a:nth-child(1) > span:nth-child(3)")
	WebElement btnDocumentActivity;
	
	@FindBy(css = ".myDropdown > a:nth-child(1) > i:nth-child(1)")
	WebElement btnPhotoOptions;
	
	@FindBy(css = ".myDropdown > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)")
	WebElement btnPhotoDelete;
	
	@FindBy(xpath = "//*[@id=\"button-1\"]")
	WebElement btnDeletePhotoYes;

	@FindBy(xpath="//*[@id='aUploadPhotosNavMenu']")
	WebElement btnUploadPhotos;
	
	@FindBy(xpath="//*[@id='switch-view']")
	WebElement btnSwitchView;
	
	@FindBy(xpath="//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/a[2]")
	WebElement btnAlbumSortBy;
	
	@FindBy(xpath="//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/ul/li[1]/a")
	WebElement btnSortByAlbumName;
	
	@FindBy(xpath="//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/ul/li[2]/a")
	WebElement btnSortByAlbumCreateDate;
	
	@FindBy(xpath="//*[@id='aSortOrder']")
	WebElement btnAlbumSortOrder;
	
	@FindBy (xpath="//*[@id='mainPageContainer']/div[1]/ul[1]/li[1]/a")
	WebElement btnProfile;
	
	@FindBy(xpath="//a[contains(text(), 'Log out')]")
	WebElement btnLogout;
	
	@FindBy(css="#button-1")
	WebElement btnYes;
	
	@FindBy(css="#btnLogin")
	WebElement btnLogin;
	
	@FindBy(xpath="//*[text()='A_Album']")
	WebElement albumA_Album;
	
	@FindBy(xpath="//*[@id='ulBreadCrumb']/li[5]")
	WebElement albumBreadcrumb;
	
	@FindBy(xpath="//*[@id='aUploadPhotosNavMenu']")
	WebElement btnUploadPhoto;
	
	@FindBy(xpath="//*[@id='switch-view']")
	WebElement btnSwitchGridListView;
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, buttonUploadPhotos, 20);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 * @return
	 */
	public ProjectGalleryPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}



	public UploadPhotosPage uploadPhotosClick() {
		
		//SkySiteUtils.waitForElement(driver, buttonUploadPhotos, 30);
		 SkySiteUtils.waitTill(3000);  
		buttonUploadPhotos.click();
		Log.message("Clicked on Upload Photos button");      
		
		return new UploadPhotosPage(driver).get();
	}

	public boolean deletePhoto() {
		
		SkySiteUtils.waitTill(3000);
		btnPhotoOptions.click();
		Log.message("Clicked on Photos options button");
		SkySiteUtils.waitTill(3000);
		btnPhotoDelete.click();
		Log.message("Clicked on Delete option");
		SkySiteUtils.waitTill(3000);
		btnDeletePhotoYes.click();
		Log.message("Clicked on YES option in the Delete Photo popover");
		SkySiteUtils.waitTill(5000);
		
		if (buttonUploadPhotos.isDisplayed()) {
			Log.message("Photo is deleted");
			return true;
		} 
	 
	 else{
			Log.message("Photo delete failed");
			return false;
		}
		
	}
	
	
	public ProjectActivitiesPage documentActivity() {
		
		//SkySiteUtils.waitForElement(driver, buttonUploadPhotos, 30);
		 SkySiteUtils.waitTill(3000);  
		 btnProjectActivity.click();
		 Log.message("Clicked on Project Activity button");
		 SkySiteUtils.waitTill(3000);
		 btnDocumentActivity.click();
		 Log.message("Clicked on Document Activity button");      
		
		 return new ProjectActivitiesPage(driver).get();
	}
	
	
	/** 
	 * Method for validating descending order sorting by Album Name
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingByAlbumName() 
	{
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortBy, 30);
		Log.message("File Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/a[2]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Album Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByAlbumName, 30);
		Log.message("Sort by File Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/ul/li[1]/a"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Album Name");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
		String sortOrder=btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Album sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedAlbumList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals(""))
			{
				obtainedAlbumList.add(we.getText()); 
		     
			}
		}		
		Log.message("Obtained Album count before sorting:- " +obtainedAlbumList.size());		 
		Log.message("Obtained Albums are:- " +obtainedAlbumList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Album sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnAlbumSortOrder.click();
			Log.message("Clicked on order by button");
			SkySiteUtils.waitTill(8000);
			
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		
	    
		ArrayList<String> sortedAlbumList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals(""))
			{
				sortedAlbumList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Albums count after sorting:- " +sortedAlbumList.size());		 
		Log.message("Obtained Albums are:- " +sortedAlbumList);
		
		if(obtainedAlbumList.get(0).contains(sortedAlbumList.get(sortedAlbumList.size()-1)) && obtainedAlbumList.get(obtainedAlbumList.size()-1).contains(sortedAlbumList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
		
	}


	
	/** 
	 * Method for restoring default sort settings for File Level
	 * Scripted By: Ranadeep
	 * @return 
	 */
	 public void resetToDefaultAlbumSortSettings() 
	{
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortBy, 30);
		Log.message("File Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/a[2]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on File Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByAlbumName, 30);
		Log.message("Sort by File Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/ul/li[1]/a"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by File Name");
		SkySiteUtils.waitTill(8000);
		
		
		String sortOrder=btnAlbumSortOrder.getAttribute("data-original-title");
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		
		if(sortOrder.contains(expectedSortOrder))
		{
			
			WebElement element2 = driver.findElement(By.xpath("//*[@id='aSortOrder']"));
			JavascriptExecutor executor2 = (JavascriptExecutor)driver;
			executor2.executeScript("arguments[0].click();", element2);
			Log.message("Clicked on order by button");
			SkySiteUtils.waitTill(8000);
			
		}
			
		Log.message("Default sorting settings restored");
		
	}

	
	 /** 
	 * Method for validating ascending order sorting by Album Name
	 * Scripted By: Ranadeep
	 * @return 
	 */
	 public boolean ascendingOrderSortingByAlbumName() 
	 {
		 
			SkySiteUtils.waitForElement(driver, btnAlbumSortBy, 30);
			Log.message("File Sort by button is displayed");
			WebElement element = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/a[2]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			Log.message("Clicked on Album Sort by button");
			
			SkySiteUtils.waitForElement(driver, btnSortByAlbumName, 30);
			Log.message("Sort by File Name button is displayed");
			WebElement element1 = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/ul/li[1]/a"));
			JavascriptExecutor executor1 = (JavascriptExecutor)driver;
			executor1.executeScript("arguments[0].click();", element1);
			Log.message("Clicked on Sort by Album Name");
			SkySiteUtils.waitTill(5000);
		 
			SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
			String sortOrder=btnAlbumSortOrder.getAttribute("data-original-title");
		    Log.message("Album sort order is:-" +sortOrder);
		 
		    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
			Log.message("Expected Folder sort order is:- " +expectedSortOrder);
			if(sortOrder.contains(expectedSortOrder))
			{
				
				btnAlbumSortOrder.click();
				SkySiteUtils.waitTill(5000);
				
			}
		    
			ArrayList<String> obtainedAlbumList = new ArrayList<>();
			java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
			
			for(WebElement we:elementList)
			{
				if(!we.getText().equals(""))
				{
					obtainedAlbumList.add(we.getText()); 
			     
				}
			}		
			Log.message("Obtained Album count before sorting:- " +obtainedAlbumList.size());		 
			Log.message("Obtained Albums are:- " +obtainedAlbumList);
			
			SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
			String sortOrder1=btnAlbumSortOrder.getAttribute("data-original-title");
		    Log.message("Present Album sort order is:-" +sortOrder1);
			
			String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
			Log.message("Expected Album sort order is:- " +expectedSortOrder1);
			
			if(sortOrder1.contains(expectedSortOrder1))
			{
				btnAlbumSortOrder.click();
				Log.message("Clicked on order by button");
			}
			else
			{
				Log.message("Sort order is not descending");
			}
			SkySiteUtils.waitTill(8000);
		    
			ArrayList<String> sortedAlbumList = new ArrayList<>();
			java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
			
			for(WebElement we:elementList1)
			{
				if(!we.getText().equals(""))
				{
					sortedAlbumList.add(we.getText()); 
			     
				}
			}
			Log.message("Obtained Albums count after sorting:- " +sortedAlbumList.size());		 
			Log.message("Obtained Albums are:- " +sortedAlbumList);
			
			if(obtainedAlbumList.get(0).contains(sortedAlbumList.get(sortedAlbumList.size()-1)) && obtainedAlbumList.get(obtainedAlbumList.size()-1).contains(sortedAlbumList.get(0)))
		    {	
				return true;
				
		    }
		    else
		    {
		    	return false;
		    	
		    }
	 
	 }

	
	 /** 
	 * Method for validating descending order sorting by Album Create Date
	 * Scripted By: Ranadeep
 	 * @return 
	 */
	public boolean descendingOrderSortingByAlbumCreateDate() 
	{
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortBy, 30);
		Log.message("File Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/a[2]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Album Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByAlbumCreateDate, 30);
		Log.message("Sort by File Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/ul/li[2]/a"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Album Create Date");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
		String sortOrder=btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Album sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedAlbumList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals(""))
			{
				obtainedAlbumList.add(we.getText()); 
		     
			}
		}		
		Log.message("Obtained Album count before sorting:- " +obtainedAlbumList.size());		 
		Log.message("Obtained Albums are:- " +obtainedAlbumList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected album sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnAlbumSortOrder.click();
			Log.message("Clicked on order by button");
			SkySiteUtils.waitTill(8000);
			
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		
	    
		ArrayList<String> sortedAlbumList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals(""))
			{
				sortedAlbumList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Albums count after sorting:- " +sortedAlbumList.size());		 
		Log.message("Obtained Albums are:- " +sortedAlbumList);
		
		if(obtainedAlbumList.get(0).contains(sortedAlbumList.get(sortedAlbumList.size()-1)) && obtainedAlbumList.get(obtainedAlbumList.size()-1).contains(sortedAlbumList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
	}

	
	/** 
	 * Method for validating ascending order sorting by Album Create Date
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingByAlbumCreateDate() 
	{
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortBy, 30);
		Log.message("File Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/a[2]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Album Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByAlbumCreateDate, 30);
		Log.message("Sort by File Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/ul/li[2]/a"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Album Create Date");
		SkySiteUtils.waitTill(5000);
	 
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
		String sortOrder=btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Album sort order is:-" +sortOrder);
	 
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		if(sortOrder.contains(expectedSortOrder))
		{
			
			btnAlbumSortOrder.click();
			SkySiteUtils.waitTill(5000);
			
		}
	    
		ArrayList<String> obtainedAlbumList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals(""))
			{
				obtainedAlbumList.add(we.getText()); 
		     
			}
		}		
		Log.message("Obtained Album count before sorting:- " +obtainedAlbumList.size());		 
		Log.message("Obtained Albums are:- " +obtainedAlbumList);
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
		String sortOrder1=btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Present Album sort order is:-" +sortOrder1);
		
		String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Album sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			btnAlbumSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
	    
		ArrayList<String> sortedAlbumList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals(""))
			{
				sortedAlbumList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Albums count after sorting:- " +sortedAlbumList.size());		 
		Log.message("Obtained Albums are:- " +sortedAlbumList);
		
		if(obtainedAlbumList.get(0).contains(sortedAlbumList.get(sortedAlbumList.size()-1)) && obtainedAlbumList.get(obtainedAlbumList.size()-1).contains(sortedAlbumList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
		
	}

	/** 
	 * Method to change sorting order for Album and save data to be validated later in Gallery level
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public ArrayList<String> sortOrderSave() 
	{
	
		SkySiteUtils.waitForElement(driver, btnAlbumSortBy, 30);
		Log.message("File Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/a[2]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Album Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByAlbumName, 30);
		Log.message("Sort by File Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/ul/li[1]/a"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Album Name");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
		btnAlbumSortOrder.click();
		Log.message("Clicked on sort order button");
		SkySiteUtils.waitTill(8000);
		String sortOrder=btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Album sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedAlbumList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals(""))
			{
				obtainedAlbumList.add(we.getText()); 
		     
			}
		}		
		Log.message("Obtained Album count after sorting:- " +obtainedAlbumList.size());		 
		Log.message("Obtained Albums are:- " +obtainedAlbumList);
		
		
		return obtainedAlbumList;
		
	}
	 
	
	/** 
	 * Method written for Logout from Projects Gallery Album level
	 * Scripted By: Ranadeep
	 * @return
	 */
	public boolean Logout_Projects()
	{
		SkySiteUtils.waitForElement(driver, btnProfile, 20);
		btnProfile.click();
		Log.message("Clicked on Profile.");
		SkySiteUtils.waitForElement(driver, btnLogout, 20);
		SkySiteUtils.waitTill(3000);
		btnLogout.click();
		SkySiteUtils.waitForElement(driver, btnYes, 20);
		btnYes.click();
		SkySiteUtils.waitForElement(driver, btnLogin, 30);
		
		Log.message("Checking whether Login button is present?");
		if(btnLogin.isDisplayed())
			return true;
		else
			return true;
		
	}

	/** 
	 * Method to verify sorting order saved earlier after Log In in Gallery Album level
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean sortOrderSaveAfterLogIn(ArrayList<String> arr) 
	{
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);	
		String sortBy = btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Actual Album sort by order is:-" +sortBy);
	    
	    String expectedSortBy= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Album sort by order is:- " +expectedSortBy);
	    
		SkySiteUtils.waitForElement(driver, btnAlbumSortBy, 30);
		String sortOrder = btnAlbumSortBy.getText();
	    Log.message("Actual Album sort order is:-" +sortOrder);
	    
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE3");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
	    
		ArrayList<String> obtainedAlbumList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedAlbumList.add(we.getText()); 
		     
			}
		}		
		Log.message("Obtained Album count after login:- " +obtainedAlbumList.size());		 
		Log.message("Obtained Album list order is:- " +obtainedAlbumList);
		
		if(sortBy.contains(expectedSortBy) && sortOrder.contains(expectedSortOrder) && arr.equals(obtainedAlbumList))
		{
			return true;
			
		}
		else 
		{
			return false;
			
		}
		
	}

	/** 
	 * Method for entering Album A_Album
	 * Scripted By: Ranadeep
	 * @return 
	 */
	
	public void enterAlbum()
	{
		
		SkySiteUtils.waitForElement(driver, btnUploadPhoto, 30);
		Log.message("Upload photos button is displayed");
		
		SkySiteUtils.waitForElement(driver, albumA_Album, 30);
		Log.message("Album A_Album is displayed");
		albumA_Album.click();
		Log.message("CLicked to enter album A_Album");
		
		SkySiteUtils.waitForElement(driver, albumBreadcrumb, 30);
		String actualAlbumName = albumBreadcrumb.getAttribute("data-original-title");
		Log.message("Entered album name is:- " +actualAlbumName);
		
		String expectedAlbumName= PropertyReader.getProperty("EXPECTEDALBUMNAME");
		Log.message("Expected album name is:- " +expectedAlbumName);
		
		if(btnUploadPhoto.isDisplayed() && actualAlbumName.contains(expectedAlbumName))
		{
			Log.message("Entered album A_Album");
			
		}
		else
		{
			Log.message("Entering album A_Album failed!!!");
		}
		
	}
	
	
	/** 
	 * Method for validating descending order sorting by Photo Name
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingByPhotoName() 
	{
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortBy, 30);
		Log.message("Photo Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/a[2]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Photo Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByAlbumName, 30);
		Log.message("Sort by File Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/ul/li[1]/a"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Photo Name");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
		String sortOrder=btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Album sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedPhotoList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals(""))
			{
				obtainedPhotoList.add(we.getText()); 
		     
			}
		}		
		Log.message("Obtained Photo count before sorting:- " +obtainedPhotoList.size());		 
		Log.message("Obtained Photos are:- " +obtainedPhotoList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Photo sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnAlbumSortOrder.click();
			Log.message("Clicked on order by button");
			SkySiteUtils.waitTill(8000);
			
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		
	    
		ArrayList<String> sortedPhotoList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals(""))
			{
				sortedPhotoList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Albums count after sorting:- " +sortedPhotoList.size());		 
		Log.message("Obtained Albums are:- " +sortedPhotoList);
		
		if(obtainedPhotoList.get(0).contains(sortedPhotoList.get(sortedPhotoList.size()-1)) && obtainedPhotoList.get(obtainedPhotoList.size()-1).contains(sortedPhotoList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
		
	}

	
	/** 
	 * Method for validating ascending order sorting by Photo Name
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingByPhotoName() 
	{
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortBy, 30);
		Log.message("File Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/a[2]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Photo Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByAlbumName, 30);
		Log.message("Sort by File Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/ul/li[1]/a"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Photo Name");
		SkySiteUtils.waitTill(5000);
	 
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
		String sortOrder=btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Photo sort order is:-" +sortOrder);
	 
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		if(sortOrder.contains(expectedSortOrder))
		{
			
			btnAlbumSortOrder.click();
			SkySiteUtils.waitTill(5000);
			
		}
	    
		ArrayList<String> obtainedPhotoList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals(""))
			{
				obtainedPhotoList.add(we.getText()); 
		     
			}
		}		
		Log.message("Obtained Photo count before sorting:- " +obtainedPhotoList.size());		 
		Log.message("Obtained Photos are:- " +obtainedPhotoList);
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
		String sortOrder1=btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Present Photo sort order is:-" +sortOrder1);
		
		String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Photo sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			btnAlbumSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
	    
		ArrayList<String> sortedPhotoList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals(""))
			{
				sortedPhotoList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Photo count after sorting:- " +sortedPhotoList.size());		 
		Log.message("Obtained Photos are:- " +sortedPhotoList);
		
		if(obtainedPhotoList.get(0).contains(sortedPhotoList.get(sortedPhotoList.size()-1)) && obtainedPhotoList.get(obtainedPhotoList.size()-1).contains(sortedPhotoList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
	}

	
	
	/** 
	 * Method to change sorting order for Photo and save data to be validated later in Gallery level
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public ArrayList<String> sortOrderSave1() 
	{
	
		SkySiteUtils.waitForElement(driver, btnAlbumSortBy, 30);
		Log.message("File Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/a[2]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Photo Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByAlbumName, 30);
		Log.message("Sort by File Name button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/ul/li[1]/a"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Photo Name");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
		btnAlbumSortOrder.click();
		Log.message("Clicked on sort order button");
		SkySiteUtils.waitTill(8000);
		String sortOrder=btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Photo sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedPhotoList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals(""))
			{
				obtainedPhotoList.add(we.getText()); 
		     
			}
		}		
		Log.message("Obtained Album count after sorting:- " +obtainedPhotoList.size());		 
		Log.message("Obtained Albums are:- " +obtainedPhotoList);
		
		
		return obtainedPhotoList;
		
	}

	

	/** 
	 * Method to verify sorting order saved earlier after Log In in Gallery Photo level
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean sortOrderSaveAfterLogIn1(ArrayList<String> arr) 
	{
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);	
		String sortBy = btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Actual Photo sort by order is:-" +sortBy);
	    
	    String expectedSortBy= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Photo sort by order is:- " +expectedSortBy);
	    
		SkySiteUtils.waitForElement(driver, btnAlbumSortBy, 30);
		String sortOrder = btnAlbumSortBy.getText();
	    Log.message("Actual Photo sort order is:-" +sortOrder);
	    
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE3");
		Log.message("Expected Photo sort order is:- " +expectedSortOrder);
	    
		ArrayList<String> obtainedPhotoList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@id='divPhotoItemContainer']/ul/li"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals("")) 
			{
				obtainedPhotoList.add(we.getText()); 
		     
			}
		}		
		Log.message("Obtained Album count after login:- " +obtainedPhotoList.size());		 
		Log.message("Obtained Album list order is:- " +obtainedPhotoList);
		
		if(sortBy.contains(expectedSortBy) && sortOrder.contains(expectedSortOrder) && arr.equals(obtainedPhotoList))
		{
			return true;
			
		}
		else 
		{
			return false;
			
		}
		
	}
	
	
	/** 
	 * Method for changing photo/album view from grid to list
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void changeAlbumViewGridToList() 
	{
		
		SkySiteUtils.waitForElement(driver, btnSwitchGridListView, 30);
		Log.message("Switch photo/album view button is displayed");
		
		String actualFolderView = btnSwitchGridListView.getAttribute("data-original-title");
		Log.message("Actual photo/album view is:- " +actualFolderView);
		
		String expectedFolderView = PropertyReader.getProperty("ALBUMGRIDTOLIST");
		Log.message("Expected photo/album view is:- " +expectedFolderView);
		
		if(actualFolderView.contains(expectedFolderView))
		{
			//btnSwitchGridListView.click();
			WebElement element = driver.findElement(By.xpath("//*[@id='switch-view']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			Log.message("Clicked on switch photo/album view button");
			SkySiteUtils.waitTill(8000);
			
		}
		else
		{
			btnSwitchGridListView.click();
			SkySiteUtils.waitTill(8000);
			btnSwitchGridListView.click();
			Log.message("Clicked on switch photo/album view button");
			SkySiteUtils.waitTill(8000);
			
		}
		
		String actualLatestFolderView = btnSwitchGridListView.getAttribute("data-original-title");
		Log.message("Latest photo/album view is:- " +actualLatestFolderView);
		
		String expectedLatestFolderView = PropertyReader.getProperty("ALBUMLISTTOGRID");
		Log.message("Expected latest photo/album view is:- " +expectedLatestFolderView);
		
		if(actualLatestFolderView.contains(expectedLatestFolderView))
		{
			Log.message("Grid to List view change is successfull");
			
		}
		else
		{
			Log.message("Grid to List view change failed");
			
		}

	}
	
	
	
	/** 
	 * Method for changing photo/album view from list to grid
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public void changeAlbumViewListToGrid() 
	{
		
		SkySiteUtils.waitForElement(driver, btnSwitchGridListView, 30);
		Log.message("Switch photo/album view button is displayed");
		
		SkySiteUtils.waitTill(5000);
		String actualFolderView = btnSwitchGridListView.getAttribute("data-original-title");
		Log.message("Actual photo/album view is:- " +actualFolderView);
		
		String expectedFolderView = PropertyReader.getProperty("ALBUMLISTTOGRID");
		Log.message("Expected photo/album view is:- " +expectedFolderView);
		
		
		if(actualFolderView.contains(expectedFolderView))
		{
			
			WebElement element = driver.findElement(By.xpath("//*[@id='switch-view']"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			Log.message("Clicked on switch photo/album view button");
			SkySiteUtils.waitTill(8000);
			
		}
		
		String actualLatestFolderView = btnSwitchGridListView.getAttribute("data-original-title");
		Log.message("Latest photo/album view is:- " +actualLatestFolderView);
		
		String expectedLatestFolderView = PropertyReader.getProperty("ALBUMGRIDTOLIST");
		Log.message("Expected latest photo/album view is:- " +expectedLatestFolderView);
		
		if(actualLatestFolderView.contains(expectedLatestFolderView))
		{
			Log.message("List to Grid view change is successfull");
			
		}
		else
		{
			Log.message("List to Grid view change failed");
			
		}
		
	}

	
	/** 
	 * Method for validating descending order sorting by Photo Create Date
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean descendingOrderSortingByPhotoCreateDate() 
	{
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortBy, 30);
		Log.message("Photo Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/a[2]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Photo Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByAlbumCreateDate, 30);
		Log.message("Sort by Photo Create Date button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/ul/li[2]/a"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Photo Create Date");
		SkySiteUtils.waitTill(5000);
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
		String sortOrder=btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Album sort order is:-" +sortOrder);
	    
	    ArrayList<String> obtainedPhotoList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@data-bind='text: ItemCreateTimeStr()']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals(""))
			{
				obtainedPhotoList.add(we.getText()); 
		     
			}
		}		
		Log.message("Obtained Photo count before sorting:- " +obtainedPhotoList.size());		 
		Log.message("Obtained Photos are:- " +obtainedPhotoList);
		
		String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Photo sort order is:- " +expectedSortOrder);
		
		if(sortOrder.contains(expectedSortOrder))
		{
			btnAlbumSortOrder.click();
			Log.message("Clicked on order by button");
			SkySiteUtils.waitTill(8000);
			
		}
		else
		{
			Log.message("Sort order is not ascending");
		}
		
	    
		ArrayList<String> sortedPhotoList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@data-bind='text: ItemCreateTimeStr()']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals(""))
			{
				sortedPhotoList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Albums count after sorting:- " +sortedPhotoList.size());		 
		Log.message("Obtained Albums are:- " +sortedPhotoList);
		
		if(obtainedPhotoList.get(0).contains(sortedPhotoList.get(sortedPhotoList.size()-1)) && obtainedPhotoList.get(obtainedPhotoList.size()-1).contains(sortedPhotoList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
	}

	
	/** 
	 * Method for validating ascending order sorting by Photo Create Date
	 * Scripted By: Ranadeep
	 * @return 
	 */
	public boolean ascendingOrderSortingByPhotoCreateDate() 
	{
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortBy, 30);
		Log.message("Photo Sort by button is displayed");
		WebElement element = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/a[2]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Photo Sort by button");
		
		SkySiteUtils.waitForElement(driver, btnSortByAlbumCreateDate, 30);
		Log.message("Sort by Photo Create Date button is displayed");
		WebElement element1 = driver.findElement(By.xpath("//*[@id='divPhotosContainer']/div[1]/div[2]/ul[3]/li[5]/ul/li[2]/a"));
		JavascriptExecutor executor1 = (JavascriptExecutor)driver;
		executor1.executeScript("arguments[0].click();", element1);
		Log.message("Clicked on Sort by Photo Create Date");
		SkySiteUtils.waitTill(5000);
	 
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
		String sortOrder=btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Photo sort order is:-" +sortOrder);
	 
	    String expectedSortOrder= PropertyReader.getProperty("EXPECTEDSORTBYVALUE1");
		Log.message("Expected Folder sort order is:- " +expectedSortOrder);
		if(sortOrder.contains(expectedSortOrder))
		{
			
			btnAlbumSortOrder.click();
			SkySiteUtils.waitTill(5000);
			
		}
	    
		ArrayList<String> obtainedPhotoList = new ArrayList<>();
		java.util.List<WebElement> elementList= driver.findElements(By.xpath("//*[@data-bind='text: ItemCreateTimeStr()']"));
		
		for(WebElement we:elementList)
		{
			if(!we.getText().equals(""))
			{
				obtainedPhotoList.add(we.getText()); 
		     
			}
		}		
		Log.message("Obtained Photo count before sorting:- " +obtainedPhotoList.size());		 
		Log.message("Obtained Photos are:- " +obtainedPhotoList);
		
		SkySiteUtils.waitForElement(driver, btnAlbumSortOrder, 30);
		String sortOrder1=btnAlbumSortOrder.getAttribute("data-original-title");
	    Log.message("Present Photo sort order is:-" +sortOrder1);
		
		String expectedSortOrder1= PropertyReader.getProperty("EXPECTEDSORTBYVALUE2");
		Log.message("Expected Photo sort order is:- " +expectedSortOrder1);
		
		if(sortOrder1.contains(expectedSortOrder1))
		{
			btnAlbumSortOrder.click();
			Log.message("Clicked on order by button");
		}
		else
		{
			Log.message("Sort order is not descending");
		}
		SkySiteUtils.waitTill(8000);
	    
		ArrayList<String> sortedPhotoList = new ArrayList<>();
		java.util.List<WebElement> elementList1= driver.findElements(By.xpath("//*[@data-bind='text: ItemCreateTimeStr()']"));
		
		for(WebElement we:elementList1)
		{
			if(!we.getText().equals(""))
			{
				sortedPhotoList.add(we.getText()); 
		     
			}
		}
		Log.message("Obtained Photo count after sorting:- " +sortedPhotoList.size());		 
		Log.message("Obtained Photos are:- " +sortedPhotoList);
		
		if(obtainedPhotoList.get(0).contains(sortedPhotoList.get(sortedPhotoList.size()-1)) && obtainedPhotoList.get(obtainedPhotoList.size()-1).contains(sortedPhotoList.get(0)))
	    {	
			return true;
			
	    }
	    else
	    {
	    	return false;
	    	
	    }
	}
	
}
