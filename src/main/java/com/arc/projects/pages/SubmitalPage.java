package com.arc.projects.pages;

import java.awt.AWTException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.sun.jna.platform.unix.X11.XClientMessageEvent.Data;

import java.util.Random;

public class SubmitalPage extends LoadableComponent<SubmitalPage> {

	WebDriver driver;
	private boolean isPageLoaded;

	ProjectDashboardPage projectDashboardPage;
	FolderPage folderPage;

	/**
	 * Identifying web elements using FindBy annotation.
	 */

	@FindBy(css = ".dropdown-toggle.aPunchrfimenu")
	WebElement btnPrjManagement;

	@FindBy(css = "#a_showSUBMITTALList")
	WebElement btnSubmittalList;

	@FindBy(xpath = "//a[@class='new-submittal dev-new-submittal']")
	WebElement btnnewSubmittal;

	@FindBy(css = ".wrk-temp")
	WebElement tabStandardTemplate;

	@FindBy(xpath = "(//button[@data-workflow-name='Template 1'])[2]")
	WebElement imgFirstTemplate;

	@FindBy(xpath = "(//button[@data-workflow-name='Template 2'])[2]")
	WebElement imgSecondTemplate;

	@FindBy(css = "#txtWFName")
	WebElement txtWFName;

	@FindBy(xpath = "(//button[@class='btn btn-primary' and @type='submit'])[2]")
	WebElement btnSubmitCopyWF;

	@FindBy(css = ".col-md-12>h4>span")
	WebElement workFlowName;

	@FindBy(css = "#txtSubmittalNumber")
	WebElement txtSubmittalNumber;

	@FindBy(css = "#txtSubmittalName")
	WebElement txtSubmittalName;

	@FindBy(css = "#txtSubmittalDueDate")
	WebElement txtSubmittalDueDate;
	
	//@FindBy(xpath = "(//button[@class='btn btn-default dropdown-toggle'])[2]")//Changed on 27th Aug Naresh
	@FindBy(xpath = "(//button[@class='btn btn-default dropdown-toggle'])[1]")
	WebElement dropDownSubmittalType;

	@FindBy(xpath = "//a[@class='SubmittaltypeList' and @data-name='LEED']")
	WebElement optionSubmittalTypeSelect;

	@FindBy(xpath = "(//button[@class='btn btn-default' and @data-target='.company-list'])[1]")
	WebElement btnCompanyList;
	
	@FindBy(xpath = "(//button[@class='btn btn-default' and @data-target='.company-list'])[2]")
	WebElement btnCompanyList2;

	@FindBy(xpath = "(//input[@type='radio'])[1]")
	WebElement radiobtnSelectCompany;
			
	@FindBy(xpath = "(//button[@id='btnCreateSubmittal'])[2]")
	WebElement btnCreateSubmittal;//Newly Added _ Naresh
	
	@FindBy(xpath = "//button [@class='btn btn-primary btn-lg new-submittal dev-new-submittal']")
	WebElement btnSubmittal;
	
	

	@FindBy(css = "#button-0")
	WebElement btnNo_WanttoCreateSubmittal;

	@FindBy(css = "#button-0")
	WebElement btnOK_appliedToAllImpSubmittals;
	
	@FindBy(css = "#button-1")
	WebElement btnYes_WanttoCreateSubmittal;

	@FindBy(css = "#button-2")
	WebElement btnYes_WithCoverPage;

	@FindBy(css = ".noty_text")
	WebElement notificationMsg;

	@FindBy(xpath = "(//span[@class='sbmtl-id'])[1]")
	WebElement submIdUnderList;

	@FindBy(xpath = "(//span[@class='sbmtl-nm'])[1]")
	WebElement submNameUnderList;

	@FindBy(xpath = "(//span[@class='sbmtl-type'])[1]")
	WebElement submTypeUnderList;

	@FindBy(xpath = "(//span[@class='text-info'])[1]")
	WebElement submDueDateUnderList;

	@FindBy(xpath = "(//span[@class='yellow-txt'])[1]")//Newly Added _ Naresh
	WebElement submOpenStatusUnderList;

	@FindBy(xpath = "(//span[@class='submited'])[1]")//Newly Added _ Naresh
	WebElement submSubmittedStatusUnderList;

	@FindBy(xpath = "(//span[@class='green-txt'])[1]")//Newly Added _ Naresh
	WebElement submApprovedStatusUnderList;

	@FindBy(xpath = "(//span[@class='red-text'])[1]")//Newly Added _ Naresh
	WebElement submRejectStatusUnderList;

	@FindBy(xpath = "(//span[@class='submited'])[1]")//Newly Added _ Naresh
	WebElement submResubmitStatusUnderList;
	
	@FindBy(xpath = "(//span[@class='reviewed-text'])[1]")//Newly Added _ Naresh
	WebElement submReviewedStatusUnderList;
	

	// Submit Submittal
	@FindBy(css = "#liFirstTab")
	WebElement tabMySubmittals;

	@FindBy(css = "#liThirdTab")
	WebElement tabAllSubmittals;

	@FindBy(css = "#btnCreatesubmittallist")
	WebElement btnSubmitsubmittal;

	@FindBy(css = "#selectAssignedToUser")
	WebElement selectAssignedToUser;
	
	@FindBy(css = "#add-project-file")
	WebElement btnAddProjectFile;
	
	@FindBy(xpath = "//i[@class='icon icon-3x icon-folder-close']")
	WebElement iconFolderWhileAttachFile;
	
	@FindBy(css = "#txtDocSearchKeyword")
	WebElement txtSearchProjectFile;
	
	@FindBy(css = ".icon.icon-search.icon-lg")
	WebElement iconSearchProjectFiles;
	
	@FindBy(xpath = "(//input[@type='checkbox' and @data-bind='checked: IsChecked'])[1]")
	WebElement chkboxSearchResults;
	
	@FindBy(css = "#btnCancelAddContact")
	WebElement btnCancelAddContact;

	@FindBy(css = "#txtUserSearchKeyword")
	WebElement txtUserSearchKeyword;

	@FindBy(xpath = "//input[@class='pull-left' and @type='checkbox']")
	WebElement chkboxEmpSelectWind;

	@FindBy(css = "#txtSubmittalComment")
	WebElement txtSubmittalComment;

	@FindBy(xpath = "(//a[@id='Submittal_download'])[1]")
	WebElement btnSubmitalDownload;

	// Approver Side objects
	@FindBy(css = "#txtCCComment")
	WebElement txtCommentApprover;

	@FindBy(css = "#btnAddComment")
	WebElement btnAddComment;

	@FindBy(css = ".btn.btn-success")
	WebElement btnApprove;

	@FindBy(css = ".btn.btn-danger")
	WebElement btnReject;

	@FindBy(css = "#txtCompanyAddRecipient")
	WebElement txtCompanyAddRecipient;
	
	@FindBy(css = "#txtSubmittalToRecipient")
	WebElement txtSubmittalToRecipient;

	@FindBy(css = ".btn.btn-default.btn-small.reassign-btn")
	WebElement btnReassign;

	@FindBy(xpath = "//span[@class='input-group-btn' and @title='Contact list']")
	WebElement btnContactsReassign;

	@FindBy(xpath = "//i[@class='icon icon-save icon-lg']")
	WebElement btnSaveReassign;

	@FindBy(xpath = "//button[@class='btn btn-success' and @disabled='disabled']")
	WebElement btnApproveDisabled;

	@FindBy(xpath = "//button[@class='btn btn-danger to-disable-btn' and @disabled='disabled']")
	WebElement btnRejectDisabled;

	@FindBy(xpath = "//button[@class='btn btn-default btn-small reassign-btn' and @disabled='disabled']")
	WebElement btnReassignDisabled;

	@FindBy(xpath = "//button[@class='btn btn-default' and @data-bind='visible: canCancel']")
	WebElement btnCancelSubmittalDetailsWind;
	
	@FindBy(xpath = "(//i[@class='icon icon-app-contact icon-lg'])[3]")
	WebElement iconEmailContact;
	
	@FindBy(xpath = "//textarea[@class='form-control' and @placeholder='Comments']")
	WebElement txtCommentAssignto;
	
	@FindBy(xpath="//*[@id='submittallistContainer']/div[1]/div[1]/ul/li[2]/a")
	WebElement buttonCreateSubmittal;
	
	@FindBy(xpath="//button[text()='Create']")
	WebElement buttonCreateSubmittalPopover;
	
	@FindBy(xpath="//*[@class='day active']")
	WebElement activeSubmittalDate;
	
	@FindBy(xpath="//*[@id='divCreateSubmittal']/div/div[2]/div/div[1]/div[4]/div/div/button[1]")
	WebElement dropdownSubmittalType;
	
	@FindBy(xpath="//*[@id='divCreateSubmittal']/div/div[2]/div/div[1]/div[4]/div/div/ul/li[1]/a")
	WebElement submittalType;
	
	@FindBy (xpath=".//*[@id='ulSubmittalList']/li[1]/div[2]/div[1]")
	WebElement createdSubmitalInfo;
	
	@FindBy (xpath="//*[@id='liActivityMain']/a/span[1]")
	WebElement btnProjectActivity;
	
	@FindBy (xpath="//*[@id='liActivity']/a/span")
	WebElement btnDocumentActivity;
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnPrjManagement, 20);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 * @return
	 */
	public SubmitalPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	/**
	 * Method written for Select Submittal List from Project Management Tab
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public FolderPage SelectSubmittalList() 
	{
		SkySiteUtils.waitForElement(driver, btnPrjManagement, 20);
		btnPrjManagement.click();
		Log.message("Clicked on Project Management tab.");
		SkySiteUtils.waitForElement(driver, btnSubmittalList, 20);
		btnSubmittalList.click();
		Log.message("Clicked on Submittal List tab.");
		SkySiteUtils.waitForElement(driver, btnnewSubmittal, 20);

		if (btnnewSubmittal.isDisplayed())
			Log.message("Create new submittal button is available.");
		return new FolderPage(driver).get();
	}

	/**
	 * Method written for Select 1st workflow template 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	
	@FindBy(xpath="//a[@title='Change workflow']")
	WebElement iconChangeworkflow;

	public boolean select_Workflow_Template(String WF_Template_Name) 
	{
		SkySiteUtils.waitForElement(driver, btnnewSubmittal, 20);
		btnnewSubmittal.click();
		Log.message("Clicked on new submittal button.");
		SkySiteUtils.waitForElement(driver, btnCreateSubmittal, 20);
		SkySiteUtils.waitTill(2000);
		
		iconChangeworkflow.click();//Newly Added 25th - Naresh
		Log.message("Clicked on change workflow icon.");
		SkySiteUtils.waitTill(15000);
		SkySiteUtils.waitForElement(driver, tabStandardTemplate, 20);
		SkySiteUtils.waitTill(3000);
		//tabStandardTemplate.click();
		//modified
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", tabStandardTemplate);
		Log.message("Clicked on Standard Template tab.");
		SkySiteUtils.waitTill(3000);
		SkySiteUtils.waitForElement(driver, imgFirstTemplate, 20);
		imgFirstTemplate.click();// Select 1st template
		SkySiteUtils.waitForElement(driver, txtWFName, 40);
		SkySiteUtils.waitTill(3000);
		txtWFName.clear();//Clear the Existed Name
		SkySiteUtils.waitTill(2000);
		txtWFName.sendKeys(WF_Template_Name);
		btnSubmitCopyWF.click();// Click on submit
		Log.message("Clicked on submit button.");
		SkySiteUtils.waitForElement(driver, workFlowName, 20);
		SkySiteUtils.waitTill(2000);
		String Exp_WFName = "Workflow - " + WF_Template_Name;
		String Act_WFName = workFlowName.getText();
		Log.message("Expected WF Name is: " + Act_WFName);
		if (Act_WFName.equalsIgnoreCase(Exp_WFName)) 
		{
			Log.message("User selected WF successfully.");
			return true;
		} 
		else 
		{
			Log.message("User Failed to select WF.");
			return false;
		}
	}

	/**
	 * Method written for Select 2nd workflow template 
	 * Scipted By: Naresh Babu
	 * @return
	 */

	public boolean select_Workflow_Template2(String Project_Name) 
	{
		SkySiteUtils.waitForElement(driver, btnnewSubmittal, 20);
		btnnewSubmittal.click();
		Log.message("Clicked on new submittal button.");
		SkySiteUtils.waitForElement(driver, iconChangeworkflow, 20);
		SkySiteUtils.waitTill(2000);
		iconChangeworkflow.click();//Newly Added - Naresh
		Log.message("Clicked on change workflow icon.");
		SkySiteUtils.waitForElement(driver, tabStandardTemplate, 40);
		SkySiteUtils.waitTill(3000);
		//tabStandardTemplate.click();
		//modified
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", tabStandardTemplate);
		SkySiteUtils.waitTill(4000);
		SkySiteUtils.waitForElement(driver, imgSecondTemplate, 20);
		imgSecondTemplate.click();// Select 2nd template
		SkySiteUtils.waitForElement(driver, txtWFName, 20);
		SkySiteUtils.waitTill(5000);
		txtWFName.clear();// Clear existed name
		SkySiteUtils.waitTill(2000);
		txtWFName.sendKeys(Project_Name);
		btnSubmitCopyWF.click();// Click on submit
		Log.message("Clicked on submit button.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, workFlowName, 20);
		String Exp_WFName = "Workflow - " + Project_Name;
		String Act_WFName = workFlowName.getText();
		Log.message("Expected WF Name is: " + Act_WFName);
		if (Act_WFName.equalsIgnoreCase(Exp_WFName)) 
		{
			Log.message("User selected WF Template2 successfully.");
			return true;
		} 
		else 
		{
			Log.message("User Failed to select WF Template2.");
			return false;
		}
	}
		
	/**
	 * Method written for Create submittal 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Create_Submital_AndValidate(String Submittal_Number,String Submittal_Name) 
	{
		SkySiteUtils.waitForElement(driver, btnCreateSubmittal, 30);
		txtSubmittalNumber.sendKeys(Submittal_Number);
		txtSubmittalName.sendKeys(Submittal_Name);
		txtSubmittalDueDate.click();// Click on due date
		SkySiteUtils.waitTill(2000);
		driver.findElement(By.xpath("//div[@class='bootstrap-datetimepicker-widget dropdown-menu datetimepickermodalclose bottom']/div/div/table[@class='table-condensed']/tbody/tr[5]/td[5]")).click();// Select due date from cal
		SkySiteUtils.waitTill(2000);
		dropDownSubmittalType.click();
		SkySiteUtils.waitTill(2000);
		optionSubmittalTypeSelect.click();
		Log.message("Selected submittal Type from dropdown.");
		
		//Getting number of approvers
		int Avl_Approvers_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath("//button[@class='btn btn-default' and @data-target='.company-list']")); 
		for (WebElement Element : allElements) 
		{ 
			Avl_Approvers_Count = Avl_Approvers_Count+1; 	
		} 
		Log.message("Available Approver Count is: "+Avl_Approvers_Count);
		
		if(Avl_Approvers_Count==1)
		{
			btnCompanyList.click();
			SkySiteUtils.waitForElement(driver, radiobtnSelectCompany, 20);
			SkySiteUtils.waitTill(2000);
			radiobtnSelectCompany.click();
			Log.message("Selected first company radio button.");
		}
		else if(Avl_Approvers_Count==2)
		{
			btnCompanyList.click();
			SkySiteUtils.waitForElement(driver, radiobtnSelectCompany, 20);
			SkySiteUtils.waitTill(2000);
			radiobtnSelectCompany.click();
			Log.message("Selected first company radio button.");
			SkySiteUtils.waitTill(2000);
			btnCompanyList2.click();
			SkySiteUtils.waitForElement(driver, radiobtnSelectCompany, 20);
			SkySiteUtils.waitTill(2000);
			radiobtnSelectCompany.click();
			Log.message("Selected first company radio button.");
		}
		SkySiteUtils.waitTill(2000);
		btnCreateSubmittal.click();
		Log.message("Clicked on create submittal button.");
		//SkySiteUtils.waitForElement(driver, btnNo_WanttoCreateSubmittal, 20);
		//btnYes_WanttoCreateSubmittal.click();
		//Log.message("Clicked on button 'YES' for conform to create submittal.");
		//SkySiteUtils.waitTill(1000);
		//SkySiteUtils.waitForElement(driver, notificationMsg, 20);
		//String NotifyMsg_SubmitalCreate=notificationMsg.getText();
		//Log.message("Message displayed after submittal create is: "+NotifyMsg_SubmitalCreate);
		SkySiteUtils.waitForElement(driver, submIdUnderList, 20);
		SkySiteUtils.waitTill(5000);
		String SubmId_AllSubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_AllSubTab);
		String SubmName_AllSubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_AllSubTab);
		//String SubmType_AllSubTab = submTypeUnderList.getText();
		//Log.message("Sub Type is: " + SubmType_AllSubTab);
		String SubmStatus_AllSubTab = submOpenStatusUnderList.getText();
		Log.message("Sub status is: " + SubmStatus_AllSubTab);

		if ((SubmId_AllSubTab.equalsIgnoreCase(Submittal_Number)) && (SubmName_AllSubTab.equalsIgnoreCase(Submittal_Name))
				&& (SubmStatus_AllSubTab.equalsIgnoreCase("OPEN"))) 
		{
			Log.message("Creating a Submittal is successfully.");
			return true;
		} else {
			Log.message("Creating a Submittal is failed.");
			return false;
		}
	}

	//temp
	
	@FindBy(xpath = "//*[@id='aAdvanceFilter']")
	WebElement AdvanceSearch_Link;
	
	@FindBy(xpath = "(//span [@class='label label-warning'])[1]")
	WebElement RFI_Duedate_Text;
	
	
	/**
	 * Method written for Create submittal 
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Submital_AndValidate(String Submittal_Number,String Submittal_Name) 
	{
		
		boolean result1 = false;
		SkySiteUtils.waitTill(8000);
		SkySiteUtils.waitForElement(driver, btnSubmittal, 20);
		btnSubmittal.click();
		SkySiteUtils.waitForElement(driver, btnCreateSubmittal, 20);
		Log.message("created And submit button clicked");	
		txtSubmittalNumber.sendKeys(Submittal_Number);
		txtSubmittalName.sendKeys(Submittal_Name);		
		/*driver.findElement(By.xpath(".//*[@id='txtSubmittalDueDate']")).click();
		SkySiteUtils.waitTill(3000);
		String date1=driver.findElement(By.xpath("//td[@class='day active']")).getText().toString();
		Log.message(" Date is:"+date1);
		driver.findElement(By.xpath("//td[@class='day active']")).click();
		SkySiteUtils.waitTill(3000);*/		
		txtSubmittalDueDate.click();// Click on due date
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("html/body/div[10]/div/div[1]/table/tbody/tr[5]/td[7]")).click();// Select due date from cal
		SkySiteUtils.waitTill(2000);
		dropDownSubmittalType.click();
		SkySiteUtils.waitTill(2000);
		optionSubmittalTypeSelect.click();
		Log.message("Selected submittal Type from dropdown.");		
		driver.findElement(By.xpath("(//*[@id='btnCreateSubmittal'])[1]")).click();
		Log.message("create and submit button has been clicked");	
		
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, AdvanceSearch_Link, 120);
		AdvanceSearch_Link.click();
		Log.message("Advance Search Option Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		driver.findElement(By.xpath("//span[@class='icon-calender icon-calendar icon']")).click();
		int i=1;
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd");  
		   LocalDateTime now = LocalDateTime.now();  
		   System.out.println(dtf.format(now)); 
		   int myDate = Integer.parseInt(String.valueOf(dtf.format(now)))+1;
		   System.out.println(myDate); 
		   String updatedDate = String.valueOf(myDate);
		   
		   String element = String.format("(//td[@class='day' and text()='%s'])[1]", updatedDate);
		   
		   Log.message("------------------------"+element);
		   
		driver.findElement(By.xpath(element)).click();
		
		
		String duedate = RFI_Duedate_Text.getText();
		
		Log.message("Due date"+duedate);

		String[] date = duedate.split("/S/");
		int dateIndex = 1;
		for (String Dates : date) {
			Log.message(dateIndex + ". " + date);

			dateIndex++;
		}

		String textStr[] = duedate.split("\\s", 10);
		String my_Date = textStr[4] + " " + textStr[5] + " " + textStr[6];
		Log.message(textStr[0]);
		Log.message(textStr[1]);
		Log.message(textStr[2]);
		Log.message(my_Date);

		Calendar cal = Calendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		 String dayOfMonthStr = String.valueOf(dayOfMonth);
		return result1;
		
	}
	
	public static void main(String[] args) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd");  
		   LocalDateTime now = LocalDateTime.now();  
		   System.out.println(dtf.format(now)); 
		   int myDate = Integer.parseInt(String.valueOf(dtf.format(now)))+1;
		   System.out.println(myDate); 
	}
	
	/**
	 * Method written for Submiting a submittal with attachments 
	 * Scipted By: Naresh Babu
	 * @return
	 */

	public boolean Submit_Submittal_WithAttachments(String Submittal_Number,String Emp_Details, String FolderPath,
			String SubmComment_WhileSubmit) throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		SkySiteUtils.waitTill(5000);
		String SubmId_AllSubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_AllSubTab);
		if (SubmId_AllSubTab.equalsIgnoreCase(Submittal_Number)) 
		{
			Log.message("Expected submittal available..you can submit!!!");
			submIdUnderList.click();
			Log.message("Clicked on expected submittal.");
			SkySiteUtils.waitForElement(driver, btnSubmitsubmittal, 30);
			SkySiteUtils.waitTill(5000);
			selectAssignedToUser.click();
			SkySiteUtils.waitForElement(driver, btnCancelAddContact, 30);
			SkySiteUtils.waitTill(5000);
			txtUserSearchKeyword.sendKeys(Emp_Details);
			driver.findElement(By.xpath("//a[@data-bind='click: SearchClick' and @class='input-group-addon pw-contact-list-modal']")).click();
			SkySiteUtils.waitTill(5000);
			String Emp_SearchResults = driver.findElement(By.xpath("//a[@data-bind='text: Email()']")).getText();
			Log.message("Search results for employee is: " + Emp_SearchResults);
			if (Emp_SearchResults.equalsIgnoreCase(Emp_Details)) 
			{
				Log.message("Search functionality for contact is working.");
				chkboxEmpSelectWind.click();
				driver.findElement(By.xpath("//button[@data-bind='click: SelectContactEvent']")).click();
				Log.message("Clicked on select user button.");
				SkySiteUtils.waitForElement(driver, txtSubmittalComment, 20);
				SkySiteUtils.waitTill(3000);
				txtSubmittalComment.sendKeys(SubmComment_WhileSubmit);
				SkySiteUtils.waitTill(3000);
				//modified
				driver.findElement(By.xpath(".//*[@id='btn-Load-attachment']")).click();
				Log.message(" add attachment button is clicked");
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath(".//*[@id='upload-file-attach']/div/div[2]/div/button")).click();
				Log.message("select file otion is clicked");
				SkySiteUtils.waitTill(9000);
				//driver.findElement(By.xpath("//input[@type='file' and @name='qqfile']")).click();// Click on choose files
				//SkySiteUtils.waitTill(10000);
				// Writing File names into a text file for using in AutoIT Script
				BufferedWriter output;
				randomFileName rn = new randomFileName();
				String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
				output = new BufferedWriter(new FileWriter(tmpFileName, true));

				String expFilename = null;
				File[] files = new File(FolderPath).listFiles();

				for (File file : files) {
					if (file.isFile()) {
						expFilename = file.getName();// Getting File Names into
						// a variable
						Log.message("Expected File name is:" + expFilename);
						output.append('"' + expFilename + '"');
						output.append(" ");
						SkySiteUtils.waitTill(500);
					}
				}

				output.flush();
				output.close();

				// Executing .exe autoIt file
				String AutoIt_ExeFile_Path = PropertyReader
						.getProperty("AutoItExe_FilePath");
				Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath+" "+tmpFileName );
				Log.message("AutoIT Script Executed!!");
				SkySiteUtils.waitTill(20000);
				SkySiteUtils.waitForElement(driver, btnSubmitsubmittal, 30);
				try {
					File file = new File(tmpFileName);
					if (file.delete()) {
						Log.message(file.getName() + " is deleted!");
					} else {
						Log.message("Delete operation is failed.");
					}
				} catch (Exception e) {
					Log.message("Exception occured!!!" + e);

				}
				SkySiteUtils.waitTill(5000);
				//btnSubmitsubmittal.click();
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", btnSubmitsubmittal);
				Log.message("Submit submital button is clicked");
				SkySiteUtils.waitTill(2000);
				/*SkySiteUtils.waitTill(20000); 
				tabMySubmittals.click();
				SkySiteUtils.waitTill(5000); 
				tabAllSubmittals.click();
				SkySiteUtils.waitTill(5000);*/
				SkySiteUtils.waitForElement(driver, btnSubmitalDownload, 30);
				SkySiteUtils.waitTill(2000);
				tabAllSubmittals.click();//temp
				SkySiteUtils.waitTill(5000);
				// SkySiteUtils.waitForElement(driver, notificationMsg, 20);
				// String NotifyMsg_SubmitalSubmit=notificationMsg.getText();
				// Log.message("Message displayed after submiting the submittal is: "+NotifyMsg_SubmitalSubmit);
				String SubmId_AfterSubmit = submIdUnderList.getText();
				Log.message("Sub ID After Submit is: " + SubmId_AfterSubmit);
				String Sub_StatusAfterSubmit = submSubmittedStatusUnderList.getText();
				Log.message("Status after submit a submittal is: "+ Sub_StatusAfterSubmit);
				if ((SubmId_AfterSubmit.equalsIgnoreCase(Submittal_Number))	&& (Sub_StatusAfterSubmit.equalsIgnoreCase("SUBMITTED"))) 
				{
					result1 = true;
					Log.message("Status changed successfully to submitted.");

					// Validations from 'My Submittal(s)' tab
					tabMySubmittals.click();
					Log.message("Clicked on my Submittals tab.");
					SkySiteUtils.waitTill(3000);
					String SubmId_UnderMySubmittalsTab = submIdUnderList.getText();
					Log.message("Sub ID Under My Submittals Tab is: "+ SubmId_UnderMySubmittalsTab);
					String Sub_StatusUnderMySubmittalsTab = submSubmittedStatusUnderList.getText();
					Log.message("Status Under My Submittals Tab is: "+ Sub_StatusUnderMySubmittalsTab);
					if ((SubmId_UnderMySubmittalsTab.equalsIgnoreCase(Submittal_Number))
							&& (Sub_StatusUnderMySubmittalsTab.equalsIgnoreCase("Submitted"))) 
					{
						result2 = true;
						Log.message("Expected Submittal is available under My Submittal(s) Tab.");
					} else {
						result2 = false;
						Log.message("Expected Submittal is NOT available under My Submittal(s) Tab.");
					}

				} 
				else 
				{
					result1 = false;
					Log.message("Failed to submit submittal.");
				}

			}

		}

		if ((result1 == true) && (result2 == true)) {
			Log.message("Submitting a Submittal to an employee is successfully.");
			return true;
		} else {
			Log.message("Submitting a Submittal to an employee is failed.");
			return false;
		}
	}

	
	/**
	 * Method written for Submiting a submittal with internal and projects attachments Scipted By:
	 * Naresh Babu
	 * @return
	 */

	public boolean Submit_Submittal_With_BothTypesOf_Attachments(String Submittal_Number,String Emp_Details, String FolderPath,
			String SubmComment_WhileSubmit,String Search_FileName) throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		SkySiteUtils.waitTill(5000);
		String SubmId_AllSubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_AllSubTab);
		if (SubmId_AllSubTab.equalsIgnoreCase(Submittal_Number)) 
		{
			Log.message("Expected submittal available..you can submit!!!");
			submIdUnderList.click();
			Log.message("Clicked on expected submittal.");
			SkySiteUtils.waitForElement(driver, btnSubmitsubmittal, 60);
			//SkySiteUtils.waitTill(5000);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", selectAssignedToUser);
			//selectAssignedToUser.click();
			SkySiteUtils.waitForElement(driver, btnCancelAddContact, 30);
			SkySiteUtils.waitTill(5000);
			txtUserSearchKeyword.sendKeys(Emp_Details);
			driver.findElement(By.xpath("//a[@data-bind='click: SearchClick' and @class='input-group-addon pw-contact-list-modal']")).click();
			SkySiteUtils.waitTill(5000);
			String Emp_SearchResults = driver.findElement(By.xpath("//a[@data-bind='text: Email()']")).getText();
			Log.message("Search results for employee is: " + Emp_SearchResults);
			if (Emp_SearchResults.equalsIgnoreCase(Emp_Details)) 
			{
				Log.message("Search functionality for contact is working.");
				chkboxEmpSelectWind.click();
				driver.findElement(By.xpath("//button[@data-bind='click: SelectContactEvent']")).click();
				Log.message("Clicked on select user button.");
				SkySiteUtils.waitForElement(driver, txtSubmittalComment, 20);
				SkySiteUtils.waitTill(3000);
				txtSubmittalComment.sendKeys(SubmComment_WhileSubmit);
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath(".//*[@id='btn-Load-attachment']")).click();
				Log.message(" add attachment button is clicked");
				SkySiteUtils.waitTill(3000);
				driver.findElement(By.xpath(".//*[@id='upload-file-attach']/div/div[2]/div/button")).click();
				Log.message("select file otion is clicked");
				//driver.findElement(By.xpath("//input[@type='file' and @name='qqfile']")).click();// Click on choose files
				SkySiteUtils.waitTill(10000);
				// Writing File names into a text file for using in AutoIT Script
				BufferedWriter output;
				randomFileName rn = new randomFileName();
				String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
				output = new BufferedWriter(new FileWriter(tmpFileName, true));

				String expFilename = null;
				File[] files = new File(FolderPath).listFiles();

				for (File file : files) {
					if (file.isFile()) {
						expFilename = file.getName();// Getting File Names into
						// a variable
						Log.message("Expected File name is:" + expFilename);
						output.append('"' + expFilename + '"');
						output.append(" ");
						SkySiteUtils.waitTill(500);
					}
				}

				output.flush();
				output.close();

				// Executing .exe autoIt file
				String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
				Runtime.getRuntime().exec(AutoIt_ExeFile_Path +" "+ FolderPath+" "+tmpFileName );
				Log.message("AutoIT Script Executed!!");
				SkySiteUtils.waitTill(20000);
				SkySiteUtils.waitForElement(driver, btnSubmitsubmittal, 30);
				try {
					File file = new File(tmpFileName);
					if (file.delete()) {
						Log.message(file.getName() + " is deleted!");
					} else {
						Log.message("Delete operation is failed.");
					}
				} catch (Exception e) {
					Log.message("Exception occured!!!" + e);

				}
				SkySiteUtils.waitTill(10000);
				//Attaching from project
				SkySiteUtils.waitForElement(driver, btnSubmitalDownload, 30);
				btnAddProjectFile.click();
				Log.message("Clicked on Project Files Tab.");
				SkySiteUtils.waitTill(2000);
				SkySiteUtils.waitForElement(driver, iconFolderWhileAttachFile, 30);
				iconFolderWhileAttachFile.click();
				Log.message("Clicked on Folder icon.");
				SkySiteUtils.waitTill(7000);
				SkySiteUtils.waitForElement(driver, txtSearchProjectFile, 30);
				txtSearchProjectFile.sendKeys(Search_FileName);
				Log.message("Search text entered.");
				SkySiteUtils.waitTill(2000);
				iconSearchProjectFiles.click();
				Log.message("Clicked on Search icon.");
				SkySiteUtils.waitTill(5000);
				SkySiteUtils.waitForElement(driver, chkboxSearchResults, 20);
				SkySiteUtils.waitTill(5000);
				chkboxSearchResults.click();
				SkySiteUtils.waitTill(3000);
				btnSubmitsubmittal.click();
				SkySiteUtils.waitTill(5000);
						
				/*SkySiteUtils.waitTill(20000); 
				tabMySubmittals.click();
				SkySiteUtils.waitTill(5000); 
				tabAllSubmittals.click();
				SkySiteUtils.waitTill(5000);*/
				SkySiteUtils.waitForElement(driver, btnSubmitalDownload, 120);
				SkySiteUtils.waitTill(5000);
				tabAllSubmittals.click();//temp
				SkySiteUtils.waitTill(5000);
				// SkySiteUtils.waitForElement(driver, notificationMsg, 20);
				// String NotifyMsg_SubmitalSubmit=notificationMsg.getText();
				// Log.message("Message displayed after submiting the submittal is: "+NotifyMsg_SubmitalSubmit);
				String SubmId_AfterSubmit = submIdUnderList.getText();
				Log.message("Sub ID After Submit is: " + SubmId_AfterSubmit);
				String Sub_StatusAfterSubmit = submSubmittedStatusUnderList.getText();
				Log.message("Status after submit a submittal is: "+ Sub_StatusAfterSubmit);
				if ((SubmId_AfterSubmit.equalsIgnoreCase(Submittal_Number))	&& (Sub_StatusAfterSubmit.equalsIgnoreCase("SUBMITTED"))) 
				{
					result1 = true;
					Log.message("Status changed successfully to submitted.");

					// Validations from 'My Submittal(s)' tab
					tabMySubmittals.click();
					Log.message("Clicked on my Submittals tab.");
					SkySiteUtils.waitTill(5000);
					String SubmId_UnderMySubmittalsTab = submIdUnderList.getText();
					Log.message("Sub ID Under My Submittals Tab is: "+ SubmId_UnderMySubmittalsTab);
					String Sub_StatusUnderMySubmittalsTab = submSubmittedStatusUnderList.getText();
					Log.message("Status Under My Submittals Tab is: "+ Sub_StatusUnderMySubmittalsTab);
					if ((SubmId_UnderMySubmittalsTab.equalsIgnoreCase(Submittal_Number))
							&& (Sub_StatusUnderMySubmittalsTab.equalsIgnoreCase("Submitted"))) 
					{
						result2 = true;
						Log.message("Expected Submittal is available under My Submittal(s) Tab.");
					} else {
						result2 = false;
						Log.message("Expected Submittal is NOT available under My Submittal(s) Tab.");
					}

				} 
				else 
				{
					result1 = false;
					Log.message("Failed to submit submittal.");
				}

			}

		}

		if ((result1 == true) && (result2 == true)) {
			Log.message("Submitting a Submittal to an employee is successfully.");
			return true;
		} else {
			Log.message("Submitting a Submittal to an employee is failed.");
			return false;
		}
	}

	/**
	 * Method written for download submittal report of version1 - V1 Scipted By:
	 * Naresh Babu
	 * @return
	 */

	@FindBy(css = "#download")
	WebElement btnDownloadSubmReport;

	public boolean Submittal_ReportDownload_V1(String Submittal_Number)
			throws IOException, InterruptedException {
		boolean result1 = false;
		boolean result2 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		SkySiteUtils.waitTill(5000);
		String SubmId_AllSubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_AllSubTab);
		if ((SubmId_AllSubTab.equalsIgnoreCase(Submittal_Number))
				&& (btnSubmitalDownload.isDisplayed())) {
			result1 = true;
			Log.message("Expected submittal available..you can Download the Report!!!");
			btnSubmitalDownload.click();
			Log.message("Clicked on Submittal Report Download.");
			SkySiteUtils.waitForElement(driver, btnYes_WithCoverPage, 30);
			SkySiteUtils.waitTill(2000);
			btnYes_WithCoverPage.click();
			Log.message("Clicked on Yes - download with cover page.");
			SkySiteUtils.waitTill(15000);// Download Page Load time

			// Navigating to the new browser window of Download
			String parentHandle = driver.getWindowHandle();// Getting Parent
			// window name
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle);// Focus on the newly opened
				// window
			}

			// Calling "Deleting download folder files" method
			String Sys_Download_Path = PropertyReader
					.getProperty("SysDownloadPath");
			Log.message("Download Path is: " + Sys_Download_Path);
			projectDashboardPage = new ProjectDashboardPage(driver).get();
			projectDashboardPage
					.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
			SkySiteUtils.waitTill(10000);

			SkySiteUtils.waitForElement(driver, btnDownloadSubmReport, 30);
			SkySiteUtils.waitTill(10000);
			// String Exp_DownloadFile =
			// driver.findElement(By.xpath(".//*[@id='pageContainer1']/xhtml:div[2]/xhtml:div[52]")).getAttribute("value");
			// Log.message("Expected Report download pdf file name is: "+Exp_DownloadFile);
			btnDownloadSubmReport.click();
			Log.message("Clicked on report download button.");

			SkySiteUtils.waitTill(30000);
			// After Download, checking whether Downloaded file is existed under
			// download folder or not
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) {
				if (file.isFile()) {
					ActualFilename = file.getName();// Getting File Names into a
					// variable
					Log.message("Actual File name is:" + ActualFilename);
					SkySiteUtils.waitTill(1000);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename.contentEquals(ActualFilename))
							&& (ActualFileSize != 0)) {
						Log.message("Downloaded Submittal report file is available in downloads!!!");
						result2 = true;
						break;
					}

				}

			}

		} else {
			result1 = false;
			Log.message("Expected submittal is NOT available!!!");
		}

		if ((result1 == true) && (result2 == true)) {
			Log.message("Download submittal report is successfully.");
			return true;
		} else {
			Log.message("Download submittal report is failed.");
			return false;
		}
	}

	/**
	 * Method written for selecting a Submittal icon from expected project.
	 * Scripted By : NARESH
	 * 
	 * @return
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws IOException
	 */

	@FindBy(css = "#aPrivateProjects")
	WebElement PrivateProjectsTab;

	public boolean SelectSubmit_Icon_For_ExpectProject(String Prj_Name)throws InterruptedException, AWTException, IOException 
	{
		SkySiteUtils.waitForElement(driver, PrivateProjectsTab, 30);
		//SkySiteUtils.waitTill(5000);
		//PrivateProjectsTab.click();
		//Log.message("Private Projects button has been clicked!!!");
		//SkySiteUtils.waitTill(5000);
		// Getting count of available projects
		int Avl_Projects_Count = 0;
		//List<WebElement> allElements = driver.findElements(By.xpath("//*[contains(@id, 'PName_')]"));
		//modified
		List<WebElement> allElements=driver.findElements(By.xpath("//div[@id='ProjectlistContainer']//div[@id='view']//div//div[@class='project-content']//h4"));
		for (WebElement Element : allElements) 
		{
			Avl_Projects_Count = Avl_Projects_Count + 1;
		}
		Log.message("Available private projects count is: "+ Avl_Projects_Count);
		SkySiteUtils.waitTill(5000);

		for (int i = 1; i <= Avl_Projects_Count; i++) 
		{
			//String Exp_ProjName = driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+i+"]/div/section[1]/h4")).getText();
			String Exp_ProjName = driver.findElement(By.xpath("//div[@id='ProjectlistContainer']//div[@id='view']//div["+i+"]//div[@class='project-content']//h4")).getText();
			// Validating - Expected project is selected or not
			if (Exp_ProjName.trim().contentEquals(Prj_Name.trim())) 
			{
				Log.message("Maching Project Found!!");
				//driver.findElement(By.xpath("html/body/div[1]/div[3]/div[3]/div/ul/li["+i+"]/div/section[3]/ul/li[2]/a/i")).click();
				//Log.message("Clicked on submittal icon of an expected project!!");
				//modified
				driver.findElement(By.xpath("//div[@id='ProjectlistContainer']//div[@id='view']//div["+i+"]//div[@class='project-content']//h4")).click();
				Log.message("Clicked on project");
		        SkySiteUtils.waitTill(4000);
		        WebElement projectmanagementicon=driver.findElement(By.xpath(".//*[@id='lirfiPunch']/a/i"));
		        JavascriptExecutor executor = (JavascriptExecutor)driver;
		        executor.executeScript("arguments[0].click();", projectmanagementicon);		       
		        Log.message("Project Management icon is clicked");
		        SkySiteUtils.waitTill(4000);
		        driver.findElement(By.xpath(".//*[@id='a_showSUBMITTALList']/span")).click();
		        Log.message("Submitted icon is clicked");
				//SkySiteUtils.waitForElement(driver, btnnewSubmittal, 30);
				SkySiteUtils.waitTill(7000);
				break;
			}

		}

		if (btnnewSubmittal.isDisplayed()) 
		{
			Log.message("Submittal icon of an expected project is selected successfully.");
			return true;
		} else {
			Log.message("Submittal icon of an expected project is Failed to select.");
			return false;
		}

	}

	/**
	 * Method written for Commenting and Approving the Submittal as an Approver
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Comment_Approve_Submittal_AsAnApprover(String Submittal_Number, String Approver_Comment,
			String Submittal_Name, String SubmComment_WhileSubmit)throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		SkySiteUtils.waitTill(5000);
		String SubmId_MySubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_MySubTab);
		String SubmName_MySubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_MySubTab);
		String SubStatus_MySubTab = submSubmittedStatusUnderList.getText();
		Log.message("Status after submit a submittal is: " + SubStatus_MySubTab);
		if ((SubmId_MySubTab.equalsIgnoreCase(Submittal_Number)) && (SubmName_MySubTab.equalsIgnoreCase(Submittal_Name))
				&& (SubStatus_MySubTab.equalsIgnoreCase("SUBMITTED"))) 
		{
			result1 = true;
			Log.message("Expected submittal available..you can Comment and approve!!!");
			submIdUnderList.click();
			Log.message("Clicked on expected submittal.");
			SkySiteUtils.waitForElement(driver, btnApprove, 50);//Newly Added Naresh
			
			SkySiteUtils.waitForElement(driver, txtCommentApprover, 30);
			SkySiteUtils.waitTill(5000);
			txtCommentApprover.sendKeys(Approver_Comment);
			btnAddComment.click();
			Log.message("Clicked on Add Comment button.");
			// Need to add notify message validation
			SkySiteUtils.waitTill(5000);
			String Approver_Comment_History = driver.findElement(By.xpath("(//span[@data-bind='html: Comment()'])[1]")).getText();
			Log.message("Approver Comment History is: "+ Approver_Comment_History);
			String Submitter_Comment_History = driver.findElement(By.xpath("(//span[@data-bind='html: Comment()'])[2]")).getText();
			Log.message("Submitter Comment History is: "+ Submitter_Comment_History);
			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", btnApprove);
			//btnApprove.click();
			SkySiteUtils.waitTill(2000);
			driver.findElement(By.xpath("(//a[contains(text(), 'Approve')])[1]")).click();
			Log.message("Clicked on Approved.");
			//SkySiteUtils.waitForElement(driver, btnYes_WanttoCreateSubmittal, 20);
			//btnYes_WanttoCreateSubmittal.click();
			// Need to add notify message validation
			SkySiteUtils.waitTill(10000);
			SkySiteUtils.waitForElement(driver, btnSubmitalDownload, 60);
			String SubmId_AfterApprove = submIdUnderList.getText();
			Log.message("Sub ID After Approve is: " + SubmId_AfterApprove);
			String Sub_StatusAfterApprove = submApprovedStatusUnderList.getText();
			Log.message("Status after Approve a submittal is: "+ Sub_StatusAfterApprove);
			if ((SubmId_AfterApprove.equalsIgnoreCase(Submittal_Number)) && (Sub_StatusAfterApprove.equalsIgnoreCase("Approved"))
					&& (Approver_Comment_History.equalsIgnoreCase(Approver_Comment)) 
					&& (Submitter_Comment_History.equalsIgnoreCase(SubmComment_WhileSubmit))) 
			{
				result2 = true;
				Log.message("Comment and Status changed successfully to Approved.");
				// Validations from 'All Submittal(s)' tab
				tabAllSubmittals.click();
				Log.message("Clicked on All Submittals tab.");
				SkySiteUtils.waitTill(5000);
				String SubmId_UnderAllSubmittalsTab = submIdUnderList.getText();
				Log.message("Sub ID Under All Submittals Tab is: "+ SubmId_UnderAllSubmittalsTab);
				String Sub_StatusUnderAllSubmittalsTab = submApprovedStatusUnderList.getText();
				Log.message("Status Under My Submittals Tab is: "+ Sub_StatusUnderAllSubmittalsTab);
				if ((SubmId_UnderAllSubmittalsTab.equalsIgnoreCase(Submittal_Number)) && (Sub_StatusUnderAllSubmittalsTab.equalsIgnoreCase("Approved")))
				{
					result3 = true;
					Log.message("Expected Submittal is available under All Submittal(s) Tab.");
				} else {
					result3 = false;
					Log.message("Expected Submittal is NOT available under All Submittal(s) Tab.");
				}

			} else {
				result2 = false;
				Log.message("Failed to Comment and approve the Submittal.");
			}

		} else {
			result1 = false;
			Log.message("Expected submittal NOT available!!!");
		}
		if ((result1 == true) && (result2 == true) && (result3 == true)) {
			Log.message("Comment and Approved of a Submittal as an Approver is successfully.");
			return true;
		} else {
			Log.message("Comment and Approved of a Submittal as an Approver is failed.");
			return false;
		}
	}

	
	/**
	 * Method written for Approving the Submittal as a 2nd Approver
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Approve_Submittal_AsA2ndApprover(String Submittal_Number,String Submittal_Name,int FinalApproveType)throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		SkySiteUtils.waitTill(5000);
		String SubmId_MySubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_MySubTab);
		String SubmName_MySubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_MySubTab);
		String SubStatus_MySubTab = submReviewedStatusUnderList.getText();
		Log.message("Status after submit a submittal is: " + SubStatus_MySubTab);
		if ((SubmId_MySubTab.equalsIgnoreCase(Submittal_Number))&& (SubmName_MySubTab.equalsIgnoreCase(Submittal_Name))
				&& (SubStatus_MySubTab.equalsIgnoreCase("Reviewed"))) 
		{
			result1 = true;
			Log.message("Expected submittal available..you can approve!!!");
			submIdUnderList.click();
			Log.message("Clicked on expected submittal.");
			SkySiteUtils.waitForElement(driver, btnApprove, 30);
			SkySiteUtils.waitTill(2000);
			btnApprove.click();
			SkySiteUtils.waitTill(2000);
			driver.findElement(By.xpath("(//a[contains(text(), 'Approve')])["+FinalApproveType+"]")).click();
			Log.message("Clicked on Approved as Noted.");
			//SkySiteUtils.waitForElement(driver, btnYes_WanttoCreateSubmittal,20);
			//btnYes_WanttoCreateSubmittal.click();
			// Need to add notify message validation
			SkySiteUtils.waitTill(10000);
			SkySiteUtils.waitForElement(driver, submApprovedStatusUnderList, 60);
			String SubmId_AfterApprove = submIdUnderList.getText();
			Log.message("Sub ID After Approve is: " + SubmId_AfterApprove);
			String Sub_StatusAfterApprove = submApprovedStatusUnderList.getText();
			Log.message("Status after Approve a submittal is: "+ Sub_StatusAfterApprove);
			if ((SubmId_AfterApprove.equalsIgnoreCase(Submittal_Number)) 
					&&(Sub_StatusAfterApprove.equalsIgnoreCase("Approved")||Sub_StatusAfterApprove.equalsIgnoreCase("APPROVED AS NOTED"))) 
			{
				result2 = true;
				Log.message("Status changed successfully to "+Sub_StatusAfterApprove);
				// Validations from 'All Submittal(s)' tab
				tabAllSubmittals.click();
				Log.message("Clicked on All Submittals tab.");
				SkySiteUtils.waitTill(10000);
				String SubmId_UnderAllSubmittalsTab = submIdUnderList.getText();
				Log.message("Sub ID Under All Submittals Tab is: "+ SubmId_UnderAllSubmittalsTab);
				String Sub_StatusUnderAllSubmittalsTab = submApprovedStatusUnderList.getText();
				Log.message("Status Under My Submittals Tab is: "+ Sub_StatusUnderAllSubmittalsTab);
				if ((SubmId_UnderAllSubmittalsTab.equalsIgnoreCase(Submittal_Number))
						&&(Sub_StatusAfterApprove.equalsIgnoreCase("Approved")||Sub_StatusAfterApprove.equalsIgnoreCase("APPROVED AS NOTED")))
				{
					result3 = true;
					Log.message("Expected Submittal is available under All Submittal(s) Tab.");
				} 
				else 
				{
					result3 = false;
					Log.message("Expected Submittal is NOT available under All Submittal(s) Tab.");
				}

			} 
			else 
			{
				result2 = false;
				Log.message("Failed to approve the Submittal.");
			}

		}
		else 
		{
			result1 = false;
			Log.message("Expected submittal NOT available!!!");
		}
		if ((result1 == true) && (result2 == true) && (result3 == true)) 
		{
			Log.message("Comment and Approved of a Submittal as an Approver is successfully.");
			return true;
		} else {
			Log.message("Comment and Approved of a Submittal as an Approver is failed.");
			return false;
		}
	}
	
	/**
	 * Method written for Approving the Submittal by select "No Exceptions Taken" as a 2nd Approver
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Approve_Submittal_BySelect_NoExceptionsTaken(String Submittal_Number,String Submittal_Name)throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		SkySiteUtils.waitTill(5000);
		String SubmId_MySubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_MySubTab);
		String SubmName_MySubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_MySubTab);
		String SubStatus_MySubTab = submReviewedStatusUnderList.getText();
		Log.message("Status after submit a submittal is: " + SubStatus_MySubTab);
		if ((SubmId_MySubTab.equalsIgnoreCase(Submittal_Number))&& (SubmName_MySubTab.equalsIgnoreCase(Submittal_Name))
				&& (SubStatus_MySubTab.equalsIgnoreCase("Reviewed"))) 
		{
			result1 = true;
			Log.message("Expected submittal available..you can approve!!!");
			submIdUnderList.click();
			Log.message("Clicked on expected submittal.");
			SkySiteUtils.waitForElement(driver, btnApprove, 30);
			SkySiteUtils.waitTill(5000);
			btnApprove.click();
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("//a[contains(text(), 'No Exceptions Taken')]")).click();
			Log.message("Clicked on Approved as No Exceptions Taken.");
			//SkySiteUtils.waitForElement(driver, btnYes_WanttoCreateSubmittal,20);
			//btnYes_WanttoCreateSubmittal.click();
			// Need to add notify message validation
			SkySiteUtils.waitTill(10000);
			SkySiteUtils.waitForElement(driver, submApprovedStatusUnderList, 60);
			String SubmId_AfterApprove = submIdUnderList.getText();
			Log.message("Sub ID After Approve is: " + SubmId_AfterApprove);
			String Sub_StatusAfterApprove = submApprovedStatusUnderList.getText();
			Log.message("Status after Approve a submittal is: "+ Sub_StatusAfterApprove);
			if ((SubmId_AfterApprove.equalsIgnoreCase(Submittal_Number))&&(Sub_StatusAfterApprove.equalsIgnoreCase("No Exceptions Taken"))) 
			{
				result2 = true;
				Log.message("Status changed successfully to "+Sub_StatusAfterApprove);
				// Validations from 'All Submittal(s)' tab
				tabAllSubmittals.click();
				Log.message("Clicked on All Submittals tab.");
				SkySiteUtils.waitTill(10000);
				String SubmId_UnderAllSubmittalsTab = submIdUnderList.getText();
				Log.message("Sub ID Under All Submittals Tab is: "+ SubmId_UnderAllSubmittalsTab);
				String Sub_StatusUnderAllSubmittalsTab = submApprovedStatusUnderList.getText();
				Log.message("Status Under My Submittals Tab is: "+ Sub_StatusUnderAllSubmittalsTab);
				if ((SubmId_UnderAllSubmittalsTab.equalsIgnoreCase(Submittal_Number))
						&&(Sub_StatusAfterApprove.equalsIgnoreCase("No Exceptions Taken")))
				{
					result3 = true;
					Log.message("Expected Submittal is available under All Submittal(s) Tab.");
				} 
				else 
				{
					result3 = false;
					Log.message("Expected Submittal is NOT available under All Submittal(s) Tab.");
				}

			} 
			else 
			{
				result2 = false;
				Log.message("Failed to approve the Submittal.");
			}

		}
		else 
		{
			result1 = false;
			Log.message("Expected submittal NOT available!!!");
		}
		if ((result1 == true) && (result2 == true) && (result3 == true)) 
		{
			Log.message("Approved of a Submittal as an Approver is successfully.");
			return true;
		} else {
			Log.message("Approved of a Submittal as an Approver is failed.");
			return false;
		}
	}

	/**
	 * Method written for Commenting and Approving the Submittal by select 2nd approver
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Comment_Approve_Submittal_BySelect2ndApprover(String Submittal_Number, String Approver_Comment,
			String Submittal_Name, String SubmComment_WhileSubmit,String Emp2_MailId,
			String Approver_ReassignComment,String FolderPath)throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		SkySiteUtils.waitTill(5000);
		String SubmId_MySubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_MySubTab);
		String SubmName_MySubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_MySubTab);
		String SubStatus_MySubTab = submSubmittedStatusUnderList.getText();
		Log.message("Status after submit a submittal is: " + SubStatus_MySubTab);
		
		Log.message("Submittal number is " +Submittal_Number);
		Log.message("Submittal name is " +Submittal_Name);
		if ((SubmId_MySubTab.equalsIgnoreCase(Submittal_Number))&& (SubmName_MySubTab.equalsIgnoreCase(Submittal_Name))
				&& (SubStatus_MySubTab.equalsIgnoreCase("SUBMITTED"))) 
		{
			result1 = true;
			Log.message("Expected submittal available..you can Comment and approve!!!");
			submIdUnderList.click();
			Log.message("Clicked on expected submittal.");
			SkySiteUtils.waitForElement(driver, txtCommentApprover, 30);
			SkySiteUtils.waitTill(5000);
			txtCommentApprover.sendKeys(Approver_Comment);
			btnAddComment.click();
			Log.message("Clicked on Add Comment button.");
			// Need to add notify message validation
			SkySiteUtils.waitTill(8000);
			String Approver_Comment_History = driver.findElement(By.xpath("(//span[@data-bind='html: Comment()'])[1]")).getText();
			Log.message("Approver Comment History is: "+ Approver_Comment_History);
			String Submitter_Comment_History = driver.findElement(By.xpath("(//span[@data-bind='html: Comment()'])[2]")).getText();
			Log.message("Submitter Comment History is: "+ Submitter_Comment_History);
			//Assigning to 2nd approver by comment and review
			SkySiteUtils.waitForElement(driver, iconEmailContact, 20);
			iconEmailContact.click();
			Log.message("Clicked on contact icon to select second approver.");
			SkySiteUtils.waitForElement(driver, btnCancelAddContact, 30);
			SkySiteUtils.waitTill(5000);
			txtUserSearchKeyword.sendKeys(Emp2_MailId);
	driver.findElement(By.xpath("//a[@data-bind='click: SearchClick' and @class='input-group-addon pw-contact-list-modal']")).click();
			SkySiteUtils.waitTill(10000);
			String Emp_SearchResults = driver.findElement(By.xpath("//a[@data-bind='text: Email()']")).getText();
			Log.message("Search results for employee is: " + Emp_SearchResults);
			if (Emp_SearchResults.equalsIgnoreCase(Emp2_MailId)) 
			{
				Log.message("Search functionality for contact is working.");
				chkboxEmpSelectWind.click();
				Log.message("Clicked on select checkbox of 2nd approver.");
				driver.findElement(By.xpath("//button[@data-bind='click: SelectContactEvent']")).click();
				Log.message("Clicked on select user button.");
				SkySiteUtils.waitForElement(driver, txtCommentAssignto, 30);
				txtCommentAssignto.sendKeys(Approver_ReassignComment);
				SkySiteUtils.waitTill(3000);
				//driver.findElement(By.xpath("//input[@type='file' and @name='qqfile']")).click();// Click on choose files
				driver.findElement(By.xpath(".//*[@id='btn-Load-attachment']")).click();
				Log.message(" add attachment button is clicked");
				SkySiteUtils.waitTill(5000);
				driver.findElement(By.xpath(".//*[@id='divAttachment']/div/div[2]/div/button")).click();
				Log.message("select file otion is clicked");
				SkySiteUtils.waitTill(10000);
				// Writing File names into a text file for using in AutoIT Script
				BufferedWriter output;
				randomFileName rn = new randomFileName();
				String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
				output = new BufferedWriter(new FileWriter(tmpFileName, true));

				String expFilename = null;
				File[] files = new File(FolderPath).listFiles();

				for (File file : files) 
				{
					if (file.isFile()) {
						expFilename = file.getName();// Getting File Names into a
						// variable
						Log.message("Expected File name is:" + expFilename);
						output.append('"' + expFilename + '"');
						output.append(" ");
						SkySiteUtils.waitTill(500);
					}
				}

				output.flush();
				output.close();

				// Executing .exe autoIt file
				String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
				Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
				Log.message("AutoIT Script Executed!!");
				SkySiteUtils.waitTill(20000);
				SkySiteUtils.waitForElement(driver, btnApprove, 30);
				try {
					File file = new File(tmpFileName);
					if (file.delete()) {
						Log.message(file.getName() + " is deleted!");
					} else {
						Log.message("Delete operation is failed.");
					}
				} catch (Exception e) {
					Log.message("Exception occured!!!" + e);

				}
				SkySiteUtils.waitTill(5000);
				btnApprove.click();
				Log.message("Clicked on Approve button.");
				//SkySiteUtils.waitForElement(driver, btnYes_WanttoCreateSubmittal,20);
				//btnYes_WanttoCreateSubmittal.click();
				// Need to add notify message validation
				SkySiteUtils.waitTill(10000);
				SkySiteUtils.waitForElement(driver, submReviewedStatusUnderList, 60);
				String SubmId_AfterApprove = submIdUnderList.getText();
				Log.message("Sub ID After Approve is: " + SubmId_AfterApprove);
				String Sub_StatusAfterApprove = submReviewedStatusUnderList.getText();
				Log.message("Status after Approve a submittal is: "+ Sub_StatusAfterApprove);
				if ((SubmId_AfterApprove.equalsIgnoreCase(Submittal_Number))&& (Sub_StatusAfterApprove.equalsIgnoreCase("Reviewed"))
					&& (Submitter_Comment_History.equalsIgnoreCase(SubmComment_WhileSubmit))
					&& (Approver_Comment_History.equalsIgnoreCase(Approver_Comment))) 
				{
					result2 = true;
					Log.message("Comment and Status changed successfully to Reviewed.");
					// Validations from 'All Submittal(s)' tab
					tabAllSubmittals.click();
					Log.message("Clicked on All Submittals tab.");
					SkySiteUtils.waitTill(10000);
					String SubmId_UnderAllSubmittalsTab = submIdUnderList.getText();
					Log.message("Sub ID Under All Submittals Tab is: "+ SubmId_UnderAllSubmittalsTab);
					String Sub_StatusUnderAllSubmittalsTab = submReviewedStatusUnderList.getText();
					Log.message("Status Under My Submittals Tab is: "+ Sub_StatusUnderAllSubmittalsTab);
					if ((SubmId_UnderAllSubmittalsTab.equalsIgnoreCase(Submittal_Number))
						&& (Sub_StatusUnderAllSubmittalsTab.equalsIgnoreCase("Reviewed"))) 
					{
						result3 = true;
						Log.message("Expected Submittal is available under All Submittal(s) Tab.");
					} 
					else 
					{
						result3 = false;
						Log.message("Expected Submittal is NOT available under All Submittal(s) Tab.");
					}

				} 
				else 
				{
					result2 = false;
					Log.message("Failed to Comment and approve the Submittal.");
				}
			}
		} 
		else 
		{
			result1 = false;
			Log.message("Expected submittal NOT available!!!");
		}
		if ((result1 == true) && (result2 == true) && (result3 == true)) 
		{
			Log.message("Comment and Reviewed as 1stApprover and assign to 2nd Approver is successfully.");
			return true;
		} 
		else 
		{
			Log.message("Comment and Reviewed as 1stApprover and assign to 2nd Approver is failed.");
			return false;
		}
	}

	/**
	 * Method written for Approving the Resubmitted Submittal by select 2nd approver
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Approve_ResubmittedSubmittal_BySelect2ndApprover(String Submittal_Number, String Submittal_Name, String Emp2_MailId,
			String Approver_ReassignComment)throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		SkySiteUtils.waitTill(5000);
		String SubmId_MySubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_MySubTab);
		String SubmName_MySubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_MySubTab);
		String SubStatus_MySubTab = submResubmitStatusUnderList.getText();
		Log.message("Status after Resubmit a submittal is: " + SubStatus_MySubTab);
		if ((SubmId_MySubTab.equalsIgnoreCase(Submittal_Number))&& (SubmName_MySubTab.equalsIgnoreCase(Submittal_Name))
				&& (SubStatus_MySubTab.equalsIgnoreCase("Resubmitted"))) 
		{
			result1 = true;
			Log.message("Expected submittal available..you can approve!!!");
			submIdUnderList.click();
			Log.message("Clicked on expected submittal.");
			//Assigning to 2nd approver by comment and review
			SkySiteUtils.waitForElement(driver, iconEmailContact, 20);
			iconEmailContact.click();
			Log.message("Clicked on contact icon to select second approver.");
			SkySiteUtils.waitForElement(driver, btnCancelAddContact, 30);
			SkySiteUtils.waitTill(5000);
			txtUserSearchKeyword.sendKeys(Emp2_MailId);
	driver.findElement(By.xpath("//a[@data-bind='click: SearchClick' and @class='input-group-addon pw-contact-list-modal']")).click();
			SkySiteUtils.waitTill(10000);
			String Emp_SearchResults = driver.findElement(By.xpath("//a[@data-bind='text: Email()']")).getText();
			Log.message("Search results for employee is: " + Emp_SearchResults);
			if (Emp_SearchResults.equalsIgnoreCase(Emp2_MailId)) 
			{
				Log.message("Search functionality for contact is working.");
				chkboxEmpSelectWind.click();
				Log.message("Clicked on select checkbox of 2nd approver.");
				driver.findElement(By.xpath("//button[@data-bind='click: SelectContactEvent']")).click();
				Log.message("Clicked on select user button.");
				SkySiteUtils.waitForElement(driver, txtCommentAssignto, 30);
				txtCommentAssignto.sendKeys(Approver_ReassignComment);
				SkySiteUtils.waitTill(5000);
				btnApprove.click();
				Log.message("Clicked on Approve button.");
				//SkySiteUtils.waitForElement(driver, btnYes_WanttoCreateSubmittal,20);
				//btnYes_WanttoCreateSubmittal.click();
				// Need to add notify message validation
				SkySiteUtils.waitTill(10000);
				SkySiteUtils.waitForElement(driver, submReviewedStatusUnderList, 60);
				String SubmId_AfterApprove = submIdUnderList.getText();
				Log.message("Sub ID After Approve is: " + SubmId_AfterApprove);
				String Sub_StatusAfterApprove = submReviewedStatusUnderList.getText();
				Log.message("Status after Approve a submittal is: "+ Sub_StatusAfterApprove);
				if ((SubmId_AfterApprove.equalsIgnoreCase(Submittal_Number))&& (Sub_StatusAfterApprove.equalsIgnoreCase("Reviewed"))) 
				{
					result2 = true;
					Log.message("Status changed successfully to Reviewed.");
					// Validations from 'All Submittal(s)' tab
					tabAllSubmittals.click();
					Log.message("Clicked on All Submittals tab.");
					SkySiteUtils.waitTill(10000);
					String SubmId_UnderAllSubmittalsTab = submIdUnderList.getText();
					Log.message("Sub ID Under All Submittals Tab is: "+ SubmId_UnderAllSubmittalsTab);
					String Sub_StatusUnderAllSubmittalsTab = submReviewedStatusUnderList.getText();
					Log.message("Status Under My Submittals Tab is: "+ Sub_StatusUnderAllSubmittalsTab);
					if ((SubmId_UnderAllSubmittalsTab.equalsIgnoreCase(Submittal_Number))
						&& (Sub_StatusUnderAllSubmittalsTab.equalsIgnoreCase("Reviewed"))) 
					{
						result3 = true;
						Log.message("Expected Submittal is available under All Submittal(s) Tab.");
					} 
					else 
					{
						result3 = false;
						Log.message("Expected Submittal is NOT available under All Submittal(s) Tab.");
					}

				} 
				else 
				{
					result2 = false;
					Log.message("Failed to approve the Submittal.");
				}
			}
		} 
		else 
		{
			result1 = false;
			Log.message("Expected submittal NOT available!!!");
		}
		if ((result1 == true) && (result2 == true) && (result3 == true)) 
		{
			Log.message("Reviewed as 1stApprover and assign to 2nd Approver is successfully.");
			return true;
		} 
		else 
		{
			Log.message("Reviewed as 1stApprover and assign to 2nd Approver is failed.");
			return false;
		}
	}
	
	
	/**
	 * Method written for Commenting and Rejecting the Submittal as an Approver
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Comment_Reject_Submittal_AsAnApprover(String Submittal_Number, String Approver_RejectComment,
			String Submittal_Name, String SubmComment_WhileSubmit)throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		SkySiteUtils.waitTill(5000);
		String SubmId_MySubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_MySubTab);
		String SubmName_MySubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_MySubTab);
		String SubStatus_MySubTab = submSubmittedStatusUnderList.getText();
		Log.message("Status after submit a submittal is: " + SubStatus_MySubTab);
		if ((SubmId_MySubTab.equalsIgnoreCase(Submittal_Number))&& (SubmName_MySubTab.equalsIgnoreCase(Submittal_Name))
				&& (SubStatus_MySubTab.equalsIgnoreCase("SUBMITTED"))) 
		{
			result1 = true;
			Log.message("Expected submittal available..you can Comment and Reject!!!");
			submIdUnderList.click();
			Log.message("Clicked on expected submittal.");
			SkySiteUtils.waitForElement(driver, btnReject, 30);
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, txtCommentApprover, 30);
			SkySiteUtils.waitTill(5000);
			txtCommentApprover.sendKeys(Approver_RejectComment);
			btnAddComment.click();
			Log.message("Clicked on Add Comment button.");
			// Need to add notify message validation
			SkySiteUtils.waitTill(8000);
			String Approver_Comment_History = driver.findElement(By.xpath("(//span[@data-bind='html: Comment()'])[1]")).getText();
			Log.message("Approver Comment History is: "+ Approver_Comment_History);
			String Submitter_Comment_History = driver.findElement(By.xpath("(//span[@data-bind='html: Comment()'])[2]")).getText();
			Log.message("Submitter Comment History is: "+ Submitter_Comment_History);
			btnReject.click();
			Log.message("Clicked on Reject.");
			//SkySiteUtils.waitForElement(driver, btnYes_WanttoCreateSubmittal, 20);
			//btnYes_WanttoCreateSubmittal.click();
			// Need to add notify message validation
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, btnSubmitalDownload, 60);

			String SubmId_AfterReject = submIdUnderList.getText();
			Log.message("Sub ID After Reject is: " + SubmId_AfterReject);
			String Sub_StatusAfterReject = submRejectStatusUnderList.getText();
			Log.message("Status after Reject a submittal is: "+ Sub_StatusAfterReject);
			if ((SubmId_AfterReject.equalsIgnoreCase(Submittal_Number)) && (Sub_StatusAfterReject.equalsIgnoreCase("Rejected"))
					&& (Approver_Comment_History.equalsIgnoreCase(Approver_RejectComment))
					&& (Submitter_Comment_History.equalsIgnoreCase(SubmComment_WhileSubmit))) 
			{
				result2 = true;
				Log.message("Comment and Status changed successfully to Rejected.");
				// Validations from 'All Submittal(s)' tab
				SkySiteUtils.waitTill(5000);
				tabAllSubmittals.click();
				Log.message("Clicked on All Submittals tab.");
				SkySiteUtils.waitTill(3000);
				String SubmId_UnderAllSubmittalsTab = submIdUnderList.getText();
				Log.message("Sub ID Under All Submittals Tab is: "+ SubmId_UnderAllSubmittalsTab);
				String Sub_StatusUnderAllSubmittalsTab = submRejectStatusUnderList.getText();
				Log.message("Status Under My Submittals Tab is: "+ Sub_StatusUnderAllSubmittalsTab);
				if ((SubmId_UnderAllSubmittalsTab.equalsIgnoreCase(Submittal_Number))
						&& (Sub_StatusUnderAllSubmittalsTab.equalsIgnoreCase("Rejected"))) 
				{
					result3 = true;
					Log.message("Expected Submittal is available under All Submittal(s) Tab.");
				} else {
					result3 = false;
					Log.message("Expected Submittal is NOT available under All Submittal(s) Tab.");
				}

			} else {
				result2 = false;
				Log.message("Failed to Comment and Reject the Submittal.");
			}

		} else {
			result1 = false;
			Log.message("Expected submittal NOT available!!!");
		}
		if ((result1 == true) && (result2 == true) && (result3 == true)) {
			Log.message("Comment and Reject of a Submittal as an Approver is successfully.");
			return true;
		} else {
			Log.message("Comment and Reject of a Submittal as an Approver is failed.");
			return false;
		}
	}

	/**
	 * Method written for Commenting and Rejecting the Submittal as an Approver2
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Comment_Reject_Submittal_AsAnApprover2(String Submittal_Number, String Approver_RejectComment,
			String Submittal_Name, String Approver_ReassignComment)throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		SkySiteUtils.waitTill(5000);
		String SubmId_MySubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_MySubTab);
		String SubmName_MySubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_MySubTab);
		String SubStatus_MySubTab = submReviewedStatusUnderList.getText();
		Log.message("Status after submit a submittal is: " + SubStatus_MySubTab);
		if ((SubmId_MySubTab.equalsIgnoreCase(Submittal_Number))&& (SubmName_MySubTab.equalsIgnoreCase(Submittal_Name))
				&& (SubStatus_MySubTab.equalsIgnoreCase("Reviewed"))) 
		{
			result1 = true;
			Log.message("Expected submittal available..you can Comment and Reject!!!");
			submIdUnderList.click();
			Log.message("Clicked on expected submittal.");
			
			SkySiteUtils.waitForElement(driver, txtCommentApprover, 30);
			SkySiteUtils.waitTill(5000);
			txtCommentApprover.sendKeys(Approver_RejectComment);
			btnAddComment.click();
			Log.message("Clicked on Add Comment button.");
			// Need to add notify message validation
			SkySiteUtils.waitTill(8000);
			String Approver_Comment_History = driver.findElement(By.xpath("(//span[@data-bind='html: Comment()'])[1]")).getText();
			Log.message("Approver Comment History is: "+ Approver_Comment_History);
			String Submitter_Comment_History = driver.findElement(By.xpath("(//span[@data-bind='html: Comment()'])[2]")).getText();
			Log.message("Submitter Comment History is: "+ Submitter_Comment_History);
			
			SkySiteUtils.waitForElement(driver, btnReject, 30);
			btnReject.click();
			SkySiteUtils.waitTill(2000);
			Log.message("Clicked on Reject button.");
			//SkySiteUtils.waitForElement(driver, btnYes_WanttoCreateSubmittal, 20);
			//btnYes_WanttoCreateSubmittal.click();
			// Need to add notify message validation
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, submRejectStatusUnderList, 60);

			String SubmId_AfterReject = submIdUnderList.getText();
			Log.message("Sub ID After Reject is: " + SubmId_AfterReject);
			String Sub_StatusAfterReject = submRejectStatusUnderList.getText();
			Log.message("Status after Reject a submittal is: "+ Sub_StatusAfterReject);
			//&& (Submitter_Comment_History.equalsIgnoreCase(Approver_ReassignComment))&& (Approver_Comment_History.equalsIgnoreCase(Approver_RejectComment))
			if ((SubmId_AfterReject.equalsIgnoreCase(Submittal_Number)) && (Sub_StatusAfterReject.equalsIgnoreCase("Rejected"))
					&& (Submitter_Comment_History.equalsIgnoreCase(Approver_ReassignComment)) 
					&& (Approver_Comment_History.equalsIgnoreCase(Approver_RejectComment))) 
			{
				result2 = true;
				Log.message("Comment and Status changed successfully to Rejected.");
				// Validations from 'All Submittal(s)' tab
				SkySiteUtils.waitTill(5000);
				tabAllSubmittals.click();
				Log.message("Clicked on All Submittals tab.");
				SkySiteUtils.waitTill(5000);
				String SubmId_UnderAllSubmittalsTab = submIdUnderList.getText();
				Log.message("Sub ID Under All Submittals Tab is: "+ SubmId_UnderAllSubmittalsTab);
				String Sub_StatusUnderAllSubmittalsTab = submRejectStatusUnderList.getText();
				Log.message("Status Under My Submittals Tab is: "+ Sub_StatusUnderAllSubmittalsTab);
				if ((SubmId_UnderAllSubmittalsTab.equalsIgnoreCase(Submittal_Number))
						&& (Sub_StatusUnderAllSubmittalsTab.equalsIgnoreCase("Rejected"))) 
				{
					result3 = true;
					Log.message("Expected Submittal is available under All Submittal(s) Tab.");
				} 
				else 
				{
					result3 = false;
					Log.message("Expected Submittal is NOT available under All Submittal(s) Tab.");
				}

			} 
			else 
			{
				result2 = false;
				Log.message("Failed to Comment and Reject the Submittal.");
			}

		} 
		else 
		{
			result1 = false;
			Log.message("Expected submittal NOT available!!!");
		}
		if ((result1 == true) && (result2 == true) && (result3 == true)) 
		{
			Log.message("Comment and Reject of a Submittal as an Approver2 is successfully.");
			return true;
		} 
		else 
		{
			Log.message("Comment and Reject of a Submittal as an Approver2 is failed.");
			return false;
		}
	}
	
	
	
	/**
	 * Method written for Commenting and Reassign the Submittal to other employee
	 * Scipted By: Naresh Babu
	 * @return
	 */

	public boolean Comment_Reassign_Submittal_ToOtheremployee(String Submittal_Number, String Submittal_Name,
			String Approver_ReassignComment, String Emp2_MailId, String Emp2_Name) throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		SkySiteUtils.waitTill(5000);
		String SubmId_MySubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_MySubTab);
		String SubmName_MySubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_MySubTab);
		String SubStatus_MySubTab = submSubmittedStatusUnderList.getText();
		Log.message("Status after submit a submittal is: " + SubStatus_MySubTab);
		if ((SubmId_MySubTab.equalsIgnoreCase(Submittal_Number))
				&& (SubmName_MySubTab.equalsIgnoreCase(Submittal_Name))
				&& (SubStatus_MySubTab.equalsIgnoreCase("SUBMITTED"))) {
			result1 = true;
			Log.message("Expected submittal available..you can Comment and Reassign!!!");
			submIdUnderList.click();
			Log.message("Clicked on expected submittal.");
			SkySiteUtils.waitForElement(driver, btnReassign, 30);
			
			SkySiteUtils.waitForElement(driver, txtCommentApprover, 30);
			SkySiteUtils.waitTill(5000);
			txtCommentApprover.sendKeys(Approver_ReassignComment);
			btnAddComment.click();
			Log.message("Clicked on Add Comment button.");
			// Need to add notify message validation
			SkySiteUtils.waitTill(8000);
			String Approver_Comment_History = driver.findElement(By.xpath("(//span[@data-bind='html: Comment()'])[1]")).getText();
			Log.message("Approver Comment History is: "+ Approver_Comment_History);
			SkySiteUtils.waitTill(3000);
			btnReassign.click();
			Log.message("Clicked on Reassign.");
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, btnContactsReassign, 20);
			btnContactsReassign.click();
			Log.message("Clicked on contact icon of reassign.");
			SkySiteUtils.waitForElement(driver, btnCancelAddContact, 30);
			SkySiteUtils.waitTill(5000);
			txtUserSearchKeyword.sendKeys(Emp2_MailId);
			driver.findElement(By.xpath("//a[@data-bind='click: SearchClick' and @class='input-group-addon pw-contact-list-modal']")).click();
			SkySiteUtils.waitTill(5000);
			String Emp_SearchResults = driver.findElement(By.xpath("//a[@data-bind='text: Email()']")).getText();
			Log.message("Search results for employee is: " + Emp_SearchResults);
			if (Emp_SearchResults.equalsIgnoreCase(Emp2_MailId)) 
			{
				Log.message("Search functionality for contact is working.");
				chkboxEmpSelectWind.click();
				SkySiteUtils.waitTill(2000);
				driver.findElement(By.xpath("//button[@data-bind='click: SelectContactEvent']")).click();
				Log.message("Clicked on select user button.");
				SkySiteUtils.waitForElement(driver, btnSaveReassign, 30);
				btnSaveReassign.click();
				Log.message("Clicked on save reassign button.");
				//SkySiteUtils.waitForElement(driver,	btnYes_WanttoCreateSubmittal, 20);
				//btnYes_WanttoCreateSubmittal.click();
				// Need to add notify message validation
				SkySiteUtils.waitTill(10000);
				SkySiteUtils.waitForElement(driver, btnSubmitalDownload, 30);
				String SubmId_AfterReject = submIdUnderList.getText();
				Log.message("Sub ID After Reject is: " + SubmId_AfterReject);
				String Sub_StatusAfterReject = submSubmittedStatusUnderList.getText();
				Log.message("Status after Reject a submittal is: "+ Sub_StatusAfterReject);
				if ((SubmId_AfterReject.equalsIgnoreCase(Submittal_Number))	&& (Sub_StatusAfterReject.equalsIgnoreCase("SUBMITTED"))
						&& (Approver_Comment_History.equalsIgnoreCase(Approver_ReassignComment))) 
				{
					result2 = true;
					Log.message("Comment and Reassign successfully to other Employee.");
					// Validations After Reassign the submittal
					submIdUnderList.click();
					Log.message("Clicked on expected submittal.");
					SkySiteUtils.waitForElement(driver, btnApproveDisabled, 30);
					SkySiteUtils.waitTill(5000);
					String UserAction_Reassign = driver.findElement(By.xpath("(//span[@data-bind='html: Action()'])[1]")).getText();
					Log.message("User Action after Reassign from history is: " +UserAction_Reassign);
                    Log.message("Reassigned submittal to " +Emp2_Name);
            		SkySiteUtils.waitTill(5000);
					if ((btnApproveDisabled.isDisplayed()) && (btnRejectDisabled.isDisplayed())	&& (btnReassignDisabled.isDisplayed())
							&&  (UserAction_Reassign.contains(Emp2_Name))) 
					{
						result3 = true;
						Log.message("All the buttons from the approver side is disabled after Reassign.");
						btnCancelSubmittalDetailsWind.click();
						Log.message("Clicked on cancel button of submittal details window");
					} else {
						result3 = false;
						Log.message("All the buttons from the approver side still enabled after Reassign.");
					}

				}

			} else {
				result2 = false;
				Log.message("Failed to Comment and Reassign the Submittal.");
			}

		} else {
			result1 = false;
			Log.message("Expected submittal NOT available!!!");
		}
		if ((result1 == true) && (result2 == true) && (result3 == true)) {
			Log.message("Comment and Reassign of a Submittal is successfully.");
			return true;
		} else {
			Log.message("Comment and Reassign of a Submittal is failed.");
			return false;
		}
	}

	/**
	 * Method written for Resubmit the Submittal as a Submitter 
	 * Scipted By: Naresh Babu
	 * @return
	 */

	public boolean Resubmit_Submittal_AsASubmitter(String Submittal_Number,	String Submittal_Name, String Approver_Name, String Approver_Mail,
			String Resubmit_Comment, String FolderPath) throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		SkySiteUtils.waitTill(5000);
		String SubmId_MySubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_MySubTab);
		String SubmName_MySubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_MySubTab);
		String SubStatus_MySubTab = submRejectStatusUnderList.getText();
		Log.message("Status after Reject the Submittal is: "+ SubStatus_MySubTab);
		String Last_Activity_By = driver.findElement(By.xpath(".//*[@id='ulSubmittalList']/li[1]/div[2]/div[2]/div/h4/span")).getText();
		Log.message("Submittal rejected by: " + Last_Activity_By);
		SkySiteUtils.waitTill(5000);
		Log.message("The number is " +Submittal_Number);
		Log.message("The name is " +Submittal_Name);
		Log.message("The approver name is " +Approver_Name);
		if ((SubmId_MySubTab.equalsIgnoreCase(Submittal_Number)) && (SubmName_MySubTab.equalsIgnoreCase(Submittal_Name))
				&& (Last_Activity_By.contains(Approver_Name)) && (SubStatus_MySubTab.equalsIgnoreCase("REJECTED"))) 
		{
			result1 = true;
			Log.message("Expected submittal available..you can Resubmit!!!");
			submIdUnderList.click();
			Log.message("Clicked on expected submittal.");
			SkySiteUtils.waitForElement(driver, btnSubmitsubmittal, 30);
			SkySiteUtils.waitTill(5000);
			String AutoSelectOf_Approver = txtCompanyAddRecipient.getAttribute("value");
			Log.message("Auto select of approver mail id is: "+ AutoSelectOf_Approver);
			txtSubmittalComment.sendKeys(Resubmit_Comment);
			Log.message("Entered Comment while resubmit.");
			SkySiteUtils.waitTill(3000);
			
			//driver.findElement(By.xpath("//input[@type='file' and @name='qqfile']")).click();// Click on choose files
			//modified
			driver.findElement(By.xpath(".//*[@id='btn-Load-attachment']")).click();
			Log.message(" add attachment button is clicked");
			SkySiteUtils.waitTill(3000);
			driver.findElement(By.xpath(".//*[@id='upload-file-attach']/div/div[2]/div/button")).click();
			Log.message("select file option is clicked");
		//JavascriptExecutor executor = (JavascriptExecutor)driver;
		//executor.executeScript("arguments[0].click();", selectfileoption);
	
			SkySiteUtils.waitTill(5000);
			//SkySiteUtils.waitTill(8000);
			// Writing File names into a text file for using in AutoIT Script
			BufferedWriter output;
			randomFileName rn = new randomFileName();
			String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
			output = new BufferedWriter(new FileWriter(tmpFileName, true));

			String expFilename = null;
			File[] files = new File(FolderPath).listFiles();

			for (File file : files) {
				if (file.isFile()) {
					expFilename = file.getName();// Getting File Names into a
					// variable
					Log.message("Expected File name is:" + expFilename);
					output.append('"' + expFilename + '"');
					output.append(" ");
					SkySiteUtils.waitTill(500);
				}
			}

			output.flush();
			output.close();

			// Executing .exe autoIt file
			String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
			Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
			Log.message("AutoIT Script Executed!!");
			SkySiteUtils.waitTill(25000);
			SkySiteUtils.waitForElement(driver, btnSubmitsubmittal, 30);
			try {
				File file = new File(tmpFileName);
				if (file.delete()) {
					Log.message(file.getName() + " is deleted!");
				} else {
					Log.message("Delete operation is failed.");
				}
			} catch (Exception e) {
				Log.message("Exception occured!!!" + e);

			}
			SkySiteUtils.waitTill(10000);
			btnSubmitsubmittal.click();
			Log.message("Clicked on Submit button");
			SkySiteUtils.waitTill(20000);
			SkySiteUtils.waitForElement(driver, submResubmitStatusUnderList, 60);
			SkySiteUtils.waitTill(5000);

			String SubmId_AfterResubmit = submIdUnderList.getText();
			Log.message("Sub ID After Resubmit is: " + SubmId_AfterResubmit);
			String Sub_StatusAfterResubmit = submResubmitStatusUnderList.getText();
			Log.message("Status after Resubmit a submittal is: "+ Sub_StatusAfterResubmit);
			if ((SubmId_AfterResubmit.equalsIgnoreCase(Submittal_Number)) && (Sub_StatusAfterResubmit.equalsIgnoreCase("RESUBMITTED"))
					&& (AutoSelectOf_Approver.equalsIgnoreCase(Approver_Mail))) 
			{
				result2 = true;
				Log.message("Comment and Status changed successfully to Resubmitted.");
				// Validations from 'All Submittal(s)' tab
				SkySiteUtils.waitTill(5000);
				tabAllSubmittals.click();
				Log.message("Clicked on All Submittals tab.");
				SkySiteUtils.waitTill(2000);
				String SubmId_UnderAllSubmittalsTab = submIdUnderList.getText();
				Log.message("Sub ID Under All Submittals Tab is: "+ SubmId_UnderAllSubmittalsTab);
				String Sub_StatusUnderAllSubmittalsTab = submResubmitStatusUnderList.getText();
				Log.message("Status Under My Submittals Tab is: "+ Sub_StatusUnderAllSubmittalsTab);
				if ((SubmId_UnderAllSubmittalsTab.equalsIgnoreCase(Submittal_Number))
						&& (Sub_StatusUnderAllSubmittalsTab.equalsIgnoreCase("RESUBMITTED"))) 
				{
					result3 = true;
					Log.message("Expected Submittal is available under All Submittal(s) Tab.");
				} else {
					result3 = false;
					Log.message("Expected Submittal is NOT available under All Submittal(s) Tab.");
				}

			} else {
				result2 = false;
				Log.message("Failed to Comment and Resubmitted the Submittal.");
			}

		} else {
			result1 = false;
			Log.message("Expected submittal NOT available!!!");
		}
		if ((result1 == true) && (result2 == true) && (result3 == true)) {
			Log.message("Comment and Resubmitted of a Submittal is successfully.");
			return true;
		} else {
			Log.message("Comment and Resubmitted of a Submittal is failed.");
			return false;
		}
	}

	/**
	 * Method written for Approver1 will Review and assign to Approver2
	 * Scipted By: Naresh Babu
	 * @return
	 */

	public boolean A1WillReview_AgainAssaignBackToA2_AsA1(String Submittal_Number,String Submittal_Name,String Approver_Name,String Approver_Mail) throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		SkySiteUtils.waitTill(5000);
		String SubmId_MySubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_MySubTab);
		String SubmName_MySubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_MySubTab);
		String SubStatus_MySubTab = submRejectStatusUnderList.getText();
		Log.message("Status after Reject the Submittal is: "+ SubStatus_MySubTab);
		String Last_Activity_By = driver.findElement(By.xpath(".//*[@id='ulSubmittalList']/li[1]/div[2]/div[2]/div/h4/span")).getText();
		Log.message("Submittal rejected by: " + Last_Activity_By);
		
		if ((SubmId_MySubTab.equalsIgnoreCase(Submittal_Number)) && (SubmName_MySubTab.equalsIgnoreCase(Submittal_Name))
				&& (Last_Activity_By.contains(Approver_Name)) && (SubStatus_MySubTab.equalsIgnoreCase("Rejected"))) 
		{
			result1 = true;
			Log.message("Expected submittal available..you can Resubmit!!!");
			submIdUnderList.click();
			Log.message("Clicked on expected submittal.");
			SkySiteUtils.waitForElement(driver, btnApprove, 30);
			SkySiteUtils.waitTill(5000);
			String AutoSelectOf_Approver = txtSubmittalToRecipient.getAttribute("value");
			Log.message("Auto select of approver2 mail id is: "+ AutoSelectOf_Approver);
			btnApprove.click();
			Log.message("Clicked on Approve button.");
			//SkySiteUtils.waitForElement(driver, btnYes_WanttoCreateSubmittal,20);
			//btnYes_WanttoCreateSubmittal.click();
			// Need to add notify message validation
			SkySiteUtils.waitTill(10000);
			SkySiteUtils.waitForElement(driver, submReviewedStatusUnderList, 60);

			String SubmId_AfterResubmit = submIdUnderList.getText();
			Log.message("Sub ID After Assign to A2 is: " + SubmId_AfterResubmit);
			String Sub_StatusAfterResubmit = submReviewedStatusUnderList.getText();
			Log.message("Status after Assign to A2 is: "+ Sub_StatusAfterResubmit);
			if ((SubmId_AfterResubmit.equalsIgnoreCase(Submittal_Number)) && (Sub_StatusAfterResubmit.equalsIgnoreCase("Reviewed"))
					&& (AutoSelectOf_Approver.equalsIgnoreCase(Approver_Mail))) 
			{
				result2 = true;
				Log.message("A1 Assign to A2 and Status changed successfully to Reviewed.");
				// Validations from 'All Submittal(s)' tab
				tabAllSubmittals.click();
				Log.message("Clicked on All Submittals tab.");
				SkySiteUtils.waitTill(10000);
				String SubmId_UnderAllSubmittalsTab = submIdUnderList.getText();
				Log.message("Sub ID Under All Submittals Tab is: "+ SubmId_UnderAllSubmittalsTab);
				String Sub_StatusUnderAllSubmittalsTab = submReviewedStatusUnderList.getText();
				Log.message("Status Under My Submittals Tab is: "+ Sub_StatusUnderAllSubmittalsTab);
				if ((SubmId_UnderAllSubmittalsTab.equalsIgnoreCase(Submittal_Number))&&(Sub_StatusUnderAllSubmittalsTab.equalsIgnoreCase("Reviewed"))) 
				{
					result3 = true;
					Log.message("Expected Submittal is available under All Submittal(s) Tab.");
				} else {
					result3 = false;
					Log.message("Expected Submittal is NOT available under All Submittal(s) Tab.");
				}

			} 
			else 
			{
				result2 = false;
				Log.message("Failed to Assign to Approver2.");
			}

		}
		else 
		{
			result1 = false;
			Log.message("Expected submittal NOT available!!!");
		}
		if ((result1 == true) && (result2 == true) && (result3 == true)) 
		{
			Log.message("Review and assign back to Approver2 is successfully.");
			return true;
		} else {
			Log.message("Review and assign back to Approver2 is failed.");
			return false;
		}
	}

	
	
	/**
	 * Method written for Approving the Resubmitted Submittal as an Approver
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public boolean Approve_ResubmittedSubmittal_AsAnApprover(String Submittal_Number, String Approver_Comment,
			String Submittal_Name, String Resubmit_Comment) throws IOException 
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;

		SkySiteUtils.waitForElement(driver, submIdUnderList, 30);
		String SubmId_MySubTab = submIdUnderList.getText();
		Log.message("Sub ID is: " + SubmId_MySubTab);
		String SubmName_MySubTab = submNameUnderList.getText();
		Log.message("Sub Name is: " + SubmName_MySubTab);
		String SubStatus_MySubTab = submResubmitStatusUnderList.getText();
		Log.message("Status after Re-submit a submittal is: " + SubStatus_MySubTab);
		if ((SubmId_MySubTab.equalsIgnoreCase(Submittal_Number)) && (SubmName_MySubTab.equalsIgnoreCase(Submittal_Name))
				&& (SubStatus_MySubTab.equalsIgnoreCase("Resubmitted"))) 
		{
			result1 = true;
			Log.message("Expected submittal available..you can Comment and approve!!!");
			submIdUnderList.click();
			Log.message("Clicked on expected submittal.");
			SkySiteUtils.waitForElement(driver, btnApprove, 30);
			
			SkySiteUtils.waitForElement(driver, txtCommentApprover, 30);
			SkySiteUtils.waitTill(5000);
			txtCommentApprover.sendKeys(Approver_Comment);
			btnAddComment.click();
			Log.message("Clicked on Add Comment button.");
			// Need to add notify message validation
			SkySiteUtils.waitTill(10000);
			String Approver_Comment_History = driver.findElement(By.xpath("(//span[@data-bind='html: Comment()'])[1]")).getText();
			Log.message("Approver Comment History is: "+ Approver_Comment_History);
			String Submitter_Comment_History = driver.findElement(By.xpath("(//span[@data-bind='html: Comment()'])[2]")).getText();
			Log.message("Submitter Comment History is: "+ Submitter_Comment_History);
			
			//btnApprove.click();
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", btnApprove);
			SkySiteUtils.waitTill(2000);
			driver.findElement(By.xpath("//a[contains(text(), 'Approved')]")).click();//Newly Added
			Log.message("Clicked on Approved.");
			//SkySiteUtils.waitForElement(driver, btnYes_WanttoCreateSubmittal, 20);
			//btnYes_WanttoCreateSubmittal.click();
			// Need to add notify message validation
			SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, btnSubmitalDownload, 60);
			String SubmId_AfterApprove = submIdUnderList.getText();
			Log.message("Sub ID After Approve is: " + SubmId_AfterApprove);
			String Sub_StatusAfterApprove = submApprovedStatusUnderList.getText();
			Log.message("Status after Approve a submittal is: "+ Sub_StatusAfterApprove);
			SkySiteUtils.waitTill(5000);
			if ((SubmId_AfterApprove.equalsIgnoreCase(Submittal_Number)) && (Sub_StatusAfterApprove.equalsIgnoreCase("Approved"))
					&& (Approver_Comment_History.equalsIgnoreCase(Approver_Comment))
					&& (Submitter_Comment_History.equalsIgnoreCase(Resubmit_Comment))) 
			{
				result2 = true;
				Log.message("Comment and Status changed successfully to Approved.");
				// Validations from 'All Submittal(s)' tab
				//tabAllSubmittals.click();
				JavascriptExecutor executor1 = (JavascriptExecutor)driver;
				executor1.executeScript("arguments[0].click();", tabAllSubmittals);
				Log.message("Clicked on All Submittals tab.");
				SkySiteUtils.waitTill(10000);
				String SubmId_UnderAllSubmittalsTab = submIdUnderList.getText();
				Log.message("Sub ID Under All Submittals Tab is: "+ SubmId_UnderAllSubmittalsTab);
				String Sub_StatusUnderAllSubmittalsTab = submApprovedStatusUnderList.getText();
				Log.message("Status Under My Submittals Tab is: "+ Sub_StatusUnderAllSubmittalsTab);
				if ((SubmId_UnderAllSubmittalsTab.equalsIgnoreCase(Submittal_Number))
						&& (Sub_StatusUnderAllSubmittalsTab.equalsIgnoreCase("Approved"))) 
				{
					result3 = true;
					Log.message("Expected Submittal is available under All Submittal(s) Tab.");
				} else {
					result3 = false;
					Log.message("Expected Submittal is NOT available under All Submittal(s) Tab.");
				}

			} else {
				result2 = false;
				Log.message("Failed to Comment and approve the Submittal.");
			}

		} else {
			result1 = false;
			Log.message("Expected submittal NOT available!!!");
		}
		if ((result1 == true) && (result2 == true) && (result3 == true)) {
			Log.message("Comment and Approved of a Submittal as an Approver is successfully.");
			return true;
		} else {
			Log.message("Comment and Approved of a Submittal as an Approver is failed.");
			return false;
		}
	}

	/**
	 * Method written for Import submittal and validate Scipted By: Naresh Babu
	 * 
	 * @return
	 */

	@FindBy(css = ".import-submittal")
	WebElement btnImportSubmittal;

	@FindBy(css = "#btnSelectFileNext")
	WebElement btnSelectFileNext;

	@FindBy(css = "#btnSubmittalImportMap")
	WebElement btnAddWorkflow;

	@FindBy(xpath = "//div[@class='sys-column' and @data-syscolumnindex='0']")
	WebElement headerSubNumber;

	@FindBy(xpath = "//div[@class='sys-column' and @data-syscolumnindex='1']")
	WebElement headerSubName;

	@FindBy(xpath = "//div[@class='sys-column' and @data-syscolumnindex='2']")
	WebElement headerSubDueDate;

	@FindBy(xpath = "//div[@class='sys-column' and @data-syscolumnindex='3']")
	WebElement headerSubType;

	@FindBy(css = "#btnImportSubmittal")
	WebElement btnApplyImportSubmittal;

	@FindBy(css = ".btn.btn-primary.tabindex")
	WebElement btnFinalApplyImportSubmittal;

	@FindBy(css = "#dvSuccess")
	WebElement successMsgImportSuccessfully;

	@FindBy(xpath = "//button[@class='btn btn-primary' and @type='button']")
	WebElement btnDoneImportSubmital;

	public boolean Import_Submital_AndValidate(String Submit_Import_FilePath,String WF_Template_Name) throws IOException 
	{

		boolean result1 = false;
		boolean result2 = false;
		SkySiteUtils.waitForElement(driver, btnCreateSubmittal, 20);
		btnImportSubmittal.click();
		Log.message("Clicked on Import Submittal Items button.");
		SkySiteUtils.waitForElement(driver, btnSelectFileNext, 20);
		SkySiteUtils.waitTill(15000);
		driver.findElement(By.xpath("//input[@type='file' and @name='qqfile']")).click();
		// Click on choose files
		SkySiteUtils.waitTill(10000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));

		String expFilename = null;
		File[] files = new File(Submit_Import_FilePath).listFiles();

		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a
				// variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(500);
			}
		}

		output.flush();
		output.close();

		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path +" "+Submit_Import_FilePath +" "+ tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(20000);
		SkySiteUtils.waitForElement(driver, btnSelectFileNext, 30);
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);

		}
		SkySiteUtils.waitTill(10000);
		btnSelectFileNext.click();
		Log.message("Clicked on Next button from Import Submittal window.");
		SkySiteUtils.waitForElement(driver, btnAddWorkflow, 30);
		SkySiteUtils.waitTill(3000);

		String First_HeaderName = headerSubNumber.getText();
		Log.message("First Header name is: " + First_HeaderName);
		Select First_Map = new Select(driver.findElement(By.xpath("(//select[@class='form-control input-sm ddlMapper'])[1]")));
		First_Map.selectByVisibleText(First_HeaderName);
		Log.message("First value maping is done.");
		SkySiteUtils.waitTill(3000);

		String Second_HeaderName = headerSubName.getText();
		Log.message("Second Header name is: " + Second_HeaderName);
		Select Second_Map = new Select(driver.findElement(By.xpath("(//select[@class='form-control input-sm ddlMapper'])[2]")));
		Second_Map.selectByVisibleText(Second_HeaderName);
		Log.message("Second value maping is done.");
		SkySiteUtils.waitTill(3000);

		String Third_HeaderName = headerSubDueDate.getText();
		Log.message("Third Header name is: " + Third_HeaderName);
		Select Third_Map = new Select(driver.findElement(By.xpath("(//select[@class='form-control input-sm ddlMapper'])[3]")));
		Third_Map.selectByVisibleText(Third_HeaderName);
		Log.message("Third value maping is done.");
		SkySiteUtils.waitTill(3000);

		String Fourth_HeaderName = headerSubType.getText();
		Log.message("Third Header name is: " + Fourth_HeaderName);
		if (Fourth_HeaderName.contentEquals("Type")) 
		{
			Select Fourth_Map = new Select(driver.findElement(By.xpath("(//select[@class='form-control input-sm ddlMapper'])[4]")));
			Fourth_Map.selectByVisibleText("Submittal Type");
			Log.message("Fourth value maping is done.");
		}
		SkySiteUtils.waitTill(2000);
		btnAddWorkflow.click();
		Log.message("Clicked on Add Workflow button.");
		
		SkySiteUtils.waitForElement(driver, btnOK_appliedToAllImpSubmittals, 20);//Newly Added 25th - Naresh
		btnOK_appliedToAllImpSubmittals.click();
		Log.message("Clicked on OK applied for all imported submittals alert.");
		SkySiteUtils.waitTill(2000);
		iconChangeworkflow.click();
		Log.message("Clicked on change workflow icon.");
		
		SkySiteUtils.waitForElement(driver, tabStandardTemplate, 20);
		SkySiteUtils.waitTill(2000);
		tabStandardTemplate.click();
		Log.message("Clicked on Standard Template(s) tab.");
		SkySiteUtils.waitForElement(driver, imgFirstTemplate, 20);
		SkySiteUtils.waitTill(5000);
		imgFirstTemplate.click();// Select 1st template
		Log.message("Clicked to select first template.");
		SkySiteUtils.waitForElement(driver, txtWFName, 20);
		SkySiteUtils.waitTill(3000);
		txtWFName.clear();// Clear existed name
		txtWFName.sendKeys(WF_Template_Name);
		btnSubmitCopyWF.click();// Click on submit
		Log.message("Clicked on submit button.");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, workFlowName, 20);
		String Exp_WFName = "Workflow - " + WF_Template_Name;
		String Act_WFName = workFlowName.getText();
		Log.message("Expected WF Name is: " + Act_WFName);
		if (Act_WFName.equalsIgnoreCase(Exp_WFName)) 
		{
			result1 = true;
			Log.message("User selected WF successfully.");
			btnCompanyList.click();
			SkySiteUtils.waitForElement(driver, radiobtnSelectCompany, 20);
			SkySiteUtils.waitTill(2000);
			radiobtnSelectCompany.click();
			Log.message("Selected first company radio button.");
			SkySiteUtils.waitTill(2000);
			btnApplyImportSubmittal.click();
			Log.message("Clicked on Apply button on Import Submittal Window.");
			SkySiteUtils.waitTill(20000);
			SkySiteUtils.waitForElement(driver, btnFinalApplyImportSubmittal, 20);
			btnFinalApplyImportSubmittal.click();
			Log.message("Clicked on Apply button of Final import window.");
			SkySiteUtils.waitForElement(driver, btnYes_WanttoCreateSubmittal, 20);
			SkySiteUtils.waitTill(2000);
			btnYes_WanttoCreateSubmittal.click();
			Log.message("Clicked on Yes you want to Import Submittal.");
			SkySiteUtils.waitForElement(driver, successMsgImportSuccessfully, 20);
			SkySiteUtils.waitTill(5000);
			String SuccessMsg = successMsgImportSuccessfully.getAttribute("value");//Data imported successfully.
			Log.message("Message After Import Submittal is: " + SuccessMsg);
			btnDoneImportSubmital.click();
			Log.message("Clicked on DONE button.");
			SkySiteUtils.waitForElement(driver, tabAllSubmittals, 30);
			SkySiteUtils.waitTill(5000); 
			tabAllSubmittals.click();
			SkySiteUtils.waitTill(5000); 
			//tabMySubmittals.click();
			//SkySiteUtils.waitTill(5000);
			SkySiteUtils.waitForElement(driver, submIdUnderList, 20);
			SkySiteUtils.waitTill(5000);
			String SubmId_AllSubTab = submIdUnderList.getText();
			Log.message("Sub ID is: " + SubmId_AllSubTab);
			String SubmName_AllSubTab = submNameUnderList.getText();
			Log.message("Sub Name is: " + SubmName_AllSubTab);
			//String SubmType_AllSubTab = submTypeUnderList.getText();
			//Log.message("Sub Type is: " + SubmType_AllSubTab);
			String SubmStatus_AllSubTab = submOpenStatusUnderList.getText();
			Log.message("Sub status is: " + SubmStatus_AllSubTab);

			// Reading data from Xlsx Imported file
			String SubImport_FilePath = PropertyReader.getProperty("ImpSub_FilePath");
			XSSFWorkbook workbook = new XSSFWorkbook(SubImport_FilePath);
			XSSFSheet sheet = workbook.getSheetAt(0);

			XSSFRow row = sheet.getRow(1);
			List value = new ArrayList<String>();
			for (int i = 0; i < row.getLastCellNum(); i++) 
			{
				XSSFCell cell = row.getCell(i);
				value.add(cell);
				System.out.println(cell);
			}
			System.out.println(value);

		if ((SubmId_AllSubTab.equalsIgnoreCase(value.get(0).toString())) && (SubmName_AllSubTab.equalsIgnoreCase(value.get(1).toString()))
				&& (SubmStatus_AllSubTab.equalsIgnoreCase("OPEN"))) 
			{
				Log.message("Imporing a Submittal is successfully.");
				result2 = true;
			} else {
				Log.message("Imporing a Submittal is failed.");
				result2 = false;
			}

		} else {
			Log.message("User Failed to select WF.");
			result1 = false;
		}

		if ((result1 == true) && (result2 == true)) {
			Log.message("Imporing a Submittal is successfully.");
			return true;
		} else {
			Log.message("Imporing a Submittal is failed.");
			return false;
		}
	}
	
	
	 /**Method generate random Submittal Number name
	 * Scripted By: Ranadeep
	 * @return
	 */
	  public String RandomSubmittalNumber()
	    {
		  	
		  	String submittal="SUB";
		  	Random r = new Random( System.currentTimeMillis() );
            String number = submittal+String.valueOf((10000 + r.nextInt(20000)));
            return number;
	           
	    }	
	
	/** 
	 * Method for creating Submittal for Activity tracking
	 * Scripted By: Ranadeep
	 * @return 
	 */
	
	public ProjectActivitiesPage createSubmittal(String submittalNumber)	 
	{
		
		WebElement element = driver.findElement(By.xpath("//*[@id='submittallistContainer']/div[1]/div[1]/ul/li[2]/a"));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
		Log.message("Clicked on Create Submittal button" );
		
		SkySiteUtils.waitForElement(driver, txtSubmittalNumber, 20);		
		txtSubmittalNumber.click();
		txtSubmittalNumber.clear();
		txtSubmittalNumber.sendKeys(submittalNumber);
		Log.message("Entered Submittal Number is:-" +submittalNumber);
		
		SkySiteUtils.waitForElement(driver, txtSubmittalName, 20);	
		String submittalName = PropertyReader.getProperty("SUBMITTALNAME");
		txtSubmittalName.clear();
		txtSubmittalName.sendKeys(submittalName);
		Log.message("Entered Submittal name is:- " +submittalName);
		
		SkySiteUtils.waitForElement(driver, txtSubmittalDueDate, 20);
		txtSubmittalDueDate.click();
		Log.message("Clicked on Submittal due date dropdown button");
		
		SkySiteUtils.waitForElement(driver, activeSubmittalDate, 20);
		activeSubmittalDate.click();
		Log.message("Submittal due date is selected");
		
		SkySiteUtils.waitForElement(driver, dropdownSubmittalType, 20);
		dropdownSubmittalType.click();
		Log.message("Clicked on Submittal Type selection dropdown");
		
		SkySiteUtils.waitForElement(driver, submittalType, 20);
		submittalType.click();
		Log.message("Submittal Type selected");
		
		SkySiteUtils.waitForElement(driver, buttonCreateSubmittalPopover, 20);
		buttonCreateSubmittalPopover.click();
		Log.message("Clicked on create submittal button");
		
		SkySiteUtils.waitTill(10000); 
		String actualSubmittalInfo= createdSubmitalInfo.getText();
		Log.message("Created submittal info:-" +actualSubmittalInfo);
		
		if(actualSubmittalInfo.contains(submittalNumber) && actualSubmittalInfo.contains(submittalName))
		{
			Log.message("Submittal created successfully");
			
			SkySiteUtils.waitForElement(driver, btnProjectActivity, 20);
			WebElement element2 = driver.findElement(By.xpath("//*[@id='liActivityMain']/a/span[1]"));
		    JavascriptExecutor executor2 = (JavascriptExecutor)driver;
		    executor2.executeScript("arguments[0].click();", element2);
			Log.message("Clicked on Project Activity");
			
			SkySiteUtils.waitForElement(driver, btnDocumentActivity, 20);
			WebElement element3 = driver.findElement(By.xpath("//*[@id='liActivity']/a/span"));
		    JavascriptExecutor executor3 = (JavascriptExecutor)driver;
		    executor3.executeScript("arguments[0].click();", element3);
			Log.message("Clicked on Document Activity");
			
			return new ProjectActivitiesPage(driver).get();
		}
		
		else
		{
				Log.message("Submittal creation failed");
				return null;
		}	
		
	}
}
