package com.arc.projects.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class PublishLogPage extends LoadableComponent<PublishLogPage> {
	
	WebDriver driver;
	private boolean isPageLoaded;
	
	/** Identifying web elements using FindBy annotation*/

	@FindBy(css = ".last > a:nth-child(1)")
	WebElement breadcrumbPublishingLog;
	
	@FindBy(css = ".col-md-2 > a:nth-child(4)")
	WebElement btnProcessCompleted;
	
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, breadcrumbPublishingLog, 20);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 * @return
	 */
	public PublishLogPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public FolderPage publishLogProcessCompleted() {
		
		SkySiteUtils.waitForElement(driver, btnProcessCompleted, 30);
		 
		 if (btnProcessCompleted.isDisplayed()) {
				Log.message("File uploaded sucessfully");	
			} 
		 
		 else{
				Log.message("File upload failed");
			 }
		
		 	SkySiteUtils.waitTill(5000);
			btnProcessCompleted.click();
			Log.message("Clicked on Process Completed button");
			
			return new FolderPage(driver).get();
	}
	
}
