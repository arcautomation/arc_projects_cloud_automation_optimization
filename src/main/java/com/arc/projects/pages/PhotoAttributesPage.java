package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.CommonMethod;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import com.arc.projects.pages.ProjectGalleryPage;

public class PhotoAttributesPage extends LoadableComponent<PhotoAttributesPage> {
 
	
	WebDriver driver;
	private boolean isPageLoaded;
	
	/** Identifying web elements using FindBy annotation*/
	
	@FindBy(css=".col-lg-push-5 > button:nth-child(1)")
	WebElement buttonSaveAttributes;
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, buttonSaveAttributes, 20);

	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 * @return
	 */
	public PhotoAttributesPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	
	public ProjectGalleryPage saveAttributes() {
		
		 SkySiteUtils.waitForElement(driver, buttonSaveAttributes, 30);
		 //SkySiteUtils.waitTill(3000);  
		 buttonSaveAttributes.click();
		 Log.message("Clicked Save Attributes button");      
		
		return new ProjectGalleryPage(driver).get();

	}

}

