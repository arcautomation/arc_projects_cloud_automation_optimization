package com.arc.projects.pages;

import java.awt.AWTException;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;



public class ProjectsRegistrationPage extends LoadableComponent<ProjectsRegistrationPage>
{
	WebDriver driver;
	private boolean isPageLoaded;
	
	/** Identifying web elements using FindBy annotation*/
	
	@FindBy (xpath="//*[@id='txtFirstName']")
	WebElement txtboxFirstname;
		
	@FindBy (xpath="//*[@id='txtLastName']")
	WebElement txtboxLastname;
	
	@FindBy (xpath="//*[@id='txtEmail']")
	WebElement txtboxEmailId;
	
	@FindBy (xpath="//*[@id='ddlASStateDrop']/button")
	WebElement btnStateDropdown;
	
	@FindBy (xpath="//*[@id='ddlASState']/li[1]/a")
	WebElement stateValue;

	@FindBy (xpath="//*[@id='txtASPhone']")
	WebElement txtboxPhoneNo;
	
	@FindBy (xpath="//*[@id='txtPassword']")
	WebElement txtboxPassword;
	
	@FindBy (xpath="//*[@id='aSubmit']")
	WebElement btnCreateAccount;
	
	
	
	@Override
	protected void load() 
	{
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnCreateAccount, 20);

	}
	
	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
		
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory
	 * class.
	 * 
	 * @param driver
	 * @return
	 */
	public ProjectsRegistrationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	/** Method to create Random Email for registration
	 *  By Ranadeep
	 */

	public String RandomEmailId()
    {
           
           String str="testuserpa";
           String str2="@mailinator.com";
           Random r = new Random(System.currentTimeMillis());
           String abc = str+String.valueOf((10000 + r.nextInt(20000)))+str2;
           return abc;
           
    }
	

	/** 
	 * Method written for the registration with the valid credentials
	 * @return
	 * Created By Ranadeep
	 */
	
	public ProjectDashboardPage RegistrationWithValidCredentials(String newEmailId) throws AWTException
	{
		
		SkySiteUtils.waitForElement(driver, txtboxFirstname, 30);
		String FirstName = PropertyReader.getProperty("UFIRSTNAME");
		txtboxFirstname.clear();
		txtboxFirstname.sendKeys(FirstName);
		Log.message("Firstname entered is:- " +FirstName );
		
		SkySiteUtils.waitForElement(driver, txtboxLastname, 30);
		String LastName = PropertyReader.getProperty("ULASTNAME");
		txtboxLastname.clear();
		txtboxLastname.sendKeys(LastName);
		Log.message("Lastname entered is:- " +LastName );
		
		SkySiteUtils.waitForElement(driver, txtboxEmailId, 30);
		txtboxEmailId.clear();
		txtboxEmailId.sendKeys(newEmailId);
		Log.message("Email Id entered is:- " +newEmailId);
		
		SkySiteUtils.waitForElement(driver, btnStateDropdown, 30);
		btnStateDropdown.click();
		Log.message("Clicked on dropdown to select state");
		
		SkySiteUtils.waitForElement(driver, stateValue, 30);
		String State=stateValue.getText();
		stateValue.click();
		Log.message("Selected state is:- " +State);
		
		SkySiteUtils.waitForElement(driver, txtboxPhoneNo, 30);
		String PhoneNo = PropertyReader.getProperty("UPHONENO");
		txtboxPhoneNo.clear();
		txtboxPhoneNo.sendKeys(PhoneNo);
		Log.message("Phone No entered is:- " +PhoneNo);
		
		SkySiteUtils.waitForElement(driver, txtboxPassword, 30);
		String Password = PropertyReader.getProperty("UPASSWORD");
		txtboxPassword.clear();
		txtboxPassword.sendKeys(Password);
		Log.message("Password entered is:- " +Password);
		
		SkySiteUtils.waitForElement(driver, btnCreateAccount, 30);
		btnCreateAccount.click();
		Log.message("Clicked on Create Account button");
		
		return new ProjectDashboardPage(driver).get();
	}

	
	
}
