package com.arc.projects.utils;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

public class CustomFluentWait {

	
		FluentWait <WebDriver> fluentWait;
		
		static final int MAXWAITINSECONDS = 25;
		
		
		public CustomFluentWait(WebDriver driver) {
			this(driver,MAXWAITINSECONDS);
		}
		
		public CustomFluentWait(WebDriver driver,int maxWaitInSeconds) {
			fluentWait = new FluentWait<WebDriver>(driver).withTimeout(maxWaitInSeconds, TimeUnit.SECONDS)
					.pollingEvery(5, TimeUnit.SECONDS).withMessage(" WebElement Not Found ");
		}	
		
		public WebElement waitForElementToShowOnPage(WebElement element){
			return this.waitForElementToShowOnPage(element,MAXWAITINSECONDS);
		}
		
		public boolean waitForElementToBeHiddenOnPage(By locator){
			return this.waitForElementToBeHiddenOnPage(locator,MAXWAITINSECONDS);
		}	
		
		
		public WebElement waitForElementToShowOnPage(WebElement element,int waitInSeconds){
			return fluentWait.withTimeout(waitInSeconds, TimeUnit.SECONDS).until(ExpectedConditions.visibilityOf(element));	
		}
		
		public WebElement waitForElementToShowOnPage(By locator,int waitInSeconds){
			return fluentWait.withTimeout(waitInSeconds, TimeUnit.SECONDS).until(ExpectedConditions.visibilityOfElementLocated(locator));	
		}
			
		public boolean waitForElementToBeHiddenOnPage(By locator,int waitInSeconds){
			return fluentWait.until(ExpectedConditions.invisibilityOfElementLocated(locator));	
		}
		
		public WebElement waitForElementToBeClickable(WebElement element,int waitInSeconds){
			return fluentWait.until(ExpectedConditions.elementToBeClickable(element));
		}
		
		public WebElement waitForElementToBeClickable(By locator,int waitInSeconds){
			return fluentWait.until(ExpectedConditions.elementToBeClickable(locator));
		}
		
		public boolean waitForElementToBeSelected(WebElement element,int waitInSeconds){
			return fluentWait.until(ExpectedConditions.elementToBeSelected(element));
		}
		
		public boolean waitForElementToBeSelected(By locator,int waitInSeconds){
			return fluentWait.until(ExpectedConditions.elementToBeSelected(locator));
		}		
		
		public WebElement waitForElementToBePresent(By locator,int waitInSeconds){
			return fluentWait.until(ExpectedConditions.presenceOfElementLocated(locator));
		}
		
		public WebElement waitForElementToBeRefreshed(By locator,int waitInSeconds){
			return fluentWait.until(ExpectedConditions.refreshed(new ExpectedCondition<WebElement>(){

				@Override
				public WebElement apply(WebDriver driver) {
					return driver.findElement(locator);
				}
				
			}));
		}		
		
		public WebDriver switchToFrame(By locator){
			return fluentWait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(locator));
		}
		
		
}
